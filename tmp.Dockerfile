FROM node:10-alpine
WORKDIR /usr/src/app
COPY package*.json ./

# proxy
#ENV http_proxy "http://172.16.1.6:3128"
#ENV https_proxy "http://172.16.1.6:3128"

# under a proxy
RUN npm config set registry https://registry.npmjs.org/
#RUN npm config set http-proxy http://172.16.1.6:3128
#RUN npm config set https-proxy http://172.16.1.6:3128
#RUN npm config set proxy http://172.16.1.6:3128
RUN npm install

RUN npm audit fix
COPY . .

RUN  chmod -R 777 upload_image
EXPOSE 1011 25

CMD [ "npm", "start" ]
