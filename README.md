# BioTech Recruitement Tracking System (RecTrack)

BioTech Recruitement Tracking System is a web based application designed to manage the applicants and to keep track on the status and date of the applicant.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Guidelines

This document provides guidelines and examples for RecTrack API, encouraging consistency, maintainability and best practices across platforms. RecTrack API's aim to balance a truly RESTful API interface with positive developer experience.

### Installation

Use Visual Studio Code for code editor redefined and optimized for building and debugging modern web and cloud applications.

```
*[Visual Studio Code](https://code.visualstudio.com/download)
```

Use PostgreSQL for object-relational database management system.

```
*[Postgresql](https://www.postgresql.org/download/)
```

Use Postman for complete API Development Environment

```
*[Postman](https://www.getpostman.com/downloads/)
```

Download NodeJS to serve as our server

```
*[NodeJS](https://nodejs.org/en/download/)
```

To get started in this walkthrough, open the BioTech Recruitement Tracking System (RecTrack) in Visual Studio Code. You'll need to open a new terminal('ctrl + ~') for node and npm command-line tools.

To install all dependencies inside 'package.json' file, you need to install node package manager(npm).
NPM is built in when you install NodeJS.

```
npm init
```

The command above initializes the 'package.json' of the API, where the dependencies will be saved.

## PACKAGES

```
npm i address
```

This package allows the API to get the IP address of the local machine.

```
npm i axios
```

This package allows us to make http request to our API.

```
npm i change-case
```

This package converts the case of the string. Alternative for .toLowerCase() or .toUpperCase().

```
npm i cors
```

This package allows our API to be accessible in other machines.
It add a response header to every route: Access-Control-Allow-Origin: \*

```
npm i dotenv
```

This package allows us to use .env files; where our confidential variables are stored.

```
npm i email-validator
```

This package checks if the email format is valid or not.

```
npm i eslint
```

ESLint is a tool for identifying and reporting on patterns found in ECMAScript/JavaScript code

```
npm i express
```

Express is a minimal and flexible Node.js web application framework that provides a robust set of features to develop web and mobile applications.

```
npm i express-zip
```

An extension of the express package that allows us to export multiple files in zip.

```
npm i google-spreadsheet
```

This package allows us to get data from a google spreadsheet after which we store in our database.

```
npm i jest
```

This package is used in unit testing, it checks whether the function is running correct as expected or not; returns pass or fail.

```
npm i jsonwebtoken
```

This package generates token which is used to authenticate every route in our API.

```
npm i moment
```

This package allows us to format date or date time data.

```
npm i multer
```

This package is used to upload files to our server.

```
npm i nodemailer
```

This package allows our API to send auto emails.

```
npm i papaparse
```

This package parses the data from our database and generates an excel file.

```
npm i pg
```

This package allows us to communicate and connect to our backend PostgreSQL.

```
npm i randomstring
```

This package generates random strings.

```
npm i simple-excel-to-json
```

This package get all the data in an excel file and converts it into a JSON.

```
npm i unique-string
```

This package generates a unique string. It is used in the API to name the files that are uploaded to the server.

```
npm i nodemon
```

This package shows the changes in our code every after save. You don't have to run the app again and again.

## SCRIPTS

```
npm run start
```

This script runs the main file app.js

```
npm run dev
```

This script runs nodemon and observe the main file app.js for changes and runs it immediately.

```
npm run test
```

This script runs our test suites.

## REQUEST ROUTES

# USERS GET REQUESTS

```
* http://localhost:5001/users/check-token/:token
    - Checks if the token still exist or not.
    - Logout user if doesn't exist.
    - :token is the token of the logged in user.

* http://localhost:5001/users/position-list/:id
    - Show possible positions to re-assign applicant.
    - :id is the applicant's id.

* http://localhost:5001/users/firstname/:token
    - Retrive the first name of the logged in user.
    - :token is the token of the logged in user.

* http://localhost:5001/users/received/:id
    - Retrieve all received applicants base on the department.
    - :id is the department's id.

* http://localhost:5001/users/received/:id/:posId
    - Retrieve all received applicants on PG.
    - Filtered by department and position.
    - :id is the department id and :posId is the position id.

* http://localhost:5001/users/online-exam/:id
    - Retrieve all online exam applicants base on the department.
    - :id is the department's id.

* http://localhost:5001/users/online-exam
    - Retrieve all online exam applicants for PG.

* http://localhost:5001/users/endorsed/:id
    - Retrieve all endorsement applicants base on the department.
    - :id is the department's id.

* http://localhost:5001/users/endorsed
    - Retrieve all endorsement applicants for PG.

* http://localhost:5001/users/for-endorsed/:id
    - Retrieve all for endorsement applicants base on the department.
    - :id is the department's id.

* http://localhost:5001/users/for-endorsed
    - Retrieve all for endorsement applicants for PG.

* http://localhost:5001/users/rejected/:id
    - Retrieve all rejected applicants base on the department.
    - :id is the department's id.

* http://localhost:5001/users/rejected
    - Retrieve all rejected applicants for PG.

* http://localhost:5001/users/blacklisted/:id
    - Retrieve all blacklisted applicants base on the department.
    - :id is the department's id.

* http://localhost:5001/users/blacklisted
    - Retrieve all blacklisted applicants for PG.

* http://localhost:5001/users/assessment/:id
    - Retrieve all assessment applicants base on the department.
    - :id is the department's id.

* http://localhost:5001/users/assessment
    - Retrieve all assessment applicants for PG.

* http://localhost:5001/users/pending/:id
    - Retrieve all pending applicants base on the department.
    - :id is the department's id.

* http://localhost:5001/users/pending
    - Retrieve all pending applicants for PG.

* http://localhost:5001/users/kept-reference/:id
    - Retrieve all kept for reference applicants base on the department.
    - :id is the department's id.

* http://localhost:5001/users/kept-reference
    - Retrieve all kept for reference applicants for PG.

* http://localhost:5001/users/shortlist/:id
    - Retrieve all shortlist applicants base on the department.
    - :id is the department's id.

* http://localhost:5001/users/shortlist
    - Retrieve all shortlist applicants for PG.

* http://localhost:5001/users/deployed/:id
    - Retrieve all deployed applicants base on the department.
    - :id is the department's id.

* http://localhost:5001/users/deployed
    - Retrieve all deployed applicants for PG.

* http://localhost:5001/users/job-order/:id
    - Retrieve all job order applicants base on the department.
    - :id is the department's id.

* http://localhost:5001/users/job-order
    - Retrieve all job order applicants for PG.

* http://localhost:5001/users/received-notif/count
    - Count the new received applicants.
    - Used as notification.

* http://localhost:5001/users/received-notif/count/:id
    - Count the new received applicants.
    - Used as notification.
    - :id is the department's id.

* http://localhost:5001/users/endorsed-notif/count
    - Count the new endorsed applicants.
    - Used as notification.

* http://localhost:5001/users/shortlist-notif/count
    - Count the new shortlist applicants.
    - Used as notification.

* http://localhost:5001/users/assessment-notif/count/:id
    - Count the new assessment applicants.
    - Used as notification.
    - :id is the department's id.

* http://localhost:5001/users/deployed-notif/count/:id
    - Count the new deployed applicants.
    - Used as notification.
    - :id is the department's id.

* http://localhost:5001/users/admin/view
    - Retrieve all active users.

* http://localhost:5001/users/admin/view/:id
    - Retrieve the data of a specific user.
    - :id is the id of the user.

* http://localhost:5001/users/application-form/:id
    - Retrieve existing data for application form during fill up.
    - :id is the id of the user.

* http://localhost:5001/users/application-form-view/:id
    - Retrieve all the data of application form for printing.
    - :id is the id of the user.

* http://localhost:5001/users/jeonsoft
    - Retrieve all applicants ready for export to jeonsoft.

* http://localhost:5001/users/jeonsoft/download
    - Download csv files for jeonsoft on client side.

* http://localhost:5001/users/time/:token
    - Get start and end time for online exam
    - :token is the token of logged in applicant

* http://localhost:5001/users/time-iqbe/:token
    - Get time for the IQBE
    - :token is the token of logged in applicant

* http://localhost:5001/users/referral-form
    -Display all referral forms

* http://localhost:5001/users/referral-form/:id
    - Retrieve specific referral form

* http://localhost:5001/users/online-exam-result/:id/:whichExam
    - Retrieve the result of the exam
    - :id is the id of the applicant.
    - :whichExam determines where online exam or IQBE

* http://localhost:5001/users/referral-form-fetch/:id
    - Fetch data of referral form for update
    - :id is the id of the applicant.
```

# USERS POST REQUESTS

```
* http://localhost:5001/users/view/logs

    {
        "date": "2019-09-03"
    }
    - shows the logs on that date

* http://localhost:5001/users/add

    {
    "email": "joe.bell@gmail.com",
    "firstname": "Joe",
    "lastname": "Bell",
    "mobile": "09972701262",
    "livestock": "N/A",
    "travel": "yes",
    "resume_url": "github.com",
    "position_id":"2",
    "middlename": "Walter"
    }
    - add new applicant; position_id is FK to positions table.

* http://localhost:5001/users/google-sheet/add
    - retrieve data on google sheet and insert to our database.

* http://localhost:5001/users/application-form/:id

    {
        "firstname": "YinzarW",
        "middlename": "AlEmCPR",
        "lastname": "Rxr4DPn",
        "salary_desired": "15000",
        "UnitRoomNumberFloor": "4th floor",
        "BuildingName": "My Building",
        "LotBlockPhaseHouseNumber": "404",
        "StreetName": "Adelfa Street",
        "VillageSubdivision": "",
        "Barangay": "Poblacion",
        "TownDistrict": "",
        "Municipality": "Polomolok",
        "CityProvince": "South Cotabato",
        "place_birth": "Polomolok, South Cotabato",
        "bday": "1995-02-18",
        "age": "24",
        "height": "163",
        "weight": "75",
        "gender": "Male",
        "is_single_parent": "f",
        "civil_status": "Single",
        "citizenship": "Filipino",
        "religion": "Roman Catholic",
        "who_referred": "Monkey D. Luffy",
        "when_to_start": "ASAP",
        "person_to_notify": "Roronoa Zoro",
        "person_relationship": "Swordsman",
        "person_address": "East blue",
        "person_telephone": "Denden Mushi",
        "position_id_second_choice": "2",
        "CS_eligible": "t",
        "CS_date_taken": "2017-08-06",
        "CS_certificate_image_path": "D:/Images/src/scan.jpg",
        "date_application": "2019-07-01",
        "user_image_path": "D:/Images/src/scan(1).jpg",
        "educationalBgArray": [
            {
                "educational_attainment": [
                    "Elementary",
                    "High school",
                    "College"
                ],
                "institution_name": [
                    "Polomolok Central Elementary School",
                    "San Lorenzo Ruiz Academy of Polomolok",
                    "Holy Trinity College of General Santos City"
                ],
                "institution_address": [
                    "Pioneer Ave., Pol., So. Cot.",
                    "Kaunlaran Subd., Cannery Site, Pol., So. Cot.",
                    "Daproza Ave., General Santos City"
                ],
                "course_degree": [
                    "Grade 1-6",
                    "1st - 4th year",
                    "BS Computer Engineering"
                ],
                "years_attended": [
                    "2001-2007",
                    "2007-2011",
                    "2014-2019"
                ],
                "honors_awards": [
                    "",
                    "",
                    ""
                ]
            }
        ],
        "relativeArray": [
            {
                "full_name": [
                    "Trafalgar Law",
                    "Eustass Kid"
                ],
                "position": [
                    "Doctor",
                    "Mechanics"
                ],
                "branch": [
                    "Clinic",
                    "Mechanical Dept"
                ],
                "relationship": [
                    "Nakama",
                    "Alliance"
                ]
            }
        ],
        "professionalLicensesArray": [
            {
                "professional_licenses_held": [
                    "Nursing",
                    "Engineering"
                ],
                "certificate_image_path": [
                    "D:/Images/src/scan(2).jpg",
                    "D:/Images/src/scan(3).jpg"
                ],
                "certification_number": [
                    "123",
                    "456"
                ]
            }
        ],
        "orgMembershipArray": [
            {
                "association_name": [
                    "East blue pirates",
                    "Yonkou"
                ],
                "position_title": [
                    "Oyabun",
                    "5th Emperor"
                ],
                "year_participation": [
                    "2018",
                    "2019"
                ]
            }
        ],
        "seminarsAttendedArray": [
            {
                "seminar_name": [
                    "Haki Tutorial Seminar",
                    "Pirate King Seminar"
                ],
                "description": [
                    "Teaches the fundamentals of armament haki, observation haki and emperor's haki.",
                    "How to be a pirate king."
                ],
                "venue": [
                    "Ruskaina Island",
                    "Shakky's bar"
                ],
                "conducted_by": [
                    "Silver Rayleigh",
                    "Silver Rayleigh, Shakky"
                ],
                "dates": [
                    "2015-05-05",
                    "2016-06-06"
                ],
                "certificate_image_path": [
                    "D:/Images/src/scan(4).jpg",
                    "D:/Images/src/scan(5).jpg"
                ]
            }
        ],
        "workExperienceArray": [
            {
                "company_name": [
                    "KCC",
                    "DOLE"
                ],
                "address": [
                    "J. Catolico Ave., GSC",
                    "Cannery Site, Pol., So. Cot."
                ],
                "position": [
                    "Bagger",
                    "Janitor"
                ],
                "start_date": [
                    "2015-01-01",
                    "2019-02-18"
                ],
                "end_date": [
                    "2018-01-10",
                    "2019-05-18"
                ],
                "last_salary": [
                    "5000",
                    "2000"
                ],
                "reason_leaving": [
                    "Not having sleep",
                    ""
                ]
            }
        ],
        "workWithBFIArray": [
            {
                "have_work_with_bfi": "t",
                "branch_company": [
                    "Agro",
                    "Farm"
                ],
                "dates": [
                    "2015-04-05",
                    "2018-03-28"
                ],
                "position": [
                    "Feeder",
                    "Farmer"
                ],
                "department": [
                    "Feeder Dept",
                    "Farmer Dept"
                ]
            }
        ],
        "otherInfoArray": [
            {
                "typing_wpm": "500",
                "steno_wpm": "450",
                "hobbies": "Playing computer games, boxing, sleeping, eating",
                "language_speak_write": "English, Tagalog, Cebuano, Japanese",
                "machines_can_operate": "Computer, microcontrollers",
                "any_crime_committed": "f",
                "been_hospitalized": "t",
                "computer": "t",
                "art_work": "f",
                "crime_details": "",
                "hospitalized_details": "fever, diarrhea"
            }
        ],
        "familyBgArray": [
            {
                "name": [
                    "Nami",
                    "Robin",
                    "Vivi"
                ],
                "relationship": [
                    "Sister",
                    "Mother",
                    "Sister"
                ],
                "age": [
                    "21",
                    "32",
                    "20"
                ],
                "occupation": [
                    "Navigator",
                    "Archeologist",
                    "Princess of Alabasta Kingdom"
                ],
                "company_address": [
                    "Strawhat Pirate Ship",
                    "Strawhat Pirate Ship",
                    "Alabasta Kingdom"
                ]
            }
        ],
        "isMarriedArray": [
            {
                "is_married": "t",
                "number_children": "3",
                "range_age": "4-12",
                "do_you": "Live in Own House",
                "with_who": "Wife"
            }
        ],
        "referencesArray": [
            {
                "name": [
                    "Silvers Rayleigh",
                    "King Neptune",
                    "King Cobra"
                ],
                "address": [
                    "Sabaody Archipelago",
                    "Merman Island",
                    "Alabasta Kingdom"
                ],
                "telephone": [
                    "159-753",
                    "456-654",
                    "357-123"
                ],
                "occupation": [
                    "Former Pirate",
                    "King",
                    "Former King"
                ],
                "how_long_known": [
                    "2 years",
                    "3 months",
                    "6 months"
                ]
            }
        ],
        "willingnessArray": [
            {
                "explain_why_hire": "You should hire me because it's me.",
                "willing_train_apprentice": "t",
                "willing_to_BOND": "t",
                "sss": "123-456-981",
                "tin": "123-456-0000",
                "residence_certificate": "123-951-354-798-156",
                "residence_certificate_date_issue": "2012-01-01",
                "residence_certificate_place_issue": "Pol So Cot."
            }
        ]
    }

    - applicant fill up application form.

* http://localhost:5001/users/admin/add

    {
        "email": "karl.baquiring@gmail.com",
        "password": "qwe",
        "firstname": "Karl",
        "lastname": "Baquiring",
        "mobile": "09978305773",
        "role_id":"4",
        "position_id":"2",
        "middlename": "Lazarraga"
    }
    - add new user; position_id is FK to positions table.

* http://localhost:5001/users/referral-form/add

    {
        "referrer_name": "Rodney N. Lingganay",
        "referrer_dept": "BFI IT",
        "referrer_position": "DevOps",
        "referrer_id": "154151399",
        "referrer_contact": "09978305773",
        "referral_name": "Joe W. Bell",
        "referral_contact": "09978305776",
        "referral_position_applying": "SQA",
        "referral_relationship_referrer": "friend"
    }
    -add new referral form

* http://localhost:5001/users/deployed/export

    {
        "data": [
            {
                "EmployeeCode": "154151399",
                "id": "92"
            }
        ]
    }
    - export .csv file for jeonsoft

* http://localhost:5001/users/upload
    - upload files to server; only images and excel file.
    - form-data with any name.

* http://localhost:5001/users/add/logs

    {
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiNzJlNTI5YmM2NjU4MmE1YTA3YWM2ZjY2ZDhmZTIyNjhmYzJiMjY2OGI4ZDAiLCJwYXNzd29yZCI6IjYxZjQyNSJ9LCJpYXQiOjE1NjczOTU3NDB9.BCkF9tfRgGM3C7yvzDNpgiIpg_SOZInkJDnHmCuk1cY",
        "module": "Login",
        "table_affected": "users",
        "activity": "Logged in to the system."
    }
    - add new system logs
```

# USERS PUT REQUESTS

```
* http://localhost:5001/users/login

    {
        "email": "bfi.rectrack@gmail.com",
        "password": "qwe"
    }
    - user login to system

* http://localhost:5001/users/status/online-exam/:id
    - change status of applicant to online exam.
    - :id here is the id of applicant

* http://localhost:5001/users/status/blacklist/:id
    - change status of applicant to blacklist.
    - :id here is the id of applicant

* http://localhost:5001/users/status/endorsement/:id
    - change status of applicant to endorsement.
    - :id here is the id of applicant

* http://localhost:5001/users/status/reject/:id
    - change status of applicant to reject.
    - :id here is the id of applicant

* http://localhost:5001/users/status/assessment/:id
    - change status of applicant to assessment.
    - :id here is the id of applicant

* http://localhost:5001/users/status/pending/:id
    - change status of applicant to pending.
    - :id here is the id of applicant

* http://localhost:5001/users/status/kept-reference/:id
    - change status of applicant to kept for reference.
    - :id here is the id of applicant

* http://localhost:5001/users/status/shortlist/:id
    - change status of applicant to shortlist.
    - :id here is the id of applicant

* http://localhost:5001/users/status/deployed/:id
    - change status of applicant to deployed.
    - :id here is the id of applicant

* http://localhost:5001/users/status/for-endorsement/:id
    - change status of applicant to for endorsement.
    - :id here is the id of applicant

* http://localhost:5001/users/applicant/login/online-exam

    {
        "email": "rodney.linggasanay@biotechfarms.net",
        "password": "3XyV93GEIwdr"
    }
    - applicant login for online exam

* http://localhost:5001/users/applicant/login/IQ-behavioral-exam

    {
        "email": "rodney.linggasanay@biotechfarms.net",
        "password": "3XyV93GEIwdr"
    }
    - applicant login for IQBE

* http://localhost:5001/users/applicant/login/application-form

    {
        "email": "rodney.linggasanay@biotechfarms.net",
        "password": "3XyV93GEIwdr"
    }
    - applicant login for application form fill up

* http://localhost:5001/users/endorsed/set-date/:id

    {
        "date": "07-02-2019 11:23:00",
        "link": "google.com",
        "name": "Ma'am Regine Porras",
        "contact": "090987987090798",
        "location": "KCC Marbel"
    }
    - make schedule for interview with corresponding details

* http://localhost:5001/users/status/job-order/:id
    - change status of applicant to job order.
    - :id here is the id of applicant

* http://localhost:5001/users/admin/edit/user/:id

    {
        "email": "karl.baquiring@gmail.com",
        "password": "qwe",
        "firstname": "Karl",
        "lastname": "Baquiring",
        "mobile": "09978305773",
        "role_id":"4",
        "position_id":"2",
        "middlename": "Lazarraga",
        "status":"active"
    }
    - update existing user

* http://localhost:5001/users/online-exam/done/:id
    - update to true when done online exam
    - :id here is the applicant id

* http://localhost:5001/users/IQBE/done/:id
    - update to true when done IQBE
    - :id here is the applicant id

* http://localhost:5001/users/referral-form/:id
    - update referral form
    - :id here is the applicant id

* http://localhost:5001/users/position-update/:userId/:posId
    - update position of applicant in assessment
    - :userId is applicant's id; posId is the id of the position

* http://localhost:5001/users/deployed-date-update/:id

    {
        "date": "2019-05-26"
    }
    - update the date of deployement of applicant
    - :id here is the applicant id
```

# TEAMS GET REQUESTS

```
* http://localhost:5001/teams/admin/view
    - display all team

* http://localhost:5001/teams/admin/view/:id
    - display details of one team

* http://localhost:5001/teams/admin/view-dept/:id
    - display all teams filter by department
```

# TEAMS POST REQUESTS

```
* http://localhost:5001/teams/admin/add

    {
        "name": "Technical Team",
        "description": "Maintains the facilities of BFI..",
        "department_id": "2",
        "no_of_items": "30"
    }
    - add new team; department_id is FK to departments table.
```

# TEAMS PUT REQUESTS

```
* http://localhost:5001/teams/admin/edit-status-inactive/:id
    - change status of team to inactive

* http://localhost:5001/teams/admin/edit/:id

    {
        "name": "Technical Team",
        "description": "Maintains the facilities of BFI..",
        "department_id": "2",
        "no_of_items": "30"
    }
    - update existing team; department_id is FK to departments table.
```

# ROLES GET REQUESTS

```
* http://localhost:5001/roles/view
    -view all roles

* http://localhost:5001/roles/view:id
    - view specific roles
    - :id here is the role id
```

# ROLES POST REQUESTS

```
* http://localhost:5001/roles/add

    {
        "name":"Standard",
        "description":"Limited."
    }
    - add new role
```

# ROLES PUT REQUESTS

```
* http://localhost:5001/roles/edit/:id

    {
        "name":"Standard",
        "description":"Limited."
    }
    - edit existing role;
    - :id here is the role id
```

# QUESTIONS GET REQUESTS

```
* http://localhost:5001/questions/view/:id
    - fetch a single question
    - :id here is the question id

* http://localhost:5001/questions/views/:teamId/:deptId/:catgId
    - view all questions filtered by team, department then category respectively
```

# QUESTIONS POST REQUESTS

```
* http://localhost:5001/questions/online-exam

    {
        "categoryId": [
            3,
            2,
            4,
            1,
            5
        ],
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiNjJlYzI0ZmM3MTQ0Njc0MjFjYTM2YjZhZjlmNzJlNzBkNTIwNjU2YWJlZDE1ZTNiMmM3NiIsInBhc3N3b3JkIjoiNGFiMjc4ZDk1OTQ0N2Y2OTA1Yjg0ZTdlIn0sImlhdCI6MTU2NTU4MDE0NX0.qjpO4djYXaAFsVWzhhCRLm3vhohArfSSQK6H-8gepx0"
    }
    - get question per category; category is auto generated during login

* http://localhost:5001/questions/add

    {
        "questions": "What is the meaning of SQL?",
        "correct_answer": "Structured query language",
        "categories_id": "2",
        "details": ["Structured query language","Stratify query language","Strategic query language","Standardized query language"]
    }
    - add new question

* http://localhost:5001/questions/add/import

    {
        "path":"C:/Users/Default.DESKTOP-H07SCEJ/Desktop/math_test.xlsx",
        "categories_id":"7"
    }
    - import questions from excel file;
    - path is auto generated once excel file is uploaded.

* http://localhost:5001/questions/IQBE

    {
        "categoryId": [
            3,
            2,
            4,
            1,
            5
        ],
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiNjJlYzI0ZmM3MTQ0Njc0MjFjYTM2YjZhZjlmNzJlNzBkNTIwNjU2YWJlZDE1ZTNiMmM3NiIsInBhc3N3b3JkIjoiNGFiMjc4ZDk1OTQ0N2Y2OTA1Yjg0ZTdlIn0sImlhdCI6MTU2NTU4MDE0NX0.qjpO4djYXaAFsVWzhhCRLm3vhohArfSSQK6H-8gepx0"
    }
    - get question per category; category is auto generated during login
```

# QUESTIONS PUT REQUESTS

```
* http://localhost:5001/questions/edit/:id

    {
        "questions": "What is the meaning of SQL?",
        "correct_answer": "Structured query language",
        "categories_id": "2",
        "details": ["Structured query language","Stratify query language","Strategic query language","Standardized query language"]
    }
    - edit existing question
    - :id here is the question id

* http://localhost:5001/questions/online-exam

    {
        "categoryId": [
            2,
            3
        ],
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiNzhlMjM1ZmU3NTEzMjI1YjEyYjg2NTZjZDhmZTIyNjhmYzJiMjY2OGI4ZDAiLCJwYXNzd29yZCI6IjVlZjMxMmZkNzUwOTBhIn0sImlhdCI6MTU2MjMyMDE1OX0.uyHYgc0hFqegoPszAqCi3PWPKYjJD_-aJW7WK5Wqpdg",
        "answers": [
            {
                "questionId": 997,
                "answer": "3227"
            },
            {
                "questionId": 992,
                "answer": "3218"
            },
            {
                "questionId": 994,
                "answer": "true"
            },
            {
                "questionId": 993,
                "answer": "3221"
            },
            {
                "questionId": 995,
                "answer": "3225"
            }
        ]
    }
    - applicant submit answer in online exam

* http://localhost:5001/questions/IQBE

    {
        "categoryId": [
            2,
            3
        ],
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiNzhlMjM1ZmU3NTEzMjI1YjEyYjg2NTZjZDhmZTIyNjhmYzJiMjY2OGI4ZDAiLCJwYXNzd29yZCI6IjVlZjMxMmZkNzUwOTBhIn0sImlhdCI6MTU2MjMyMDE1OX0.uyHYgc0hFqegoPszAqCi3PWPKYjJD_-aJW7WK5Wqpdg",
        "answers": [
            {
                "questionId": 997,
                "answer": "3227"
            },
            {
                "questionId": 992,
                "answer": "3218"
            },
            {
                "questionId": 994,
                "answer": "true"
            },
            {
                "questionId": 993,
                "answer": "3221"
            },
            {
                "questionId": 995,
                "answer": "3225"
            }
        ]
    }
    - applicant submit answer in IQBE
```

# POSITIONS GET REQUESTS

```
* http://localhost:5001/positions/vacant
    - display all vacant positions

* http://localhost:5001/positions/not-vacant
    - display all not vacant positions

* http://localhost:5001/positions/view
    - display all positions

* http://localhost:5001/positions/view/:id
    - retrieve data of specific position

* http://localhost:5001/positions/view-by-department/:id
    - retrieve all position filtered per department
    - :id here is department id
```

# POSITIONS POST REQUESTS

```
* http://localhost:5001/positions/add

    {
        "name": "UX Designer",
        "description": "Develops the frontend of the system.",
        "teams_id": "2",
        "is_office_staff": "t"
    }
    - add new position
```

# POSITIONS PUT REQUESTS

```
* http://localhost:5001/positions/change/not-vacant/:id
    - change position to not vacant
    - :id here is position id

* http://localhost:5001/positions/change/vacant/:id
    - change position to vacant

* http://localhost:5001/positions/update/:id

    {
        "name": "UX Designer",
        "description": "Develops the frontend of the system.",
        "teams_id": "2",
        "is_office_staff": "t"
        "is_vacant": "t"
    }
    - update position
```

# IAF GET REQUESTS

```
* http://localhost:5001/interview-assessment/user/view/:id
    - view specific IAF
    - :id here is applicant's id

* http://localhost:5001/interview-assessment/user/view
    - view all IAF
```

# IAF POST REQUESTS

```
* http://localhost:5001/interview-assessment/add

    {
        "user_id_applicant": "93",
        "date_interview": "2019-06-24",
        "communication_skills": "yes",
        "communication_skills_note": "sometimes unclear words",
        "confidence": "yes",
        "confidence_note": "sometimes looking down on the floor",
        "physical_appearance": "yes",
        "physical_appearance_note": "N/A",
        "knowledge_skills": "no",
        "knowledge_skills_note": "talking about nonsense",
        "asking_rate": "often",
        "availability": "always",
        "others": "good hire",
        "gen_remarks_recommendations": "highly recommended",
        "user_id_interviewer": "88"
    }
    - add new IAF
```

# IAF PUT REQUESTS

```
* http://localhost:5001/interview-assessment/user/view/:id

    {
        "user_id_applicant": "93",
        "date_interview": "2019-06-24",
        "communication_skills": "yes",
        "communication_skills_note": "sometimes unclear words",
        "confidence": "yes",
        "confidence_note": "sometimes looking down on the floor",
        "physical_appearance": "yes",
        "physical_appearance_note": "N/A",
        "knowledge_skills": "no",
        "knowledge_skills_note": "talking about nonsense",
        "asking_rate": "often",
        "availability": "always",
        "others": "good hire",
        "gen_remarks_recommendations": "highly recommended",
        "user_id_interviewer": "88"
    }
    - update existing IAF
    - :id here is applicant's id
```

# DEPARTMENT GET REQUESTS

```
* http://localhost:5001/departments/admin/view
    - display all department

* http://localhost:5001/departments/admin/view/:id
    - display specific department
    - :id here is department id
```

# DEPARTMENT POST REQUESTS

```
* http://localhost:5001/departments/admin/add

    {
        "name":"Accounting Department",
        "description":"Manages the finances of BFI.",
        "user_id":"14"
    }
    - add new department
```

# DEPARTMENT PUT REQUESTS

```
* http://localhost:5001/departments/admin/edit-status-inactive/:id
    - change status to inactive
    - :id here is department id

* http://localhost:5001/departments/admin/edit/:id

    {
        "name":"Accounting Department",
        "description":"Manages the finances of BFI.",
        "user_id":"14",
        "status":"active"
    }
    - update existing department
```

# CATEGORIES GET REQUESTS

```
* http://localhost:5001/categories/view
    - view all categories

* http://localhost:5001/categories/view/:id
    - view specific category
    - :id here is category id

* http://localhost:5001/categories/view-team/:id
    - view categories per team
    - :id here is team id
```

# CATEGORIES POST REQUESTS

```
* http://localhost:5001/categories/add

    {
        "name": "IQ Test",
        "description": "Exam about IQ Test.",
        "teams_id":"4",
        "time_limit_on_seconds":"30"
    }
    - add new category
```

# CATEGORIES PUT REQUESTS

```
* http://localhost:5001/categories/edit/:id

    {
        "name": "IQ Test",
        "description": "Exam about IQ Test.",
        "teams_id":"4",
        "time_limit_on_seconds":"30"
    }
    - update existing category
    - :id here is category id
```
