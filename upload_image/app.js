// for image download
const multer = require("multer");
const uniqueString = require("unique-string");
const fs = require("fs");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads");
  },
  filename: (req, file, cb) => {

    cb(null, uniqueString() + "-" + file.originalname.toLowerCase());
  }
});

const fileFilter = (req, file, cb) => {
  // change file upload validation 
  // .xlxs,.jpg,.jpeg,.png,.docx,.pdf files too
  if (
    file.mimetype === "image/jpeg" ||
    file.mimetype === "image/png" ||
    file.mimetype === "application/pdf" ||
    file.mimetype ===
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
    file.mimetype ===
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
    file.mimetype === 'application/vnd.ms-excel' ||
    file.mimetype === 'application/msword'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

// to upload image
const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 10 },
  fileFilter: fileFilter
});

// to make folder for upload
const makeFolder = (req, res, next) => {
  const path = "./uploads";
  // if directory doesn't exits.. make it..
  fs.access(path, fs.F_OK, err => {
    if (err) {
      // file doesn't exist
      // so write new file
      fs.mkdir(path, { recursive: true }, err => {
        if (err) {
          console.log(err.message);
        } else {
          console.log("Make the folder..");
          next();
        }
      });
      return;
    } else {
      // folder exists
      next();
    }
  });
};

module.exports = { upload, makeFolder };
