const app = require("./app");
const randomstring = require("randomstring"); // used to insert dummy accounts
// require query to db required for testing
const testingDb = require("../src/data-access/unit-testing/app");
const cleanData = require("./query");
/*************/
/* NEW TESTS */
/*************/
// uncomment to clear data
// afterAll(() => cleanDatas());
const cleanDatas = cleanData({ testingDb });

// admin account
const admin_email = "bfi.rectrack@gmail.com";
const admin_password = "qwe";

// applicant account
const app_email = "rodentskie@gmail.com";
const app_password = "QsHFtSj";

// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// PREREQUISITES
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
describe("Prerequisites to test the system.", () => {
  
  test("addNewDepartment() - add new departments; with token..", async () => {
    const email = admin_email;
    const password = admin_password;
    const tokenData = await app.loginUser(email, password);
    const token = tokenData.patched.token;
   

    const name = randomstring.generate(10);
    const description = randomstring.generate(10);

    // 1 is the id of the auto added account
    const results = await app.addNewDepartment(token, name, description, "1");
    expect(results).not.toBe("Error");
  });
//   //
//   test("addNewTeam() - add new team; with token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;
    

//     const data = await app.selectAllDepartment(token);
//     const deptArray = data;
//     let deptId = [];
//     for (let i = 0; i < deptArray.length; i++) {
//       const e = deptArray[i];
//       deptId.push(e.id);
//     }
//     // random user_id

//     const randomId = deptId[Math.floor(Math.random() * deptId.length)];
//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const no_of_items = "30";

//     const results = await app.addNewTeam(
//       token,
//       name,
//       description,
//       randomId,
//       no_of_items
//     );
//     expect(results).not.toBe("Error");
//   });
  
//   test("addNewCategory() - add new category IQ; with token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllTeam(token);
//     let teamId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       teamId.push(e.id);
//     }

//     // random team
//     const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];

//     const name = "IQ Test";
//     const description = randomstring.generate(10);
//     const teams_id = randomTeamId;
//     const time_limit_on_seconds = "25";

//     const results = await app.addNewCategory(
//       token,
//       name,
//       description,
//       teams_id,
//       time_limit_on_seconds
//     );
//     expect(results).not.toBe("Error");
//   });
//   //
//   test("addNewCategory() - add new category Psycho; with token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllTeam(token);
//     let teamId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       teamId.push(e.id);
//     }

//     // random team
//     const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];

//     const name = "Psychological Test";
//     const description = randomstring.generate(10);
//     const teams_id = randomTeamId;
//     const time_limit_on_seconds = "25";

//     const results = await app.addNewCategory(
//       token,
//       name,
//       description,
//       teams_id,
//       time_limit_on_seconds
//     );
//     expect(results).not.toBe("Error");
//   });
//   //
//   test("addNewCategory() - add new category Math; with token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllTeam(token);
//     let teamId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       teamId.push(e.id);
//     }

//     // random team
//     const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];

//     const name = "Math Test";
//     const description = randomstring.generate(10);
//     const teams_id = randomTeamId;
//     const time_limit_on_seconds = "25";

//     const results = await app.addNewCategory(
//       token,
//       name,
//       description,
//       teams_id,
//       time_limit_on_seconds
//     );
//     expect(results).not.toBe("Error");
//   });
//   //
//   test("addNewCategory() - add new category Word Analogy; with token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllTeam(token);
//     let teamId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       teamId.push(e.id);
//     }

//     // random team
//     const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];

//     const name = "Word Analogy";
//     const description = randomstring.generate(10);
//     const teams_id = randomTeamId;
//     const time_limit_on_seconds = "25";

//     const results = await app.addNewCategory(
//       token,
//       name,
//       description,
//       teams_id,
//       time_limit_on_seconds
//     );
//     expect(results).not.toBe("Error");
//   });
//   //
//   test("addQuestionImport() - add a question through import; with token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;
//     let categoriesId = [];
//     const data = await app.selectAllCategories(token);
//     for (let i = 0; i < data.view.length; i++) {
//       const e = data.view[i];
//       categoriesId.push(e.catid);
//     }
//     // select random category
//     const randomCategoryId =
//       categoriesId[Math.floor(Math.random() * categoriesId.length)];
//     const path = "questions/math_test.xlsx";
//     const result = await app.addQuestionImport(token, path, randomCategoryId);
//     expect(result).not.toBe("Error");
//   }, 20000);
//   //
//   test("addNewPosition() - add new team; with  token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllTeam(token);
//     let teamId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       teamId.push(e.id);
//     }

//     // random team
//     const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];
//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const is_office_staff = "t";

//     const data = await app.addNewPosition(
//       token,
//       name,
//       description,
//       randomTeamId,
//       is_office_staff
//     );
//     expect(data).not.toBe("Error");
//   });
//   //
//   test("applicantRegister() - applicant self register.. all fields entered.", async () => {
//     const emails = admin_email;
//     const passwords = admin_password;
//     const tokenData = await app.loginUser(emails, passwords);
//     const token = tokenData.patched.token;
//     const data = await app.selectAllPositions(token);

//     let positionId = "";
    
    
//     // get one position id
//     for (let i = 0; i < 1; i++) {
//       const e = data[i];
//       positionId = e.id;
      
//     }
    
  

//     const email = randomstring.generate(7);
//     const firstname = randomstring.generate(7);
//     const lastname = randomstring.generate(7);
//     const mobile = randomstring.generate(7);
//     const livestock = randomstring.generate(7);
//     const travel = "yes"; // yes or no only
//     const resume_url = randomstring.generate(7);
//     const middlename = randomstring.generate(7);
//     const religion = randomstring.generate(7);

//     const datas = await app.applicantRegister(
//       token,
//       `${email}@gmail.com`,
//       firstname,
//       lastname,
//       mobile,
//       livestock,
//       travel,
//       resume_url,
//       positionId,
//       middlename,
//       religion
//     );
  
//     expect(datas).not.toBe("Error");
//   });
// });
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // END PREREQUISITES
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

// // global variables
// const g_email = randomstring.generate(7);
// const g_password = randomstring.generate(3);
// const g_firstname = randomstring.generate(3);
// const g_middlename = randomstring.generate(3);
// const g_lastname = randomstring.generate(3);
// const g_mobile = randomstring.generate(7);

// //
// //
// //
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // USERS
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// //-------------------------------------------------
// describe("Logging in user (department head & PG)", () => {
//   //
//   test("loginUser() - must have a email and password. Account doesn't exists.", async () => {
//     const email = "dummy@gmail.com";
//     const password = "dummy";
//     const data = await app.loginUser(email, password);
//     expect(data).toBe("Error");
//   });
//   //
//   test("loginUser() - no email entered, just password.", async () => {
//     const email = "";
//     const password = admin_password;
//     const data = await app.loginUser(email, password);
//     expect(data).toBe("Error");
//   });
//   //
//   test("loginUser() - no password entered, just email.", async () => {
//     const email = admin_email;
//     const password = "";
//     const data = await app.loginUser(email, password);
//     expect(data).toBe("Error");
//   });
//   //
//   test("loginUser() - must have a email and password. Account exists.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const data = await app.loginUser(email, password);
//     const result = data.patched;
 
//     expect(result).not.toBe(0);
//   });
// });
// //-------------------------------------------------
// describe("Logging in user (applicant)", () => {
//   //
//   test("loginApplicantOnlineExam() - applicant login for online exam. Account doesn't exists.", async () => {
//     const email = "dummy@gmail.com";
//     const password = "dummy";
//     const data = await app.loginApplicantOnlineExam(email, password);

//     expect(data).toBe("Error");
//   });
//   //
//   test("loginApplicantOnlineExam() - applicant login, no email entered, just password.", async () => {
//     const email = "";
//     const password = app_password;
//     const data = await app.loginApplicantOnlineExam(email, password);
//     expect(data).toBe("Error");
//   });
//   //
//   test("loginApplicantOnlineExam() - applicant login, no password entered, just email.", async () => {
//     const email = app_email;
//     const password = "";
//     const data = await app.loginApplicantOnlineExam(email, password);
//     expect(data).toBe("Error");
//   });
  
//   test("loginApplicantOnlineExam() - applicant login for online exam. Account exists.", async () => {
//     const email = app_email;
//     const password = app_password;
//     const data = await app.loginApplicantOnlineExam(email, password);
//     const result = data.patched;
//     expect(result).not.toBe(0);
//   });
  
//   test("loginApplicantIQBExam() - applicant login for IQ and behavioral exam. Account doesn't exists.", async () => {
//     const email = "dummy@gmail.com";
//     const password = "dummy";
//     const data = await app.loginApplicantIQBExam(email, password);
//     expect(data).toBe("Error");
//   });
//   //
//   test("loginApplicantIQBExam() - applicant login, no email entered, just password.", async () => {
//     const email = "";
//     const password = app_password;
//     const data = await app.loginApplicantIQBExam(email, password);
//     expect(data).toBe("Error");
//   });
//   //
//   test("loginApplicantIQBExam() - applicant login, no password entered, just email.", async () => {
//     const email = app_email;
//     const password = "";
//     const data = await app.loginApplicantIQBExam(email, password);
//     expect(data).toBe("Error");
//   });
  
//   test("loginApplicantIQBExam() - applicant login for IQ and behavioral exam. Account exists.", async () => {
//     const {decrypt} = require("../src/entities/users/crypting")
//     let _email="77ec3ae7545a2c5a1bac686cb6fa2064";
//     let _password="69db0aab6c4a7b74168a4b47";
//     const email = decrypt(_email);
//     const password = decrypt(_password);
//     const data = await app.loginApplicantIQBExam(email, password);    
//     const result = data.patched;


//     expect(result).not.toBe(0);
//   });
// });
// //-------------------------------------------------
// describe("Making new account / Update for the admin (department head & PG)", () => {
//   //
//   // VALID ACCOUNT
//   test("selectAllRoles() - select all roles with token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllRoles(token);
//     const dataCount = data.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //-------
//   test("selectAllRoles() - select all roles with wrong token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllRoles(token + "s");
//     expect(data).toBe("Error");
//   });
//   //-------
//   test("addNewRole() - add new role; with token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);

//     const data = await app.addNewRole(token, name, description);
//     expect(data).not.toBe("Error");
//   });
//   //-------
//   test("addNewRole() - add new role; with wrong token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);

//     const data = await app.addNewRole(token + "s", name, description);
//     expect(data).toBe("Error");
//   });
//   //-------
//   test("selectAllPositions() - select all positions with token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllPositions(token);
//     const dataCount = data.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //-------
//   test("selectAllPositions() - select all positions with wrong token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllPositions(token + "s");
//     expect(data).toBe("Error");
//   });
//   //-------
//   test("selectAllVacant() - select all vacant positions with token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllVacant(token);
//     expect(data).not.toBe("Error");
//   });
//   //-------
//   test("selectAllVacant() - select all vacant positions with wrong token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllVacant(token + "s");
//     expect(data).toBe("Error");
//   });
//   //-------
//   test("selectAllNotVacant() - select all not vacant positions with token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllNotVacant(token);
//     expect(data).not.toBe("Error");
//   });
//   //-------
//   test("selectAllNotVacant() - select all not vacant positions with wrong token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllNotVacant(token + "s");
//     expect(data).toBe("Error");
//   });
//   //-------
//   test("addNewPosition() - add new team; with  token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllTeam(token);
//     let teamId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       teamId.push(e.id);
//     }

//     // random team
//     const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];
//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const is_office_staff = "t";

//     const data = await app.addNewPosition(
//       token,
//       name,
//       description,
//       randomTeamId,
//       is_office_staff
//     );
//     expect(data).not.toBe("Error");
//   });
//  // -------
//   test("addNewPosition() - add new team; with wrong  token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllTeam(token);
//     let teamId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       teamId.push(e.id);
//     }

//     // random team
//     const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];
//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const is_office_staff = "t";

//     const data = await app.addNewPosition(
//       token + "s",
//       name,
//       description,
//       randomTeamId,
//       is_office_staff
//     );
//     expect(data).toBe("Error");
//   });
//   //-------
//   test("positionToNotVacant() - change position to not vacant; with token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllPositions(token);
//     let positionId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       positionId.push(e.id);
//     }
//     const randomPositionId =
//       positionId[Math.floor(Math.random() * positionId.length)];
//     const result = await app.positionToNotVacant(token, randomPositionId);
//     expect(result).not.toBe("Error");
//   });
//   //-------
//   test("positionToNotVacant() - change position to not vacant; with wrong token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllPositions(token);
//     let positionId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       positionId.push(e.id);
//     }
//     const randomPositionId =
//       positionId[Math.floor(Math.random() * positionId.length)];
//     const result = await app.positionToNotVacant(token + "s", randomPositionId);
//     expect(result).toBe("Error");
//   });
//   //-------
//   test("positionToVacant() - change position to vacant; with token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllPositions(token);
//     let positionId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       positionId.push(e.id);
//     }
//     const randomPositionId =
//       positionId[Math.floor(Math.random() * positionId.length)];
//     const result = await app.positionToVacant(token, randomPositionId);
//     expect(result).not.toBe("Error");
//   });
//   //-------
//   test("positionToVacant() - change position to vacant; with wrong token", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllPositions(token);
//     let positionId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       positionId.push(e.id);
//     }
//     const randomPositionId =
//       positionId[Math.floor(Math.random() * positionId.length)];
//     const result = await app.positionToVacant(token + "s", randomPositionId);
//     expect(result).toBe("Error");
//   });
//   //-------
//   test("adminAddUser() - add new user with token; fields are all entered with data..", async () => {
//     const emails = admin_email;
//     const passwords = admin_password;
//     const tokenData = await app.loginUser(emails, passwords);
//     const token = tokenData.patched.token;

//     const dataPosition = await app.selectAllPositions(token);
//     const dataRole = await app.selectAllRoles(token);

//     // get any position and role to enter
//     const positionId = dataPosition[0].id;
//     const roleId = dataRole[0].id;

//     const email = `${g_email}@gmail.com`;
//     const password = g_password;
//     const firstname = g_firstname;
//     const lastname = g_lastname;
//     const mobile = g_mobile;
//     const role_id = roleId;
//     const position_id = positionId;
//     const middlename = g_middlename;

//     const addUser = await app.adminAddUser(
//       token,
//       email,
//       password,
//       firstname,
//       lastname,
//       mobile,
//       role_id,
//       position_id,
//       middlename
//     );
//     expect(addUser).not.toBe("Error");
//   });
//   //-------
//   test("adminAddUser() - add new user without token; fields are all entered with data..", async () => {
//     const emails = admin_email;
//     const passwords = admin_password;
//     const tokenData = await app.loginUser(emails, passwords);
//     const token = tokenData.patched.token;

//     const dataPosition = await app.selectAllPositions(token);
//     const dataRole = await app.selectAllRoles(token);

//     // get any position and role to enter
//     const positionId = dataPosition[0].id;
//     const roleId = dataRole[0].id;

//     const email = `${g_email}@gmail.com`;
//     const password = g_password;
//     const firstname = g_firstname;
//     const lastname = g_lastname;
//     const mobile = g_mobile;
//     const role_id = roleId;
//     const position_id = positionId;
//     const middlename = g_middlename;

//     const addUser = await app.adminAddUser(
//       token + "s",
//       email,
//       password,
//       firstname,
//       lastname,
//       mobile,
//       role_id,
//       position_id,
//       middlename
//     );
//     expect(addUser).toBe("Error");
//   });
//   //-------
//   test("adminAddUser() - add new user with token; fields are all entered with data, but existing account.", async () => {
//     const emails = admin_email;
//     const passwords = admin_password;
//     const tokenData = await app.loginUser(emails, passwords);
//     const token = tokenData.patched.token;

//     const dataPosition = await app.selectAllPositions(token);
//     const dataRole = await app.selectAllRoles(token);

//     // get any position and role to enter
//     const positionId = dataPosition[0].id;
//     const roleId = dataRole[0].id;

//     const email = `${g_email}@gmail.com`;
//     const password = g_password;
//     const firstname = g_firstname;
//     const lastname = g_lastname;
//     const mobile = g_mobile;
//     const role_id = roleId;
//     const position_id = positionId;
//     const middlename = g_middlename;

//     const addUser = await app.adminAddUser(
//       token,
//       email,
//       password,
//       firstname,
//       lastname,
//       mobile,
//       role_id,
//       position_id,
//       middlename
//     );
//     expect(addUser).not.toBe("Error");
//   });
//   //-------
//   test("adminAddUser() - add new user with token; some fields are not entered with data..", async () => {
//     const emails = admin_email;
//     const passwords = admin_password;
//     const tokenData = await app.loginUser(emails, passwords);
//     const token = tokenData.patched.token;

//     const dataPosition = await app.selectAllPositions(token);
//     const dataRole = await app.selectAllRoles(token);

//     // get any position and role to enter
//     const positionId = dataPosition[0].id;
//     const roleId = dataRole[0].id;

//     const email = `${g_email}@gmail.com`;
//     const password = g_password;
//     const firstname = g_firstname;
//     const lastname = g_lastname;
//     const mobile = "";
//     const role_id = roleId;
//     const position_id = positionId;
//     const middlename = "";

//     const addUser = await app.adminAddUser(
//       token,
//       email,
//       password,
//       firstname,
//       lastname,
//       mobile,
//       role_id,
//       position_id,
//       middlename
//     );
//     expect(addUser).toBe("Error");
//   });
//   //-------
//   test("updateAdminInfo() - update user with token; fields are all entered with data..", async () => {
//     const emails = admin_email;
//     const passwords = admin_password;
//     const tokenData = await app.loginUser(emails, passwords);
//     const token = tokenData.patched.token;

//     const dataPosition = await app.selectAllPositions(token);
//     const dataRole = await app.selectAllRoles(token);

//     // get any position and role to enter
//     const positionId = dataPosition[0].id;
//     const roleId = dataRole[0].id;

//     const email = `${g_email}@gmail.com`;
//     const password = g_password;
//     const firstname = g_firstname;
//     const lastname = g_lastname;
//     const mobile = g_mobile;
//     const role_id = roleId;
//     const position_id = positionId;
//     const middlename = g_middlename;
//     const status = "active";

//     const addUser = await app.adminAddUser(
//       token,
//       email,
//       password,
//       firstname,
//       lastname,
//       mobile,
//       role_id,
//       position_id,
//       middlename
//     );
//     // existing user to update
//     const id = addUser.posted.rows[0].id;

//     const updateUser = await app.updateAdminInfo(
//       token,
//       id,
//       email,
//       password,
//       firstname,
//       lastname,
//       mobile,
//       role_id,
//       position_id,
//       middlename,
//       status
//     );

//     expect(updateUser).not.toBe("Error");
//   });
//   //-------
// });
// //-------------------------------------------------
// describe("Selecting applicants filtered by its corressponding status.", () => {
//   //
//   test("selectAllReceivedUsers() - select all received applicants; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllReceivedUsers(token);
//     const dataCount = data.view.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllReceivedUsers() - select all received applicants; without token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllReceivedUsers(token + "s");
//     expect(data).toBe("Error");
//   });
//   //
//   test("selectAllOnlineExamUsers() - select all online-exam applicants; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllOnlineExamUsers(token);
//     const dataCount = data.view.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllOnlineExamUsers() - select all online-exam applicants; without token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllOnlineExamUsers(token + "s");
//     expect(data).toBe("Error");
//   });
//   //
//   test("selectAllEndorsedUsers() - select all endorsed applicants; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllEndorsedUsers(token);
//     const dataCount = data.view.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllEndorsedUsers() - select all endorsed applicants; without token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllEndorsedUsers(token +"s");
//     expect(data).toBe("Error");
//   });
//   //
//   test("selectAllRejectedUsers() - select all rejected applicants; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllRejectedUsers(token);
//     const dataCount = data.view.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllRejectedUsers() - select all rejected applicants; without token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllRejectedUsers(token + "s");
//     expect(data).toBe("Error");
//   });
//   //
//   test("selectAllBlacklistUsers() - select all blacklisted applicants; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllBlacklistUsers(token);
//     const dataCount = data.view.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllBlacklistUsers() - select all blacklisted applicants; without token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllBlacklistUsers(token + "s");
//     expect(data).toBe("Error");
//   });
//   //
//   test("selectAllAssessmentUsers() - select all assessment applicants; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllAssessmentUsers(token);
//     const dataCount = data.view.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllAssessmentUsers() - select all assessment applicants; without token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllAssessmentUsers(token + "s");
//     expect(data).toBe("Error");
//   });
//   //
//   test("selectAllPendingUsers() - select all pending applicants; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllPendingUsers(token);
//     const dataCount = data.view.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllPendingUsers() - select all pending applicants; without token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllPendingUsers(token + "s");
//     expect(data).toBe("Error");
//   });
//   //
//   test("selectAllKeptForReferenceUsers() - select all kept for reference applicants; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllKeptForReferenceUsers(token);
//     const dataCount = data.view.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllKeptForReferenceUsers() - select all kept for reference applicants; without token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllKeptForReferenceUsers(token + "s");
//     expect(data).toBe("Error");
//   });
//   //
//   test("selectAllShortlistUsers() - select all shortlist applicants; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllShortlistUsers(token);
//     const dataCount = data.view.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllShortlistUsers() - select all shortlist applicants; without token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllShortlistUsers(token + "s");
//     expect(data).toBe("Error");
//   });
//   //
//   test("selectAllDeployedUsers() - select all deployed applicants; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllDeployedUsers(token);
//     const dataCount = data.view.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllDeployedUsers() - select all deployed applicants; without token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllDeployedUsers(token + "s");
//     expect(data).toBe("Error");
//   });
//   //
//   test("selectAllJobOrderUsers() - select all job order applicants; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllJobOrderUsers(token);
//     const dataCount = data.view.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllJobOrderUsers() - select all job order applicants; without token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllJobOrderUsers(token + "s");
//     expect(data).toBe("Error");
//   });
// });
// //-------------------------------------------------
// describe("For notifications.", () => {
//   //
//   test("getCountForReceived() - select all received applicants.", async () => {
//     const data = await app.getCountForReceived();
//     const dataCount = data.view.length;

//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("getCountForEndorsed() - select all endorsed applicants.", async () => {
//     const data = await app.getCountForEndorsed();
//     const dataCount = data.view.length;
//     expect(dataCount).not.toBeUndefined;
//   });
// });
// //-------------------------------------------------
// describe("Admin viewing of users.", () => {
//   //
//   test("selectAllUsers() - select all users; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllUsers(token);
//     const dataCount = data.view.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllUsers() - select all users; without token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllUsers(token + "s");
//     expect(data).toBe("Error");
//   });
//   //
//   test("selectUserDetails() - select user's details; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllUsers(token);
//     // loop thru array and get one id
//     for (let i = 0; i < 1; i++) {
//       const e = data.view[i];
//       const id = e.id;
//       const result = await app.selectUserDetails(token, id);
//       const count = result.view.length;
//       expect(count).not.toBeUndefined();
//     }
//   });
  
//   test("selectUserDetails() - select user's details; without token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllUsers(token);
//     // loop thru array and get one id
//     for (let i = 0; i < 1; i++) {
//       const e = data.view[i];
//       const id = e.id;
//       const result = await app.selectUserDetails(token + "s", id);
//       expect(result).toBe("Error");
//     }
//   });
// });
// //-------------------------------------------------
// describe("Applicant self registration", () => {
//   //
//   test("applicantRegister() - applicant self register.. all fields entered.", async () => {
//     const emails = admin_email;
//     const passwords = admin_password;
//     const tokenData = await app.loginUser(emails, passwords);
//     const token = tokenData.patched.token;
//     const data = await app.selectAllPositions(token);
    
//     let positionId = "";

//     // get one position id
//     for (let i = 0; i < 1; i++) {
//       const e = data[i];
//       positionId = e.id;
//     }

//     const email = randomstring.generate(7);
//     const firstname = randomstring.generate(7);
//     const lastname = randomstring.generate(7);
//     const mobile = randomstring.generate(7);
//     const livestock = randomstring.generate(7);
//     const travel = "yes"; // yes or no only
//     const resume_url = randomstring.generate(7);
//     const middlename = randomstring.generate(7);
//     const religion = randomstring.generate(7);

//     const datas = await app.applicantRegister(
//       token,
//       `${email}@gmail.com`,
//       firstname,
//       lastname,
//       mobile,
//       livestock,
//       travel,
//       resume_url,
//       positionId,
//       middlename,
//       religion
//     );
   
//     expect(datas).not.toBe("Error");
//   });
//   //
//   test("applicantRegister() - applicant self register.. not all fields entered.", async () => {
//     const emails = admin_email;
//     const passwords = admin_password;
//     const tokenData = await app.loginUser(emails, passwords);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllPositions(token);
//     let positionId = "";

//     // get one position id
//     for (let i = 0; i < 1; i++) {
//       const e = data[i];
//       positionId = e.id;
//     }

//     const email = randomstring.generate(7);
//     const firstname = randomstring.generate(7);
//     const lastname = randomstring.generate(7);
//     const mobile = randomstring.generate(7);
//     const livestock = randomstring.generate(7);
//     const travel = "yes"; // yes or no only
//     const resume_url = randomstring.generate(7);
//     const middlename = randomstring.generate(7);
//     const religion = randomstring.generate(7);

//     const datas = await app.applicantRegister(
//       token,
//       `${email}@gmail.com`,
//       firstname,
//       lastname,
//       "",
//       livestock,
//       travel,
//       resume_url,
//       positionId,
//       "",
//       religion
//     );
//     expect(datas).toBe("Error");
//   });
  
//   // test("applicantFillUpApplicationForm() - applicant fill up application form completely.", async () => {
//   //   const {decrypt} = require("../src/entities/users/crypting")
//   //   let _email="77e62ed273583d4014a96d23fbf622";
//   //   let _password="52c90bea515f7b482db93c47";

//   //   const email = decrypt(_email);
//   //   const password = decrypt(_password);
    
//   //   const tokenData = await app.loginApplicantIQBExam(email, password);
//   //   // token
 
//   //   const token = tokenData.patched.token;
  

//   //   // select an applicant : status online exam
//   //   const data = await app.selectAllOnlineExamUsers(token);
//   //   // id of applicant
  

//   //   const id = data.view[0].id;
  


//   //   // select two position id
//   //   const pdata = await app.selectAllPositions(token);
    
//   //   let pId = [];
//   //   for (let i = 0; i < 2; i++) {
//   //     const e = pdata[i];
//   //     pId.push(e.id);
//   //   }
    

   

//   //   // name is retrive from get
//   //   const firstname = "test";
//   //   const middlename = "test";
//   //   const lastname = "test";
//   //   //
//   //   const salary_desired = "15000";
//   //   const city_address = "Gensan City";
//   //   const provincial_address = "Polomolok, South Cotabato";
//   //   const place_birth = "Polomolok, South Cotabato";
//   //   const bday = "1995-02-18";
//   //   const age = "24";
//   //   const height = "163";
//   //   const weight = "75";
//   //   const gender = "Male";
//   //   const is_single_parent = "f";
//   //   const civil_status = "Single";
//   //   const citizenship = "Filipino";
//   //   const religion = "Roman Catholic";
//   //   const who_referred = "Monkey D. Luffy";
//   //   const when_to_start = "ASAP";
//   //   const person_to_notify = "Roronoa Zoro";
//   //   const person_relationship = "Swordsman";
//   //   const person_address = "East blue";
//   //   const person_telephone = "Denden Mushi";
//   //  // const position_id_first_choice = pId[0];
//   //   const position_id_second_choice = pId[1];
//   //   const CS_eligible = "t";
//   //   const CS_date_taken = "2017-08-06";
//   //   const CS_certificate_image_path = "D:/Images/src/scan.jpg";
//   //   const date_application = "2019-07-01";
//   //   const user_image_path = "D:/Images/src/scan(1).jpg";
//   //   const educationalBgArray = {
//   //     educational_attainment: ["Elementary", "High school", "College"],
//   //     institution_name: [
//   //       "Polomolok Central Elementary School",
//   //       "San Lorenzo Ruiz Academy of Polomolok",
//   //       "Holy Trinity College of General Santos City"
//   //     ],
//   //     institution_address: [
//   //       "Pioneer Ave., Pol., So. Cot.",
//   //       "Kaunlaran Subd., Cannery Site, Pol., So. Cot.",
//   //       "Daproza Ave., General Santos City"
//   //     ],
//   //     course_degree: ["Grade 1-6", "1st - 4th year", "BS Computer Engineering"],
//   //     years_attended: ["2001-2007", "2007-2011", "2014-2019"],
//   //     honors_awards: ["", "", ""]
//   //   };
//   //   const relativeArray = {
//   //     full_name: ["Trafalgar Law", "Eustass Kid"],
//   //     position: ["Doctor", "Mechanics"],
//   //     branch: ["Clinic", "Mechanical Dept"],
//   //     relationship: ["Nakama", "Alliance"]
//   //   };
//   //   const professionalLicensesArray = {
//   //     professional_licenses_held: ["Nursing", "Engineering"],
//   //     certificate_image_path: [
//   //       "D:/Images/src/scan(2).jpg",
//   //       "D:/Images/src/scan(3).jpg"
//   //     ]
//   //   };
//   //   const orgMembershipArray = {
//   //     association_name: ["East blue pirates", "Yonkou"],
//   //     position_title: ["Oyabun", "5th Emperor"],
//   //     year_participation: ["2018", "2019"]
//   //   };
//   //   const seminarsAttendedArray = {
//   //     seminar_name: ["Haki Tutorial Seminar", "Pirate King Seminar"],
//   //     description: [
//   //       "Teaches the fundamentals of armament haki, observation haki and emperor's haki.",
//   //       "How to be a pirate king."
//   //     ],
//   //     venue: ["Ruskaina Island", "Shakky's bar"],
//   //     conducted_by: ["Silver Rayleigh", "Silver Rayleigh, Shakky"],
//   //     dates: ["2015-05-05", "2016-06-06"],
//   //     certificate_image_path: [
//   //       "D:/Images/src/scan(4).jpg",
//   //       "D:/Images/src/scan(5).jpg"
//   //     ]
//   //   };
//   //   const workExperienceArray = {
//   //     company_name: ["KCC", "DOLE"],
//   //     address: ["J. Catolico Ave., GSC", "Cannery Site, Pol., So. Cot."],
//   //     position: ["Bagger", "Janitor"],
//   //     start_date: ["2015-01-01", "2019-02-18"],
//   //     end_date: ["2018-01-10", "2019-05-18"],
//   //     last_salary: ["5000", "2000"],
//   //     reason_leaving: ["Not having sleep", ""]
//   //   };
//   //   const workWithBFIArray = {
//   //     have_work_with_bfi: "t",
//   //     branch_company: ["Agro", "Farm"],
//   //     dates: ["2015-04-05", "2018-03-28"],
//   //     position: ["Feeder", "Farmer"],
//   //     department: ["Feeder Dept", "Farmer Dept"]
//   //   };
//   //   const otherInfoArray = {
//   //     typing_wpm: "500",
//   //     steno_wpm: "450",
//   //     hobbies: "Playing computer games, boxing, sleeping, eating",
//   //     language_speak_write: "English, Tagalog, Cebuano, Japanese",
//   //     machines_can_operate: "Computer, microcontrollers",
//   //     any_crime_committed: "f",
//   //     been_hospitalized: "t",
//   //     computer: "t",
//   //     art_work: "f",
//   //     crime_details: "",
//   //     hospitalized_details: "fever, diarrhea"
//   //   };
//   //   const familyBgArray = {
//   //     name: ["Nami", "Robin", "Vivi"],
//   //     relationship: ["Sister", "Mother", "Sister"],
//   //     age: ["21", "32", "20"],
//   //     occupation: ["Navigator", "Archeologist", "Princess of Alabasta Kingdom"],
//   //     company_address: [
//   //       "Strawhat Pirate Ship",
//   //       "Strawhat Pirate Ship",
//   //       "Alabasta Kingdom"
//   //     ]
//   //   };
//   //   const isMarriedArray = {
//   //     is_married: "t",
//   //     number_children: "3",
//   //     range_age: "4-12",
//   //     do_you: "Live in Own House",
//   //     with_who: "Wife"
//   //   };
//   //   const referencesArray = {
//   //     name: ["Silvers Rayleigh", "King Neptune", "King Cobra"],
//   //     address: ["Sabaody Archipelago", "Merman Island", "Alabasta Kingdom"],
//   //     telephone: ["159-753", "456-654", "357-123"],
//   //     occupation: ["Former Pirate", "King", "Former King"],
//   //     how_long_known: ["2 years", "3 months", "6 months"]
//   //   };
//   //   const willingnessArray = {
//   //     explain_why_hire: "You should hire me because it's me.",
//   //     willing_train_apprentice: "t",
//   //     willing_to_BOND: "t",
//   //     sss: "123-456-981",
//   //     tin: "123-456-0000",
//   //     residence_certificate: "123-951-354-798-156",
//   //     residence_certificate_date_issue: "2012-01-01",
//   //     residence_certificate_place_issue: "Pol So Cot."
//   //   };
    
//   //   const UnitRoomNumberFloor= randomstring.generate(7);
//   //   const BuildingName= randomstring.generate(7);
//   //   const LotBlockPhaseHouseNumber= randomstring.generate(7);
//   //   const StreetName= randomstring.generate(7);
//   //   const VillageSubdivision= randomstring.generate(7);
//   //   const Barangay = randomstring.generate(7)
//   //   const TownDistrict=randomstring.generate(7)
//   //   const Municipality= randomstring.generate(7);
//   //   const CityProvince= randomstring.generate(7);
//   //   const AddressType= randomstring.generate(7);
 

//   //   const result = await app.applicantFillUpApplicationForm(
//   //     token,
//   //     id,
//   //     // firstname,
//   //     // middlename,
//   //     // lastname,
//   //     salary_desired,
//   //     UnitRoomNumberFloor,
//   //     BuildingName,
//   //     LotBlockPhaseHouseNumber,
//   //     StreetName,
//   //     VillageSubdivision,
//   //     Barangay,
//   //     TownDistrict,
//   //     Municipality,
//   //     CityProvince,
//   //     AddressType,
//   //     place_birth,
//   //     bday,
//   //     age,
//   //     height,
//   //     weight,
//   //     gender,
//   //     is_single_parent,
//   //     civil_status,
//   //     citizenship,
//   //     religion,
//   //     who_referred,
//   //     when_to_start,
//   //     person_to_notify,
//   //     person_relationship,
//   //     person_address,
//   //     person_telephone,
//   //     // position_id_first_choice,
//   //     position_id_second_choice,
//   //     CS_eligible,
//   //     CS_date_taken,
//   //     CS_certificate_image_path,
//   //     date_application,
//   //     user_image_path,
//   //     educationalBgArray,
//   //     relativeArray,
//   //     professionalLicensesArray,
//   //     orgMembershipArray,
//   //     seminarsAttendedArray,
//   //     workExperienceArray,
//   //     workWithBFIArray,
//   //     otherInfoArray,
//   //     familyBgArray,
//   //     isMarriedArray,
//   //     referencesArray,
//   //     willingnessArray
//   //   );
   
//   //   expect(result).not.toBe("Error");
//   // });
  
//   test("applicantFillUpApplicationForm() - applicant fill up application form incompletely.", async () => {
//     const {decrypt} = require("../src/entities/users/crypting")
//     let _email="77e62ed273583d4014a96d23fbf622";
//     let _password="52c90bea515f7b482db93c47";
  
//     const emails = decrypt(_email);
//     const passwords = decrypt(_password);
//     const tokenData = await app.loginApplicantIQBExam(emails, passwords);
//     // token
//     const token = tokenData.patched.token;

//     // select an applicant : status online exam
//     const data = await app.selectAllOnlineExamUsers(token);

//     // id of applicant
//     const id = data.view[1].id;
    


//     // select two position id
//     const pdata = await app.selectAllPositions(token);
//     let pId = [];
//     for (let i = 0; i < 2; i++) {
//       const e = pdata[i];
//       pId.push(e.id);
//     }
//     // name is retrive from get
//     const firstname = "test";
//     const middlename = "test";
//     const lastname = "test";
//     //
//     const salary_desired = "15000";
//     const city_address = "Gensan City";
//     const provincial_address = "Polomolok, South Cotabato";
//     const place_birth = "Polomolok, South Cotabato";
//     const bday = "1995-02-18";
//     const age = "24";
//     const height = "163";
//     const weight = "75";
//     const gender = "Male";
//     const is_single_parent = "f";
//     const civil_status = "Single";
//     const citizenship = "Filipino";
//     const religion = "Roman Catholic";
//     const who_referred = "Monkey D. Luffy";
//     const when_to_start = "ASAP";
//     const person_to_notify = "Roronoa Zoro";
//     const person_relationship = "Swordsman";
//     const person_address = "East blue";
//     const person_telephone = "Denden Mushi";
//    // const position_id_first_choice = pId[0];
//     const position_id_second_choice = pId[1];
//     const CS_eligible = "t";
//     const CS_date_taken = "2017-08-06";
//     const CS_certificate_image_path = "D:/Images/src/scan.jpg";
//     const date_application = "2019-07-01";
//     const user_image_path = "D:/Images/src/scan(1).jpg";
//     const educationalBgArray = {
//       educational_attainment: ["Elementary", "High school", "College"],
//       institution_name: [
//         "Polomolok Central Elementary School",
//         "San Lorenzo Ruiz Academy of Polomolok",
//         "Holy Trinity College of General Santos City"
//       ],
//       institution_address: [
//         "Pioneer Ave., Pol., So. Cot.",
//         "Kaunlaran Subd., Cannery Site, Pol., So. Cot.",
//         "Daproza Ave., General Santos City"
//       ],
//       course_degree: ["Grade 1-6", "1st - 4th year", "BS Computer Engineering"],
//       years_attended: ["2001-2007", "2007-2011", "2014-2019"],
//       honors_awards: ["", "", ""]
//     };
//     const relativeArray = {
//       full_name: ["Trafalgar Law", "Eustass Kid"],
//       position: ["Doctor", "Mechanics"],
//       branch: ["Clinic", "Mechanical Dept"],
//       relationship: ["Nakama", "Alliance"]
//     };
//     const professionalLicensesArray = {
//       professional_licenses_held: ["Nursing", "Engineering"],
//       certificate_image_path: [
//         "D:/Images/src/scan(2).jpg",
//         "D:/Images/src/scan(3).jpg"
//       ]
//     };
//     const orgMembershipArray = {
//       association_name: ["East blue pirates", "Yonkou"],
//       position_title: ["Oyabun", "5th Emperor"],
//       year_participation: ["2018", "2019"]
//     };
//     const seminarsAttendedArray = {
//       seminar_name: ["Haki Tutorial Seminar", "Pirate King Seminar"],
//       description: [
//         "Teaches the fundamentals of armament haki, observation haki and emperor's haki.",
//         "How to be a pirate king."
//       ],
//       venue: ["Ruskaina Island", "Shakky's bar"],
//       conducted_by: ["Silver Rayleigh", "Silver Rayleigh, Shakky"],
//       dates: ["2015-05-05", "2016-06-06"],
//       certificate_image_path: [
//         "D:/Images/src/scan(4).jpg",
//         "D:/Images/src/scan(5).jpg"
//       ]
//     };
//     const workExperienceArray = {
//       company_name: ["KCC", "DOLE"],
//       address: ["J. Catolico Ave., GSC", "Cannery Site, Pol., So. Cot."],
//       position: ["Bagger", "Janitor"],
//       start_date: ["2015-01-01", "2019-02-18"],
//       end_date: ["2018-01-10", "2019-05-18"],
//       last_salary: ["5000", "2000"],
//       reason_leaving: ["Not having sleep", ""]
//     };
//     const workWithBFIArray = {
//       have_work_with_bfi: "t",
//       branch_company: ["Agro", "Farm"],
//       dates: ["2015-04-05", "2018-03-28"],
//       position: ["Feeder", "Farmer"],
//       department: ["Feeder Dept", "Farmer Dept"]
//     };
//     const otherInfoArray = {
//       typing_wpm: "500",
//       steno_wpm: "450",
//       hobbies: "Playing computer games, boxing, sleeping, eating",
//       language_speak_write: "English, Tagalog, Cebuano, Japanese",
//       machines_can_operate: "Computer, microcontrollers",
//       any_crime_committed: "f",
//       been_hospitalized: "t",
//       computer: "t",
//       art_work: "f",
//       crime_details: "",
//       hospitalized_details: "fever, diarrhea"
//     };
//     const familyBgArray = {
//       name: ["Nami", "Robin", "Vivi"],
//       relationship: ["Sister", "Mother", "Sister"],
//       age: ["21", "32", "20"],
//       occupation: ["Navigator", "Archeologist", "Princess of Alabasta Kingdom"],
//       company_address: [
//         "Strawhat Pirate Ship",
//         "Strawhat Pirate Ship",
//         "Alabasta Kingdom"
//       ]
//     };
//     const isMarriedArray = {
//       is_married: "t",
//       number_children: "3",
//       range_age: "4-12",
//       do_you: "Live in Own House",
//       with_who: "Wife"
//     };
//     const referencesArray = {
//       name: ["Silvers Rayleigh", "King Neptune", "King Cobra"],
//       address: ["Sabaody Archipelago", "Merman Island", "Alabasta Kingdom"],
//       telephone: ["159-753", "456-654", "357-123"],
//       occupation: ["Former Pirate", "King", "Former King"],
//       how_long_known: ["2 years", "3 months", "6 months"]
//     };
//     const willingnessArray = {
//       explain_why_hire: "You should hire me because it's me.",
//       willing_train_apprentice: "t",
//       willing_to_BOND: "t",
//       sss: "123-456-981",
//       tin: "123-456-0000",
//       residence_certificate: "123-951-354-798-156",
//       residence_certificate_date_issue: "2012-01-01",
//       residence_certificate_place_issue: "Pol So Cot."
//     };
//     const UnitRoomNumberFloor= randomstring.generate(7);
//     const BuildingName= randomstring.generate(7);
//     const LotBlockPhaseHouseNumber= randomstring.generate(7);
//     const StreetName= randomstring.generate(7);
//     const VillageSubdivision= randomstring.generate(7);

//     const result = await app.applicantFillUpApplicationForm(
//       token,
//       id,
//       firstname,
//       middlename,
//       lastname,
//       salary_desired,
//       UnitRoomNumberFloor,
//       BuildingName,
//       LotBlockPhaseHouseNumber,
//       StreetName,
//       VillageSubdivision,
//       city_address,
//       provincial_address,
//       place_birth,
//       bday,
//       age,
//       height,
//       weight,
//       gender,
//       is_single_parent,
//       civil_status,
//       citizenship,
//       religion,
//       who_referred,
//       when_to_start,
//       person_to_notify,
//       person_relationship,
//       person_address,
//       person_telephone,
//       // position_id_first_choice,
//       position_id_second_choice,
//       CS_eligible,
//       CS_date_taken,
//       CS_certificate_image_path,
//       "",
//       user_image_path,
//       educationalBgArray,
//       "",
//       professionalLicensesArray,
//       orgMembershipArray,
//       seminarsAttendedArray,
//       workExperienceArray,
//       workWithBFIArray,
//       otherInfoArray,
//       familyBgArray,
//       isMarriedArray,
//       referencesArray,
//       willingnessArray
//     );
//     expect(result).toBe("Error");
//   });
//  });
// // -------------------------------------------------
// describe("Head set date for assessment of applicant", () => {
//   //
//   test("setDataForAssessment() - set date for assessment; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;
 

//     const data = await app.selectAllEndorsedUsers(token);

//     // id of applicant
 
//     const id = data.view[0].id;
//     const date = "2019/08/05";
//     const link="asdasdasds"
//     const name= "Brian"
//     const contact = "09123456789"
//     const location = "KCC Marbel"

//     const datas = await app.setDataForAssessment(id, token, date,link,name,contact,location);
//     const rowCount = datas.patched;


//     expect(rowCount).not.toBeUndefined();
//   });
  
//   test("setDataForAssessment() - set date for assessment; with wrong token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;
//     const data = await app.selectAllEndorsedUsers(token);
//     // id of applicant
//     const id = data.view[0].id;

//     const date = "2019/07/05";
//     const datas = await app.setDataForAssessment(token + "s", id, date);
//     expect(datas).toBe("Error");
//   });
//  });
// //-------------------------------------------------
// describe("Change status of applicant..", () => {
//   //
//   test("setStatusToJobOrder() - change status of selected applicant to job order.", async () => {
//     const emails = admin_email;
//     const passwords = admin_password;
//     const tokenData = await app.loginUser(emails, passwords);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllPositions(token);
  
//     let positionId = "";

//     // get one position id
//     for (let i = 0; i < 1; i++) {
//       const e = data[i];
//       positionId = e.id;
//     }

//     const email = randomstring.generate(7);
//     const firstname = randomstring.generate(7);
//     const lastname = randomstring.generate(7);
//     const mobile = randomstring.generate(7);
//     const livestock = randomstring.generate(7);
//     const travel = "yes"; // yes or no only
//     const resume_url = randomstring.generate(7);
//     const middlename = randomstring.generate(7);

//     const datas = await app.applicantRegister(
//       `${email}@gmail.com`,
//       firstname,
//       lastname,
//       mobile,
//       livestock,
//       travel,
//       resume_url,
//       positionId,
//       middlename
//     );

//     let id = [];
//     const rdata = await app.selectAllReceivedUsers(token);
//     for (let i = 0; i < rdata.view.length; i++) {
//       const e = rdata.view[i];
//       id.push(e.id);
//     }
//     // select random id from array
//     const randomId = id[Math.floor(Math.random() * id.length)];
//     const time = "12:30"
//     const location = "KCC Marbel"
//     const contactPerson ="asdas"
//     const date = "2019/07/05";
//     const contactNumber = "09123456789";

//     // change status
//     const applicant = await app.setStatusToJobOrder(randomId,date,time,location,token, contactPerson,contactNumber);
//     const rowCount = applicant.patched;
    
//     expect(rowCount).not.toBeUndefined();
//   });
  
//   test("setStatusToOnlineExam() - change status of selected applicant to online exam.", async () => {
//     const emails = admin_email;
//     const passwords = admin_password;
//     const tokenData = await app.loginUser(emails, passwords);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllPositions(token);
//     let positionId = "";

//     // get one position id
//     for (let i = 0; i < 1; i++) {
//       const e = data[i];
//       positionId = e.id;
//     }

//     const email = randomstring.generate(7);
//     const firstname = randomstring.generate(7);
//     const lastname = randomstring.generate(7);
//     const mobile = randomstring.generate(7);
//     const livestock = randomstring.generate(7);
//     const travel = "yes"; // yes or no only
//     const resume_url = randomstring.generate(7);
//     const middlename = randomstring.generate(7);

//     const datas = await app.applicantRegister(
//       `${email}@gmail.com`,
//       firstname,
//       lastname,
//       mobile,
//       livestock,
//       travel,
//       resume_url,
//       positionId,
//       middlename
//     );

//     let id = [];
//     const rdata = await app.selectAllReceivedUsers(token);
//     for (let i = 0; i < rdata.view.length; i++) {
//       const e = rdata.view[i];
//       id.push(e.id);
//     }
//     // select random id from array
//     const randomId = id[Math.floor(Math.random() * id.length)];
//     // const time = "12:30"
//     // const location = "KCC Marbel"
//     // const contactPerson ="asdas"
//     // const date = "2019/07/05";
//     // const contactNumber = "09123456789";
//     // change status
//     const applicant = await app.setStatusToOnlineExam(token, randomId);
  
//     const rowCount = applicant.patched.count;

//     expect(rowCount).not.toBeUndefined();
//   });
  
//   test("setStatusToBlacklist() - change status of selected applicant to blacklist.", async () => {
//     const emails = admin_email;
//     const passwords = admin_password;
//     const tokenData = await app.loginUser(emails, passwords);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllPositions(token);
//     let positionId = "";

//     // get one position id
//     for (let i = 0; i < 1; i++) {
//       const e = data[i];
//       positionId = e.id;
//     }

//     const email = randomstring.generate(7);
//     const firstname = randomstring.generate(7);
//     const lastname = randomstring.generate(7);
//     const mobile = randomstring.generate(7);
//     const livestock = randomstring.generate(7);
//     const travel = "yes"; // yes or no only
//     const resume_url = randomstring.generate(7);
//     const middlename = randomstring.generate(7);
//     const religion = randomstring.generate(7);
//     const remarks  = randomstring.generate(7) 
     

//     const datas = await app.applicantRegister(
//       token,
//       `${email}@gmail.com`,
//       firstname,
//       lastname,
//       mobile,
//       livestock,
//       travel,
//       resume_url,
//       positionId,
//       middlename,
//       religion,
      
//     );
  

//     let id = [];
//     const rdata = await app.selectAllReceivedUsers(token);
 
//     for (let i = 0; i < rdata.view.length; i++) {
//       const e = rdata.view[i];
//       id.push(e.id);
//     }
  
//     // select random id from array
//     const randomId = id[Math.floor(Math.random() * id.length)];
//     // change status
//     const applicant = await app.setStatusToBlacklist(token, randomId,remarks);
//     console.log(applicant)
//     const rowCount = applicant.patched;
//     expect(rowCount).not.toBeUndefined();
//   });
//   //
//   // test("setStatusToEndorsement() - change status of selected applicant to endorsement.", async () => {
//   //   const emails = admin_email;
//   //   const passwords = admin_password;
//   //   const tokenData = await app.loginUser(emails, passwords);
//   //   const token = tokenData.patched.token;

//   //   const data = await app.selectAllPositions(token);
//   //   let positionId = "";

//   //   // get one position id
//   //   for (let i = 0; i < 1; i++) {
//   //     const e = data[i];
//   //     positionId = e.id;
//   //   }

//   //   const email = randomstring.generate(7);
//   //   const firstname = randomstring.generate(7);
//   //   const lastname = randomstring.generate(7);
//   //   const mobile = randomstring.generate(7);
//   //   const livestock = randomstring.generate(7);
//   //   const travel = "yes"; // yes or no only
//   //   const resume_url = randomstring.generate(7);
//   //   const middlename = randomstring.generate(7);

//   //   const datas = await app.applicantRegister(
//   //     `${email}@gmail.com`,
//   //     firstname,
//   //     lastname,
//   //     mobile,
//   //     livestock,
//   //     travel,
//   //     resume_url,
//   //     positionId,
//   //     middlename
//   //   );

//   //   let id = [];
//   //   const rdata = await app.selectAllReceivedUsers(token);
//   //   for (let i = 0; i < rdata.view.length; i++) {
//   //     const e = rdata.view[i];
//   //     id.push(e.id);
//   //   }
//   //   // select random id from array
//   //   const randomId = id[Math.floor(Math.random() * id.length)];
//   //   // change status
//   //   const applicant = await app.setStatusToEndorsement(token, randomId);
//   //   console.log(applicant)
//   //   const rowCount = applicant.patched.rowCount;
//   //   expect(rowCount).not.toBeUndefined();
//   // });
//   //
// //   test("setStatusToReject() - change status of selected applicant to reject.", async () => {
// //     const emails = admin_email;
// //     const passwords = admin_password;
// //     const tokenData = await app.loginUser(emails, passwords);
// //     const token = tokenData.patched.token;

// //     const data = await app.selectAllPositions(token);
// //     let positionId = "";

// //     // get one position id
// //     for (let i = 0; i < 1; i++) {
// //       const e = data[i];
// //       positionId = e.id;
// //     }

// //     const email = randomstring.generate(7);
// //     const firstname = randomstring.generate(7);
// //     const lastname = randomstring.generate(7);
// //     const mobile = randomstring.generate(7);
// //     const livestock = randomstring.generate(7);
// //     const travel = "yes"; // yes or no only
// //     const resume_url = randomstring.generate(7);
// //     const middlename = randomstring.generate(7);

// //     const datas = await app.applicantRegister(
// //       `${email}@gmail.com`,
// //       firstname,
// //       lastname,
// //       mobile,
// //       livestock,
// //       travel,
// //       resume_url,
// //       positionId,
// //       middlename
// //     );

// //     let id = [];
// //     const rdata = await app.selectAllReceivedUsers(token);
// //     for (let i = 0; i < rdata.view.length; i++) {
// //       const e = rdata.view[i];
// //       id.push(e.id);
// //     }
// //     // select random id from array
// //     const randomId = id[Math.floor(Math.random() * id.length)];
// //     // change status
// //     const applicant = await app.setStatusToReject(token, randomId);
// //     const rowCount = applicant.patched.rowCount;
// //     expect(rowCount).not.toBeUndefined();
// //   });
// //   //
//   // test("setStatusToAssessment() - change status of selected applicant to assessment.", async () => {
//   //   const emails = admin_email;
//   //   const passwords = admin_password;
//   //   const tokenData = await app.loginUser(emails, passwords);
//   //   const token = tokenData.patched.token;

//   //   const data = await app.selectAllPositions(token);
//   //   let positionId = "";

//   //   // get one position id
//   //   for (let i = 0; i < 1; i++) {
//   //     const e = data[i];
//   //     positionId = e.id;
//   //   }

//   //   const email = randomstring.generate(7);
//   //   const firstname = randomstring.generate(7);
//   //   const lastname = randomstring.generate(7);
//   //   const mobile = randomstring.generate(7);
//   //   const livestock = randomstring.generate(7);
//   //   const travel = "yes"; // yes or no only
//   //   const resume_url = randomstring.generate(7);
//   //   const middlename = randomstring.generate(7);

//   //   const datas = await app.applicantRegister(
//   //     `${email}@gmail.com`,
//   //     firstname,
//   //     lastname,
//   //     mobile,
//   //     livestock,
//   //     travel,
//   //     resume_url,
//   //     positionId,
//   //     middlename
//   //   );

//   //   let id = [];
//   //   const rdata = await app.selectAllReceivedUsers(token);
//   //   for (let i = 0; i < rdata.view.length; i++) {
//   //     const e = rdata.view[i];
//   //     id.push(e.id);
//   //   }
//   //   // select random id from array
//   //   const randomId = id[Math.floor(Math.random() * id.length)];
//   //   // change status
//   //   const applicant = await app.setStatusToAssessment(token, randomId);
//   //   const rowCount = applicant.patched.rowCount;
//   //   expect(rowCount).not.toBeUndefined();
//   // });
//   // //
//   // test("setStatusToPending() - change status of selected applicant to pending.", async () => {
//   //   const emails = admin_email;
//   //   const passwords = admin_password;
//   //   const tokenData = await app.loginUser(emails, passwords);
//   //   const token = tokenData.patched.token;

//   //   const data = await app.selectAllPositions(token);
//   //   let positionId = "";

//   //   // get one position id
//   //   for (let i = 0; i < 1; i++) {
//   //     const e = data[i];
//   //     positionId = e.id;
    

//   //   }


//   //   const email = randomstring.generate(7);
//   //   const firstname = randomstring.generate(7);
//   //   const lastname = randomstring.generate(7);
//   //   const mobile = randomstring.generate(7);
//   //   const livestock = randomstring.generate(7);
//   //   const travel = "yes"; // yes or no only
//   //   const resume_url = randomstring.generate(7);
//   //   const middlename = randomstring.generate(7);

//   //   const datas = await app.applicantRegister(
//   //     `${email}@gmail.com`,
//   //     firstname,
//   //     lastname,
//   //     mobile,
//   //     livestock,
//   //     travel,
//   //     resume_url,
//   //     positionId,
//   //     middlename
//   //   );

//   //   let id = [];
//   //   const rdata = await app.selectAllReceivedUsers(token);
//   //   for (let i = 0; i < rdata.view.length; i++) {
//   //     const e = rdata.view[i];
//   //     id.push(e.id);
//   //   }
//   //   // select random id from array
//   //   const randomId = id[Math.floor(Math.random() * id.length)];
  
//   //   // change status
//   //   const applicant = await app.setStatusToPending(token, randomId);
//   //   const rowCount = applicant.patched.rowCount;
//   //   expect(rowCount).not.toBeUndefined();
//   // });
//   // //
//   // test("setStatusToReference() - change status of selected applicant to kept for reference.", async () => {
//   //   const emails = admin_email;
//   //   const passwords = admin_password;
//   //   const tokenData = await app.loginUser(emails, passwords);
//   //   const token = tokenData.patched.token;

//   //   const data = await app.selectAllPositions(token);
//   //   let positionId = "";

//   //   // get one position id
//   //   for (let i = 0; i < 1; i++) {
//   //     const e = data[i];
//   //     positionId = e.id;
//   //   }

//   //   const email = randomstring.generate(7);
//   //   const firstname = randomstring.generate(7);
//   //   const lastname = randomstring.generate(7);
//   //   const mobile = randomstring.generate(7);
//   //   const livestock = randomstring.generate(7);
//   //   const travel = "yes"; // yes or no only
//   //   const resume_url = randomstring.generate(7);
//   //   const middlename = randomstring.generate(7);

//   //   const datas = await app.applicantRegister(
//   //     `${email}@gmail.com`,
//   //     firstname,
//   //     lastname,
//   //     mobile,
//   //     livestock,
//   //     travel,
//   //     resume_url,
//   //     positionId,
//   //     middlename
//   //   );

//   //   let id = [];
//   //   const rdata = await app.selectAllReceivedUsers(token);
//   //   for (let i = 0; i < rdata.view.length; i++) {
//   //     const e = rdata.view[i];
//   //     id.push(e.id);
//   //   }
//   //   // select random id from array
//   //   const randomId = id[Math.floor(Math.random() * id.length)];
//   //   // change status
//   //   const applicant = await app.setStatusToReference(token, randomId);
//   //   console.log(applicant)
//   //   const rowCount = applicant.patched.rowCount;
//   //   expect(rowCount).not.toBeUndefined();
//   // });
  
//   // test("setStatusToShortlist() - change status of selected applicant to shortlist.", async () => {
//   //   const emails = admin_email;
//   //   const passwords = admin_password;
//   //   const tokenData = await app.loginUser(emails, passwords);
//   //   const token = tokenData.patched.token;

//   //   const data = await app.selectAllPositions(token);
//   //   let positionId = "";

//   //   // get one position id
//   //   for (let i = 0; i < 1; i++) {
//   //     const e = data[i];
//   //     positionId = e.id;
//   //   }

//   //   const email = randomstring.generate(7);
//   //   const firstname = randomstring.generate(7);
//   //   const lastname = randomstring.generate(7);
//   //   const mobile = randomstring.generate(7);
//   //   const livestock = randomstring.generate(7);
//   //   const travel = "yes"; // yes or no only
//   //   const resume_url = randomstring.generate(7);
//   //   const middlename = randomstring.generate(7);

//   //   const datas = await app.applicantRegister(
//   //     `${email}@gmail.com`,
//   //     firstname,
//   //     lastname,
//   //     mobile,
//   //     livestock,
//   //     travel,
//   //     resume_url,
//   //     positionId,
//   //     middlename
//   //   );

//   //   let id = [];
//   //   const rdata = await app.selectAllReceivedUsers(token);
//   //   for (let i = 0; i < rdata.view.length; i++) {
//   //     const e = rdata.view[i];
//   //     id.push(e.id);
//   //   }
//   //   // select random id from array
//   //   const randomId = id[Math.floor(Math.random() * id.length)];
//   //   // change status
//   //   const applicant = await app.setStatusToShortlist(token, randomId);
//   //   const rowCount = applicant.patched.rowCount;
//   //   expect(rowCount).not.toBeUndefined();
//   // });
//   //
// //   test("setStatusToDeployed() - change status of selected applicant to deployed.", async () => {
// //     const emails = admin_email;
// //     const passwords = admin_password;
// //     const tokenData = await app.loginUser(emails, passwords);
// //     const token = tokenData.patched.token;

// //     const data = await app.selectAllPositions(token);
  
// //     let positionId = "";

// //     // get one position id
// //     for (let i = 0; i < 1; i++) {
// //       const e = data[i];
// //       positionId = e.id;
// //     }

// //     const email = randomstring.generate(7);
// //     const firstname = randomstring.generate(7);
// //     const lastname = randomstring.generate(7);
// //     const mobile = randomstring.generate(7);
// //     const livestock = randomstring.generate(7);
// //     const travel = "yes"; // yes or no only
// //     const resume_url = randomstring.generate(7);
// //     const middlename = randomstring.generate(7);
// //     const religion = randomstring.generate(7);

// //     const datas = await app.applicantRegister(
// //       token,
// //       `${email}@gmail.com`,
// //       firstname,
// //       lastname,
// //       mobile,
// //       livestock,
// //       travel,
// //       resume_url,
// //       positionId,
// //       middlename,
// //       religion
// //     );
    

// //     let id = [];
// //     const date = "2019-09-13";
// //     const message = "asdasda";
// //     const rdata = await app.selectAllReceivedUsers(token);
// //     for (let i = 0; i < rdata.view.length; i++) {
// //       const e = rdata.view[i];
// //       id.push(e.id);
// //     }
// //     // select random id from array
// //     const randomId = id[Math.floor(Math.random() * id.length)];
// //     // change status
// //     const applicant = await app.setStatusToDeployed(token,randomId,date,message);
// //     console.log(applicant)
// //     const rowCount = applicant.patched;
// //     expect(rowCount).not.toBeUndefined();
// //   });
//   });

// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // QUESTIONS
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // describe("Selecting questions per category", () => {
// //   //
// //   test("selectQuestionPerCategory() - select all questions on a category; with token..", async () => {
// //     const email = admin_email;
// //     const password = admin_password;
// //     const tokenData = await app.loginUser(email, password);
// //     const token = tokenData.patched.token;

// //     const data = await app.selectAllCategories(token);
// //     let categoryId = [];
// //     for (let i = 0; i < data.length; i++) {
// //       const e = data[i];
// //       categoryId.push(e.id);
// //     }
// //     // random category id
// //     const randomId = categoryId[Math.floor(Math.random() * categoryId.length)];
// //     const question = await app.selectQuestionPerCategory(token, randomId);
// //     console.log(question)
// //     const dataCount = question.view.length;
// //     expect(dataCount).not.toBeUndefined();
// //   });
  
//   // test("selectQuestionPerCategory() - select all questions on a category;no category entered; with token..", async () => {
//   //   const email = admin_email;
//   //   const password = admin_password;
//   //   const tokenData = await app.loginUser(email, password);
//   //   const token = tokenData.patched.token;

//   //   const data = await app.selectAllCategories(token);
//   //   let categoryId = [];
//   //   for (let i = 0; i < data.length; i++) {
//   //     const e = data[i];
//   //     categoryId.push(e.id);
//   //   }
//   //   // random category id
//   //   const randomId = categoryId[Math.floor(Math.random() * categoryId.length)];
//   //   const question = await app.selectQuestionPerCategory(token, "");
//   //   expect(question).toBe("Error");
//   // });
//   //
//   test("selectQuestionPerCategory() - select all questions on a category; with wrong token..", async () => {
   
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
 
//     const token = tokenData.patched.token;

//     const data = await app.selectAllCategories(token);
    
//     let categoryId = [];
//     for (let i = 0; i < data.length; i++) {
//       const e = data[i];
//       categoryId.push(e.id);
//     }
//     // random category id
//     const randomId = categoryId[Math.floor(Math.random() * categoryId.length)];
//     const question = await app.selectQuestionPerCategory(token + "s", randomId);
//     expect(question).toBe("Error");
//   });
// // });
// // describe("Applicant Online Exam", () => {
// //   //
// //   test("selectQuestionForOnlineExam() - select all questions on a category for online exam; with token..", async () => {
// //     const {decrypt} = require("../src/entities/users/crypting")
    
// //     let _email = "7aec25f07151256e12a87863f9fd2e27f62865";
// //     let _password="29bb77a421097a1c44";

// //     const email = decrypt(_email);
// //     const password = decrypt(_password);
// //     const data = await app.loginApplicantOnlineExam(email, password);
// //     console.log(data)
// //     const token = data.patched.token;
// //     const category = data.patched.categories;
// //     let categoryId = [];
// //     for (let i = 0; i < category.length; i++) {
// //       const e = category[i];
// //       categoryId.push(e.id);
// //     }
// //     const datas = await app.selectQuestionForOnlineExam(token, categoryId);
// //     expect(datas).not.toBe("Error");
// //   });
  
//   // test("selectQuestionForOnlineExam() - select all questions on a category for online exam; with wrong token..", async () => {
//   //   const email = app_email;
//   //   const password = app_password;
//   //   const data = await app.loginApplicantOnlineExam(email, password);
//   //   const token = data.patched.token;
//   //   const category = data.patched.categories;
//   //   let categoryId = [];
//   //   for (let i = 0; i < category.length; i++) {
//   //     const e = category[i];
//   //     categoryId.push(e.id);
//   //   }
//   //   const datas = await app.selectQuestionForOnlineExam(
//   //     token + "s",
//   //     categoryId
//   //   );
//   //   expect(datas).toBe("Error");
//   // });
//   //
//   // test("selectQuestionForIQBE() - select all questions on a category for IQ and behavioral exam; with token..", async () => {
//   //   const email = app_email;
//   //   const password = app_password;
//   //   const data = await app.loginApplicantIQBExam(email, password);
//   //   const token = data.patched.token;
//   //   const category = data.patched.categories;
//   //   let categoryId = [];
//   //   for (let i = 0; i < category.length; i++) {
//   //     const e = category[i];
//   //     categoryId.push(e);
//   //   }
//   //   const datas = await app.selectQuestionForIQBE(token, categoryId);
//   //   expect(datas).not.toBe("Error");
//   // });
//   //
//   // test("selectQuestionForIQBE() - select all questions on a category for IQ and behavioral exam; with wrong token..", async () => {
//   //   const email = app_email;
//   //   const password = app_password;
//   //   const data = await app.loginApplicantIQBExam(email, password);
//   //   const token = data.patched.token;
//   //   const category = data.patched.categories;
//   //   let categoryId = [];
//   //   for (let i = 0; i < category.length; i++) {
//   //     const e = category[i];
//   //     categoryId.push(e);
//   //   }
//   //   const datas = await app.selectQuestionForIQBE(token + "s", categoryId);
//   //   expect(datas).toBe("Error");
//   // });

//   // test("applicantAnswerOnlineExam() - applicant answer exam..; with token..", async () => {
//   //   const email = app_email;
//   //   const password = app_password;
//   //   const data = await app.loginApplicantOnlineExam(email, password);
//   //   const token = data.patched.token;
//   //   const category = data.patched.categories;
//   //   let categoryId = [];
//   //   for (let i = 0; i < category.length; i++) {
//   //     const e = category[i];
//   //     categoryId.push(e.id);
//   //   }
//   //   // answer per category auto
//   //   for (let i = 0; i < categoryId.length; i++) {
//   //     // start here
//   //     const datas = await app.selectQuestionForOnlineExam(token, categoryId);
//   //     const questions = datas.view.questions;
//   //     let qId = [];
//   //     let choicess = [];
//   //     // get the question ID and choices per category
//   //     for (let i = 0; i < 5; i++) {
//   //       const e = questions[i];
//   //       const questionId = e.questionId;
//   //       const choices = e.choices;
//   //       // push in array; they have same length too.
//   //       choicess.push(choices);
//   //       qId.push(questionId);
//   //     }
//   //     let choicesId = [];
//   //     // make an array of per choice; get the ID
//   //     for (let i = 0; i < choicess.length; i++) {
//   //       const e = choicess[i];
//   //       let tempArray = [];
//   //       for (let x = 0; x < e.length; x++) {
//   //         const y = e[x];
//   //         const id = y.id;
//   //         tempArray.push(id);
//   //       }
//   //       choicesId.push(tempArray);
//   //     }
//   //     let answers = [];
//   //     // select random answer per question
//   //     for (let i = 0; i < choicesId.length; i++) {
//   //       const e = choicesId[i];
//   //       const id = qId[i];
//   //       const randomAnswer = e[Math.floor(Math.random() * e.length)];
//   //       const userAnswer = {
//   //         questionId: id,
//   //         answer: randomAnswer
//   //       };
//   //       answers.push(userAnswer);
//   //     }
//   //     const result = await app.applicantAnswerOnlineExam(
//   //       token,
//   //       categoryId,
//   //       answers
//   //     );
//   //     expect(result).not.toBe("Error");
//   //     // end
//   //   }
//   // });
//   // test("applicantAnswerOnlineExam() - applicant answer exam..; with wrong token..", async () => {
//   //   const email = app_email;
//   //   const password = app_password;
//   //   const data = await app.loginApplicantOnlineExam(email, password);
//   //   const token = data.patched.token;
//   //   const category = data.patched.categories;
//   //   let categoryId = [];
//   //   for (let i = 0; i < category.length; i++) {
//   //     const e = category[i];
//   //     categoryId.push(e.id);
//   //   }
//   //   // answer per category auto
//   //   for (let i = 0; i < categoryId.length; i++) {
//   //     // start here
//   //     const datas = await app.selectQuestionForOnlineExam(token, categoryId);
//   //     const questions = datas.view.questions;
//   //     let qId = [];
//   //     let choicess = [];
//   //     // get the question ID and choices per category
//   //     for (let i = 0; i < 5; i++) {
//   //       const e = questions[i];
//   //       const questionId = e.questionId;
//   //       const choices = e.choices;
//   //       // push in array; they have same length too.
//   //       choicess.push(choices);
//   //       qId.push(questionId);
//   //     }
//   //     let choicesId = [];
//   //     // make an array of per choice; get the ID
//   //     for (let i = 0; i < choicess.length; i++) {
//   //       const e = choicess[i];
//   //       let tempArray = [];
//   //       for (let x = 0; x < e.length; x++) {
//   //         const y = e[x];
//   //         const id = y.id;
//   //         tempArray.push(id);
//   //       }
//   //       choicesId.push(tempArray);
//   //     }
//   //     let answers = [];
//   //     // select random answer per question
//   //     for (let i = 0; i < choicesId.length; i++) {
//   //       const e = choicesId[i];
//   //       const id = qId[i];
//   //       const randomAnswer = e[Math.floor(Math.random() * e.length)];
//   //       const userAnswer = {
//   //         questionId: id,
//   //         answer: randomAnswer
//   //       };
//   //       answers.push(userAnswer);
//   //     }
//   //     const result = await app.applicantAnswerOnlineExam(
//   //       token + "s",
//   //       categoryId,
//   //       answers
//   //     );
//   //     expect(result).toBe("Error");
//   //     // end
//   //   }
//   // });

//   // test("applicantAnswerIQBE() - applicant answer IQ and behavioral exam..; with token..", async () => {
//   //   const email = app_email;
//   //   const password = app_password;
//   //   const data = await app.loginApplicantIQBExam(email, password);
//   //   const token = data.patched.token;
//   //   const category = data.patched.categories;
//   //   let categoryId = [];
//   //   for (let i = 0; i < category.length; i++) {
//   //     const e = category[i];
//   //     await categoryId.push(await e);
//   //   }
//   //   // answer per category auto
//   //   for (let i = 0; i < categoryId.length; i++) {
//   //     const datas = await app.selectQuestionForIQBE(token, categoryId);
//   //     const questions = datas.view.questions;
//   //     let qId = [];
//   //     let choicess = [];
//   //     // just answer 5 questions per category
//   //     // get the question ID and choices per category
//   //     for (let i = 0; i < 5; i++) {
//   //       const e = questions[i];
//   //       const questionId = e.questionId;
//   //       const choices = e.choices;
//   //       // push in array; they have same length too.
//   //       await choicess.push(choices);
//   //       await qId.push(questionId);
//   //     }
//   //     let choicesId = [];
//   //     // make an array of per choice; get the ID
//   //     for (let i = 0; i < choicess.length; i++) {
//   //       const e = choicess[i];
//   //       let tempArray = [];
//   //       for (let x = 0; x < e.length; x++) {
//   //         const y = e[x];
//   //         const id = y.id;
//   //         tempArray.push(id);
//   //       }
//   //       await choicesId.push(tempArray);
//   //     }
//   //     let answers = [];
//   //     // select random answer per question
//   //     for (let i = 0; i < choicesId.length; i++) {
//   //       const e = choicesId[i];
//   //       const id = qId[i];
//   //       const randomAnswer = e[Math.floor(Math.random() * e.length)];
//   //       const userAnswer = {
//   //         questionId: id,
//   //         answer: randomAnswer
//   //       };
//   //       await answers.push(userAnswer);
//   //     }
//   //     const result = await app.applicantAnswerIQBE(token, categoryId, answers);
//   //     expect(result).not.toBe("Error");
//   //   }
//   //   // end for
//   // }, 10000);
//   //
//   // test("applicantAnswerIQBE() - applicant answer IQ and behavioral exam..; with wrong token..", async () => {
//   //   const email = app_email;
//   //   const password = app_password;
//   //   const data = await app.loginApplicantIQBExam(email, password);
//   //   const token = data.patched.token;
//   //   const category = data.patched.categories;
//   //   let categoryId = [];
//   //   for (let i = 0; i < category.length; i++) {
//   //     const e = category[i];
//   //     await categoryId.push(await e);
//   //   }
//   //   // answer per category auto
//   //   for (let i = 0; i < categoryId.length; i++) {
//   //     const datas = await app.selectQuestionForIQBE(token, categoryId);
//   //     const questions = datas.view.questions;
//   //     let qId = [];
//   //     let choicess = [];
//   //     // just answer 5 questions per category
//   //     // get the question ID and choices per category
//   //     for (let i = 0; i < 5; i++) {
//   //       const e = questions[i];
//   //       const questionId = e.questionId;
//   //       const choices = e.choices;
//   //       // push in array; they have same length too.
//   //       await choicess.push(choices);
//   //       await qId.push(questionId);
//   //     }
//   //     let choicesId = [];
//   //     // make an array of per choice; get the ID
//   //     for (let i = 0; i < choicess.length; i++) {
//   //       const e = choicess[i];
//   //       let tempArray = [];
//   //       for (let x = 0; x < e.length; x++) {
//   //         const y = e[x];
//   //         const id = y.id;
//   //         tempArray.push(id);
//   //       }
//   //       await choicesId.push(tempArray);
//   //     }
//   //     let answers = [];
//   //     // select random answer per question
//   //     for (let i = 0; i < choicesId.length; i++) {
//   //       const e = choicesId[i];
//   //       const id = qId[i];
//   //       const randomAnswer = e[Math.floor(Math.random() * e.length)];
//   //       const userAnswer = {
//   //         questionId: id,
//   //         answer: randomAnswer
//   //       };
//   //       await answers.push(userAnswer);
//   //     }
//   //     const result = await app.applicantAnswerIQBE(
//   //       token + "s",
//   //       categoryId,
//   //       answers
//   //     );
//   //     expect(result).toBe("Error");
//   //   }
//   //   // end for
//   // }, 10000);

//   // test("updateQuestion() - update a question with all data required; with token..", async () => {
//   //   const email = admin_email;
//   //   const password = admin_password;
//   //   const tokenData = await app.loginUser(email, password);
//   //   const token = tokenData.patched.token;
//   //   let categoriesId = [];
//   //   const data = await app.selectAllCategories(token);
//   //   for (let i = 0; i < data.length; i++) {
//   //     const e = data[i];
//   //     categoriesId.push(e.id);
//   //   }
//   //   // select random category
//   //   const randomCategoryId =
//   //     categoriesId[Math.floor(Math.random() * categoriesId.length)];
//   //   const question = await app.selectQuestionPerCategory(
//   //     token,
//   //     randomCategoryId
//   //   );
//   //   let questionId = [];
//   //   const questionArray = question.view;
//   //   for (let i = 0; i < questionArray.length; i++) {
//   //     const e = questionArray[i];
//   //     questionId.push(e.id);
//   //   }
//   //   // select random question id
//   //   const randomQuestionId =
//   //     questionId[Math.floor(Math.random() * questionId.length)];
//   //   const questions = "What is my first name?";
//   //   const correct_answer = "Rodney";
//   //   const details = ["Joe", "Billy", "Rodney", "Walter"];
//   //   const result = await app.updateQuestion(
//   //     randomQuestionId,
//   //     token,
//   //     questions,
//   //     correct_answer,
//   //     randomCategoryId,
//   //     details
//   //   );
//   //   expect(result).not.toBe("Error");
//   // });
//   //
//   // test("updateQuestion() - update a question with all data required; with wrong token..", async () => {
//   //   const email = admin_email;
//   //   const password = admin_password;
//   //   const tokenData = await app.loginUser(email, password);
//   //   const token = tokenData.patched.token;
//   //   let categoriesId = [];
//   //   const data = await app.selectAllCategories(token);
//   //   for (let i = 0; i < data.length; i++) {
//   //     const e = data[i];
//   //     categoriesId.push(e.id);
//   //   }
//   //   // select random category
//   //   const randomCategoryId =
//   //     categoriesId[Math.floor(Math.random() * categoriesId.length)];
//   //   const question = await app.selectQuestionPerCategory(
//   //     token,
//   //     randomCategoryId
//   //   );
//   //   let questionId = [];
//   //   const questionArray = question.view;
//   //   for (let i = 0; i < questionArray.length; i++) {
//   //     const e = questionArray[i];
//   //     questionId.push(e.id);
//   //   }
//   //   // select random question id
//   //   const randomQuestionId =
//   //     questionId[Math.floor(Math.random() * questionId.length)];
//   //   const questions = "What is my first name?";
//   //   const correct_answer = "Rodney";
//   //   const details = ["Joe", "Billy", "Rodney", "Walter"];
//   //   const result = await app.updateQuestion(
//   //     randomQuestionId,
//   //     token + "s",
//   //     questions,
//   //     correct_answer,
//   //     randomCategoryId,
//   //     details
//   //   );
//   //   expect(result).toBe("Error");
//   // });
//   //
//   // test("updateQuestion() - update a question with lacking data required; with wrong token..", async () => {
//   //   const email = admin_email;
//   //   const password = admin_password;
//   //   const tokenData = await app.loginUser(email, password);
//   //   const token = tokenData.patched.token;
//   //   let categoriesId = [];
//   //   const data = await app.selectAllCategories(token);
//   //   for (let i = 0; i < data.length; i++) {
//   //     const e = data[i];
//   //     categoriesId.push(e.id);
//   //   }
//   //   // select random category
//   //   const randomCategoryId =
//   //     categoriesId[Math.floor(Math.random() * categoriesId.length)];
//   //   const question = await app.selectQuestionPerCategory(
//   //     token,
//   //     randomCategoryId
//   //   );
//   //   let questionId = [];
//   //   const questionArray = question.view;
//   //   for (let i = 0; i < questionArray.length; i++) {
//   //     const e = questionArray[i];
//   //     questionId.push(e.id);
//   //   }
//   //   // select random question id
//   //   const randomQuestionId =
//   //     questionId[Math.floor(Math.random() * questionId.length)];
//   //   const questions = "What is my first name?";
//   //   const correct_answer = "Rodney";
//   //   const details = ["Joe", "Billy", "Rodney", "Walter"];
//   //   const result = await app.updateQuestion(
//   //     randomQuestionId,
//   //     token + "s",
//   //     "",
//   //     correct_answer,
//   //     randomCategoryId,
//   //     details
//   //   );
//   //   expect(result).toBe("Error");
//   // });

//   // test("addQuestion() - add a question with all data required; with token..", async () => {
//   //   const email = admin_email;
//   //   const password = admin_password;
//   //   const tokenData = await app.loginUser(email, password);
//   //   const token = tokenData.patched.token;
//   //   let categoriesId = [];
//   //   const data = await app.selectAllCategories(token);
//   //   for (let i = 0; i < data.length; i++) {
//   //     const e = data[i];
//   //     categoriesId.push(e.id);
//   //   }
//   //   // select random category
//   //   const randomCategoryId =
//   //     categoriesId[Math.floor(Math.random() * categoriesId.length)];
//   //   const questions = "What is the name of my pet?";
//   //   const correct_answer = "pogi";
//   //   const details = ["blackie", "pogi", "budoy", "brownie"];
//   //   const result = await app.addQuestion(
//   //     token,
//   //     questions,
//   //     correct_answer,
//   //     randomCategoryId,
//   //     details
//   //   );
//   //   expect(result).not.toBe("Error");
//   // });
//   //
//   test("addQuestion() - add a question with all data required; with wrong token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;
//     let categoriesId = [];
//     const data = await app.selectAllCategories(token);
//     for (let i = 0; i < data.length; i++) {
//       const e = data[i];
//       categoriesId.push(e.id);
//     }
//     // select random category
//     const randomCategoryId =
//       categoriesId[Math.floor(Math.random() * categoriesId.length)];
//     const questions = "What is the name of my pet?";
//     const correct_answer = "pogi";
//     const details = ["blackie", "pogi", "budoy", "brownie"];
//     const result = await app.addQuestion(
//       token + "s",
//       questions,
//       correct_answer,
//       randomCategoryId,
//       details
//     );
//     expect(result).toBe("Error");
//   });
//   //
//   test("addQuestion() - add a question with lacking data required; with wrong token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;
//     let categoriesId = [];
//     const data = await app.selectAllCategories(token);
//     for (let i = 0; i < data.length; i++) {
//       const e = data[i];
//       categoriesId.push(e.id);
//     }
//     // select random category
//     const randomCategoryId =
//       categoriesId[Math.floor(Math.random() * categoriesId.length)];
//     const questions = "What is the name of my pet?";
//     const correct_answer = "pogi";
//     const details = ["blackie", "pogi", "budoy", "brownie"];
//     const result = await app.addQuestion(
//       token + "s",
//       "",
//       correct_answer,
//       randomCategoryId,
//       details
//     );
//     expect(result).toBe("Error");
//   });

//   // test("addQuestionImport() - add a question through import; with token..", async () => {
//   //   const email = admin_email;
//   //   const password = admin_password;
//   //   const tokenData = await app.loginUser(email, password);
//   //   const token = tokenData.patched.token;
//   //   let categoriesId = [];
//   //   const data = await app.selectAllCategories(token);
//   //   for (let i = 0; i < data.length; i++) {
//   //     const e = data[i];
//   //     categoriesId.push(e.id);
//   //   }
//   //   // select random category
//   //   const randomCategoryId =
//   //     categoriesId[Math.floor(Math.random() * categoriesId.length)];
//   //   const path = "questions/math_test.xlsx";
//   //   const result = await app.addQuestionImport(token, path, randomCategoryId);
//   //   expect(result).not.toBe("Error");
//   // }, 20000);
//   //
//   test("addQuestionImport() - add a question through import; with wrong token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;
//     let categoriesId = [];
//     const data = await app.selectAllCategories(token);
//     for (let i = 0; i < data.length; i++) {
//       const e = data[i];
//       categoriesId.push(e.id);
//     }
//     // select random category
//     const randomCategoryId =
//       categoriesId[Math.floor(Math.random() * categoriesId.length)];
//     const path = "questions/IQ_test.xlsx";
//     const result = await app.addQuestionImport(
//       token + "s",
//       path,
//       randomCategoryId
//     );
//     expect(result).toBe("Error");
//   });
// // });
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // DONE QUESTIONS
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // CATEGORIES
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// describe("Selecting all categories", () => {
//   //
//   test("selectAllCategories() - select all categories; with token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllCategories(token);
//     const dataCount = data.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllCategories() - select all categories; with wrong token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllCategories(token + "s");
//     expect(data).toBe("Error");
//   });
// });
// describe("Adding new categories", () => {
//   //
//   test("addNewCategory() - add new category; with token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllTeam(token);
//     let teamId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       teamId.push(e.id);
//     }

//     // random team
//     const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];

//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const teams_id = randomTeamId;
//     const time_limit_on_seconds = "25";

//     const results = await app.addNewCategory(
//       token,
//       name,
//       description,
//       teams_id,
//       time_limit_on_seconds
//     );
//     expect(results).not.toBe("Error");
//   });
//   //
//   test("addNewCategory() - add new category; with wrong token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllTeam(token);
//     let teamId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       teamId.push(e.id);
//     }

//     // random team
//     const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];

//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const teams_id = randomTeamId;
//     const time_limit_on_seconds = "25";

//     const results = await app.addNewCategory(
//       token + "s",
//       name,
//       description,
//       teams_id,
//       time_limit_on_seconds
//     );
//     expect(results).toBe("Error");
//   });
//   //
//   test("addNewCategory() - add new category; with token but incomplete data..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllTeam(token);
//     let teamId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       teamId.push(e.id);
//     }

//     // random team
//     const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];

//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const teams_id = randomTeamId;
//     const time_limit_on_seconds = "25";

//     const results = await app.addNewCategory(
//       token,
//       "",
//       description,
//       teams_id,
//       time_limit_on_seconds
//     );
//     expect(results).toBe("Error");
//   });
//   //
//   test("addNewCategory() - add new category; with wrong token and incomplete data..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const datas = await app.selectAllTeam(token);
//     let teamId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       teamId.push(e.id);
//     }

//     // random team
//     const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];

//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const teams_id = randomTeamId;
//     const time_limit_on_seconds = "25";

//     const results = await app.addNewCategory(
//       token + "s",
//       "",
//       description,
//       teams_id,
//       time_limit_on_seconds
//     );
//     expect(results).toBe("Error");
//   });
// });
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // DONE CATEGORIES
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // DEPARTMENTS
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// describe("Selecting all departments", () => {
//   //
//   test("selectAllDepartment() - select all departments; with token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllDepartment(token);
//     const dataCount = data.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllDepartment() - select all departments; with wrong token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllDepartment(token + "s");
//     expect(data).toBe("Error");
//   });
// });
// describe("Adding new departments", () => {
//   //
//   test("addNewDepartment() - add new departments; with token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const user = await app.selectAllUsers(token);
//     const userArray = user.view;
//     let userId = [];
//     for (let i = 0; i < userArray.length; i++) {
//       const e = userArray[i];
//       userId.push(e.id);
//     }
//     // random user_id
//     const randomId = userId[Math.floor(Math.random() * userId.length)];
//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);

//     const results = await app.addNewDepartment(
//       token,
//       name,
//       description,
//       randomId
//     );
//     expect(results).not.toBe("Error");
//   });
//   //
//   test("addNewDepartment() - add new departments; with wrong token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const user = await app.selectAllUsers(token);
//     const userArray = user.view;
//     let userId = [];
//     for (let i = 0; i < userArray.length; i++) {
//       const e = userArray[i];
//       userId.push(e.id);
//     }
//     // random user_id
//     const randomId = userId[Math.floor(Math.random() * userId.length)];
//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);

//     const results = await app.addNewDepartment(
//       token + "s",
//       name,
//       description,
//       randomId
//     );
//     expect(results).toBe("Error");
//   });
//   //
//   test("addNewDepartment() - add new departments; with token. incomplete data..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const user = await app.selectAllUsers(token);
//     const userArray = user.view;
//     let userId = [];
//     for (let i = 0; i < userArray.length; i++) {
//       const e = userArray[i];
//       userId.push(e.id);
//     }
//     // random user_id
//     const randomId = userId[Math.floor(Math.random() * userId.length)];
//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);

//     const results = await app.addNewDepartment(
//       token,
//       "",
//       description,
//       randomId
//     );
//     expect(results).toBe("Error");
//   });
//   //
//   test("addNewDepartment() - add new departments; with wrong token. incomplete data..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const user = await app.selectAllUsers(token);
//     const userArray = user.view;
//     let userId = [];
//     for (let i = 0; i < userArray.length; i++) {
//       const e = userArray[i];
//       userId.push(e.id);
//     }
//     // random user_id
//     const randomId = userId[Math.floor(Math.random() * userId.length)];
//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);

//     const results = await app.addNewDepartment(
//       token + "s",
//       "",
//       description,
//       randomId
//     );
//     expect(results).toBe("Error");
//   });
// });
// describe("Update existing departments", () => {
//   //
//   // test("updateDepartment() - update departments; with token..", async () => {
//   //   const email = admin_email;
//   //   const password = admin_password;
//   //   const tokenData = await app.loginUser(email, password);
//   //   const token = tokenData.patched.token;

//   //   const user = await app.selectAllUsers(token);
//   //   const userArray = user.view;
//   //   let userId = [];
//   //   for (let i = 0; i < userArray.length; i++) {
//   //     const e = userArray[i];
//   //     userId.push(e.id);
//   //   }
//   //   const data = await app.selectAllDepartment(token);
//   //   let deptId = [];
//   //   for (let i = 0; i < data.length; i++) {
//   //     const e = data[i];
//   //     deptId.push(e.id);
//   //   }
//   //   // random user_id
//   //   const randomId = userId[Math.floor(Math.random() * userId.length)];
//   //   // random dept
//   //   const randomDeptId = deptId[Math.floor(Math.random() * deptId.length)];

//   //   const name = randomstring.generate(10);
//   //   const description = randomstring.generate(10);
//   //   const status = "active";
//   //   const results = await app.updateDepartment(
//   //     randomDeptId,
//   //     token,
//   //     name,
//   //     description,
//   //     randomId,
//   //     status
//   //   );
//   //   const rowCount = results.patched.rowCount;
//   //   expect(rowCount).not.toBeUndefined();
//   // });
//   //
//   test("updateDepartment() - update departments; with wrong token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const user = await app.selectAllUsers(token);
//     const userArray = user.view;
//     let userId = [];
//     for (let i = 0; i < userArray.length; i++) {
//       const e = userArray[i];
//       userId.push(e.id);
//     }
//     const data = await app.selectAllDepartment(token);
//     let deptId = [];
//     for (let i = 0; i < data.length; i++) {
//       const e = data[i];
//       deptId.push(e.id);
//     }
//     // random user_id
//     const randomId = userId[Math.floor(Math.random() * userId.length)];
//     // random dept
//     const randomDeptId = deptId[Math.floor(Math.random() * deptId.length)];

//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const status = "active";

//     const results = await app.updateDepartment(
//       randomDeptId,
//       token + "s",
//       name,
//       description,
//       randomId,
//       status
//     );
//     expect(results).toBe("Error");
//   });
//   //
//   test("updateDepartment() - update departments; with token, incomplete data..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const user = await app.selectAllUsers(token);
//     const userArray = user.view;
//     let userId = [];
//     for (let i = 0; i < userArray.length; i++) {
//       const e = userArray[i];
//       userId.push(e.id);
//     }
//     const data = await app.selectAllDepartment(token);
//     let deptId = [];
//     for (let i = 0; i < data.length; i++) {
//       const e = data[i];
//       deptId.push(e.id);
//     }
//     // random user_id
//     const randomId = userId[Math.floor(Math.random() * userId.length)];
//     // random dept
//     const randomDeptId = deptId[Math.floor(Math.random() * deptId.length)];

//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const status = "active";

//     const results = await app.updateDepartment(
//       randomDeptId,
//       token,
//       "",
//       description,
//       randomId,
//       status
//     );
//     expect(results).toBe("Error");
//   });
//   //
//   test("updateDepartment() - update departments; with wrong token and incomplete data..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const user = await app.selectAllUsers(token);
//     const userArray = user.view;
//     let userId = [];
//     for (let i = 0; i < userArray.length; i++) {
//       const e = userArray[i];
//       userId.push(e.id);
//     }
//     const data = await app.selectAllDepartment(token);
//     let deptId = [];
//     for (let i = 0; i < data.length; i++) {
//       const e = data[i];
//       deptId.push(e.id);
//     }
//     // random user_id
//     const randomId = userId[Math.floor(Math.random() * userId.length)];
//     // random dept
//     const randomDeptId = deptId[Math.floor(Math.random() * deptId.length)];

//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const status = "active";

//     const results = await app.updateDepartment(
//       randomDeptId,
//       token + "s",
//       "",
//       description,
//       randomId,
//       status
//     );
//     expect(results).toBe("Error");
//   });
// });
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // DONE DEPARTMENTS
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // TEAMS
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// describe("Selecting all teams", () => {
//   //
//   test("selectAllTeam() - select all teams; with token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllTeam(token);
//     const dataCount = data.length;
//     expect(dataCount).not.toBeUndefined;
//   });
//   //
//   test("selectAllTeam() - select all teams; with wrong token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllTeam(token + "s");
//     expect(data).toBe("Error");
//   });
// });
// describe("Adding new teams", () => {
//   //
//   test("addNewTeam() - add new team; with token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllDepartment(token);
//     const deptArray = data;
//     let deptId = [];
//     for (let i = 0; i < deptArray.length; i++) {
//       const e = deptArray[i];
//       deptId.push(e.id);
//     }
//     // random user_id

//     const randomId = deptId[Math.floor(Math.random() * deptId.length)];
//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const no_of_items = "30";

//     const results = await app.addNewTeam(
//       token,
//       name,
//       description,
//       randomId,
//       no_of_items
//     );
//     expect(results).not.toBe("Error");
//   });
//   //
//   test("addNewTeam() - add new team; with wrong token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllDepartment(token);
//     const deptArray = data;
//     let deptId = [];
//     for (let i = 0; i < deptArray.length; i++) {
//       const e = deptArray[i];
//       deptId.push(e.id);
//     }
//     // random user_id

//     const randomId = deptId[Math.floor(Math.random() * deptId.length)];
//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const no_of_items = "30";

//     const results = await app.addNewTeam(
//       token + "s",
//       name,
//       description,
//       randomId,
//       no_of_items
//     );
//     expect(results).toBe("Error");
//   });
//   //
//   test("addNewTeam() - add new team; with token but incomplete data..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllDepartment(token);
//     const deptArray = data;
//     let deptId = [];
//     for (let i = 0; i < deptArray.length; i++) {
//       const e = deptArray[i];
//       deptId.push(e.id);
//     }
//     // random user_id

//     const randomId = deptId[Math.floor(Math.random() * deptId.length)];
//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const no_of_items = "30";

//     const results = await app.addNewTeam(
//       token,
//       "",
//       description,
//       randomId,
//       no_of_items
//     );
//     expect(results).toBe("Error");
//   });
//   //
//   test("addNewTeam() - add new team; with wrong token and incomplete data..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllDepartment(token);
//     const deptArray = data;
//     let deptId = [];
//     for (let i = 0; i < deptArray.length; i++) {
//       const e = deptArray[i];
//       deptId.push(e.id);
//     }
//     // random user_id

//     const randomId = deptId[Math.floor(Math.random() * deptId.length)];
//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const no_of_items = "30";

//     const results = await app.addNewTeam(
//       token + "s",
//       "",
//       description,
//       randomId,
//       no_of_items
//     );
//     expect(results).toBe("Error");
//   });
// });
// describe("Update existing team", () => {
//   //
//   // test("updateTeam() - update team; with token..", async () => {
//   //   const email = admin_email;
//   //   const password = admin_password;
//   //   const tokenData = await app.loginUser(email, password);
//   //   const token = tokenData.patched.token;

//   //   const data = await app.selectAllDepartment(token);
//   //   let deptId = [];
//   //   for (let i = 0; i < data.length; i++) {
//   //     const e = data[i];
//   //     deptId.push(e.id);
//   //   }

//   //   const datas = await app.selectAllTeam(token);
//   //   let teamId = [];
//   //   for (let i = 0; i < datas.length; i++) {
//   //     const e = datas[i];
//   //     teamId.push(e.id);
//   //   }

//   //   // random dept
//   //   const randomDeptId = deptId[Math.floor(Math.random() * deptId.length)];

//   //   // random team
//   //   const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];

//   //   const name = randomstring.generate(10);
//   //   const description = randomstring.generate(10);
//   //   const no_of_items = "123";
//   //   const status = "active";

//   //   const results = await app.updateTeam(
//   //     randomTeamId,
//   //     token,
//   //     name,
//   //     description,
//   //     randomDeptId,
//   //     no_of_items,
//   //     status
//   //   );
//   //   const rowCount = results.patched.rowCount;
//   //   expect(rowCount).not.toBeUndefined();
//   // });
//   //
//   test("updateTeam() - update team; with wrong token..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllDepartment(token);
//     let deptId = [];
//     for (let i = 0; i < data.length; i++) {
//       const e = data[i];
//       deptId.push(e.id);
//     }

//     const datas = await app.selectAllTeam(token);
//     let teamId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       teamId.push(e.id);
//     }

//     // random dept
//     const randomDeptId = deptId[Math.floor(Math.random() * deptId.length)];

//     // random team
//     const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];

//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const no_of_items = "123";
//     const status = "active";

//     const results = await app.updateTeam(
//       randomTeamId,
//       token + "s",
//       name,
//       description,
//       randomDeptId,
//       no_of_items,
//       status
//     );
//     expect(results).toBe("Error");
//   });
//   //
//   test("updateTeam() - update team; with token but incomplete data..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllDepartment(token);
//     let deptId = [];
//     for (let i = 0; i < data.length; i++) {
//       const e = data[i];
//       deptId.push(e.id);
//     }

//     const datas = await app.selectAllTeam(token);
//     let teamId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       teamId.push(e.id);
//     }

//     // random dept
//     const randomDeptId = deptId[Math.floor(Math.random() * deptId.length)];

//     // random team
//     const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];

//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const no_of_items = "123";
//     const status = "active";

//     const results = await app.updateTeam(
//       randomTeamId,
//       token,
//       "",
//       description,
//       randomDeptId,
//       no_of_items,
//       status
//     );
//     expect(results).toBe("Error");
//   });
//   //
//   test("updateTeam() - update team; with wrong token and incomplete data..", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllDepartment(token);
//     let deptId = [];
//     for (let i = 0; i < data.length; i++) {
//       const e = data[i];
//       deptId.push(e.id);
//     }

//     const datas = await app.selectAllTeam(token);
//     let teamId = [];
//     for (let i = 0; i < datas.length; i++) {
//       const e = datas[i];
//       teamId.push(e.id);
//     }

//     // random dept
//     const randomDeptId = deptId[Math.floor(Math.random() * deptId.length)];

//     // random team
//     const randomTeamId = teamId[Math.floor(Math.random() * teamId.length)];

//     const name = randomstring.generate(10);
//     const description = randomstring.generate(10);
//     const no_of_items = "123";
//     const status = "active";

//     const results = await app.updateTeam(
//       randomTeamId,
//       token + "s",
//       "",
//       description,
//       randomDeptId,
//       no_of_items,
//       status
//     );
//     expect(results).toBe("Error");
//   });
// });
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // DONE TEAMS
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// // INTERVIEW ASSESSMENT
// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// describe("Interview assessment", () => {
//   //
//   test("addInterviewAssessment() - add applicant interview assessment; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password; 
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const user = await app.selectAllUsers(token);
//     const userArray = user.view;
//     let userId = [];
//     for (let i = 0; i < userArray.length; i++) {
//       const e = userArray[i];
//       userId.push(e.id);
//     }
//     // random user_id
//     const randomIdApplicant = userId[Math.floor(Math.random() * userId.length)];
//     const randomIdInterviewer =
//       userId[Math.floor(Math.random() * userId.length)];

//     const user_id_applicant = randomIdApplicant;
//     const date_interview = "2019-06-24";
//     const communication_skills = "yes";
//     const communication_skills_note = "sometimes unclear words";
//     const confidence = "yes";
//     const confidence_note = "sometimes looking down on the floor";
//     const physical_appearance = "yes";
//     const physical_appearance_note = "N/A";
//     const knowledge_skills = "no";
//     const knowledge_skills_note = "talking about nonsense";
//     const asking_rate = "often";
//     const availability = "always";
//     const others = "good hire";
//     const gen_remarks_recommendations = "highly recommended";
//     const user_id_interviewer = randomIdInterviewer;

//     const result = await app.addInterviewAssessment(
//       token,
//       user_id_applicant,
//       date_interview,
//       communication_skills,
//       communication_skills_note,
//       confidence,
//       confidence_note,
//       physical_appearance,
//       physical_appearance_note,
//       knowledge_skills,
//       knowledge_skills_note,
//       asking_rate,
//       availability,
//       others,
//       gen_remarks_recommendations,
//       user_id_interviewer
//     );
//     expect(result).not.toBe("Error");
//   });
//   //
//   test("addInterviewAssessment() - add applicant interview assessment; with wrong token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const user = await app.selectAllUsers(token);
//     const userArray = user.view;
//     let userId = [];
//     for (let i = 0; i < userArray.length; i++) {
//       const e = userArray[i];
//       userId.push(e.id);
//     }
//     // random user_id
//     const randomIdApplicant = userId[Math.floor(Math.random() * userId.length)];
//     const randomIdInterviewer =
//       userId[Math.floor(Math.random() * userId.length)];

//     const user_id_applicant = randomIdApplicant;
//     const date_interview = "2019-06-24";
//     const communication_skills = "yes";
//     const communication_skills_note = "sometimes unclear words";
//     const confidence = "yes";
//     const confidence_note = "sometimes looking down on the floor";
//     const physical_appearance = "yes";
//     const physical_appearance_note = "N/A";
//     const knowledge_skills = "no";
//     const knowledge_skills_note = "talking about nonsense";
//     const asking_rate = "often";
//     const availability = "always";
//     const others = "good hire";
//     const gen_remarks_recommendations = "highly recommended";
//     const user_id_interviewer = randomIdInterviewer;

//     const result = await app.addInterviewAssessment(
//       token + "s",
//       user_id_applicant,
//       date_interview,
//       communication_skills,
//       communication_skills_note,
//       confidence,
//       confidence_note,
//       physical_appearance,
//       physical_appearance_note,
//       knowledge_skills,
//       knowledge_skills_note,
//       asking_rate,
//       availability,
//       others,
//       gen_remarks_recommendations,
//       user_id_interviewer
//     );
//     expect(result).toBe("Error");
//   });
//   //
//   test("editInterviewAssessment() - edit applicant interview assessment; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllInterviewAssessment(token);
//     const iArray = data.view;
//     let id = [];
//     for (let i = 0; i < iArray.length; i++) {
//       const e = iArray[i];
//       id.push(e.id);
//     }
//     // id to edit
//     const randomId = id[Math.floor(Math.random() * id.length)];

//     const user = await app.selectAllUsers(token);
//     const userArray = user.view;
//     let userId = [];
//     for (let i = 0; i < userArray.length; i++) {
//       const e = userArray[i];
//       userId.push(e.id);
//     }
//     // random user_id
//     const randomIdInterviewer =
//       userId[Math.floor(Math.random() * userId.length)];

//     const date_interview = "2019-06-24";
//     const communication_skills = "no";
//     const communication_skills_note = "sometimes unclear words";
//     const confidence = "no";
//     const confidence_note = "sometimes looking down on the floor";
//     const physical_appearance = "no";
//     const physical_appearance_note = "N/A";
//     const knowledge_skills = "no";
//     const knowledge_skills_note = "talking about nonsense";
//     const asking_rate = "always";
//     const availability = "all the time";
//     const others = "maybe a good hire";
//     const gen_remarks_recommendations = "highly slightly recommended";
//     const user_id_interviewer = randomIdInterviewer;

//     const result = await app.editInterviewAssessment(
//       token,
//       randomId,
//       date_interview,
//       communication_skills,
//       communication_skills_note,
//       confidence,
//       confidence_note,
//       physical_appearance,
//       physical_appearance_note,
//       knowledge_skills,
//       knowledge_skills_note,
//       asking_rate,
//       availability,
//       others,
//       gen_remarks_recommendations,
//       user_id_interviewer
//     );
//     expect(result).not.toBe("Error");
//   });
//   test("editInterviewAssessment() - edit applicant interview assessment; with wrong token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllInterviewAssessment(token);
//     const iArray = data.view;
//     let id = [];
//     for (let i = 0; i < iArray.length; i++) {
//       const e = iArray[i];
//       id.push(e.id);
//     }
//     // id to edit
//     const randomId = id[Math.floor(Math.random() * id.length)];

//     const user = await app.selectAllUsers(token);
//     const userArray = user.view;
//     let userId = [];
//     for (let i = 0; i < userArray.length; i++) {
//       const e = userArray[i];
//       userId.push(e.id);
//     }
//     // random user_id
//     const randomIdInterviewer =
//       userId[Math.floor(Math.random() * userId.length)];

//     const date_interview = "2019-06-24";
//     const communication_skills = "no";
//     const communication_skills_note = "sometimes unclear words";
//     const confidence = "no";
//     const confidence_note = "sometimes looking down on the floor";
//     const physical_appearance = "no";
//     const physical_appearance_note = "N/A";
//     const knowledge_skills = "no";
//     const knowledge_skills_note = "talking about nonsense";
//     const asking_rate = "always";
//     const availability = "all the time";
//     const others = "maybe a good hire";
//     const gen_remarks_recommendations = "highly slightly recommended";
//     const user_id_interviewer = randomIdInterviewer;

//     const result = await app.editInterviewAssessment(
//       token + "s",
//       randomId,
//       date_interview,
//       communication_skills,
//       communication_skills_note,
//       confidence,
//       confidence_note,
//       physical_appearance,
//       physical_appearance_note,
//       knowledge_skills,
//       knowledge_skills_note,
//       asking_rate,
//       availability,
//       others,
//       gen_remarks_recommendations,
//       user_id_interviewer
//     );
//     expect(result).toBe("Error");
//   });
//   //
//   test("selectAllInterviewAssessment() - view all applicant interview assessment; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const result = await app.selectAllInterviewAssessment(token);
//     expect(result).not.toBe("Error");
//   });
//   //
//   test("selectAllInterviewAssessment() - view all applicant interview assessment; with wrong token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const result = await app.selectAllInterviewAssessment(token + "s");
//     expect(result).toBe("Error");
//   });
//   //
//   test("selectInterviewAssessment() - view applicant interview assessment; with token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllInterviewAssessment(token);
//     const iArray = data.view;
//     let id = [];
//     for (let i = 0; i < iArray.length; i++) {
//       const e = iArray[i];
//       id.push(e.id);
//     }
//     const randomId = id[Math.floor(Math.random() * id.length)];
//     const result = await app.selectInterviewAssessment(token, randomId);
//     expect(result).not.toBe("Error");
//   });
//   //
//   test("selectInterviewAssessment() - view applicant interview assessment; with wrong token.", async () => {
//     const email = admin_email;
//     const password = admin_password;
//     const tokenData = await app.loginUser(email, password);
//     const token = tokenData.patched.token;

//     const data = await app.selectAllInterviewAssessment(token);
//     const iArray = data.view;
//     let id = [];
//     for (let i = 0; i < iArray.length; i++) {
//       const e = iArray[i];
//       id.push(e.id);
//     }
//     const randomId = id[Math.floor(Math.random() * id.length)];
//     const result = await app.selectInterviewAssessment(token + "s", randomId);
//     expect(result).toBe("Error");
//    });
  });
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// DONE INTERVIEW ASSESSMENT
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//
//
//