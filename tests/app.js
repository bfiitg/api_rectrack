const axios = require("axios");
require("dotenv").config();

// users
const functions = {
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // USERS
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // LOGIN
  // =-=-=-=-=-=-=-=-=-=-
  loginUser: (email, password) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/login`,
      data: {
        email: email,
        password: password
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  loginApplicantOnlineExam: (email, password) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/applicant/login/online-exam`,
      data: {
        email: email,
        password: password
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  loginApplicantIQBExam: (email, password) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/applicant/login/IQ-behavioral-exam`,
      data: {
        email: email,
        password: password
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),
  // ROLES
  // =-=-=-=-=-=-=-=-=-=-
  selectAllRoles: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/roles/view`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  addNewRole: (token, name, description) =>
    axios({
      method: "POST",
      url: `http://localhost:${process.env.PORT}/roles/add`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: { name, description }
    })
      .then(res => res.data)
      .catch(err => "Error"),
  // POSITIONS
  // =-=-=-=-=-=-=-=-=-=-
  selectAllPositions: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/positions/view`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectAllVacant: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/positions/vacant`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectAllNotVacant: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/positions/not-vacant`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  addNewPosition: (token, name, description, teams_id, is_office_staff) =>
    axios({
      method: "POST",
      url: `http://localhost:${process.env.PORT}/positions/add`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: { name, description, teams_id, is_office_staff }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  positionToNotVacant: (token, id) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/positions/change/not-vacant/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  positionToVacant: (token, id) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/positions/change/vacant/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),
  // ADMIN
  // =-=-=-=-=-=-=-=-=-=-
  adminAddUser: (
    token,
    email,
    password,
    firstname,
    lastname,
    mobile,
    roleId,
    PositionId,
    middlename
  ) =>
    axios({
      method: "POST",
      url: `http://localhost:${process.env.PORT}/users/admin/add`,
      data: {
        email: email,
        password: password,
        firstname: firstname,
        lastname: lastname,
        mobile: mobile,
        role_id: roleId,
        position_id: PositionId,
        middlename: middlename
      },
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),
  // USERS
  // =-=-=-=-=-=-=-=-=-=-
  selectAllReceivedUsers: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/received/`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectAllOnlineExamUsers: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/online-exam/`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectAllEndorsedUsers: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/endorsed/`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err =>"Error"),

  selectAllRejectedUsers: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/rejected/`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectAllBlacklistUsers: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/blacklisted/`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectAllAssessmentUsers: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/assessment/`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectAllPendingUsers: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/pending/`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectAllKeptForReferenceUsers: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/kept-reference/`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectAllShortlistUsers: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/shortlist/`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectAllDeployedUsers: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/deployed/`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectAllJobOrderUsers: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/job-order/`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  applicantRegister: (
    token,
    email,
    firstname,
    lastname,
    mobile,
    livestock,
    travel,
    resume_url,
    positionId,
    middlename,
    religion
  ) =>
    axios({
      method: "POST",
      url: `http://localhost:${process.env.PORT}/users/add`,
      data: {
        email: email,
        firstname: firstname,
        lastname: lastname,
        mobile: mobile,
        livestock: livestock,
        travel: travel,
        resume_url: resume_url,
        position_id: positionId,
        middlename: middlename,
        religion: religion
      },
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  applicantFillUpApplicationForm: (
    token,
    id,
    // /*start here*/ firstname,
    // middlename,
    // lastname,
    salary_desired,
    // city_address,
    // provincial_address,
    UnitRoomNumberFloor,
    BuildingName,
    LotBlockPhaseHouseNumber,
    StreetName,
    VillageSubdivision,
    Barangay,
    TownDistrict,
    Municipality,
    CityProvince,
    AddressType,

    place_birth,
    bday,
    age,
    height,
    weight,
    gender,
    is_single_parent,
    civil_status,
    citizenship,
    religion,
    who_referred,
    when_to_start,
    person_to_notify,
    person_relationship,
    person_address,
    person_telephone,
    // position_id_first_choice,
    position_id_second_choice,
    CS_eligible,
    CS_date_taken,
    CS_certificate_image_path,
    date_application,
    user_image_path,
    /*arrays*/
    educationalBgArray,
    relativeArray,
    professionalLicensesArray,
    orgMembershipArray,
    seminarsAttendedArray,
    workExperienceArray,
    workWithBFIArray,
    otherInfoArray,
    familyBgArray,
    isMarriedArray,
    referencesArray,
    willingnessArray
  ) =>
    axios({
      method: "POST",
      url: `http://localhost:${process.env.PORT}/users/application-form/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        // firstname: firstname,
        // middlename: middlename,
        // lastname: lastname,
        salary_desired: salary_desired,
        // city_address: city_address,
        // provincial_address: provincial_address,
        UnitRoomNumberFloor:  UnitRoomNumberFloor,
        BuildingName: BuildingName,
        LotBlockPhaseHouseNumber: LotBlockPhaseHouseNumber,
        StreetName: StreetName,
        VillageSubdivision: VillageSubdivision,
        Barangay: Barangay,
        TownDistrict: TownDistrict,
        Municipality: Municipality,
        CityProvince: CityProvince,
        AddressType: AddressType,
        place_birth:place_birth,
        bday: bday,
        age: age,
        height: height,
        weight: weight,
        gender: gender,
        is_single_parent: is_single_parent,
        civil_status: civil_status,
        citizenship: citizenship,
        religion: religion,
        who_referred: who_referred,
        when_to_start: when_to_start,
        person_to_notify: person_to_notify,
        person_relationship: person_relationship,
        person_address: person_address,
        person_telephone: person_telephone,
        // position_id_first_choice: position_id_first_choice,
        position_id_second_choice: position_id_second_choice,
        CS_eligible: CS_eligible,
        CS_date_taken: CS_date_taken,
        CS_certificate_image_path: CS_certificate_image_path,
        date_application: date_application,
        user_image_path: user_image_path,
        educationalBgArray: educationalBgArray,
        relativeArray: relativeArray,
        professionalLicensesArray: professionalLicensesArray,
        orgMembershipArray: orgMembershipArray,
        seminarsAttendedArray: seminarsAttendedArray,
        workExperienceArray: workExperienceArray,
        workWithBFIArray: workWithBFIArray,
        otherInfoArray: otherInfoArray,
        familyBgArray: familyBgArray,
        isMarriedArray: isMarriedArray,
        referencesArray: referencesArray,
        willingnessArray: willingnessArray
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),
  // NOTIFICATIONS
  // =-=-=-=-=-=-=-=-=-=-
  getCountForReceived: () =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/received-notif/count/`
    })
      .then(res => res.data)
      .catch(err => "Error"),

  getCountForEndorsed: () =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/endorsed-notif/count/`
    })
      .then(res => res.data)
      .catch(err => "Error"),
  // USERS
  // =-=-=-=-=-=-=-=-=-=-
  selectAllUsers: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/admin/view`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectUserDetails: (token, id) =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/users/admin/view/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  setDataForAssessment: (id, token, date,link,name,contact,location) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/endorsed/set-date/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      
      data: {
        date: date,
        link: link,
        name: name,
        contact: contact,
        location:location
      }
    })
      .then(res => res.data)
      
      .catch(err => "Error"),

  setStatusToJobOrder: (id,date,time,location,token, contactPerson,contactNumber) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/status/job-order/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }, data:{
        date: date,
        time:time,
        location: location,
        contactPerson: contactPerson,
        contactNumber:contactNumber 
      }
    })
      .then(res => res.data)
      .catch(err => err),

  setStatusToOnlineExam: (token, id) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/status/online-exam/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  setStatusToBlacklist: (token, id,remarks) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/status/blacklist/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data:{
        remarks: remarks
      }
    })
      .then(res => res.data)
      .catch(err => err),

  setStatusToEndorsement: (token, id) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/status/endorsement/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  setStatusToReject: (token, id) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/status/reject/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  setStatusToAssessment: (token, id) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/status/assessment/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  setStatusToPending: (token, id) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/status/pending/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  setStatusToReference: (token, id) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/status/kept-reference/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  setStatusToShortlist: (token, id) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/status/shortlist/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  setStatusToDeployed: (token,id,date,message) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/status/deployed/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data:{
        date: date,//format('LL'),
        message: message
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  updateAdminInfo: (
    token,
    id,
    email,
    password,
    firstname,
    lastname,
    mobile,
    role_id,
    position_id,
    middlename,
    status
  ) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/users/admin/edit/user/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        email: email,
        password: password,
        firstname: firstname,
        lastname: lastname,
        mobile: mobile,
        role_id: role_id,
        position_id: position_id,
        middlename: middlename,
        status: status
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // DONE USERS
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // QUESTIONS
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  selectQuestionPerCategory: (token, id) =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/questions/view/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectQuestionForOnlineExam: (token, categoryId) =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/questions/online-exam`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        categoryId: categoryId
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectQuestionForIQBE: (token, categoryId) =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/questions/IQ-behavioral-exam`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        categoryId: categoryId
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  applicantAnswerOnlineExam: (token, categoryId, answers) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/questions/online-exam`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        categoryId: categoryId,
        token: token,
        answers: answers
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  applicantAnswerIQBE: (token, categoryId, answers) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/questions/IQ-behavioral-exam`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        categoryId: categoryId,
        token: token,
        answers: answers
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  updateQuestion: (
    id,
    token,
    questions,
    correct_answer,
    categories_id,
    details
  ) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/questions/edit/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        questions,
        correct_answer,
        categories_id,
        details
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  addQuestion: (token, questions, correct_answer, categories_id, details) =>
    axios({
      method: "POST",
      url: `http://localhost:${process.env.PORT}/questions/add/`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        questions,
        correct_answer,
        categories_id,
        details
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  addQuestionImport: (token, path, categories_id) =>
    axios({
      method: "POST",
      url: `http://localhost:${process.env.PORT}/questions/add/import/`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        path,
        categories_id
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // DONE QUESTIONS
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // CATEGORIES
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  selectAllCategories: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/categories/view/`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  addNewCategory: (token, name, description, teams_id, time_limit_on_seconds) =>
    axios({
      method: "POST",
      url: `http://localhost:${process.env.PORT}/categories/add/`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        name: name,
        description: description,
        teams_id: teams_id,
        time_limit_on_seconds: time_limit_on_seconds
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // DONE CATEGORIES
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // DEPARTMENTS
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  selectAllDepartment: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/departments/admin/view`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  addNewDepartment: (token, name, description, user_id) =>
    axios({
      method: "POST",
      url: `http://localhost:${process.env.PORT}/departments/admin/add`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        name: name,
        description: description,
        user_id: user_id
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  updateDepartment: (id, token, name, description, user_id, status) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/departments/admin/edit/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        name: name,
        description: description,
        user_id: user_id,
        status: status
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // DONE DEPARTMENTS
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // TEAMS
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  selectAllTeam: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/teams/admin/view`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  addNewTeam: (token, name, description, department_id, no_of_items) =>
    axios({
      method: "POST",
      url: `http://localhost:${process.env.PORT}/teams/admin/add`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        name: name,
        description: description,
        department_id: department_id,
        no_of_items: no_of_items
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  updateTeam: (
    id,
    token,
    name,
    description,
    department_id,
    no_of_items,
    status
  ) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/teams/admin/edit/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        name: name,
        description: description,
        department_id: department_id,
        no_of_items: no_of_items,
        status: status
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // DONE TEAMS
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // INTERVIEW ASSESSMENT
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  selectAllInterviewAssessment: token =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/interview-assessment/user/view/`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  selectInterviewAssessment: (token, id) =>
    axios({
      method: "GET",
      url: `http://localhost:${process.env.PORT}/interview-assessment/user/view/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  addInterviewAssessment: (
    token,
    user_id_applicant,
    date_interview,
    communication_skills,
    communication_skills_note,
    confidence,
    confidence_note,
    physical_appearance,
    physical_appearance_note,
    knowledge_skills,
    knowledge_skills_note,
    asking_rate,
    availability,
    others,
    gen_remarks_recommendations,
    user_id_interviewer
  ) =>
    axios({
      method: "POST",
      url: `http://localhost:${process.env.PORT}/interview-assessment/add`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        user_id_applicant,
        date_interview,
        communication_skills,
        communication_skills_note,
        confidence,
        confidence_note,
        physical_appearance,
        physical_appearance_note,
        knowledge_skills,
        knowledge_skills_note,
        asking_rate,
        availability,
        others,
        gen_remarks_recommendations,
        user_id_interviewer
      }
    })
      .then(res => res.data)
      .catch(err => "Error"),

  editInterviewAssessment: (
    token,
    id,
    date_interview,
    communication_skills,
    communication_skills_note,
    confidence,
    confidence_note,
    physical_appearance,
    physical_appearance_note,
    knowledge_skills,
    knowledge_skills_note,
    asking_rate,
    availability,
    others,
    gen_remarks_recommendations,
    user_id_interviewer
  ) =>
    axios({
      method: "PUT",
      url: `http://localhost:${process.env.PORT}/interview-assessment/user/view/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        date_interview,
        communication_skills,
        communication_skills_note,
        confidence,
        confidence_note,
        physical_appearance,
        physical_appearance_note,
        knowledge_skills,
        knowledge_skills_note,
        asking_rate,
        availability,
        others,
        gen_remarks_recommendations,
        user_id_interviewer
      }
    })
      .then(res => res.data)
      .catch(err => "Error")
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  // DONE INTERVIEW ASSESSMENT
  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
};

module.exports = functions;
