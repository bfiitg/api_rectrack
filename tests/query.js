const cleanData = ({ testingDb }) => {
  return async function db() {
    const result = await testingDb.createTestDb();
    setTimeout(() => {}, 10000);
  };
};

module.exports = cleanData;
