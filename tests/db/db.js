require('dotenv').config()
const pg = require('pg')

let config = {
   user: process.env.PGUSER,
   database: process.env.TEST_DB,
   password: process.env.PGPASSWORD,
   port: process.env.PGPORT,
   host: process.env.TEST_HOST,
   max: 20,
   idleTimeoutMillis: 30000,
   connectionTimeoutMillis: 2000
}

let pool = new pg.Pool(config)

async function dbConnection() {
   try {
      return pool
   } catch (error) {
      pool.end()
      console.log("Test pool error: ", error)
   }
}

const testDb = (db) => {
   return db(dbConnection)
}

module.exports = testDb