const { 
   makeQuestionType,
   makeUpdateQuestionType
} = require('../../../src/entities/questions_type/app')

describe('Question types', () => {

   describe('Add question type', () => {

      it('must have a question type', () => {
         let question = {
            question_type: undefined,
            status: 'active'
         }
         
         expect(() => makeQuestionType(question)).toThrow('Question Type must have a value')
      })

      it('must throw an error if question type is not a string', () => {
         let question = {
            question_type: 1,
            status: 'active'
         }

         expect(() => makeQuestionType(question)).toThrow('Question Type must be a string')
      })
   })
})