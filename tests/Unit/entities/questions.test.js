const { 
   makeQuestions,
   makeQuestionEntityImports,
   makeQuestionEssayEntities,
   makeQuestionIQ 
} = require('../../../src/entities/questions/app')

describe('Questions', () => {

   describe('Adding questions', () => {

      it('must have a question', () => {
         let question = {
            questions: undefined,
            correct_answer: 'N/A',
            categories_id: 1,
            question_type_id: 1,
            details: "Question details"
         }

         expect(() => makeQuestions(question)).toThrow("Please enter a question.")
      })

      it('must have a category', () => {
         let question = {
            questions: "What is your name?",
            correct_answer: "Hello World!",
            categories_id: undefined,
            question_type_id: 1,
            details: "Question details"
         }

         expect(() => makeQuestions(question)).toThrow("Please enter the category.")
      })

      it('must have a correct answer', () => {
         let question = {
            questions: "What is your name?",
            correct_answer: undefined,
            categories_id: 1,
            question_type_id: 1,
            details: "Question details"
         }

         expect(() => makeQuestions(question)).toThrow("Please enter the correct answer.")
      })

      it('must have a question type', () => {
         let question = {
            questions: "What is your name?",
            correct_answer: 1,
            categories_id: 1,
            question_type_id: undefined,
            details: "Question details"
         }

         expect(() => makeQuestions(question)).toThrow("Please enter question type")
      })

      it('must have choices', () => {
         let question = {
            questions: "What is your name?",
            correct_answer: 1,
            categories_id: 1,
            question_type_id: 1,
            details: ""
         }

         expect(() => makeQuestions(question)).toThrow("Please enter the choices of the question.")
      })
   })

   describe('Importing questions', () => {

      it('must have a category', () => {
         let question = {
            categories_id: undefined,
            question_type_id: 1,
            path: 'C:/Users/BFI/Download',
            data: {
               qusetion: "What is the Obama's last name?",
               answer: "The Rock"
            }
         }

         expect(() => makeQuestionEntityImports(question)).toThrow("Please enter the category.")
      })

      it('must have a question type', () => {
         let question = {
            categories_id: 1,
            question_type_id: undefined,
            path: 'C:/Users/BFI/Download',
            data: {
               qusetion: "What is the Obama's last name?",
               answer: "The Rock"
            }
         }

         expect(() => makeQuestionEntityImports(question)).toThrow("Please enter question type")
      })

      it('must have a file path', () => {
         let question = {
            categories_id: 1,
            question_type_id: 1,
            path: undefined,
            data: {
               qusetion: "What is the Obama's last name?",
               answer: "The Rock"
            }
         }

         expect(() => makeQuestionEntityImports(question)).toThrow("Please enter the file path.")
      })
   })

   describe("Adding essay questions", () => {
      it('must have a question', () => {
         let question = {
            questions: undefined,
            categories_id: 1,
            question_type_id: 1,
         }

         expect(() => makeQuestionEssayEntities(question)).toThrow("Please enter a question.")
      })

      it("must have a category", () => {
         let question = {
            questions: "What is your name?",
            categories_id: undefined,
            question_type_id: 1,
         }

         expect(() => makeQuestionEssayEntities(question)).toThrow("Please enter the category.")
      })

      it("must have a type", () => {
         let question = {
            questions: "What is your name?",
            categories_id: 1,
            question_type_id: undefined,
         }

         expect(() => makeQuestionEssayEntities(question)).toThrow("Please enter question type")
      })
   })

   describe('Add IQ questions', () => {
      it('must have a question', () => {
         let question = {
            questions: undefined,
            correct_answer: "N/A",
            question_type_id: 1,
            details: "Question details"
         }

         expect(() => makeQuestionIQ(question)).toThrow('Please enter a question.')
      })

      it('must have a correct answer', () => {
         let question = {
            questions: "What is Obama's last name?",
            correct_answer: undefined,
            question_type_id: 1,
            details: "Question details"
         }

         expect(() => makeQuestionIQ(question)).toThrow('Please enter the correct answer.')
      })

      it('must have a type', () => {
         let question = {
            questions: "What is Obama's last name?",
            correct_answer: "The Rock",
            question_type_id: undefined,
            details: "Question details"
         }

         expect(() => makeQuestionIQ(question)).toThrow('Please enter question type')
      })

      it('must have details', () => {
         let question = {
            questions: "What is Obama's last name?",
            correct_answer: "The Rock",
            question_type_id: 1,
            details: ""
         }

         expect(() => makeQuestionIQ(question)).toThrow('Please enter the choices of the question.')
      })
   })
})