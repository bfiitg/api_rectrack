const makePosition = require('../../../src/entities/positions/app')

describe('Add position', () => {
   it('must not be able to add without a position name', () => {

      let position = {
         name: undefined,
         teams_id: 1,
         description: "Test",
         is_office_staff: 'f',
         is_vacant: 't',
         status: 'active'
      }
      
      expect(() => makePosition(position)).toThrow('Please enter position name.')
   })

   it('must have a team', () => {

      let position = {
         name: 'DevOps',
         teams_id: undefined,
         description: "Test",
         is_office_staff: 'f',
         is_vacant: 't',
         status: 'active'
      }
      
      expect(() => makePosition(position)).toThrow('Please enter which team the position belongs.')
   })

   it('must have a boolean value for is_office_staff', () => {

      let position = {
         name: 'DevOps',
         teams_id: 1,
         description: "Test",
         is_office_staff: undefined,
         is_vacant: 't',
         status: 'active'
      }
      
      expect(() => makePosition(position)).toThrow('Please enter if position is office staff or not.')
   })
})