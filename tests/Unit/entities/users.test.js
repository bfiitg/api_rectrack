const { 
   makeUser,
   loginEntity,
   adminMakeUser,
   adminEditUsers,
   applicantEntityLogin, //i didn't make test for this one since it has the same code as loginEntity.
   userApplicationFormEntitys,
   makeReferralForms,
   rateAnswer,
   updateResumeEntitys
} = require('../../../src/entities/users/app')

describe("Users", () => {

   describe('Adding user validation', () => {
      it('must have an email address', () => {
         let user = {
            email: undefined,
            firstname: "Chris",
            middlename: "Pee",
            lastname: "Bacon",
            mobile: "091234567",
            livestock: "no",
            travel: "yes",
            resume_url: "resumeurl.com/portfolio",
            position_id: 1,
            religion: "Christian",
            isQuickApplicant: false
         }

         expect(() => makeUser(user)).toThrow("Please enter email.")
      })

      it('must have a firstname', () => {
         let user = {
            email: "chrispbacon@getnada.com",
            firstname: undefined,
            middlename: "Pee",
            lastname: "Bacon",
            mobile: "091234567",
            livestock: "no",
            travel: "yes",
            resume_url: "resumeurl.com/portfolio",
            position_id: 1,
            religion: "Christian",
            isQuickApplicant: false
         }

         expect(() => makeUser(user)).toThrow("Please enter first name.")
      })

      it("must have a lastname", () => {
         let user = {
            email: "chrispbacon@getnada.com",
            firstname: "Chris",
            middlename: "Pee",
            lastname: undefined,
            mobile: "091234567",
            livestock: "no",
            travel: "yes",
            resume_url: "resumeurl.com/portfolio",
            position_id: 1,
            religion: "Christian",
            isQuickApplicant: false
         }

         expect(() => makeUser(user)).toThrow("Please enter last name.")
      })

      it("must have a valid mobile number", () => {
         let user = {
            email: "chrispbacon@getnada.com",
            firstname: "Chris",
            middlename: "Pee",
            lastname: "Bacon",
            mobile: undefined,
            livestock: "N/A",
            travel: "yes",
            resume_url: "resumeurl.com/portfolio",
            position_id: 1,
            religion: "Christian",
            isQuickApplicant: false
         }

         expect(() => makeUser(user)).toThrow("Please enter a valid mobile number.")
      })

      it("livestock question must have an answer", () => {
         let user = {
            email: "chrispbacon@getnada.com",
            firstname: "Chris",
            middlename: "Pee",
            lastname: "Bacon",
            mobile: "09091234567",
            livestock: undefined,
            travel: "yes",
            resume_url: "resumeurl.com/portfolio",
            position_id: 1,
            religion: "Christian",
            isQuickApplicant: false
         }

         expect(() => makeUser(user)).toThrow("Please enter livestock, N/A for nothing.")
      })

      it("must have a valid willing to travel answer, either yes or now", () => {
         let user = {
            email: "chrispbacon@getnada.com",
            firstname: "Chris",
            middlename: "Pee",
            lastname: "Bacon",
            mobile: "09091234567",
            livestock: "N/A",
            travel: undefined,
            resume_url: "resumeurl.com/portfolio",
            position_id: 1,
            religion: "Christian",
            isQuickApplicant: false
         }

         expect(() => makeUser(user)).toThrow("Please enter willing to travel: Yes/No only.")
      })

      it("must have a resume url", () => {
         let user = {
            email: "chrispbacon@getnada.com",
            firstname: "Chris",
            middlename: "Pee",
            lastname: "Bacon",
            mobile: "09091234567",
            livestock: "N/A",
            travel: "yes",
            resume_url: undefined,
            position_id: 1,
            religion: "Christian",
            isQuickApplicant: false
         }

         expect(() => makeUser(user)).toThrow("Please upload resumé.")
      })

      it("non-quick applicants must have a position", () => {
         let user = {
            email: "chrispbacon@getnada.com",
            firstname: "Chris",
            middlename: "Pee",
            lastname: "Bacon",
            mobile: "09091234567",
            livestock: "N/A",
            travel: "yes",
            resume_url: "resumeurl.com/portfolio",
            position_id: undefined,
            religion: "Christian",
            isQuickApplicant: false
         }

         expect(() => makeUser(user)).toThrow('Please select a position.')
      })

      it("must have religion", () => {
         let user = {
            email: "chrispbacon@getnada.com",
            firstname: "Chris",
            middlename: "Pee",
            lastname: "Bacon",
            mobile: "09091234567",
            livestock: "N/A",
            travel: "yes",
            resume_url: "resumeurl.com/portfolio",
            position_id: 1,
            religion: undefined,
            isQuickApplicant: false
         }

         expect(() => makeUser(user)).toThrow('Please enter a religion.')
      })
   })

   describe("Login validation", () => {
      it("must have an email", () => {
         let info = {
            email: undefined,
            password: "hotdog",
            ip: "",
            token: ""
         }

         expect(() => loginEntity(info)).toThrow("Please enter email.")
      })

      it("must have a valid email format", () => {
         let info = {
            email: "asdadadadadsad",
            password: "hotdog",
            ip: "",
            token: ""
         }

         expect(() => loginEntity(info)).toThrow("Not a valid email format.")
      })

      it("must have a password", () => {
         let info = {
            email: "chrispbacon@getnada.com",
            password: undefined,
            ip: "",
            token: ""
         }

         expect(() => loginEntity(info)).toThrow("Please enter password.")
      })
   })

   describe("Admin: Add user validation", () => {
      it("must have an email", () => {
         let info = {
            email: undefined,
            password: "hotdog",
            firstname: "Chris",
            lastname: "Bacon",
            middlename: "Pee",
            mobile: "09091234567",
            role_id: 5,
            position_id: 12
         }

         expect(() => adminMakeUser(info)).toThrow("Please enter email.")
      })

      it("must have a valid email format", () => {
         let info = {
            email: "hotdogiyaaa",
            password: "hotdog",
            firstname: "Chris",
            lastname: "Bacon",
            middlename: "Pee",
            mobile: "09091234567",
            role_id: 5,
            position_id: 12
         }

         expect(() => adminMakeUser(info)).toThrow("Not a valid email format.")
      })

      it("must have a password", () => {
         let info = {
            email: "chrispbacon@getnada.com",
            password: undefined,
            firstname: "Chris",
            lastname: "Bacon",
            middlename: "Pee",
            mobile: "09091234567",
            role_id: 5,
            position_id: 12
         }

         expect(() => adminMakeUser(info)).toThrow("Please enter password.")
      })

      it("must have a first name", () => {
         let info = {
            email: "chrispbacon@getnada.com",
            password: "password",
            firstname: undefined,
            lastname: "Bacon",
            middlename: "Pee",
            mobile: "09091234567",
            role_id: 5,
            position_id: 12
         }

         expect(() => adminMakeUser(info)).toThrow("Please enter first name.")
      })

      it("must have a last name", () => {
         let info = {
            email: "chrispbacon@getnada.com",
            password: "password",
            firstname: "Chris",
            lastname: undefined,
            middlename: "Pee",
            mobile: "09091234567",
            role_id: 5,
            position_id: 12
         }

         expect(() => adminMakeUser(info)).toThrow("Please enter last name.")
      })

      it("must have a role", () => {
         let info = {
            email: "chrispbacon@getnada.com",
            password: "password",
            firstname: "Chris",
            lastname: "Bacon",
            middlename: "Pee",
            mobile: "09091234567",
            role_id: undefined,
            position_id: 12
         }

         expect(() => adminMakeUser(info)).toThrow("Please select a role.")
      })

      it("must have a position", () => {
         let info = {
            email: "chrispbacon@getnada.com",
            password: "password",
            firstname: "Chris",
            lastname: "Bacon",
            middlename: "Pee",
            mobile: "09091234567",
            role_id: 5,
            position_id: undefined
         }

         expect(() => adminMakeUser(info)).toThrow("Please select a position.")
      })
   })

   describe("Admin: Edit user validation", () => {
      it("must have an email", () => {
         let info = {
            email: undefined,
            password: "password",
            firstname: "Chris",
            lastname: "Bacon",
            middlename: "Pee",
            mobile: "09091234567",
            role_id: 5,
            position_id: 1,
            status: "active"
         }

         expect(() => adminEditUsers(info)).toThrow("Please enter email.")
      })

      it("must have a valid email format", () => {
         let info = {
            email: "email1234",
            password: "password",
            firstname: "Chris",
            lastname: "Bacon",
            middlename: "Pee",
            mobile: "09091234567",
            role_id: 5,
            position_id: 1,
            status: "active"
         }

         expect(() => adminEditUsers(info)).toThrow("Not a valid email format.")
      })

      it("must have a first name", () => {
         let info = {
            email: "chrispbacon@getnada.com",
            password: "password",
            firstname: undefined,
            lastname: "Bacon",
            middlename: "Pee",
            mobile: "09091234567",
            role_id: 5,
            position_id: 1,
            status: "active"
         }

         expect(() => adminEditUsers(info)).toThrow("Please enter first name.")
      })
      
      it("must have a last name", () => {
         let info = {
            email: "chrispbacon@getnada.com",
            password: "password",
            firstname: "Chris",
            lastname: undefined,
            middlename: "Pee",
            mobile: "09091234567",
            role_id: 5,
            position_id: 1,
            status: "active"
         }

         expect(() => adminEditUsers(info)).toThrow("Please enter last name.")
      })

      it("must have a role", () => {
         let info = {
            email: "chrispbacon@getnada.com",
            password: "password",
            firstname: "Chris",
            lastname: "Bacon",
            middlename: "Pee",
            mobile: "09091234567",
            role_id: undefined,
            position_id: 1,
            status: "active"
         }

         expect(() => adminEditUsers(info)).toThrow("Please select a role.")
      })

      it("must have a position", () => {
         let info = {
            email: "chrispbacon@getnada.com",
            password: "password",
            firstname: "Chris",
            lastname: "Bacon",
            middlename: "Pee",
            mobile: "09091234567",
            role_id: 5,
            position_id: undefined,
            status: "active"
         }

         expect(() => adminEditUsers(info)).toThrow("Please select a position.")
      })

      it("must have a valid status", () => {
         let info = {
            email: "chrispbacon@getnada.com",
            password: "password",
            firstname: "Chris",
            lastname: "Bacon",
            middlename: "Pee",
            mobile: "09091234567",
            role_id: 5,
            position_id: 12,
            status: undefined
         }

         expect(() => adminEditUsers(info)).toThrow("Please enter a status.")
      })
   })

   describe("Application form validation", () => {

      it("must have a firstname", () => {
         let applicationFormInfo = {
            firstname: undefined,
            lastname: "Bacon",
            salary_desired: "14,000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969/01/01",
            age: "69",
            height: `6'9`,
            weight: "69kg",
            gender: "MALE",
            is_single_parent: false,
            civil_status: "SINGLE",
            citizenship: "Filipino",
            religion: "Christianity",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Joey Ayap",
            person_relationship: "Superior",
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow('Please enter first name.')
      })

      it("must have a last name", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: undefined,
            salary_desired: "14,000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969/01/01",
            age: "69",
            height: `6'9`,
            weight: "69kg",
            gender: "MALE",
            is_single_parent: false,
            civil_status: "SINGLE",
            citizenship: "Filipino",
            religion: "Christianity",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Joey Ayap",
            person_relationship: "Superior",
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow('Please enter last name.')
      })

      it("must have desired salary", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: undefined,
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969/01/01",
            age: "69",
            height: `6'9`,
            weight: "69kg",
            gender: "MALE",
            is_single_parent: false,
            civil_status: "SINGLE",
            citizenship: "Filipino",
            religion: "Christianity",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Joey Ayap",
            person_relationship: "Superior",
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter salary desired.")
      })

      it("must have a date of birth", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: undefined,
            age: "69",
            height: `6'9`,
            weight: "69kg",
            gender: "MALE",
            is_single_parent: false,
            civil_status: "SINGLE",
            citizenship: "Filipino",
            religion: "Christianity",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Joey Ayap",
            person_relationship: "Superior",
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter date of birth.")
      })

      it("must have age", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: undefined,
            height: `6'9`,
            weight: "69kg",
            gender: "MALE",
            is_single_parent: false,
            civil_status: "SINGLE",
            citizenship: "Filipino",
            religion: "Christianity",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Joey Ayap",
            person_relationship: "Superior",
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter age.")
      })

      it("must have height", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: undefined,
            weight: "69kg",
            gender: "MALE",
            is_single_parent: false,
            civil_status: "SINGLE",
            citizenship: "Filipino",
            religion: "Christianity",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Joey Ayap",
            person_relationship: "Superior",
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter height.")
      })

      it("must have weight", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9",
            weight: undefined,
            gender: "MALE",
            is_single_parent: false,
            civil_status: "SINGLE",
            citizenship: "Filipino",
            religion: "Christianity",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Joey Ayap",
            person_relationship: "Superior",
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter weight.")
      })

      it("must have gender", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: undefined,
            is_single_parent: false,
            civil_status: "SINGLE",
            citizenship: "Filipino",
            religion: "Christianity",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Joey Ayap",
            person_relationship: "Superior",
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter gender.")
      })

      it(`must have an answer to "single parent or not" query`, () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: "MALE",
            is_single_parent: undefined,
            civil_status: "SINGLE",
            citizenship: "Filipino",
            religion: "Christianity",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Joey Ayap",
            person_relationship: "Superior",
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter single parent or not.")
      })

      it("must have a civil status", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: "MALE",
            is_single_parent: "false",
            civil_status: undefined,
            citizenship: "Filipino",
            religion: "Christianity",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Joey Ayap",
            person_relationship: "Superior",
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter civil status.")
      })

      it("must have citizenship", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: "MALE",
            is_single_parent: "false",
            civil_status: "single",
            citizenship: undefined,
            religion: "Christianity",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Joey Ayap",
            person_relationship: "Superior",
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter citizenship.")
      })

      it("must have religion", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: "MALE",
            is_single_parent: "false",
            civil_status: "single",
            citizenship: "Filipino",
            religion: undefined,
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Joey Ayap",
            person_relationship: "Superior",
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter religion.")
      })

      it("must have a date of when to start working", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: "MALE",
            is_single_parent: "false",
            civil_status: "single",
            citizenship: "Filipino",
            religion: "Atheist",
            who_referred: "Employee 1",
            when_to_start: undefined,
            person_to_notify: "Joey Ayap",
            person_relationship: "Superior",
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter when are you going to start.")
      })

      it("must have a person to notify", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: "MALE",
            is_single_parent: "false",
            civil_status: "single",
            citizenship: "Filipino",
            religion: "Atheist",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: undefined,
            person_relationship: "Superior",
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter person to notify encase of emergency.")
      })

      it("must indicate the relationship of the applicant to the person to notify", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: "MALE",
            is_single_parent: "false",
            civil_status: "single",
            citizenship: "Filipino",
            religion: "Atheist",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Bacon Deez Nuts",
            person_relationship: undefined,
            person_address:"Purok 9, Brgy. Lagao",
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter your relationship with that person.")
      })

      it("must input the address of the person to notify in case of emergency", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: "MALE",
            is_single_parent: "false",
            civil_status: "single",
            citizenship: "Filipino",
            religion: "Atheist",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Bacon Deez Nuts",
            person_relationship: "friend",
            person_address: undefined,
            person_telephone:"09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter the address of that person.")
      })

      it("must have a mobile number of the person to contact", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: "MALE",
            is_single_parent: "false",
            civil_status: "single",
            citizenship: "Filipino",
            religion: "Atheist",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Bacon Deez Nuts",
            person_relationship: "friend",
            person_address: "Purok 9, Brgy. Lagao",
            person_telephone: undefined,
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter the mobile number of that person.")
      })

      it("must have a second choice position", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: "MALE",
            is_single_parent: "false",
            civil_status: "single",
            citizenship: "Filipino",
            religion: "Atheist",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Bacon Deez Nuts",
            person_relationship: "friend",
            person_address: "Purok 9, Brgy. Lagao",
            person_telephone: "09091234567",
            position_id_second_choice: undefined,
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application:"2020-01-01",
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter your second choice position.")
      })

      it("must have a date of application", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: "MALE",
            is_single_parent: "false",
            civil_status: "single",
            citizenship: "Filipino",
            religion: "Atheist",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Bacon Deez Nuts",
            person_relationship: "friend",
            person_address: "Purok 9, Brgy. Lagao",
            person_telephone: "09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application: undefined,
            user_image_path:"userimage.com/mypicture",
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter date of application.")
      })

      it("must upload applicant's photo", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: "MALE",
            is_single_parent: "false",
            civil_status: "single",
            citizenship: "Filipino",
            religion: "Atheist",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Bacon Deez Nuts",
            person_relationship: "friend",
            person_address: "Purok 9, Brgy. Lagao",
            person_telephone: "09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application: "2022-01-01",
            user_image_path: undefined,
            relativeArray: [""],
            educationalBgArray: [""],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please upload your image.")
      })

      it("must have an educational background", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: "MALE",
            is_single_parent: "false",
            civil_status: "single",
            citizenship: "Filipino",
            religion: "Atheist",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Bacon Deez Nuts",
            person_relationship: "friend",
            person_address: "Purok 9, Brgy. Lagao",
            person_telephone: "09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application: "2022-01-01",
            user_image_path: "myphotos.com/user11222/goodlookingphoto.jpg",
            relativeArray: [""],
            educationalBgArray: undefined,
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: [""]
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter educational backgroud.")
      })

      it("must have other miscellaneous information", () => {
         let applicationFormInfo = {
            firstname: "Chris",
            lastname: "Bacon",
            salary_desired: "14000",
            UnitRoomNumberFloor: "",
            BuildingName: "",
            LotBlockPhaseHouseNumber: "",
            StreetName: "She belongs to the st.",
            VillageSubdivision: "Jepoy Subdivision",
            Barangay: "Masipag",
            TownDistrict: "Poblacion",
            Municipality: "Polomolok",
            CityProvince: "South Cotabato",
            AddressType: "Registered Address",
            place_birth: "Polomolok",
            bday: "1969-06-09",
            age: "79",
            height: "6'9'",
            weight: "69kg",
            gender: "MALE",
            is_single_parent: "false",
            civil_status: "single",
            citizenship: "Filipino",
            religion: "Atheist",
            who_referred: "Employee 1",
            when_to_start: "2022-01-01",
            person_to_notify: "Bacon Deez Nuts",
            person_relationship: "friend",
            person_address: "Purok 9, Brgy. Lagao",
            person_telephone: "09091234567",
            position_id_second_choice: "Backend Developer",
            CS_eligible:"yes",
            CS_date_taken:"2019",
            CS_certificate_image_path:"cscertificate.com/mycert",
            date_application: "2022-01-01",
            user_image_path: "myphotos.com/user11222/goodlookingphoto.jpg",
            relativeArray: [""],
            educationalBgArray: [{college: "Mindanao State University", degree: "Third"}],
            professionalLicensesArray: [""],
            orgMembershipArray: [""],
            seminarsAttendedArray: [""],
            workExperienceArray: [""],
            workWithBFIArray: [""],
            otherInfoArray: [""],
            familyBgArray: [""],
            isMarriedArray: [""],
            referencesArray: [""],
            willingnessArray: [""],
            otherInfoArray: undefined
         }

         expect(() => userApplicationFormEntitys(applicationFormInfo)).toThrow("Please enter other information.")
      })
   })

   describe("Referral form validation", () => {
      it("must have a referrer id", () => {
         let info = {
            referrer_id: undefined,
            applicant_id: 29,
            referral_relationship_referrer: "friend"
         }

         expect(() => makeReferralForms(info)).toThrow("Please enter referrer's ID.")
      })

      it("must have applicant's id", () => {
         let info = {
            referrer_id: 1,
            applicant_id: undefined,
            referral_relationship_referrer: "friend"
         }

         expect(() => makeReferralForms(info)).toThrow("Please enter applicant's ID.")
      })

      it("must indicate relationship of referrer and applicant", () => {
         let info = {
            referrer_id: 1,
            applicant_id: 29,
            referral_relationship_referrer: undefined
         }

         expect(() => makeReferralForms(info)).toThrow("Please enter referral's relationship to referrer.")
      })
   })

   describe("Applicant rating validation", () => {
      it("must have an id", () => {
         let info = {
            id: undefined,
            is_correct: 5
         }
   
         expect(() => rateAnswer(info)).toThrow("Please specifiy id.")
      })

      it("must have a score", () => {
         let info = {
            id: 1,
            is_correct: undefined
         }
   
         expect(() => rateAnswer(info)).toThrow("Please provide score.")
      })

      it("must have a numeric score", () => {
         let info = {
            id: 1,
            is_correct: "hello"
         }
   
         expect(() => rateAnswer(info)).toThrow("Score must be number.")
      }) 
   })

   describe("Update resume", () => {
      it("must have a resume url", () => {
         let info  ={
            resume_upload_url: undefined,
            mobile: "09091234567"
         }

         expect(() => updateResumeEntitys(info)).toThrow("Please provide resume.")
      })
   })
})