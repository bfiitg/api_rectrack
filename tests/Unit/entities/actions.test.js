const makeAction = require('../../../src/entities/actions/app')

describe('Action validation', () => {
   it('must have a name', () => {
      let action = {
         action_name: '',
         module_id: '1',
         status: 'status'
      }

      expect(() => makeAction(action)).toThrow('Please enter action name.')
   })

   it('must have a module id', () => {
      let action = {
         action_name: 'Action Name',
         module_id: '',
         status: 'status'
      }

      expect(() => makeAction(action)).toThrow('Please select module.')
   })

   it('must have a status', () => {
      let action = {
         action_name: 'Action Name',
         module_id: '1',
         status: ''
      }

      expect(() => makeAction(action)).toThrow('Please select status.')
   })
})