const makeAccessRights = require('../../../src/entities/access-rights/app')

describe('Access rights validation', () => {
   it('must have a description', () => {
      let accessRight = {
         discription: '',
         actions: 'test',
         status: 'pending for review'
      }

      //yes, i know wrong spelling. hahaha
      expect(() => makeAccessRights(accessRight)).toThrow('Please enter discription.')
   })

   it('must have an action', () => {
      let accessRight = {
         discription: 'test',
         actions: '',
         status: 'pending for review'
      }
      expect(() => makeAccessRights(accessRight)).toThrow("Please select action.")
   })

   it('must have a status', () => {
      let accessRight = {
         discription: 'test',
         actions: 'test',
         status: ''
      }
      expect(() => makeAccessRights(accessRight)).toThrow("Please select status.")
   })
})