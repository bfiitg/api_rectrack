const { makeInterviewAssessments, makeInterviewAssessmentEdits } = require('../../../src/entities/interview-assessment/app')

describe('Editing interview assessment', () => {
   it('must have an interview date', () => {
      let interviewAssessment = {
         date_interview: '', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Will recommend',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter date of interview conducted.')
   })

   it('must give a communication skill rating', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: '',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Will recommend',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter communication skills rate.')
      })
      
   it('must give a confidence rating', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: '',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Will recommend',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter confidence rate.')
   })

   it('must give a physical appearance rating', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: '',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Will recommend',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter physical appearance rate.')
   })

   it('must give a intelligence rating', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: '',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Will recommend',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter knowledge skills rate.')
   })

   it('must have a salary expectation', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Will recommend',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter asking rate.')
   })

   it('must have provide applicant availability', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: '',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Will recommend',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter availability.')
   })

   it('must have provide applicant availability', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: '',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter general remarks and recommendations.')
   })

   it('must have provide applicant availability', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Purty good',
         user_id_interviewer: ''
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter who conducted the interview.')
   })
})


describe('Adding interview assessments', () => {
   
   it('must have an interview date', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Will recommend',
         user_id_interviewer: '1',
         user_id_applicant: undefined
      }
   
      expect(() => makeInterviewAssessments(interviewAssessment)).toThrow('Please enter applicant id.')
   })

   it('must give a communication skill rating', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: '',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Will recommend',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter communication skills rate.')
      })
      
   it('must give a confidence rating', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: '',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Will recommend',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter confidence rate.')
   })

   it('must give a physical appearance rating', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: '',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Will recommend',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter physical appearance rate.')
   })

   it('must give a intelligence rating', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: '',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Will recommend',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter knowledge skills rate.')
   })

   it('must have a salary expectation', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Will recommend',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter asking rate.')
   })

   it('must have provide applicant availability', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: '',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Will recommend',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter availability.')
   })

   it('must have provide applicant availability', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: '',
         user_id_interviewer: '1'
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter general remarks and recommendations.')
   })

   it('must have provide applicant availability', () => {
      let interviewAssessment = {
         date_interview: '2021-05-01', //YYYY-MM-DD
         communication_skills: 'excellent',
         communication_skills_note: 'Orator of the year',
         confidence: 'excellent',
         confidence_note: 'Timid',
         physical_appearance: 'excellent',
         physical_appearance_note: 'Meh',
         knowledge_skills: 'excellent',
         knowledge_skills_note: 'Exceptionally smart',
         asking_rate: '12,000',
         availability: 'Yes',
         others: 'Nothing else to add',
         gen_remarks_recommendations: 'Purty good',
         user_id_interviewer: ''
      }
   
      expect(() => makeInterviewAssessmentEdits(interviewAssessment)).toThrow('Please enter who conducted the interview.')
   })
})