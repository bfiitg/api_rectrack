const makeRole = require('../../../src/entities/roles/app')

describe('Roles', () => {
   
   it('must have a role name', () => {
      let role = {
         name: undefined,
         description: "Test",
         status: "active"
      }
      
      expect(() => makeRole(role)).toThrow("Please enter role name.")
   })
})