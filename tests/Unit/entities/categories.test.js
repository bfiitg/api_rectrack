const makeCategory = require('../../../src/entities/categories/app')

describe('Category validation', () => {
   it('must have a name', () => {
      let category = {
         name: '',
         description: 'Category description',
         teams_id: '1',
         time_limit_on_seconds: '30',
         status: 'test'
      }
      expect(() => makeCategory(category)).toThrow('Please enter category name.')
   })

   it('must have a team id', () => {
      let category = {
         name: 'Category sample',
         description: 'Category description',
         teams_id: '',
         time_limit_on_seconds: '30',
         status: 'test'
      }
      expect(() => makeCategory(category)).toThrow('Please enter which team the category belongs.')
   })

   it('must have a time limit in seconds', () => {
      let category = {
         name: 'Category sample',
         description: 'Category description',
         teams_id: '1',
         time_limit_on_seconds: undefined,
         status: 'test'
      }
      expect(() => makeCategory(category)).toThrow('Please enter time limit per question on seconds.')
   })
})