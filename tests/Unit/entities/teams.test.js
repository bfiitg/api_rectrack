const makeTeam = require('../../../src/entities/teams/app')

describe('Teams', () => {

   it('must not add without a name', () => {
      let team = {
         name: undefined,
         description: "Test",
         department_id: 1,
         status: 'active',
         no_of_items: 15
      }

      expect(() => makeTeam(team)).toThrow('Please enter team name.')
   })

   it("must not add without a department id", () => {
      let team = {
         name: "Software Team",
         description: "Test",
         department_id: undefined,
         status: 'active',
         no_of_items: 15
      }

      expect(() => makeTeam(team)).toThrow('Please enter which department the team belongs.')
   })

   it("must not add without the number of items for the exam", () => {
      let team = {
         name: "Software Team",
         description: "Test",
         department_id: 1,
         status: 'active',
         no_of_items: undefined
      }

      expect(() => makeTeam(team)).toThrow('Please enter the number of items for the online exam.')
   })
})