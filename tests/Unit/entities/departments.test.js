const makeDepartment = require('../../../src/entities/departments/app')

describe('Validation for adding department', () => {
   it('must have a name', () => {
      let department = {
         name: '',
         description: 'Sample description',
         user_id: '1',
         status: 'Sample status'
      }
      expect(() => makeDepartment(department)).toThrow('Please enter department name.')
   })
})