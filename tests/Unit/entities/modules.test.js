const makeModule = require('../../../src/entities/modules/app')

describe('Validations for adding module', () => {
   it('must have a name', () => {
      let module = {
         module_name: undefined,
         status: "module status"
      }
      
      expect(() => makeModule(module)).toThrow('Please enter module name.')
   })
   
   it('must have a name', () => {
      let module = {
         module_name: "Test",
         status: undefined
      }
      
      expect(() => makeModule(module)).toThrow('Please select status.')
   })
})