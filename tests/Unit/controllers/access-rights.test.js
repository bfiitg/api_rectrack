const makeAccessRights  = require('../../../src/controllers/access-rights/access-rights-add'),
      updateAccessRights = require('../../../src/controllers/access-rights/access-rights-edit'),
      removeAccessRights = require('../../../src/controllers/access-rights/access-rights-remove')

console.error = jest.fn()

describe("Access Rights Controller tests", () => {

   describe("Add access rights", () => {

      const addAccessRights = makeAccessRights ({ makeAddAccessRightsUseCase: a => a })

      it("must return 201 code", async () => {
   
         const httpRequest = {
            body: {
               module_name: 'Extensions',
               status: 'active',
               discription: 'test',
               actions: 'test'
             },
             ip: '::1',
             method: 'POST',
             headers: {
               'Content-Type': 'application/json',
               Referer: undefined,
               'User-Agent': 'Godzilla/5.0',
               'Access-Control-Allow-Origin': '*'
             }
         }
   
         const actual = await addAccessRights(httpRequest)
         
         expect(actual.statusCode).toEqual(201)
      })

      it("must return 400 code if there's an error", async () => {
   
         const httpRequest = undefined
         const actual = await addAccessRights(httpRequest)
         
         expect(actual.statusCode).toEqual(400)
      })
   })

   describe('Edit access rights', () => {
      
      const editAccessRights = updateAccessRights({ updateAccessRightsUseCase: a => a })

      it("must return 200 upon successful update", async () => {

         const httpRequest = {
            body: {
               module_name: 'Extensions',
               status: 'active',
               discription: 'test',
               actions: 'test'
             },
             ip: '::1',
             method: 'POST',
             headers: {
               'Content-Type': 'application/json',
               Referer: undefined,
               'User-Agent': 'Godzilla/5.0',
               'Access-Control-Allow-Origin': '*'
             },
             params: {
                id: 1
             }
         }

         const actual = await editAccessRights(httpRequest)
         expect(actual.statusCode).toEqual(200)
      })

      it("must return 400 upon error", async () => {
         const httpRequest = undefined
         const actual = await editAccessRights(httpRequest)
         expect(actual.statusCode).toEqual(400)
      })
   })

   describe("Delete access rights", () => {
      
      const deleteAccessRight = removeAccessRights({ removeAccessRightsUseCase: a => a})
      
      it("must return 204 upon successful removal", async () => {

         const httpRequest = {
            body: {
               module_name: 'Extensions',
               status: 'active',
               discription: 'test',
               actions: 'test'
             },
             ip: '::1',
             method: 'POST',
             headers: {
               'Content-Type': 'application/json',
               Referer: undefined,
               'User-Agent': 'Godzilla/5.0',
               'Access-Control-Allow-Origin': '*'
             },
             params: {
                id: 1
             }
         }
         
         const actual = await deleteAccessRight(httpRequest)
         expect(actual.statusCode).toEqual(204)
      })
   })
})