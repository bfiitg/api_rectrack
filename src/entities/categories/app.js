const makeCategoryEntity = require("./categories-entity");

const makeCategory = makeCategoryEntity({});

module.exports = makeCategory;
