const makeCategoryEntity = ({}) => {
  return function make({
    name,
    description,
    teams_id,
    time_limit_on_seconds,
    status
  } = {}) {
    if (!name) {
      throw new Error("Please enter category name.");
    }
    if (!teams_id) {
      throw new Error("Please enter which team the category belongs.");
    }
    if (!time_limit_on_seconds) {
      throw new Error("Please enter time limit per question on seconds.");
    }
    return Object.freeze({
      getName: () => name,
      getDescription: () => description,
      getTeam: () => teams_id,
      getTimeLimit: () => time_limit_on_seconds,
      getStatus: () => status
    });
  };
};

module.exports = makeCategoryEntity;
