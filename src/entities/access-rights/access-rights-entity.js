const makeAccessRightsEntity =({})=>{
    return function make ({
        discription,
        actions,
        status
    } = {}){
        if (!discription){
            throw new Error("Please enter discription.")
        }
        if (!actions){
            throw new Error("Please select action.")
        }
        if (!status){
            throw new Error("Please select status.")
        }
        return Object.freeze({
            getDiscription:()=>discription,
            getActions:()=>actions,
            getStatus:()=> status
        });
    };
};

module.exports=makeAccessRightsEntity