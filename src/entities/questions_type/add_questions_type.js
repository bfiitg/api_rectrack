const buildQuestionType = ({}) => {
  return function make({
    question_type,
    status,
  } = {}) {
    if (!question_type) {
      throw new Error('Question Type must have a value');
    }
    if (!isNaN(question_type)) {
      throw new Error('Question Type must be a string');
    }
    return Object.freeze({
      getQuestionType: () => question_type,
      getStatus: () => status
    });
  };
};
module.exports = buildQuestionType;
