const buildQuestionType = require('./add_questions_type');
const buildUpdateQuestionType = require('./edit_questions_type');

const makeQuestionType = buildQuestionType({});
const makeUpdateQuestionType = buildUpdateQuestionType({});

module.exports = {
  makeQuestionType,
  makeUpdateQuestionType
};
