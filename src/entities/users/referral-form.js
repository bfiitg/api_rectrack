const makeReferralForm = ({ MobileValidation, encrypt }) => {
  return function make({
    referrer_id,
    applicant_id,
    referral_relationship_referrer
  } = {}) {
    
    if (!referrer_id) {
      throw new Error("Please enter referrer's ID.");
    }
    if (!applicant_id) {
      throw new Error("Please enter applicant's ID.");
    }
    if (!referral_relationship_referrer) {
      throw new Error("Please enter referral's relationship to referrer.");
    }

    return Object.freeze({
      getReferrerId: () => referrer_id,
      getApplicantId: () => applicant_id,
      getReferralRelationshipReferrer: () => referral_relationship_referrer
    });
  };
};

module.exports = makeReferralForm;
