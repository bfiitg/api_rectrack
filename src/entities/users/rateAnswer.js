const rateAnswerEntity = ({}) => {
  return function entity({ id, is_correct } = {}) {
    if (!id) {
      throw new Error("Please specifiy id.");
    }
    if (!is_correct) {
      throw new Error("Please provide score.");
    }
    if (isNaN(is_correct)) {
      throw new Error("Score must be number.");
    }

    return Object.freeze({
        getId:() => id,
        getIs_correct: () => is_correct
    })
  };
};

module.exports = rateAnswerEntity