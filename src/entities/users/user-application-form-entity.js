const userApplicationFormEntity = ({}) => {
  return function entity({
    //   users db
    firstname,
    middlename,
    lastname,
    salary_desired,
    // new fields
    UnitRoomNumberFloor,
    BuildingName,
    LotBlockPhaseHouseNumber,
    StreetName,
    VillageSubdivision,
    Barangay,
    TownDistrict,
    Municipality,
    CityProvince,
    AddressType = "Registered Address",
    //
    place_birth,
    bday,
    age,
    height,
    weight,
    gender,
    is_single_parent,
    civil_status,
    citizenship,
    religion,
    who_referred,
    when_to_start,
    person_to_notify,
    person_relationship,
    person_address,
    person_telephone,
    position_id_second_choice,
    CS_eligible,
    CS_date_taken,
    CS_certificate_image_path,
    date_application,
    user_image_path,
    //-----------
    // users_relative_in_bfi db
    relativeArray,
    //-----------
    // users_educational_bg db
    educationalBgArray,
    //-----------
    // users_professional_licenses db
    professionalLicensesArray,
    //-----------
    // users_org_memberships db
    orgMembershipArray,
    //-----------
    // users_seminar_attends db
    seminarsAttendedArray,
    //-----------
    // users_work_experiences db
    workExperienceArray,
    //-----------
    // users_have_work_with_bfi db
    workWithBFIArray,
    //-----------
    // users_other_info db
    otherInfoArray,
    //-----------
    // users_family_bg db
    familyBgArray,
    //-----------
    // users_is_married db
    isMarriedArray,
    //-----------
    // users_references db
    referencesArray,
    //-----------
    // users_willingness_to_work db
    willingnessArray
  } = {}) {
    if (!firstname) {
      throw new Error("Please enter first name.");
    }
    // if (!middlename) {
    //   throw new Error("Please enter middle name.");
    // }
    if (!lastname) {
      throw new Error("Please enter last name.");
    }
    if (!salary_desired) {
      throw new Error("Please enter salary desired.");
    }
    if (!bday) {
      throw new Error("Please enter date of birth.");
    }
    if (!age) {
      throw new Error("Please enter age.");
    }
    if (!height) {
      throw new Error("Please enter height.");
    }
    if (!weight) {
      throw new Error("Please enter weight.");
    }
    if (!gender) {
      throw new Error("Please enter gender.");
    }
    if (!is_single_parent) {
      throw new Error("Please enter single parent or not.");
    }
    if (!civil_status) {
      throw new Error("Please enter civil status.");
    }
    if (!citizenship) {
      throw new Error("Please enter citizenship.");
    }
    if (!religion) {
      throw new Error("Please enter religion.");
    }
    // if (!who_referred) {
    //   throw new Error("Please enter who referred you to our company.");
    // }
    if (!when_to_start) {
      throw new Error("Please enter when are you going to start.");
    }
    if (!person_to_notify) {
      throw new Error("Please enter person to notify encase of emergency.");
    }
    if (!person_relationship) {
      throw new Error("Please enter your relationship with that person.");
    }
    if (!person_address) {
      throw new Error("Please enter the address of that person.");
    }
    if (!person_telephone) {
      throw new Error("Please enter the mobile number of that person.");
    }
    if (!position_id_second_choice) {
      throw new Error("Please enter your second choice position.");
    }
    // if (!CS_eligible) {
    //   throw new Error("Please enter CS eligibility.");
    // }
    // if (!CS_date_taken) {
    //   throw new Error("Please enter CS date taken.");
    // }
    // if (!CS_certificate_image_path === null) {
    //   throw new Error("Please upload CS certificate image.");
    // }
    if (!date_application) {
      throw new Error("Please enter date of application.");
    }
    if (!user_image_path || (user_image_path === null || undefined)) {
      throw new Error("Please upload your image.");
    }
    if (!educationalBgArray) {
      throw new Error("Please enter educational backgroud.");
    }
    if (!otherInfoArray) {
      throw new Error("Please enter other information.");
    }
    

    return Object.freeze({
      getSalaryDesired: () => salary_desired,
      // new fields
      getUnitRoomNumberFloor: () => UnitRoomNumberFloor,
      getBuildingName: () => BuildingName,
      getLotBlockPhaseHouseNumber: () => LotBlockPhaseHouseNumber,
      getStreetName: () => StreetName,
      getVillageSubd: () => VillageSubdivision,
      getBarangay: () => Barangay,
      getTownDistrict: () => TownDistrict,
      getMunicipality: () => Municipality,
      getCityProvince: () => CityProvince,
      getAddressType: () => AddressType,
      //
      getMiddlename: () => middlename,
      getPlaceBirth: () => place_birth,
      getBday: () => bday,
      getAge: () => age,
      getHeight: () => height,
      getWeight: () => weight,
      getGender: () => gender,
      getSingleParent: () => is_single_parent,
      getCivilStatus: () => civil_status,
      getCitizenship: () => citizenship,
      getReligion: () => religion,
      getWhoReferred: () => who_referred,
      getWhenToStart: () => when_to_start,
      getPersonToNotify: () => person_to_notify,
      getPersonRelationship: () => person_relationship,
      getPersonAddress: () => person_address,
      getPersonTelephone: () => person_telephone,
      getSecondChoice: () => position_id_second_choice,
      getCSEligible: () => CS_eligible,
      getCSDateTaken: () => CS_date_taken,
      getCSImagePath: () => CS_certificate_image_path,
      getDateApplication: () => date_application,
      getUserImagePath: () => user_image_path,
      getRelative: () => relativeArray,
      getEducation: () => educationalBgArray,
      getProfessionalLicense: () => professionalLicensesArray,
      getOrgMembership: () => orgMembershipArray,
      getSeminars: () => seminarsAttendedArray,
      getWorkExperience: () => workExperienceArray,
      getWorkWithBFI: () => workWithBFIArray,
      getOtherInfo: () => otherInfoArray,
      getFamilyBg: () => familyBgArray,
      getIsMarried: () => isMarriedArray,
      getReferences: () => referencesArray,
      getWillingness: () => willingnessArray
    });
  };
};

module.exports = userApplicationFormEntity;
