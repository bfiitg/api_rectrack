const makeUserEntity = ({ MobileValidation, isValidEmail, encrypt }) => {
  return function makeUser({
    email,
    firstname,
    middlename,
    lastname,
    mobile,
    livestock,
    travel,
    resume_url,
    position_id,
    religion,
    isQuickApplicant
  } = {}) {
    if (!email || email.trim().length == 0) {
      throw new Error("Please enter email.");
    }
    // if (isValidEmail(email) === false) {
    //   throw new Error("Not a valid email format.");
    // }
    if (!firstname) {
      throw new Error("Please enter first name.");
    }
    if (!lastname) {
      throw new Error("Please enter last name.");
    }
    // optional ang middlenamme
    // if (!middlename) {
    //   throw new Error("Please enter middle name.");
    // }
    if (!mobile || /^09\d{9}$|^\+639\d{9}$/.test(mobile) == false) {
      throw new Error("Please enter a valid mobile number.");
    }
    if (!livestock || livestock.trim().length == 0) {
      throw new Error("Please enter livestock, N/A for nothing.");
    }
    if (!travel) {
      throw new Error("Please enter willing to travel: Yes/No only.");
    }
    if (!resume_url || resume_url.trim().length == 0) {
      throw new Error("Please upload resumé.");
    }
    if (!isQuickApplicant && !position_id) {
        throw new Error("Please select a position.");
    }

    if (!religion || religion.trim().length == 0) {
      throw new Error("Please enter a religion.");
    }
    return Object.freeze({
      getEmail: () => encrypt(email),
      getFirstName: () => encrypt(firstname),
      getMiddleName: () => encrypt(middlename),
      getLastName: () => encrypt(lastname),
      getMobile: () => MobileValidation(mobile),
      getLiveStock: () => livestock,
      getTravel: () => travel,
      getResumeURL: () => resume_url,
      getPosition: () => position_id,
      getReligion: () => religion,
    });
  };
};

module.exports = makeUserEntity;
