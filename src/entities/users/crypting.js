const crypto = require("crypto"),
  algorithm = "aes-256-gcm",
  password = "3zTvzr3p67VC61jmV54rIYu1545x4TlY",
  // do not use a global iv for production,
  // generate a new one for each encryption
  iv = "60iP0h6vJoEa";

function encrypt(text) {
  if (text) {
    var cipher = crypto.createCipheriv(algorithm, password, iv);
    var encrypted = cipher.update(text, "utf8", "hex");
    encrypted += cipher.final("hex");
    var tag = cipher.getAuthTag();
    return encrypted;
  }
}

function decrypt(encrypted) {
  try {
    let str = null;
    if (encrypted) {
      const decipher = crypto.createDecipheriv(algorithm, password, iv);
      const dec = decipher.update(encrypted, "hex", "utf8");
      str = dec;
    }
    if (!str) return encrypted;
    return str;
  } catch (e) {
    return encrypted;
  }
}

module.exports = { encrypt, decrypt };
