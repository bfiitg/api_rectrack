const MobileValidation = require('../../functions/app')


const makeUserEntity = require("./users-entity");
const userLoginEntity = require("./users-login-entity");
const adminAddUserEntity = require("./admin-add-entity");
const adminEditUserEntity = require("./admin-edit-entity");
const applicantLoginEntity = require("./applicant-login-entity");
const userApplicationFormEntity = require("./user-application-form-entity");
const makeReferralForm = require("./referral-form");
const rateAnswerEntity = require("./rateAnswer")
const { makeToken } = require("../../token/app");
//-----------
const emailValidator = require("email-validator");
const changeCase = require("change-case");
const address = require("address");
const randomstring = require("randomstring");
const updateResumeEntity = require("./update_resume-entity")
//
const encrypt = require("./crypting").encrypt;
const decrypt = require("./crypting").decrypt;
//
const isValidEmail = text => {
  return emailValidator.validate(text);
};

const toLowerCased = text => {
  return changeCase.lowerCase(text);
};

const getIp = () => {
  return address.ip();
};

// only numbers allowed
const allNumbers = text => {
  const isnum = /^\d+$/.test(text);
  return isnum;
};

// generate random strings for auto generate password for applicants
const generateString = () => {
  return randomstring.generate(12);
};

const makeUser = makeUserEntity({
  isValidEmail, MobileValidation,
  encrypt
});

// for admin, pg, heads
const loginEntity = userLoginEntity({
  isValidEmail,
  encrypt,
  getIp,
  makeToken
});

// loginEntity for applicants
const applicantEntityLogin = applicantLoginEntity({
  isValidEmail,
  encrypt,
  getIp,
  makeToken
});

const adminMakeUser = adminAddUserEntity({
  isValidEmail,
  MobileValidation,
  encrypt
});

const adminEditUsers = adminEditUserEntity({
  isValidEmail,
  MobileValidation,
  encrypt
});

const userApplicationFormEntitys = userApplicationFormEntity({});

const updateResumeEntitys = updateResumeEntity({})
const makeReferralForms = makeReferralForm({ MobileValidation, encrypt });

const rateAnswer = rateAnswerEntity({})

module.exports = {
  makeUser,
  loginEntity,
  decrypt,
  encrypt,
  adminMakeUser,
  adminEditUsers,
  applicantEntityLogin,
  toLowerCased,
  userApplicationFormEntitys,
  generateString,
  makeReferralForms,
  rateAnswer,
  updateResumeEntitys
};
