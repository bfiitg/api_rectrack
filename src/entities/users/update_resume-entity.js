const updateResumeEntity = ({}) => {
  return function put({ resume_upload_url, mobile } = {}) {
    if (!resume_upload_url) {
      throw new Error("Please provide resume.");
    }

    return Object.freeze({
      getResume_upload_url: () => resume_upload_url,
      getMobile: () => mobile
    });
  };
};

module.exports = updateResumeEntity;
