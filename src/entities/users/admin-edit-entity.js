const adminEditUserEntity = ({ isValidEmail, MobileValidation, encrypt }) => {
  return function makeUser({
    email,
    password,
    firstname,
    middlename,
    lastname,
    mobile,
    role_id,
    position_id,
    status
  } = {}) {
    if (!email) {
      throw new Error("Please enter email.");
    }
    if (isValidEmail(email) === false) {
      throw new Error("Not a valid email format.");
    }
    // if (!password) {
    //   throw new Error("Please enter password.");
    // }
    if (!firstname) {
      throw new Error("Please enter first name.");
    }
    if (!lastname) {
      throw new Error("Please enter last name.");
    }
    // if (!middlename) {
    //   throw new Error("Please enter middle name.");
    // }
    // if (!mobile) {
    //   throw new Error("Please enter mobile.");
    // }
    if (!role_id) {
      throw new Error("Please select a role.");
    }
    if (!position_id) {
      throw new Error("Please select a position.");
    }
    if (!status) {
      throw new Error("Please enter a status.");
    }
    return Object.freeze({
      getEmail: () => encrypt(email),
      getPassword: () => encrypt(password),
      getFirstName: () => encrypt(firstname),
      getMiddleName: () => encrypt(middlename),
      getLastName: () => encrypt(lastname),
      getMobile: () => MobileValidation(mobile),
      getRole: () => role_id,
      getPosition: () => position_id,
      getStatus: () => status
    });
  };
};

module.exports = adminEditUserEntity;
