const applicantLoginEntity = ({ isValidEmail, encrypt, getIp, makeToken }) => {
  return function loginUserEntity({
    email,
    password,
    ip = getIp(),
    token = makeToken(encrypt(email), encrypt(password))
  } = {}) {
    if (!email) {
      throw new Error("Please enter email.");
    }
    if (isValidEmail(email) === false) {
      throw new Error("Not a valid email format.");
    }
    if (!password) {
      throw new Error("Please enter password.");
    }

    return Object.freeze({
      getEmail: () => encrypt(email),
      getPassword: () => password,
      getIP: () => ip,
      getToken: () => token
    });
  };
};

module.exports = applicantLoginEntity;
