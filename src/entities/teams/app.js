const makeTeamEntity = require("./teams-entity");

const makeTeam = makeTeamEntity({});

module.exports = makeTeam;
