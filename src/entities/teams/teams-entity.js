const makeTeamEntity = ({}) => {
  return function make({
    name,
    description,
    department_id,
    status,
    no_of_items
  } = {}) {
    if (!name) {
      throw new Error("Please enter team name.");
    }
    if (!department_id) {
      throw new Error("Please enter which department the team belongs.");
    }
    if (!no_of_items) {
      throw new Error("Please enter the number of items for the online exam.");
    }
    return Object.freeze({
      getName: () => name,
      getDescription: () => description,
      getDepartment: () => department_id,
      getStatus: () => status,
      getNoItems: () => no_of_items
    });
  };
};

module.exports = makeTeamEntity;
