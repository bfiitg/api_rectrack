const makeActionEntity =({})=>{
    return function make ({
        action_name,
        module_id,
        status
    } = {}){
        if (!action_name){
            throw new Error("Please enter action name.")
        }
        if(!module_id){
            throw new Error("Please select module.")
        }
        if (!status){
            throw new Error("Please select status.")
        }
        return Object.freeze({
            getAction_name:()=>action_name,
            getModule_id:() => module_id,
            getStatus:()=> status
        });
    };
};

module.exports=makeActionEntity