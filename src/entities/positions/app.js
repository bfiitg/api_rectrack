const makePositionEntity = require("./positions-entity");

const makePosition = makePositionEntity({});

module.exports = makePosition;
