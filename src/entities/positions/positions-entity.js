const makePositionEntity = ({}) => {
  return function make({
    name,
    description,
    teams_id,
    is_office_staff,
    is_vacant = "t",
    status
  } = {}) {
    if (!name) {
      throw new Error("Please enter position name.");
    }
    if (!teams_id) {
      throw new Error("Please enter which team the position belongs.");
    }
    if (!is_office_staff) {
      throw new Error("Please enter if position is office staff or not.");
    }
    return Object.freeze({
      getName: () => name,
      getDescription: () => description,
      getTeam: () => teams_id,
      getOfficeStaff: () => is_office_staff,
      getVacant: () => is_vacant,
      getStatus: () => status
    });
  };
};

module.exports = makePositionEntity;
