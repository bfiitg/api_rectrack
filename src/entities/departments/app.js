const makeDepartmentEntity = require("./departments-entity");

const makeDepartment = makeDepartmentEntity({});

module.exports = makeDepartment;
