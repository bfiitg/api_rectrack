const makeDepartmentEntity = ({}) => {
  return function make({ name, description, user_id, status } = {}) {
    if (!name) {
      throw new Error("Please enter department name.");
    }
    // remove this because some department don't have dept head yet so this is optional
    // if (!user_id) {
    //   throw new Error("Please enter department head.");
    // }
    return Object.freeze({
      getName: () => name,
      getDescription: () => description,
      getUser: () => user_id,
      getStatus: () => status
    });
  };
};

module.exports = makeDepartmentEntity;
