const makeQuestionEntityImport = ({ importQuestions }) => {
  return function make({
    categories_id,
    question_type_id,
    path,
    data = importQuestions(path),
  
  } = {}) {
    if (!categories_id) {
      throw new Error("Please enter the category.");
    }
    if(!question_type_id){
      throw new Error("Please enter question type")
    }
    if (!path) {
      throw new Error("Please enter the file path.");
    }
  
    return Object.freeze({
      getCategories: () => categories_id,
      getQuestionType: () => question_type_id,
      getData: () => data,
      
    });
  };
};

module.exports = makeQuestionEntityImport;
