const makeQuestionEntity = ({ toLowerCased }) => {
  return function make({
    questions,
    correct_answer,
    categories_id,
    question_type_id,
    details
  } = {}) {
    if (!questions) {
      throw new Error("Please enter a question.");
    }
    if (!categories_id) {
      throw new Error("Please enter the category.");
    }
    if (!correct_answer) {
      throw new Error("Please enter the correct answer.");
    }
    if(!question_type_id){
      throw new Error("Please enter question type")
    }
    if (details.length === 0) {
      //check if no choices entered
      throw new Error("Please enter the choices of the question.");
    }
  
    
    return Object.freeze({
      getQuestion: () => questions,
      getCorrectAnswer: () => toLowerCased(correct_answer),
      getCategories: () => categories_id,
      getQuestionType: ()=> question_type_id,
      getDetails: () => details
    });
  };
};

module.exports = makeQuestionEntity;
