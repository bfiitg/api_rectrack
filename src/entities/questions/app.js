const changeCase = require("change-case");

const makeQuestionEntity = require("./questions-with-choices-entity");
const makeQuestionEntityImport = require("./questions-import");
const makeQuestionEssayEntity = require("./questions-essay");
const makeQuestionIQEntity = require("./questions-with-with-choices-IQ")

const fs = require("fs");
// for importing excel file
const parser = new (require("simple-excel-to-json").XlsParser)();
const option = {
  isNested: true // if nested json or not; true
};
// end

const toLowerCased = text => {
  return changeCase.lowerCase(text);
};

const importQuestions = async path => {
  const doc = parser.parseXls2Json(path, option);
  const data = doc[0]; // get all data in the first sheet of excel file
  let question = []; // empty array to hold the data
  try {
    for await (let e of data) {
      if (e.question !== "") {
        question.push(e);
      } else {
        // get element position and store in x
        if (e.choices[0].choice !== "") {
          const x = question.length - 1;
          question[x].choices.push(e.choices[0]);
        }
      }
    }
  } catch (e) {
    console.log("Error: ", e);
  }
  return question;
};

const makeQuestions = makeQuestionEntity({ toLowerCased });
const makeQuestionEntityImports = makeQuestionEntityImport({importQuestions});
const makeQuestionEssayEntities = makeQuestionEssayEntity({});
const makeQuestionIQ = makeQuestionIQEntity({toLowerCased}) ;

module.exports = {
  makeQuestions,
  makeQuestionEntityImports,
  toLowerCased,
  makeQuestionEssayEntities,
  makeQuestionIQ
};
