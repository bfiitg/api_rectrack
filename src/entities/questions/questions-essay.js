const makeQuestionEssayEntity = ({ }) => {
  return function make({
    questions,
    categories_id,
    question_type_id,
  } = {}) {
    if (!questions) {
      throw new Error("Please enter a question.");
    }
    if (!categories_id) {
      throw new Error("Please enter the category.");
    }
  
    if (!question_type_id){
      throw new Error("Please enter question type")
    }
 
    
    return Object.freeze({
      getQuestion: () => questions,
      getCategories: () => categories_id,
      getQuestionType: () => question_type_id,
    });
  };
};

module.exports = makeQuestionEssayEntity;
