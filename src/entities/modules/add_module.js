const makeModuleEntity =({})=>{
  return function make ({
      module_name,
      status
  } = {}){
      if (!module_name){
          throw new Error("Please enter module name.")
      }
      if (!status){
          throw new Error("Please select status.")
      }
      return Object.freeze({
          getModule_name:()=>module_name,
          getStatus:()=> status
      });
  };
};

module.exports=makeModuleEntity