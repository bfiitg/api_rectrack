const makeRoleEntity = require("./roles-entity");

const makeRole = makeRoleEntity({});

module.exports = makeRole;
