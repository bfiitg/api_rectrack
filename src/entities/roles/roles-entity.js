const makeRoleEntity = ({}) => {
  return function makeRole({ name, description, access_rights_id ,status = "active" } = {}) {
    if (!name) {
      throw new Error("Please enter role name.");
    }

    return Object.freeze({
      getName: () => name,
      getDescription: () => description,
      getStatus: () => status,
    });
  };
};

module.exports = makeRoleEntity;
