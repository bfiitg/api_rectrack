const makeInterviewAssessment = ({}) => {
  return function make({
    user_id_applicant,
    date_interview,
    communication_skills,
    communication_skills_note,
    confidence,
    confidence_note,
    physical_appearance,
    physical_appearance_note,
    knowledge_skills,
    knowledge_skills_note,
    asking_rate,
    availability,
    others,
    gen_remarks_recommendations,
    user_id_interviewer
  } = {}) {
    if (!user_id_applicant) {
      throw new Error("Please enter applicant id.");
    }
    // if (!date_interview) {
    //   throw new Error("Please enter date of interview conducted.");
    // }
    if (!communication_skills) {
      throw new Error("Please enter communication skills rate.");
    }
    if (!confidence) {
      throw new Error("Please enter confidence rate.");
    }
    if (!physical_appearance) {
      throw new Error("Please enter physical appearance rate.");
    }
    if (!knowledge_skills) {
      throw new Error("Please enter knowledge skills rate.");
    }
    if (!asking_rate) {
      throw new Error("Please enter asking rate.");
    }
    if (!availability) {
      throw new Error("Please enter availability.");
    }
    if (!gen_remarks_recommendations) {
      throw new Error("Please enter general remarks and recommendations.");
    }
    if (!user_id_interviewer) {
      throw new Error("Please enter who conducted the interview.");
    }

    return Object.freeze({
      getUserIdApplicant: () => user_id_applicant,
      getDateInterview: () => date_interview,
      getCommunicationSkills: () => communication_skills,
      getCommunicationSkillsNote: () => communication_skills_note,
      getConfidence: () => confidence,
      getConfidenceNote: () => confidence_note,
      getPhysicalAppearance: () => physical_appearance,
      getPhysicalAppearanceNote: () => physical_appearance_note,
      getKnowledge: () => knowledge_skills,
      getKnowledgeNote: () => knowledge_skills_note,
      getAskingRate: () => asking_rate,
      getAvailability: () => availability,
      getOthers: () => others,
      getRemarks: () => gen_remarks_recommendations,
      getUserIdInterviewer: () => user_id_interviewer
    });
  };
};

module.exports = makeInterviewAssessment;
