const makeInterviewAssessment = require("./interview-assessment-entity");
const makeInterviewAssessmentEdit = require("./interview-assessment-entity-edit");

const makeInterviewAssessments = makeInterviewAssessment({});
const makeInterviewAssessmentEdits = makeInterviewAssessmentEdit({});

module.exports = { makeInterviewAssessments, makeInterviewAssessmentEdits };
