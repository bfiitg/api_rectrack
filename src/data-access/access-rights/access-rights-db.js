const db = ({ dbs }) => {
    return Object.freeze({
        selectAll,
        select,
        insert,
        findByDiscription,
        checkUpdate,
        update,
        checkUpdatetoDelete,
        checkAction,
        updateActions
    })
    async function selectAll() {
        const db = await dbs();
        const sql = `SELECT *
                    FROM access_rights 
                    `;
        return db.query(sql)
    }

    async function select(info) {
        const db = await dbs();
        const sql = `SELECT *
                    FROM access_rights 
                    WHERE id = $1;
                    `;
        const params = [
            info.id
        ];
        return db.query(sql, params)
    }

    async function insert(info) {
        const db = await dbs();
        const sql =
            `INSERT INTO access_rights
            VALUES (DEFAULT, $1,NOW(), NOW(), $2, $3);`
        const params = [
            info.discription,
            info.status,
            info.actions,
        ];
        return db.query(sql, params)
    }
    async function checkAction(info) {
        const db = await dbs();
        const sql =
            `INSERT INTO access_rights
            VALUES (DEFAULT, $1,NOW(), NOW(), $2, $3);`
        const params = [
            info.discription,
            info.status,
            info.actions,
        ];
        return db.query(sql, params)
    }

    async function findByDiscription(info) {
        const db = await dbs();
        const sql = `SELECT * FROM access_rights WHERE discription=$1;`;
        const params = [
            info
        ];
        return db.query(sql, params)
    }

    async function checkUpdate(info) {
        console.log(info)
        const db = await dbs();
        const sql = `SELECT id from actions where action_name = $1 and module_id=$2`;
        const params = [
            info.action_name,
            info.module_id
        ];
        return db.query(sql, params)
    }
    async function checkUpdatetoDelete(id) {
        const db = await dbs();
        const sql = `SELECT * FROM access_rights WHERE id=$1;`;
        const params = [
            id
        ];
        return db.query(sql, params)
    }
    async function update(id, info) {
        const db = await dbs();
        const sql = `
        UPDATE access_rights
            SET discription=$1, actions=$2, time_updated=NOW(), status=$3
            WHERE id=$4;`;
        const params = [
            info.discription,
            info.actions,
            info.status,
            id
        ];
        return db.query(sql, params)
    }
    async function updateActions(actionUpdate, id) {
        const db = await dbs();
        const sql = `  UPDATE access_rights
            SET actions='${actionUpdate}', time_updated=NOW()
            WHERE id='${id}' ;`;
        return db.query(sql)
    }
}
module.exports = db