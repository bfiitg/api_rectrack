const makeAccessRightsDB = require("../db")
const db = require("./access-rights-db")

const accessRightsDb = makeAccessRightsDB({db})

module.exports = accessRightsDb;