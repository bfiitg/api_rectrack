const makeActionEntity = require("../../entities/actions/action-entity");
const makeDB = require("../db")
const db = require("./actions-db");

const actionsDb = makeDB({db})

module.exports=actionsDb;