const db =({dbs}) => {
    return Object.freeze({
        selectAll,
        select,
        insert,
        check,
        update,
        checkupdate
    })
    async function selectAll(){
        const db = await dbs();
        const sql = `SELECT 
        a.id,
        a.action_name,
        a.status,
        a.module_id,
        m.module_name
      FROM actions a
      LEFT JOIN modules m ON m.id=a.module_id ORDER BY action_name ASC`;
        return db.query(sql)
    }

    async function select(id){
        const db = await dbs();
        const sql = "SELECT * FROM actions WHERE id=$1;";
        const params = [
            id
          ];
          return db.query(sql, params)
    }

    async function insert(info) {
        const db = await dbs();
        const sql =
          "INSERT INTO actions VALUES (DEFAULT,$1,NOW(),NOW(),$2,$3);"
        const params = [
          info.action_name,
          info.status,
          info.module_id
        ];
        return db.query(sql, params)
    }   

    async function check(info) {
        const db = await dbs();
        const sql =
         "SELECT * FROM actions WHERE action_name = $1"
        const params = [
          info,
        ];
        return db.query(sql, params)
    }

    async function checkupdate(info) {
        const db = await dbs();
        const sql =
         "SELECT * FROM actions WHERE action_name = $1 and status= $2 and module_id=$3"
        const params = [
            info.action_name,
            info.status,
            info.module_id
        ];
        return db.query(sql, params)
    }

    async function update(id,info) {
        const db = await dbs();
        const sql =
          `UPDATE actions 
          SET action_name=$1, module_id=$2 ,time_updated=NOW(), status=$3 WHERE id=$4;`
        const params = [
          info.action_name,
          info.module_id,
          info.status,
          id
        ];
        return db.query(sql, params)
    }   
}
module.exports = db;