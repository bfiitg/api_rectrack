const db = ({ dbs }) => {
  return Object.freeze({
    insertWithChoices,
    displayUsersRandomQuestions,
    displayUsersRandomQuestionsChoices,
    checkifCategoryIsDone,
    getUserId,
    getTokenOfUser,
    changeToIsDone,
    getCorrectAnswerPerQuestion,
    ifAnswerIsCorrect,
    ifAnswerIsWrong,
    insertImportQuestions,
    selectSingleQuestion,
    updateSelectedQuestion,
    getIdFromCorrectAnswer,
    displayQuestions,
    displayUsersRandomQuestionsIQBE,
    //butch part
    insertQuestionEssay,
    ifAnswerIsNoChoices,
    displayQuestionsIQtest,
    insertWithChoicesIQ
   
  });
  async function displayQuestions(teamId, deptId, catgId) {
    const db = await dbs();
    const sql =
      `SELECT q.id,q.questions,q.correct_answer,qc.choices_or_imagepath, qt.question_type FROM questions q 
			    LEFT JOIN questions_choices qc ON qc.questions_id = q.id
			    JOIN categories c ON c.id = q.categories_id 
          JOIN teams t ON t.id = c.teams_id 
			    JOIN departments d ON d.id = t.department_id
			    JOIN questions_type qt ON qt.id=q.question_type_id
          WHERE t.id = $1 AND d.id = $2 AND c.id = $3 ORDER BY q.id DESC`
    const params = [teamId, deptId, catgId];
    return db.query(sql, params);
  }

  //
  async function insertWithChoicesIQ({ ...info }) {
    const db = await dbs();
    const sql =
      `INSERT INTO questions 
      (id,questions,time_created,time_updated,correct_answer,question_type_id)
       VALUES (DEFAULT,$1,NOW(),NOW(),$2,$3);`;
    const params = [
      info.questions,
      info.correct_answer,
      info.question_type_id
    ];
    await db.query(sql, params);
    const sql2 = "SELECT id FROM questions ORDER BY id DESC LIMIT 1;";
    const result = await db.query(sql2);
    const id = result.rows[0].id;
    info.details.forEach(function(e) {
      const sql = "INSERT INTO questions_choices VALUES (DEFAULT,$1,$2)";
      const params = [id, e];
      db.query(sql, params);
    });
    const sql3 =
      "SELECT q.questions,q.categories_id,qc.choices_or_imagepath " +
      "FROM questions q JOIN questions_choices qc ON qc.questions_id = q.id WHERE q.id = $1;";
    const params3 = [id];
    await db.query(sql3, params3);
    const msg = {
      msg: "Inserted successfully.."
    };
    return msg;
  }


  //
  async function insertWithChoices({ ...info }) {
    const db = await dbs();
    const sql =
      "INSERT INTO questions VALUES (DEFAULT,$1,NOW(),NOW(),$2,$3,$4);";
    const params = [
      info.questions,
      info.correct_answer,
      info.categories_id,
      info.question_type_id
    ];
    await db.query(sql, params);
    const sql2 = "SELECT id FROM questions ORDER BY id DESC LIMIT 1;";
    const result = await db.query(sql2);
    const id = result.rows[0].id;
    info.details.forEach(function(e) {
      const sql = "INSERT INTO questions_choices VALUES (DEFAULT,$1,$2)";
      const params = [id, e];
      db.query(sql, params);
    });
    const sql3 =
      "SELECT q.questions,q.categories_id,qc.choices_or_imagepath " +
      "FROM questions q JOIN questions_choices qc ON qc.questions_id = q.id WHERE q.id = $1;";
    const params3 = [id];
    await db.query(sql3, params3);
    const msg = {
      msg: "Inserted successfully.."
    };
    return msg;
  }

  /* TEST REMOVE
  async function insertTrueFalse({ ...info }) {
    const db = await dbs();
    const sql = "INSERT INTO questions VALUES (DEFAULT,$1,NOW(),NOW(),$2,$3);";
    const params = [info.questions, info.correct_answer, info.categories_id];
    await db.query(sql, params);
    const msg = {
      msg: "Inserted successfully.."
    };
    return msg;
  }*/

  // online exam
  async function displayUsersRandomQuestions(categoryId) {
    const db = await dbs();
    const sql =
      `SELECT q.id as question_id, q.questions, q.correct_answer, qt.question_type
      FROM questions q LEFT JOIN questions_type qt ON qt.id = q.question_type_id
      WHERE categories_id = $1 order by random();`;
    const params = [categoryId];
    return db.query(sql, params);
  }
  async function displayUsersRandomQuestionsChoices(questionId) {
    const db = await dbs();
    const sql =
      "SELECT id,choices_or_imagepath FROM questions_choices WHERE questions_id = $1 ORDER BY random();";
    const params = [questionId];
    return db.query(sql, params);
  }
  // for IQBE
  async function displayUsersRandomQuestionsIQBE(categoryId, token) {
    const db = await dbs();
    const sql =
     `SELECT us.question_id, q.questions, q.correct_answer, qt.question_type 
     FROM users_scores us JOIN questions q ON q.id = us.question_id JOIN users u 
     ON u.id = us.user_id LEFT JOIN questions_type qt ON qt.id = q.question_type_id 
     WHERE u.token = $2 AND q.categories_id = $1 ORDER BY random()`;
    const params = [categoryId, token];
    return db.query(sql, params);
  }
  async function checkifCategoryIsDone(categoryId) {
    const db = await dbs();
    const sql =
      "SELECT is_done FROM categories_random WHERE category_id = $1 ORDER BY id DESC LIMIT 1;";
    const params = [categoryId];
    return db.query(sql, params);
  }
  async function getUserId(email, password) {
    const db = await dbs();
    const sql = "SELECT id FROM users WHERE email = $1 AND password = $2";
    const params = [email, password];
    return db.query(sql, params);
  }
  async function getTokenOfUser(email, password) {
    const db = await dbs();
    const sql = "SELECT token FROM users WHERE email = $1 AND password = $2";
    const params = [email, password];
    return db.query(sql, params);
  }
  async function changeToIsDone(userId, categoryId) {
    const db = await dbs();
    const sql =
      "UPDATE categories_random SET is_done = 'yes', time_updated = NOW() WHERE category_id = $2 AND user_id = $1;";
    const params = [userId, categoryId];
    return db.query(sql, params);
  }
  async function getCorrectAnswerPerQuestion(questionId) {
    const db = await dbs();
    const sql = "SELECT correct_answer FROM questions WHERE id = $1;";
    const params = [questionId];
    return db.query(sql, params);
  }
  async function getIdFromCorrectAnswer(answer) {
    const db = await dbs();
    const sql =
      "SELECT id FROM questions_choices WHERE LOWER(choices_or_imagepath) = LOWER($1);";
    const params = [answer];
    return db.query(sql, params);
  }
  async function ifAnswerIsCorrect(questionId, userId, answer) {
    const db = await dbs();
    const sql =
      "UPDATE users_scores SET user_answer=$3, is_correct='yes' WHERE question_id=$1 AND user_id=$2";
    const params = [questionId, userId, answer];
    return db.query(sql, params);
  }

  // async function ifAnswerIsNoChoices(questionId, userId, answer) {
  //   const db = await dbs();
  //   const sql =
  //     "UPDATE users_scores SET user_answer=$3 WHERE question_id=$1 AND user_id=$2";
  //   const params = [questionId, userId, answer];
  //   return db.query(sql, params);
  // }

  async function ifAnswerIsWrong(questionId, userId, answer) {
    const db = await dbs();
    const sql =
      "UPDATE users_scores SET user_answer=$3, is_correct='no' WHERE question_id=$1 AND user_id=$2";
    const params = [questionId, userId, answer];
    return db.query(sql, params);
  }

  // for importing questions thru excel
  async function insertImportQuestions({ ...info }) {
    try {
      const db = await dbs();
      const details = info.details;
      // count how many data inserted
      let insertedDataCount = 0;
      
      const categories_id = info.categories_id;
      const question_type_id = info.question_type_id;
      for (let i = 0; i < details.length; i++) {
        const question = details[i].question;
        const correct_answer = details[i].correct_answer;
        // insert data
        const sql =
          "INSERT INTO questions VALUES (DEFAULT,$1,NOW(),NOW(),$2,$3,$4);";
        const params = [
          question,
          correct_answer,
          categories_id,
          question_type_id
        ];
        await db.query(sql, params);
        insertedDataCount++;

        // select latest id from db
        const sql2 = "SELECT id FROM questions ORDER BY id DESC LIMIT 1;";
        const result = await db.query(sql2);
        const id = result.rows[0].id;

        const choices = details[i].choices;
        if (choices !== undefined) {
          for (let x = 0; x < choices.length; x++) {
            const choice = choices[x].choice;
            const sql = "INSERT INTO questions_choices VALUES (DEFAULT,$1,$2)";
            const params = [id, choice + "@"];
            await db.query(sql, params);
          }
        }
      }
      const question = {
        msg: "Imported successfully..",
        dataInserted: insertedDataCount
      };
      return question;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
  async function selectSingleQuestion(categoryId) {
    try {
      const db = await dbs();
      const sql =
        "SELECT q.id,q.categories_id,q.questions,q.correct_answer,qc.choices_or_imagepath, q.question_type_id FROM  " +
        "questions q LEFT JOIN questions_choices qc ON qc.questions_id = q.id WHERE q.id = $1 " +
        "ORDER BY q.id;";
      const params = [categoryId];
      return db.query(sql, params);
    } catch (e) {
      console.log("Error: ", e);
    }
  }
  async function updateSelectedQuestion({ id, ...info }) {
    try {
      const db = await dbs();
      // update
      const sql =
        "UPDATE questions SET time_updated=NOW(), questions=$1, correct_answer=$2, categories_id=$3  WHERE " +
        "id=$4;";
      const params = [
        info.questions,
        info.correct_answer,
        info.categories_id,
        id
      ];
      await db.query(sql, params);
      // delete choices
      const sql2 = "DELETE FROM questions_choices WHERE questions_id=$1;";
      const params2 = [id];
      await db.query(sql2, params2);
      // insert new choices if not true or false
      if (info.correct_answer !== "true" && info.correct_answer !== "false") {
        info.details.forEach(function(e) {
          const sql = "INSERT INTO questions_choices VALUES (DEFAULT,$1,$2)";
          const params = [id, e];
          db.query(sql, params);
        });
      }
      const sql3 =
        "SELECT q.questions,q.correct_answer,q.categories_id,qc.choices_or_imagepath " +
        "FROM questions q LEFT JOIN questions_choices qc ON qc.questions_id = q.id WHERE q.id = $1;";
      const params3 = [id];
      const result3 = await db.query(sql3, params3);
      const msg = {
        msg: "Updated successfully..",
        data: result3.rows
      };
      return msg;
    } catch (e) {
      console.log("Error: ", e);
    }
  }
  //butch part
  //insert Essay question
  async function insertQuestionEssay(...info) {
    try{
    const db = await dbs();
    const sql =
      "INSERT INTO questions (questions,categories_id,question_type_id,time_created,time_updated) " +
      "VALUES ($1,$2,$3,NOW(),NOW());";
    const params = [info[0].questions, info[0].categories_id, info[0].question_type_id];
    const result = await db.query(sql,params)
    const msg = {
      msg: "Inserted successfully..",
    };
    return msg;
  }catch(e){
    console.log('Error: ', e);
  }
}

async function ifAnswerIsNoChoices(questionId, userId, answer) {
  const db = await dbs();
  const sql =
    "UPDATE users_scores SET user_answer=$3, is_correct=0 WHERE question_id=$1 AND user_id=$2";
  const params = [questionId, userId, answer];
  return db.query(sql, params);
}

async function displayQuestionsIQtest() {
  const db = await dbs();
  const sql =
    `SELECT q.id,q.questions,q.correct_answer,qc.choices_or_imagepath, qt.question_type FROM questions q 
        LEFT JOIN questions_choices qc ON qc.questions_id = q.id
        JOIN categories c ON c.id = q.categories_id 
        JOIN teams t ON t.id = c.teams_id 
        JOIN departments d ON d.id = t.department_id
        JOIN questions_type qt ON qt.id=q.question_type_id
        WHERE c.name='IQ Test' ORDER BY q.id DESC`
  return db.query(sql);
}
   
};

module.exports = db;
