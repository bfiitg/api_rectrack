const makeDb = require("../db");
const db = require("./questions-db");
const questionsDb = makeDb({ db });

module.exports = questionsDb;
