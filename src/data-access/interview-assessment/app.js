const makeDb = require("../db");
const db = require("./interview-assessment-db");

const interviewDb = makeDb({ db });

module.exports = interviewDb;
