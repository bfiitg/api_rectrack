const db = ({ dbs }) => {
  return Object.freeze({
    insert,
    updates,
    viewAssessmentOfEmployee,
    findByApplicantId,
    selectAll,
    checkIAFrated,
    getApplicantRaters,
    getRatebyRater
  });
  async function getApplicantRaters(interviewInfo){
    const db = await dbs();
    const sql = "SELECT COUNT * FROM interview_assessments WHERE user_id_interviewer = $1 AND user_id_applicant = $2;";
    const params = [interviewInfo.user_id_interviewer, interviewInfo.user_id_applicant];
    return db.query(sql, params);
  } 

  async function checkIAFrated(interviewInfo){
    const db = await dbs();
    const sql = "SELECT COUNT(*) FROM interview_assessments WHERE user_id_interviewer = $1 AND user_id_applicant = $2;";
    const params = [interviewInfo.user_id_interviewer, interviewInfo.user_id_applicant];
    return db.query(sql, params);
  }
  

  async function insert({ ...info }) {
    const db = await dbs();
    const sql =
      "INSERT INTO interview_assessments VALUES (DEFAULT,$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,NOW(),NOW());";
    const params = [
      info.user_id_applicant,
      info.date_interview,
      info.communication_skills,
      info.communication_skills_note,
      info.confidence,
      info.confidence_note,
      info.physical_appearance,
      info.physical_appearance_note,
      info.knowledge_skills,
      info.knowledge_skills_note,
      info.asking_rate,
      info.availability,
      info.others,
      info.gen_remarks_recommendations,
      info.user_id_interviewer
    ];
    return db.query(sql, params);
  }

  async function updates({ id, ...info }) {
    const db = await dbs();
    const sql =
      "UPDATE interview_assessments SET date_interview=$2, " +
      "communication_skills=$3, communication_skills_note=$4, confidence=$5, confidence_note=$6, " +
      "physical_appearance=$7, physical_appearance_note=$8, knowledge_skills=$9, " +
      "knowledge_skills_note=$10, asking_rate=$11, availability=$12, others=$13, " +
      "gen_remarks_recommendations=$14, user_id_interviewer=$15, time_updated=NOW() " +
      "WHERE user_id_applicant=$1;";
    const params = [
      id,
      info.date_interview,
      info.communication_skills,
      info.communication_skills_note,
      info.confidence,
      info.confidence_note,
      info.physical_appearance,
      info.physical_appearance_note,
      info.knowledge_skills,
      info.knowledge_skills_note,
      info.asking_rate,
      info.availability,
      info.others,
      info.gen_remarks_recommendations,
      info.user_id_interviewer
    ];
    return db.query(sql, params);
  }
  async function viewAssessmentOfEmployee(id) {
    const db = await dbs();
    const sql =
      `SELECT A
      .ID,
      A.user_id_interviewer,
      b.applicantFn,
      b.applicantLn,
      C.interviewerFn,
      C.interviewerLn,
      A.date_interview,
      A.communication_skills,
      A.communication_skills_note,
      A.confidence,
      A.confidence_note,
      A.physical_appearance,
      A.physical_appearance_note,
      A.knowledge_skills,
      A.knowledge_skills_note,
      A.asking_rate,
      A.availability,
      A.OTHERS,
      A.gen_remarks_recommendations
    FROM
      interview_assessments A JOIN (
      SELECT
        i.ID,
        u.firstname AS applicantFn,
        u.lastname AS applicantLn
      FROM
        interview_assessments i
        JOIN users u ON u.ID = i.user_id_applicant 
      ) b ON b.ID = A.
      ID JOIN (
      SELECT
        i.ID,
        u.firstname AS interviewerFn,
        u.lastname AS interviewerLn 
      FROM
        interview_assessments i
        JOIN users u ON u.ID = i.user_id_interviewer 
      ) C ON C.ID = A.ID 
    WHERE
      A.user_id_applicant = $1;`
    const params = [id];
    return db.query(sql, params);
  }
  async function findByApplicantId(userIdApplicant) {
    const db = await dbs();
    const sql =
      "SELECT * FROM interview_assessments WHERE user_id_applicant=$1;";
    const params = [userIdApplicant];
    return db.query(sql, params);
  }
  async function selectAll() {
    const db = await dbs();
    const sql =
      "SELECT a.id,b.applicantFn,b.applicantLn,c.interviewerFn,c.interviewerLn,a.date_interview FROM " +
      "( SELECT * FROM interview_assessments )a JOIN ( SELECT i.id,u.firstname AS applicantFn, u.lastname AS applicantLn " +
      "FROM interview_assessments i JOIN users u ON u.id = i.user_id_applicant )b ON b.id = a.id JOIN ( " +
      "SELECT i.id,u.firstname AS interviewerFn, u.lastname AS interviewerLn FROM interview_assessments i JOIN users u ON u.id = i.user_id_interviewer " +
      ")c ON c.id = a.id;";
    return db.query(sql);
  }

  async function getRatebyRater(interviewInfo) {
    const db = await dbs();
    const sql =
      "SELECT a.id,b.applicantFn,b.applicantLn,c.interviewerFn,c.interviewerLn,a.date_interview,a.communication_skills,a.communication_skills_note,a.confidence,a.confidence_note, " +
      "a.physical_appearance,a.physical_appearance_note,a.knowledge_skills,a.knowledge_skills_note,a.asking_rate,a.availability,a.others, " +
      "a.gen_remarks_recommendations FROM ( SELECT * FROM interview_assessments )a JOIN ( " +
      "SELECT i.id,u.firstname AS applicantFn, u.lastname AS applicantLn FROM interview_assessments i JOIN users u ON u.id = i.user_id_applicant " +
      ")b ON b.id = a.id JOIN ( SELECT i.id,u.firstname AS interviewerFn, u.lastname AS interviewerLn FROM interview_assessments i JOIN users u ON u.id = i.user_id_interviewer " +
      ")c ON c.id = a.id WHERE a.user_id_applicant = $1 and a.user_id_interviewer = $2;";
    const params = [interviewInfo.user_id_applicant, interviewInfo.user_id_interviewer];
    return db.query(sql, params);
  }
};

module.exports = db;
