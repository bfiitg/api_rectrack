const db = ({ dbs }) => {
  return Object.freeze({
    insert,
    findByName,
    selectAllRoles,
    selectAllRoleWithAccess,
    selectOneRole,
    selectOneRoleWithAccess,
    editRole,
    findByNameUpdate
  });
  async function findByNameUpdate(name, id) {
    const db = await dbs();
    const sql = "SELECT * FROM roles WHERE name=$1 AND id <> $2;";
    const params = [name, id];
    return db.query(sql, params);
  }
  async function editRole({ id, ...rolesInfo }) {
    const db = await dbs();
    const sql =
      "UPDATE roles SET name = $1, description = $2, time_updated = NOW(), status = $3 " +
      "WHERE id = $4;";
    const params = [
      rolesInfo.name,
      rolesInfo.description,
      rolesInfo.status,
      id
    ];
    return db.query(sql, params);
  }
  async function selectOneRole(id) {
    const db = await dbs();
    const sql = `select * from roles where id=$1 and status='active'`
    const params = [id];
    return db.query(sql, params);
  }

  async function selectOneRoleWithAccess(id) {
    const db = await dbs();
    const sql = `SELECT 
                  r.id,
                  r.name,
                  r.description,
                  r.time_created,
                  r.time_updated,
                  r.status,
                  ar.id AS arId,
                  ar.discription AS arDisc
                FROM 
                roles r 
                JOIN access_rights ar ON r.access_rights_id = ar.id 
                WHERE r.id = $1 AND r.status='active'`;
    const params = [id];
    return db.query(sql, params);
  } 
  async function insert({ ...rolesInfo }) {
    const db = await dbs();
    const sql = "INSERT INTO roles(id, name, description, status, access_rights_id, time_created, time_updated) VALUES (DEFAULT,$1,$2,$3,$4,NOW(),NOW());";
    const params = [rolesInfo.name, rolesInfo.description, rolesInfo.status,rolesInfo.access_rights_id];
    return db.query(sql, params);
  }
  async function findByName(name) {
    const db = await dbs();
    const sql = "SELECT * FROM roles WHERE name=$1;";
    const params = [name];
    return db.query(sql, params);
  }
  async function selectAllRoles() {
    const db = await dbs();
    const sql =
      "SELECT id,name,description FROM roles WHERE status='active' ORDER BY name;";
    return db.query(sql);
  }
  async function selectAllRoleWithAccess() {
    const db = await dbs();
    const sql = `SELECT 
                  r.id,
                  r.name,
                  r.description,
                  r.time_created,
                  r.time_updated,
                  r.status,
                  ar.id AS arId,
                  ar.discription AS arDisc
                FROM 
                roles r 
                JOIN access_rights ar ON r.access_rights_id = ar.id 
                WHERE  r.status='active'`;
    return db.query(sql);
  } 
};

module.exports = db;
