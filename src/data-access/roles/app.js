const makeDb = require("../db");
const db = require("./roles-db");

const rolesDb = makeDb({ db });

module.exports = rolesDb;
