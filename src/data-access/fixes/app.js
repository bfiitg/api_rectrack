const makeDb = require('../db');
const db = require('./fix');

const fixesDb = makeDb({ db });

module.exports = fixesDb;