const db = ({ dbs }) => {
  return Object.freeze({
    getAllRejectedWithoutRejectedBy,
    getRejectorFromLogs,
    updateRejectedBy,
    insertUsers,
    getPositionId,
    getApplicantId,
    insertADT,
    deleteTest,
    insertDeployedApplicants,
    returnPositionId,
    returnIdLastApplicantInsert,
    insertRejectedApplicants,
    insertReferenceApplicants,
    insertBlacklistApplicants,
    findByEmail,
    findByFullName,
    //alter table in database
    alterInDataBase,
    alterInDataBase1,
    alterInDataBase2,
    updateCol,
    //fix duplicate
    selectDuplicate,
    selectId,
    deleteDuplicate,
    //delete User
    deleteUser,
    resetOnlineExamRecord,
    
    updateToDeploy,
    updateToOnlineExam
  });
  async function returnIdLastApplicantInsert() {
    const db = await dbs();
    const sql = `SELECT id FROM users ORDER BY id DESC LIMIT 1;`;
    return db.query(sql);
  }
  async function returnPositionId(position) {
    const db = await dbs();
    const sql = `SELECT id FROM positions WHERE LOWER(TRIM(name)) = LOWER(TRIM($1));`;
    const params = [position];
    return db.query(sql, params);
  }
  async function insertDeployedApplicants({ ...info }) {
    const db = await dbs();
    const data = info.data; // get all data of applicant
    const position = data.position; // position name
    const id = await returnPositionId(position); //query position id
    const posId = id.rows[0].id; // position Id
    const sql = `INSERT INTO users (email,firstname,lastname,mobile,livestock,willing_to_travel,position_id,
      resume_upload_date,date_application,resume_upload_url,status) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$8,$9,$10)`;
    const params = [
      data.email,
      data.first_name,
      data.last_name,
      data.mobile,
      data.livestock,
      data.willing_to_travel,
      posId,
      data.date_applied,
      data.resume_url,
      data.status
    ];
    const res = await db.query(sql, params); // insert query

    // insert date deployed to applicant date tracking table
    const date_deployed = data.date_deployed;
    const date_tracking = await returnIdLastApplicantInsert();
    const userId = date_tracking.rows[0].id; // applicant id

    //
    const sql3 = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params3 = [userId];
    const datas = await db.query(sql3, params3);
    if (datas.rowCount > 0) {
      // update
      const appId = datas.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_deployed = $2 " +
        "WHERE id = $1;";
      const params = [appId, date_deployed];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,date_deployed) " +
        "VALUES ($1,$2);";
      const params = [userId, date_deployed];
      await db.query(sql, params);
    }
    //

    return res;
  }
  async function insertRejectedApplicants({ ...info }) {
    const db = await dbs();
    const data = info.data; // get all data of applicant
    const position = data.position; // position name
    const id = await returnPositionId(position); //query position id
    const posId = id.rows[0].id; // position Id
    const sql = `INSERT INTO users (email,firstname,lastname,mobile,livestock,willing_to_travel,position_id,
      resume_upload_date,date_application,resume_upload_url,status) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$8,$9,$10)`;
    const params = [
      data.email,
      data.first_name,
      data.last_name,
      data.mobile,
      data.livestock,
      data.willing_to_travel,
      posId,
      data.date_applied,
      data.resume_url,
      data.status
    ];
    const res = await db.query(sql, params); // insert query

    // insert date deployed to applicant date tracking table
    const date_rejected = data.date_rejected;
    const date_tracking = await returnIdLastApplicantInsert();
    const userId = date_tracking.rows[0].id; // applicant id

    //
    const sql3 = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params3 = [userId];
    const datas = await db.query(sql3, params3);
    if (datas.rowCount > 0) {
      // update
      const appId = datas.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_rejected = $2 " +
        "WHERE id = $1;";
      const params = [appId, date_rejected];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,date_rejected) " +
        "VALUES ($1,$2);";
      const params = [userId, date_rejected];
      await db.query(sql, params);
    }
    //

    return res;
  }
  async function insertReferenceApplicants({ ...info }) {
    const db = await dbs();
    const data = info.data; // get all data of applicant
    const position = data.position; // position name
    const id = await returnPositionId(position); //query position id
    const posId = id.rows[0].id; // position Id
    const sql = `INSERT INTO users (email,firstname,lastname,mobile,livestock,willing_to_travel,position_id,
      resume_upload_date,date_application,resume_upload_url,status) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$8,$9,$10)`;
    const params = [
      data.email,
      data.first_name,
      data.last_name,
      data.mobile,
      data.livestock,
      data.willing_to_travel,
      posId,
      data.date_applied,
      data.resume_url,
      data.status
    ];
    const res = await db.query(sql, params); // insert query

    // insert date deployed to applicant date tracking table
    const date_reference = data.date_reference;
    const date_tracking = await returnIdLastApplicantInsert();
    const userId = date_tracking.rows[0].id; // applicant id

    //
    const sql3 = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params3 = [userId];
    const datas = await db.query(sql3, params3);
    if (datas.rowCount > 0) {
      // update
      const appId = datas.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_kept_reference = $2 " +
        "WHERE id = $1;";
      const params = [appId, date_reference];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,date_kept_reference) " +
        "VALUES ($1,$2);";
      const params = [userId, date_reference];
      await db.query(sql, params);
    }
    //

    return res;
  }
  async function insertBlacklistApplicants({ ...info }) {
    const db = await dbs();
    const data = info.data; // get all data of applicant
    const position = data.position; // position name
    const id = await returnPositionId(position); //query position id
    const posId = id.rows[0].id; // position Id
    const sql = `INSERT INTO users (email,firstname,lastname,mobile,livestock,willing_to_travel,position_id,
      resume_upload_date,date_application,resume_upload_url,status) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$8,$9,$10)`;
    const params = [
      data.email,
      data.first_name,
      data.last_name,
      data.mobile,
      data.livestock,
      data.willing_to_travel,
      posId,
      data.date_applied,
      data.resume_url,
      data.status
    ];
    const res = await db.query(sql, params); // insert query

    // insert date deployed to applicant date tracking table
    const date_blacklist = data.date_blacklist;
    const date_tracking = await returnIdLastApplicantInsert();
    const userId = date_tracking.rows[0].id; // applicant id

    //
    const sql3 = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params3 = [userId];
    const datas = await db.query(sql3, params3);
    if (datas.rowCount > 0) {
      // update
      const appId = datas.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_blacklist = $2 " +
        "WHERE id = $1;";
      const params = [appId, date_blacklist];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,date_blacklist) " +
        "VALUES ($1,$2);";
      const params = [userId, date_blacklist];
      await db.query(sql, params);
    }
    //

    return res;
  }
  // to check if email exist
  async function findByEmail(email) {
    const db = await dbs();
    const sql = "SELECT * FROM users WHERE LOWER(email)=LOWER($1);";
    const params = [email];
    return db.query(sql, params);
  }
  // to check if full name exist
  async function findByFullName(firstname, lastname) {
    const db = await dbs();
    const sql =
      "SELECT * FROM users WHERE LOWER(firstname)=LOWER($1) AND LOWER(lastname)=LOWER($2);";
    const params = [firstname, lastname];
    return db.query(sql, params);
  }
  // ########################
  async function getAllRejectedWithoutRejectedBy() {
    const db = await dbs();
    const sql =
      "SELECT u.firstname, u.lastname, u.middlename, adt.*   " +
      "FROM applicant_date_trackings adt   " +
      "JOIN users u ON adt.users_id = u.id   " +
      "WHERE date_rejected IS NOT NULL AND rejected_by IS NULL";
    return db.query(sql);
  }

  async function getRejectorFromLogs(name) {
    const db = await dbs();
    const sql =
      "SELECT * FROM logs WHERE activity LIKE '%reject%  " + name + "'";
    return db.query(sql);
  }

  async function updateRejectedBy(info) {
    const db = await dbs();
    const sql =
      "UPDATE applicant_date_trackings SET rejected_by = $1 WHERE users_id = $2";
    const params = [info.rejected_by, info.users_id];
    return db.query(sql, params);
  }

  async function insertUsers(info) {
    const db = await dbs();
    const sql =
      "INSERT INTO users (email, firstname, middlename, lastname, mobile, livestock,  " +
      "willing_to_travel, position_id, resume_upload_date, resume_upload_url, status)   " +
      "VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, 'deployed')";
    const params = [
      info.email,
      info.firstname,
      info.middlename,
      info.lastname,
      info.mobile,
      info.livestock,
      info.willing_to_travel,
      info.position_id,
      info.resume_upload_date,
      info.resume_upload_url
    ];
    return db.query(sql, params);
  }

  async function getPositionId(deptname, position) {
    const db = await dbs();
    const sql =
      "SELECT p.id AS position_id, p.name AS position, t.name AS team, d.name as department  " +
      "FROM positions p LEFT JOIN teams t ON p.teams_id = t.id  " +
      "LEFT JOIN departments d ON t.department_id = d.id   " +
      "WHERE LOWER(d.name) = LOWER($1) AND LOWER(p.name) = LOWER($2)";
    const params = [deptname, position];
    return db.query(sql, params);
  }

  async function getApplicantId(info) {
    const db = await dbs();
    const sql =
      "SELECT id FROM users WHERE firstname = $1 AND middlename = $2 AND lastname = $3 AND email = $4";
    const params = [info.firstname, info.middlename, info.lastname, info.email];
    return db.query(sql, params);
  }

  async function insertADT(info) {
    const db = await dbs();
    const sql =
      "INSERT INTO applicant_date_trackings (users_id, date_received, date_deployed)   " +
      "VALUES ($1, $2, $3)";
    const params = [info.users_id, info.date_received, info.date_deployed];
    return db.query(sql, params);
  }

  async function deleteUser(id){
    const db = await dbs();
    const sql = `DELETE FROM users_work_experiences WHERE users_id=$1;`
    const params = [id]
    await db.query(sql,params)

    const sql1 = `DELETE FROM users_seminar_attends WHERE users_id=$1;`
    const params1 = [id]
    await db.query(sql1,params1)
    
    const sql2 = `DELETE FROM users_relative_in_bfi WHERE user_id=$1;`
    const params2 = [id]
    await db.query(sql2,params2)

    const sql3 = `DELETE FROM users_references WHERE users_id=$1;`
    const params3 = [id]
    await db.query(sql3,params3)
    
    const sql4 = `DELETE FROM users_org_memberships WHERE users_id=$1;`
    const params4 = [id]
    await db.query(sql4,params4)
    
    const sql5 = `DELETE FROM users_family_bg WHERE users_id=$1;`
    const params5 = [id]
    await db.query(sql5,params5)
    
    const sql6 = `DELETE FROM "users_willingness_to_work" WHERE users_id=$1;`
    const params6 = [id]
    await db.query(sql6,params6)
    
    const sql7 = `DELETE FROM "users_other_info" WHERE users_id=$1;`
    const params7 = [id]
    await db.query(sql7,params7)
    
    const sql8 = `DELETE FROM users_is_married WHERE users_id=$1;`
    const params8 = [id]
    await db.query(sql8,params8)
    
    const sql9 = `DELETE FROM users_have_work_with_bfi WHERE user_id=$1;`
    const params9 = [id]
    await db.query(sql9,params9)
    
    const sql10 = `DELETE FROM users_educational_bg WHERE users_id=$1;`
    const params10 = [id]
    await db.query(sql10,params10)
    
    const sql11 = `DELETE FROM categories_random WHERE user_id=$1;`
    const params11 = [id]
    await db.query(sql11,params11)
      
    const sql12 = `DELETE FROM users_scores WHERE user_id=$1;`
    const params12 = [id]
    await db.query(sql12,params12)
    
    const sql13 = `DELETE FROM applicant_date_trackings WHERE users_id=$1;`
    const params13 = [id]
    await db.query(sql13,params13)
    
    const sql14 = `DELETE FROM interview_assessments WHERE user_id_applicant=$1;`
    const params14 =[id]
    await db.query(sql14,params14)

    const sql15 = `DELETE FROM users WHERE id=$1;`
    const params15 = [id]
    await db.query(sql15,params15)
  }

  async function deleteTest(deptname) {
    const db = await dbs();
    const sql1 =
      "DELETE FROM applicant_date_trackings  " +
      "WHERE users_id IN  " +
      "( SELECT u.id AS users_id  " +
      "FROM users u  " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1) " +
      "OR " +
      "rejected_by IN " +
      "( SELECT u.id AS users_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params = [deptname];
    await db.query(sql1, params);
    const sql2 =
      "DELETE " +
      "FROM referral_forms " +
      "WHERE users_id IN " +
      "( SELECT u.id AS users_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params2 = [deptname];
    await db.query(sql2, params2);
    const sql3 =
      "DELETE " +
      "FROM users_work_experiences " +
      "WHERE users_id IN " +
      "( SELECT u.id AS users_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params3 = [deptname];
    await db.query(sql3, params3);
    const sql4 =
      "DELETE " +
      "FROM users_willingness_to_work " +
      "WHERE users_id IN " +
      "( SELECT u.id AS users_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params4 = [deptname];
    await db.query(sql4, params4);
    const sql5 =
      "DELETE " +
      "FROM users_seminar_attends " +
      "WHERE users_id IN " +
      " (SELECT u.id AS users_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params5 = [deptname];
    await db.query(sql5, params5);
    const sql6 =
      "DELETE " +
      "FROM users_relative_in_bfi " +
      "WHERE user_id IN " +
      "( SELECT u.id AS user_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params6 = [deptname];
    await db.query(sql6, params6);
    const sql7 =
      "DELETE " +
      "FROM users_professional_licenses " +
      "WHERE users_id IN " +
      "( SELECT u.id AS users_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params7 = [deptname];
    await db.query(sql7, params7);
    const sql8 =
      "DELETE " +
      "FROM users_references " +
      "WHERE users_id IN " +
      "( SELECT u.id AS users_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params8 = [deptname];
    await db.query(sql8, params8);
    const sql9 =
      "DELETE " +
      "FROM users_is_married " +
      "WHERE users_id IN " +
      "( SELECT u.id AS users_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params9 = [deptname];
    await db.query(sql9, params9);
    const sql10 =
      "DELETE " +
      "FROM users_org_memberships " +
      "WHERE users_id IN " +
      "( SELECT u.id AS users_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params10 = [deptname];
    await db.query(sql10, params10);
    const sql11 =
      "DELETE " +
      "FROM users_educational_bg " +
      "WHERE users_id IN " +
      " (SELECT u.id AS users_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params11 = [deptname];
    await db.query(sql11, params11);
    const sql12 =
      "DELETE " +
      "FROM users_other_info " +
      "WHERE users_id IN " +
      "( SELECT u.id AS users_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params12 = [deptname];
    await db.query(sql12, params12);
    const sql13 =
      "DELETE " +
      "FROM users_family_bg " +
      "WHERE users_id IN " +
      "( SELECT u.id AS users_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params13 = [deptname];
    await db.query(sql13, params13);
    const sql14 =
      "DELETE " +
      "FROM users_have_work_with_bfi " +
      "WHERE user_id IN " +
      "(SELECT u.id AS user_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params14 = [deptname];
    await db.query(sql14, params14);
    const sql15 =
      "DELETE " +
      "FROM interview_assessments " +
      "WHERE user_id_applicant IN " +
      "( SELECT u.id AS user_id_applicant " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params15 = [deptname];
    await db.query(sql15, params15);
    const sql16 =
      "DELETE " +
      "FROM users_scores " +
      "WHERE user_id IN " +
      "( SELECT u.id AS user_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params16 = [deptname];
    await db.query(sql16, params16);
    const sql17 =
      "DELETE " +
      "FROM categories_random " +
      "WHERE user_id IN " +
      "( SELECT u.id AS user_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params17 = [deptname];
    await db.query(sql17, params17);
    const sql18 =
      "DELETE FROM logs " +
      "WHERE user_id IN " +
      "( SELECT u.id AS user_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params18 = [deptname];
    await db.query(sql18, params18);
    const sql19 =
      "DELETE FROM " +
      "questions_choices " +
      "WHERE id IN ( " +
      " SELECT qc.id " +
      "FROM questions_choices qc " +
      "JOIN questions q ON qc.questions_id = q.id " +
      "JOIN categories c ON q.categories_id = c.id " +
      "JOIN teams t ON c.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params19 = [deptname];
    await db.query(sql19, params19);
    const sql20 =
      "DELETE FROM " +
      "questions " +
      "WHERE id IN ( " +
      " SELECT q.id " +
      "FROM questions q " +
      "JOIN categories c ON q.categories_id = c.id " +
      "JOIN teams t ON c.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params20 = [deptname];
    await db.query(sql20, params20);
    const sql21 =
      "DELETE FROM " +
      "categories " +
      "WHERE id IN ( " +
      " SELECT c.id " +
      "FROM categories c " +
      "JOIN teams t ON c.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params21 = [deptname];
    await db.query(sql21, params21);
    const sql22 =
      "DELETE FROM users " +
      "WHERE id IN " +
      "( SELECT u.id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params22 = [deptname];
    await db.query(sql22, params22);
    const sql23 =
      "DELETE FROM positions " +
      "WHERE id IN ( " +
      " SELECT p.id " +
      "FROM positions p " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params23 = [deptname];
    await db.query(sql23, params23);
    const sql24 =
      "DELETE FROM " +
      "teams " +
      "WHERE id IN ( " +
      " SELECT t.id " +
      "FROM teams t " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1); ";
    const params24 = [deptname];
    await db.query(sql24, params24);
    const sql25 =
      "DELETE FROM departments " +
      "WHERE name = $1 " +
      "OR user_id IN " +
      "( SELECT u.id AS user_id " +
      "FROM users u " +
      "JOIN positions p ON u.position_id = p.id " +
      "JOIN teams t ON p.teams_id = t.id " +
      "JOIN departments d ON t.department_id = d.id " +
      "WHERE d.name = $1);";
    const params25 = [deptname];
    await db.query(sql25, params25);
  }
// alter in table
  async function alterInDataBase() {
    const db = await dbs();
    const sql = ''
  // `
  // DROP SEQUENCE IF EXIST questions_type_id_seq

  // CREATE SEQUENCE IF NOT EXISTS questions_type_id_seq
  //     INCREMENT 1
  //     START 18
  //     MINVALUE 1
  //     MAXVALUE 2147483647
  //     CACHE 1;

  // DROP TABLE IF EXIST questions_type;
  
  // CREATE TABLE IF NOT EXIST questions_type
  // (
  //     id integer NOT NULL DEFAULT nextval('questions_type_id_seq'::regclass),
  //     question_type character varying(500) COLLATE pg_catalog."default",
  //     status character varying(500) COLLATE pg_catalog."default",
  //     CONSTRAINT questions_type_pkey PRIMARY KEY (id)
  // )
  // WITH (
  //     OIDS = FALSE
  // )
  // TABLESPACE pg_default;
  
  // ALTER TABLE questions
  //  ADD COLUMN question_type_id bigint,
  //  ADD CONSTRAINT questions_type_id FOREIGN KEY (question_type_id)
  //         REFERENCES questions_type (id) MATCH SIMPLE
  //         ON UPDATE NO ACTION
  //         ON DELETE NO ACTION;
  
  // ALTER TABLE categories
  //   ALTER COLUMN time_limit_on_seconds TYPE BIGINT;
  
  // INSERT INTO questions_type(
  //    question_type, status)
  //   VALUES ( 'Multiple choice', 'active'), ( 'Essay', 'active'),( 'Problem Solving', 'active');
  //   `;
    return db.query(sql);
  }

  async function alterInDataBase1() {
    const db = await dbs();
    const sql = ''
  // `ALTER TABLE roles
	// DROP COLUMN IF EXISTS access_rights_id;
	
  //   DROP TABLE IF EXISTS access_rights;

  //   CREATE TABLE IF NOT EXISTS access_rights
  //   (
  //       id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
  //       discription character varying(255) COLLATE pg_catalog."default",
  //       time_created date,
  //       time_updated date,
  //       status character varying(255) COLLATE pg_catalog."default",
  //       actions text COLLATE pg_catalog."default",
  //       modules character varying(225) COLLATE pg_catalog."default",
  //       CONSTRAINT access_rights_pkey PRIMARY KEY (id)
  //   )
    
  //   TABLESPACE pg_default;
      
  //   DROP TABLE IF EXISTS actions;
    
  //   CREATE TABLE IF NOT EXISTS actions
  //   (
  //       id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
  //       action_name character varying(255) COLLATE pg_catalog."default",
  //       time_created date,
  //       time_updated date,
  //       status character varying(255) COLLATE pg_catalog."default",
  //       CONSTRAINT actions_pkey PRIMARY KEY (id)
  //   )
    
  //   TABLESPACE pg_default;
    
  //   DROP TABLE IF EXISTS modules;
    
  //   CREATE TABLE IF NOT EXISTS modules
  //   (
  //       id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
  //       module_name character varying(255) COLLATE pg_catalog."default",
  //       time_created date,
  //       time_updated date,
  //       status character varying(255) COLLATE pg_catalog."default",
  //       CONSTRAINT modules_pkey PRIMARY KEY (id)
  //   )
    
  //   TABLESPACE pg_default;
    

  //   ALTER TABLE roles
  //   ADD access_rights_id bigint,
  //   ADD CONSTRAINT access_rights_fk FOREIGN KEY (access_rights_id)
  //           REFERENCES public.access_rights (id) MATCH SIMPLE
  //           ON UPDATE SET DEFAULT
  //           ON DELETE SET DEFAULT
  //           DEFERRABLE;   `;
    return db.query(sql);
  }

  async function alterInDataBase2() {
    const db = await dbs();
    const sql = ''
//   `
  
// ALTER TABLE access_rights
// DROP column IF EXISTS module_id ;

//   ALTER TABLE actions
// 	DROP column IF EXISTS modules ;

// ALTER TABLE actions
// 	ADD column IF NOT EXISTS module_id integer,
// 	ADD CONSTRAINT module_id_fk FOREIGN KEY (module_id)
//         REFERENCES modules (id) MATCH SIMPLE
//         ON UPDATE NO ACTION
//         ON DELETE NO ACTION
//         NOT VALID;`;
    return db.query(sql);
  }


  // update col
  async function updateCol(question_type_id,categories_id) {
    const db = await dbs();
    const sql = `UPDATE questions SET question_type_id=$1 WHERE categories_id=$2;`;
    const params = [question_type_id,categories_id];
    return db.query(sql, params);
  }

  async function selectDuplicate(){
    const db = await dbs();
    const sql =`SELECT mobile, count FROM (
      SELECT mobile, COUNT(id) as "count"  FROM users WHERE status = 'received' GROUP BY mobile
      ) 
      t WHERE count > 1;`;
    return db.query(sql)
  }

  async function selectId(info){
    const db = await dbs();
    const sql = `SELECT id FROM users where status = 'received' and mobile='${info}'`
    return db.query(sql)
  }

  async function deleteDuplicate(info){
    const db = await dbs();
    const sql = `DELETE FROM applicant_date_trackings WHERE users_id='${info}';`
    await db.query(sql)
    const sql2 = `DELETE FROM users where id='${info}';`
    return db.query(sql2)
  }

  async function resetOnlineExamRecord(info){
    const db = await dbs();
    const sql = `DELETE FROM categories_random WHERE user_id ='${info}';`
    await db.query(sql)
    const sql2 = `DELETE FROM users_scores WHERE user_id ='${info}';`
    return db.query(sql2)
  }

  async function updateToDeploy(info,mobile){
    const db = await dbs();
    const sql = `update users set status='deployed' where mobile=$2 and id=$1 `
    const params = [info,mobile]
    return db.query(sql,params)
  }
  
  async function updateToOnlineExam(info,mobile){
    const db = await dbs();
    const sql = `update users set status='online exam' where mobile=$2 and id=$1 `
    const params = [info,mobile]
    return db.query(sql,params)
  }
};
module.exports = db;
