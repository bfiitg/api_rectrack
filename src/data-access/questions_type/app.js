const makeDb = require('../db');
const db = require('./questions-type-db');

const QuestionTypeDb = makeDb({ db });

module.exports = QuestionTypeDb;