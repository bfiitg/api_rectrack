const db = ({dbs}) => {
    return Object.freeze({
        findAllQuestionType,
        findQuestionByType,
        findQuestionTypeById,
        addQuestionType,
        updateQuestionType,
        changeStatusActive,
        changeStatusInactive,
        checkActive,
        checkInactive,
        findQuestionTypeByQTYPE,
    });

    //find all question type
    async function findAllQuestionType(){
        const db = await dbs();
        const sql = "SELECT * FROM questions_type ;";
        return db.query(sql);
    }
    //find question by type
    async function findQuestionByType(question_type){
        const db = await dbs();
        const sql = `SELECT * FROM questions_type WHERE question_type=$1 and status='active';`;
        const params = [question_type];
        return db.query(sql, params)
    }
    //find question type by id
    async function findQuestionTypeById(id){
        const db = await dbs();
        const sql = `SELECT * FROM questions_type WHERE id=$1;`;
        const params = [id];
        return db.query(sql, params)
    }
    //add question type
    async function addQuestionType(info){
        const db = await dbs();
        const sql = `INSERT INTO questions_type (question_type,status) ` +
                    `VALUES ($1,'active');`;
        const params = [info.question_type];
        return db.query(sql, params)
    }
    //update question type
    async function updateQuestionType(info){
        const db = await dbs();
        const sql = `UPDATE questions_type SET question_type = $2, `+
                    `status = 'active' WHERE id = $1;`;
        const params = [info.id,info.question_type];
        return db.query(sql, params)
    }

    //change status inactive
    async function changeStatusInactive(info){
        const db = await dbs();
        const sql = `UPDATE questions_type set status='inactive' WHERE id=$1;`
        const params = [info.id];
        return db.query(sql,params)
    }

    
    //change status active
    async function changeStatusActive(info){
        const db = await dbs();
        const sql = `UPDATE questions_type set status='active' WHERE id=$1;`
        const params = [info.id];
        return db.query(sql,params)
    }

    //check if already inactive
    async function checkInactive(id){
        const db = await dbs();
        const sql = "SELECT * FROM questions_type WHERE id=$1 and status='inactive';"
        const params = [id];
        return db.query(sql, params)
    }

    //check if already active
    async function checkActive(id){
        const db = await dbs();
        const sql = "SELECT * FROM questions_type WHERE id=$1 and status='active';"
        const params = [id];
        return db.query(sql, params)
    }

    async function findQuestionTypeByQTYPE(question_type_id){
        const db = await dbs();
        const sql = `SELECT question_type FROM questions_type where id=$1;`
                   
        const params = [question_type_id]
        return db.query(sql,params)
      }

};
module.exports = db;