const makeDb = require("../db");
const db = require("./categories-db");

const categoriesDb = makeDb({ db });

module.exports = categoriesDb;
