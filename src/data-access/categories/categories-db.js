const db = ({ dbs }) => {
  return Object.freeze({
    insert,
    findByName,
    selectAll,
    selectOne,
    updating,
    findByNameUpdate,
    selectAllFilterTeam,
    selectByDept
  });
  // filter by team
  async function selectAllFilterTeam(id) {
    const db = await dbs();
    const sql =
      `SELECT cat.*, t.name AS teamname FROM categories cat JOIN teams t ON cat.teams_id = t.id 
      WHERE teams_id = $1 
      AND cat.status='active' 
      AND cat.name NOT LIKE '%IQ Test%'
      ORDER BY cat.name`;
    const params = [id];
    return db.query(sql, params);
  }
  async function findByNameUpdate(name, id) {
    const db = await dbs();
    const sql =
      "SELECT * FROM (SELECT * FROM categories WHERE LOWER(name) <> LOWER('GENERAL')) AS category_list WHERE LOWER(name)=LOWER($1) AND id <> $2 AND status='active';";
    const params = [name, id];
    return db.query(sql, params);
  }
  async function updating({ id, ...info }) {
    const db = await dbs();
    const sql =
      "UPDATE categories SET name=$1, description=$2, time_updated=NOW(), teams_id=$3, " +
      "time_limit_on_seconds=$4, status=$5 WHERE id = $6;";
    const params = [
      info.name,
      info.description,
      info.teams_id,
      info.time_limit_on_seconds,
      info.status,
      id
    ];
    return db.query(sql, params);
  }
  async function selectOne(id) {
    const db = await dbs();
    const sql =
      "SELECT c.*,t.name AS teamname FROM categories c JOIN teams t ON t.id = c.teams_id " +
      "WHERE c.id = $1 ";
    const params = [id];
    return db.query(sql, params);
  }
  async function insert({ ...info }) {
    const db = await dbs();
    const sql =
      "INSERT INTO categories VALUES (DEFAULT,$1,$2,$3,'active',$4,NOW(),NOW());";
    const params = [
      info.name,
      info.description,
      info.teams_id,
      info.time_limit_on_seconds
    ];
    return db.query(sql, params);
  }
  async function findByName(name) {
    const db = await dbs();
    const sql = "SELECT * FROM (SELECT * FROM categories WHERE LOWER(name) <> LOWER('GENERAL')) AS category_list WHERE LOWER(name)=LOWER($1);";
    const params = [name];
    return db.query(sql, params);
  }
  async function selectAll() {
    const db = await dbs();
    const sql =
      "SELECT c.id AS catid, c.name AS catg, c.description, t.id AS teamid, t.name, c.status " +
      "FROM categories c JOIN teams t ON t.id = c.teams_id " +
      "ORDER BY c.name;";
    return db.query(sql);
  }
  async function selectByDept(id) {
    const db = await dbs();
    const sql =
      "SELECT c.id AS catid, c.name AS catg, c.description, c.status, t.id AS teamid, t.name " +
      "FROM categories c JOIN teams t ON t.id = c.teams_id " +
      "WHERE t.department_id = $1 ORDER BY c.name;";
    const params = [id];
    return db.query(sql, params);
  }
};

module.exports = db;
