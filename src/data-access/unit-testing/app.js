const makeDb = require("../db");
const db = require("./unit-testing-db");


const testingDb = makeDb({ db });

module.exports = testingDb;
