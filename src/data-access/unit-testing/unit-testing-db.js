const db = ({ dbs }) => {
  return Object.freeze({
    createTestDb
  });
  async function createTestDb() {
    const db = await dbs();
    const sql =
      "TRUNCATE TABLE categories RESTART IDENTITY CASCADE;TRUNCATE TABLE categories_random RESTART IDENTITY CASCADE; " +
      "TRUNCATE TABLE departments RESTART IDENTITY CASCADE;TRUNCATE TABLE interview_assessments RESTART IDENTITY CASCADE; " +
      "TRUNCATE TABLE logs RESTART IDENTITY CASCADE;TRUNCATE TABLE positions RESTART IDENTITY CASCADE; " +
      "TRUNCATE TABLE questions RESTART IDENTITY CASCADE;TRUNCATE TABLE questions_choices RESTART IDENTITY CASCADE; " +
      "TRUNCATE TABLE roles RESTART IDENTITY CASCADE;TRUNCATE TABLE teams RESTART IDENTITY CASCADE; " +
      "TRUNCATE TABLE users RESTART IDENTITY CASCADE;TRUNCATE TABLE users_educational_bg RESTART IDENTITY CASCADE; " +
      "TRUNCATE TABLE users_family_bg RESTART IDENTITY CASCADE;TRUNCATE TABLE users_have_work_with_bfi RESTART IDENTITY CASCADE; " +
      "TRUNCATE TABLE users_is_married RESTART IDENTITY CASCADE;TRUNCATE TABLE users_org_memberships RESTART IDENTITY CASCADE; " +
      "TRUNCATE TABLE users_other_info RESTART IDENTITY CASCADE;TRUNCATE TABLE users_professional_licenses RESTART IDENTITY CASCADE; " +
      "TRUNCATE TABLE users_references RESTART IDENTITY CASCADE;TRUNCATE TABLE users_relative_in_bfi RESTART IDENTITY CASCADE; " +
      "TRUNCATE TABLE users_scores RESTART IDENTITY CASCADE;TRUNCATE TABLE users_seminar_attends RESTART IDENTITY CASCADE; " +
      "TRUNCATE TABLE users_willingness_to_work RESTART IDENTITY CASCADE;TRUNCATE TABLE users_work_experiences RESTART IDENTITY CASCADE; " +
      "INSERT INTO users (email,password,status) VALUES ('72e529bc66582a5a07ac6f66d8fe2268fc2b2668b8d0','61f425','active'); " +
      "TRUNCATE TABLE referral_forms RESTART IDENTITY CASCADE; " +
      "INSERT INTO users (email,password,status) VALUES ('62ec24f77a493a451ca84c6af5f82665bb246766','41f008d4606e23','received');";
    await db.query(sql);
  }
};

module.exports = db;
