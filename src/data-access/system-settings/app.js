const makeDb = require("../db");
const db = require("./system-settings-db");

const systemSettingsDb = makeDb({ db });

module.exports = systemSettingsDb;