const db = ({ dbs }) => {
    return Object.freeze({
        getColors,
        setSystemSettings,
        getCurrentSystemSettings
    });

    async function getColors(){
        const db = await dbs();
        const sql = "SELECT * FROM system_settings.\"systemColors\"";
        return db.query(sql);
    }

    async function setSystemSettings(logoPath, colorHex){
        const db = await dbs();
        const sql = "UPDATE  system_settings.\"systemSettings\" SET logo = $1, color = $2 WHERE id = 1";
        params = [logoPath, colorHex];
        return db.query(sql, params);
    }

    async function getCurrentSystemSettings(){
        const db = await dbs();
        const sql = "SELECT * FROM system_settings.\"systemSettings\" WHERE id = 1";
        return db.query(sql);
    }
}

module.exports = db;