const db = require("./users-db");
const makeDb = require("../db");

const userDb = makeDb({ db });

module.exports = userDb;
