const db = ({ dbs }) => {
  return Object.freeze({
    insert,
    findByEmail,
    findByFullName,
    logins,
    findAllReceived,
    changeStatusOnlineExam,
    changeStatusBlacklist,
    findAllOnlineExam,
    changeStatusEndorsement,
    changeStatusForEndorsement,
    findAllEndorsementDH,
    findAllRejectDh,
    findAllForEndorsement,
    changeStatusReject,
    findAllRejected,
    getJobOrderTh,
    getJobOfferDh,
    findAllBlacklisted,
    changeStatusAssessment,
    findAllQuickApplicantsByDate,
    findAllAssessment,
    changeStatusPending,
    findAllPending,
    changeStatusKeptForReference,
    findAllKeptForReference,
    changeStatusShortList,
    findAllShortList,
    changeStatusDeployed,
    findAllDeployed,
    findAllDeployedDH,
    viewAllUsers,
    viewDetailsSpecificUser,
    adminInsert,
    adminUpdateUser,
    loginsApplicant,
    doneOnlineExam,
    getApplicantID,
    selectRandomCategories,
    selectRandomCategories1st,
    insertRandomCategories,
    selectRandomQuestions,
    insertRandomQuestions,
    scheduleForAssessment,
    changeStatusJobOrder,
    findAllJobOrder,
    fillUpApplication,
    notificationReceived,
    notificationQuickApplicantPg,
    notificationQuickApplicantOh,
    notificationReceivedTrue,
    notificationQuickApplicantTruePg,
    notificationQuickApplicantTrueAdmin,
    notificationQuickApplicantTrueOh,
    notificationEndorsed,
    notificationEndorsedTrue,
    getAllCategoryIdForIQBExam,
    selectAllQuestionsIQB,
    insertAllQuestionsIQB,
    getTokenOfUser,
    getRoleOfUser,
    getRoleOfUserVerify,
    // for jeonsoft queries to export as csv
    getEmployeeAddressContactEmergency,
    getEmployeeCertifications,
    getEmployeeEducation,
    getEmployeeFamilyBg,
    getEmployeeWorkExperience,
    getEmployeePreviousEmployer,
    isExportedToTrue,
    displayApplicantsReadyForExport,
    // end
    getUsersDataApplicationForm,
    getSameTeamPositionApplicationForm,
    getTeamNumberOfItems,
    examEndDate,
    returnStartEndDate,
    elapsedTimes,
    loginsApplicants,
    loginsApplicantApplication,
    timeForIQBE,
    IQBEGetTime,
    minusTimeForIQBE,
    doneIQBE,
    returnEmailAndName,
    getPositionName,
    applicationFormData,
    applicantsEducation,
    applicantsRelative,
    applicantsProfLicenses,
    applicantsOrgMemberships,
    applicantsSeminars,
    applicantsWorkExp,
    applicantsWorkWithBfi,
    applicantsOtherInfo,
    applicantsFamilyBg,
    applicantsMarried,
    applicantsReference,
    applicantsWillingness,
    getNameThruToken,
    addReferralForm,
    viewAllReferralForm,
    viewOneReferralForm,
    updateReferralForm,
    getIdThruToken, // get id of user thru token
    insertUserLogs, //for logs table
    onlineExamResult,
    // find all for pg
    findAllEndorsementPG,
    findAllForEndorsementFiltered,
    findAllKeptForReferencePG,
    findAllOnlineExamPG,
    findAllPendingPG,
    findAllReject,
    findAllShortListPG,
    // til here
    getUsersDataApplicationFormPrintView,
    applicationFormDataPrintView,
    fetchExistingDataReferral,
    assessmentUpdatePositionShowList,
    updatePositionOfApplicant,
    notificationReceivedDp,
    notificationAssessmentDp,
    notificationAssessmentTrue,
    notificationDeployedDp,
    notificationDeployedTrue,
    notificationShorlist,
    notificationShortlistTrue,
    updateDeployedDate,
    checkToken,
    assessmentContacts,
    returnPositionIdFromName,
    insertApplicantFromGoogleSheet,
    selectAllLogs,
    iqbeExamResult,
    checkStatusOfApplicant,
    findAllReceivedPGDefault,
    selectAllFilesInDb,
    findAllReceivedPGDeptOnly,
    deleteIAFWhenReference,
    notificationForEndorsed,
    notificationForEndorsedTrue,
    checkIfHasReferral,
    onlineExamRetake,
    firstTimeLoginChangePass,
    filterReports,
    findAllOnlineExamDH,
    findAllOnlineExamPGDeptAndPosition,
    //for team heads
    findAllReceivedTh,
    findAllOnlineExamTH,
    findAllEndorsementTh,
    findAllRejectTh,
    findAllJobOrderTh,
    findAllShortListTh,
    findAllKeptForReferenceTh,
    findAllPendingTh,
    findAllForEndorsementTh,
    findAllApplicantsDH,
    findAllApplicants,
    findAllQuickApplicants,
    findAllDeployedTH,
    findAllAssessmentTH,
    changeStatusReceived,
    getDhEmail,
    getRemainingOnlineExamTime,
    getUserDetails,
    getDhEmailWithId,
    getOperationsHeadEmail,
    notificationReceivedTh,
    notificationForEndorsedTh,
    notificationAssessmentTh,
    notificationDeployedTh,
    notificationAssessmentTrueTh,
    notificationReceivedTrueTh,
    notificationForEndorsedTrueTh,
    notificationDeployedTrueTh,
    //oh notifications
    notificationReceivedOh,
    notificationReceivedTrueOh,
    notificationAssessmentOh,
    notificationAssessmentTrueOh,
    notificationForEndorsedOh,
    notificationForEndorsedTrueOh,
    notificationDeployedOh,
    notificationDeployedTrueOh,
    notificationShorlistOh,
    notificationShortlistOhTrue,
    notificationEndorsedOh,
    notificationEndorsedOhTrue,
    getApplicantDateTrackings,
    getApplicantAssessmentSchedule,
    findAllfetchedQuickApplicants,
    //admin
    notificationDeployedAdmin,
    notificationQuickApplicantAdmin,
    notificationAssessmentTrueAdmin,
    findAllEndorsementFiltered,
    notificationReceivedTrueAdmin,
    findAllEndorsement,



    //butch part
    filterKeptfroRef,
    filterDeployed,
    filterRejected,
    filterBlacklist,
    checkCatRandom,
    onlineExamResultQTYPE,
    IQTestResultQTYPE,
    rateAnswer,
    findScoreById,
    updateResume,
    updatePositionToRecieved,
    findUser,
    find,
    findbyPosition,
    fetchAllPendingStatus,


    monitorAnswer,
    findUserEmail,
    updateUserEmail,
    forgotPassword,
    getPassword,
    updateToken,
    getStatusApplicant,
    checkApplicantIfReApplied,
    reAppliedIncrementCount,
    reAppliedApplicant,
    selectReAppliedApplicant,
    checkApplicantIfReAppliedQA,
    getReAppliedApplicantName,
    getUserData,
    getAllPendingReApply,
    processReApplyApplicant,
    findAllApplicantsTH,
    changeJobOrder,
    viewAllIQBE,
    ifTEst,
    findApplicantById,
    findApplicantWithoutPositionById,
    findAllPendingDh, findAllShortListDh, getAllkeepforReferences, findAllKeptForReferenceDh,
    getAllJobOffer,
    findAllAssessmentDH,
    findAllBlacklistedTH,
    findAllBlacklistedDH,
    getAllApplicants,
    getAllAssessment,
    getAllDeployed,
    getAllRejected,
    getAllBlacklisted,
    getAllOnlineExam,
    getAllReceived,
    getAllShortlisted,
    addApplicantNotification,
    getApplicantNotification,
    updateApplicantNotification,
    updateNotificationViewedBy,
    fetchDepartmentTeamPositionBy,
    fetchDepartmentHeadDetailsBy,
    fetchPGDetails,

    passwordVerification,
    fetchApplicantsWithoutReferral,
    fetchUsers,
    getApplicantHistory,
    checkUserHasIQBE
  });
  
  async function processReApplyApplicant({ applicant, id }) {
    const { position_id, previous_position, userId, pendingId } = applicant;
    const db = await dbs();
    let counter = 0;

    // update user status and position
    let sql = null;
    if (position_id) {
      sql = `UPDATE users SET position_id = $1, status = 'received', resume_upload_date=NOW() WHERE id = $2;`;
      const params = [position_id, userId];
      await db.query(sql, params);
    } else {
      sql = `UPDATE users SET status = 'quick applicant', resume_upload_date=NOW() WHERE id = $1;`;
      const params = [userId];
      await db.query(sql, params);
      counter++;
    }

    // update re-applied applicant status and prev position
    const sql1 = `UPDATE reapplied_applicants SET isprocessed = 't', previous_position_id = $1 WHERE id = $2;`;
    const params1 = [previous_position, id];
    await db.query(sql1, params1);

    // delete applicant date trackings
    const sql2 = `DELETE FROM applicant_date_trackings WHERE users_id = $1;`;
    const params2 = [userId];
    await db.query(sql2, params2);

    // insert applicant date trackings
    const sql3 = `INSERT INTO applicant_date_trackings (users_id,date_received) VALUES ($1,NOW());`;
    const params3 = [userId];
    await db.query(sql3, params3);

    for (let i = 0; i < pendingId.length; i++) {
      const e = pendingId[i];

      // update re-applied applicant status and prev position
      const sql = `UPDATE reapplied_applicants SET isprocessed = 't', previous_position_id = $1 WHERE id = $2;`;
      const params = [previous_position, e];
      await db.query(sql, params);
      counter++; // increment
    }
    let bool = false;
    if (counter > 0) bool = true;
    return bool; // :)
  }
  async function getAllPendingReApply({ applicant }) {
    const { firstname, lastname } = applicant;
    const db = await dbs();
    const sql = `SELECT id FROM reapplied_applicants WHERE firstname = $1 AND lastname = $2;`;
    const params = [firstname, lastname];
    return db.query(sql, params);
  }
  async function getUserData({ applicant }) {
    const { firstname, lastname } = applicant;
    const db = await dbs();
    const sql = `SELECT id,position_id FROM users WHERE firstname = $1 AND lastname = $2;`;
    const params = [firstname, lastname];
    return db.query(sql, params);
  }
  async function getReAppliedApplicantName({ id }) {
    const db = await dbs();
    const sql = `SELECT email,firstname,lastname,position_id FROM reapplied_applicants WHERE id = $1;`;
    const params = [id];
    return db.query(sql, params);
  }
  // for quick apply
  async function checkApplicantIfReAppliedQA({ applicant }) {
    const { email } = applicant;
    const db = await dbs();
    const sql = `SELECT id,reapply_count,created_at FROM reapplied_applicants
      WHERE email = $1 AND position_id IS NULL;`;
    const params = [email];
    return db.query(sql, params);
  }
  async function selectReAppliedApplicant({ }) {
    const db = await dbs();
    const sql = `SELECT a.*,b.name,
    (
    SELECT name FROM positions WHERE id = a.previous_position_id
    ) AS prev_position
    FROM reapplied_applicants a
    LEFT JOIN positions b ON b.id = a.position_id ORDER BY id;`;
    return db.query(sql);
  }
  async function reAppliedApplicant({ applicant }) {
    const {
      email,
      firstname,
      lastname,
      position_id,
      mobile,
      resume_url,
      reapply_count,
    } = applicant;
    const db = await dbs();
    const sql = `INSERT INTO reapplied_applicants (id, email, firstname, lastname,created_at, position_id, mobile, resume_url, reapply_count) VALUES (DEFAULT,$1,$2,$3,NOW(),$4,$5,$6,$7);`;
    const params = [
      email,
      firstname,
      lastname,
      position_id,
      mobile,
      resume_url,
      reapply_count,
    ];
    return db.query(sql, params);
  }
  async function reAppliedIncrementCount({ toUpdate }) {
    const { id, reapply_count } = toUpdate;
    const db = await dbs();
    const sql = `UPDATE reapplied_applicants SET reapply_count = $2 WHERE id = $1;`;
    const params = [id, reapply_count];
    return db.query(sql, params);
  }
  async function checkApplicantIfReApplied({ applicant }) {
    const { email, position_id } = applicant;
    const db = await dbs();
    const sql = `SELECT id,reapply_count,created_at FROM reapplied_applicants
      WHERE email = $1 AND position_id = $2;`;
    const params = [email, position_id];
    return db.query(sql, params);
  }
  async function getStatusApplicant({ applicant }) {
    const { firstname, lastname } = applicant;
    const db = await dbs();
    const sql =
      "SELECT status FROM users WHERE firstname = $1 AND lastname = $2;";
    const params = [firstname, lastname];
    return db.query(sql, params);
  }
  async function updateToken(token, id) {
    const db = await dbs();
    const sql = "UPDATE users SET token=$1 WHERE id=$2;";
    const params = [token, id];
    return db.query(sql, params);
  }
  async function getPassword(email) {
    const db = await dbs();
    const sql = "SELECT id,password FROM users WHERE email=$1;";
    const params = [email];
    return db.query(sql, params);
  }
  // change password first time login
  async function firstTimeLoginChangePass(id, password) {
    const db = await dbs();
    const sql = "UPDATE users SET password = $2,sign_in_count=0 WHERE id = $1;";
    const params = [id, password];
    return db.query(sql, params);
  }
  async function forgotPassword({ info }) {
    const { id, pw } = info; // deconstruct
    const db = await dbs();
    const sql = "UPDATE users SET password = $2, token = NULL WHERE id = $1;";
    const params = [id, pw];
    return db.query(sql, params);
  }
  // check if already has referral
  async function checkIfHasReferral(id) {
    const db = await dbs();
    const sql = "SELECT * FROM referral_forms WHERE applicant_id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // delete existing IAF when change status to kept for reference
  async function deleteIAFWhenReference(id) {
    const db = await dbs();
    const sql =
      "DELETE FROM interview_assessments WHERE user_id_applicant = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // select all files in db
  async function selectAllFilesInDb() {
    const db = await dbs();
    let files = []; //array to store files
    const sql =
      "SELECT user_image_path FROM users WHERE user_image_path IS NOT NULL AND user_image_path <> '';";
    const res = await db.query(sql);
    const userImage = res.rows;
    // add to array user image
    userImage.forEach((e) => {
      files.push(e.user_image_path);
    });

    // cs image
    const sql2 =
      "SELECT cs_certificate_image_path FROM users " +
      "WHERE cs_certificate_image_path IS NOT NULL AND cs_certificate_image_path <> '';";
    const res2 = await db.query(sql2);
    const csImage = res2.rows;
    // add to array
    csImage.forEach((e) => {
      files.push(e.cs_certificate_image_path);
    });

    // scanned resume
    const sql3 =
      "SELECT resume_upload_url " +
      "FROM users WHERE resume_upload_url IS NOT NULL AND resume_upload_url LIKE 'uploads%';";
    const res3 = await db.query(sql3);
    const resumeImage = res3.rows;
    // add to array
    resumeImage.forEach((e) => {
      files.push(e.resume_upload_url);
    });

    // seminar certificates
    const sql4 =
      "SELECT certificate_image_path FROM users_seminar_attends " +
      "WHERE certificate_image_path IS NOT NULL AND certificate_image_path <> '';";
    const res4 = await db.query(sql4);
    const seminarImage = res4.rows;
    // add to array
    seminarImage.forEach((e) => {
      files.push(e.certificate_image_path);
    });

    // professional licenses images certificates
    const sql5 =
      "SELECT certificate_image_path FROM users_professional_licenses " +
      "WHERE certificate_image_path IS NOT NULL AND certificate_image_path <> '';";
    const res5 = await db.query(sql5);
    const profImage = res5.rows;
    // add to array
    profImage.forEach((e) => {
      files.push(e.certificate_image_path);
    });

    return files; //return array of files
  }
  // display all logs
  async function selectAllLogs(date, dateTo) {
    const db = await dbs();
    const sql =
      "SELECT u.id, u.firstname,u.lastname,l.module,l.table_affected,l.activity, TO_CHAR(l.date_time, 'MM/DD/YYYY HH12:MI:SS AM') AS dates " +
      "FROM logs l JOIN users u ON u.id = l.user_id WHERE DATE(l.date_time) BETWEEN $1 AND $2 ORDER BY l.date_time DESC;";
    const params = [date, dateTo];
    return db.query(sql, params);
  }

  async function insertApplicantFromGoogleSheet({ ...userInfo }) {
    const db = await dbs();
    let sql =
      "INSERT INTO users (id,email,time_created,time_updated,sign_in_count, " +
      "firstname,lastname,mobile,livestock,willing_to_travel,resume_upload_date, " +
      "resume_upload_url,position_id,status,is_viewed,is_exported,is_done_online_exam, " +
      "is_done_iqbe_exam,is_viewed_oh, is_viewed_th) " +
      "VALUES (DEFAULT,$1,NOW(),NOW(),0, " +
      "$2,$3,$4,$5,$6,NOW(),$7,$8,'received','f','f','f','f','f','f');";

    let params = [
      userInfo.email,
      userInfo.firstname,
      userInfo.lastname,
      userInfo.mobile,
      userInfo.livestock,
      userInfo.travel,
      userInfo.resume_url,
      userInfo.position_id,
    ];

    if (!userInfo.position_id) {
      sql =
        "INSERT INTO users (id,email,time_created,time_updated,sign_in_count, " +
        "firstname,lastname,mobile,livestock,willing_to_travel,resume_upload_date, " +
        "resume_upload_url,status,is_viewed,is_exported,is_done_online_exam, " +
        "is_done_iqbe_exam,is_viewed_oh) " +
        "VALUES (DEFAULT,$1,NOW(),NOW(),0, " +
        "$2,$3,$4,$5,$6,NOW(),$7,'quick applicant','f','f','f','f','f');";

      params = [
        userInfo.email,
        userInfo.firstname,
        userInfo.lastname,
        userInfo.mobile,
        userInfo.livestock,
        userInfo.travel,
        userInfo.resume_url,
      ];
    }

    await db.query(sql, params);
    const sql2 = "SELECT id FROM users ORDER BY id DESC LIMIT 1;"; // select the id
    const datas = await db.query(sql2);
    const id = datas.rows[0].id;

    //
    const sql3 = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params3 = [id];
    const data = await db.query(sql3, params3);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_received = NOW() " +
        "WHERE id = $1;";
      const params = [appId];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,date_received) " +
        "VALUES ($1,NOW())";
      const params = [id];
      await db.query(sql, params);
    }
    //
  }
  async function returnPositionIdFromName(name) {
    const db = await dbs();
    const sql = "SELECT id FROM positions WHERE LOWER(name) = LOWER($1)";
    const params = [name];
    return db.query(sql, params);
  }
  async function assessmentContacts(id, name, contact, location) {
    const db = await dbs();
    //
    const sql = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET person_to_meet=$2, person_contact=$3, " +
        "location=$4 WHERE id = $1;";
      const params = [appId, name, contact, location];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,person_to_meet,person_contact,location) " +
        "VALUES ($1,$2,$3,$4)";
      const params = [id, name, contact, location];
      await db.query(sql, params);
    }
    //
  }
  // check token; logout if doesn't exists
  async function checkToken(token) {
    const db = await dbs();
    const sql = "SELECT token FROM users WHERE token = $1;";
    const params = [token];
    return db.query(sql, params);
  }
  async function updateDeployedDate(userId, date) {
    const db = await dbs();
    const sql =
      "UPDATE applicant_date_trackings SET date_deployed = $2 WHERE users_id = $1;";
    const params = [userId, date];
    return db.query(sql, params);
  }
  async function updatePositionOfApplicant(userId, posId) {
    const db = await dbs();
    const sql = "UPDATE users SET position_id = $2 WHERE id = $1;";
    const params = [userId, posId];
    return db.query(sql, params);
  }
  async function assessmentUpdatePositionShowList(id) {
    const db = await dbs();
    const sql = "SELECT position_id FROM users WHERE id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    // position id
    const posId = data.rows[0].position_id;
    //
    const sql2 =
      "SELECT d.id FROM positions p JOIN teams t ON t.id = p.teams_id " +
      "JOIN departments d ON d.id = t.department_id WHERE p.id = $1;";
    const params2 = [posId];
    const data2 = await db.query(sql2, params2);

    // dept id
    const deptId = data2.rows[0].id;
    //
    const sql3 =
      "SELECT p.id,p.name FROM positions p JOIN teams t ON t.id = p.teams_id " +
      "JOIN departments d ON d.id = t.department_id WHERE d.id = $1;";
    const params3 = [deptId];
    const data3 = await db.query(sql3, params3);
    // list of positions 
    return data3.rows;
  }
  async function fetchExistingDataReferral(id) {
    const db = await dbs();
    const sql =
      "SELECT who_referred,firstname,lastname,mobile,p.name FROM users u JOIN " +
      "positions p ON p.id = u.position_id WHERE u.id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // this route is not filtered this is for PG

  // received is filtered by dept and position
  async function findAllReceived(dateFrom, dateTo) {
    const db = await dbs(),
          dateFilter = (dateFrom && dateTo) ? ` AND resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : '',
          sql =
            `SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,
                    willing_to_travel,p.name,resume_upload_date,resume_upload_url,d.name as deptname,
                    adt.date_received, cat_count.category_count, (now() - adt.date_received) as running_time, d.id as deptid
            FROM users u JOIN positions p ON p.id = u.position_id
            JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id
            LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
            LEFT JOIN 
              (SELECT teams_id, COUNT(teams_id) as category_count
                FROM categories
                GROUP BY teams_id) as cat_count 
              ON t.id = cat_count.teams_id
            WHERE u.status = 'received' 
            ${dateFilter}
            ORDER BY u.id DESC;`
    return db.query(sql);
  }


  async function getAllReceived(deptid, posid) {
    const db = await dbs(),
          filter = (deptid && posid) ? ` AND d.id = ${deptid} AND p.id = ${posid}` : ` AND d.id = ${deptid}`,
          sql =
            `SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,
                    willing_to_travel,p.name,resume_upload_date,resume_upload_url,d.name as deptname,
                    adt.date_received, cat_count.category_count, (now() - adt.date_received) as running_time, d.id as deptid
            FROM users u JOIN positions p ON p.id = u.position_id
            JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id
            LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
            LEFT JOIN 
              (SELECT teams_id, COUNT(teams_id) as category_count
                FROM categories
                GROUP BY teams_id) as cat_count 
              ON t.id = cat_count.teams_id
            WHERE u.status = 'received'
            ${filter}
            ORDER BY u.id DESC;`
    return db.query(sql);
  }

  // received is filtered by dept only
async function findAllReceivedPGDeptOnly({deptID, dateFrom, dateTo}) {
    let extQuery = '';
    if (dateFrom && dateTo) { extQuery = `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` }

    const db = await dbs();
    const sql =
      "SELECT u.id,email,firstname,lastname,middlename,mobile,livestock, " +
      "willing_to_travel,p.name,resume_upload_date,resume_upload_url,d.name as deptname, " +
      "adt.date_received, cat_count.category_count, (now() - adt.date_received) as running_time, d.id as deptid " +
      "FROM users u JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
      "LEFT JOIN ( " +
      "SELECT teams_id, COUNT(teams_id) as category_count " +
      "FROM categories " +
      "GROUP BY teams_id " +
      ") as cat_count ON t.id = cat_count.teams_id " +
      `WHERE u.status = 'received' AND d.id = $1 
      ${extQuery} 
      ORDER BY u.id DESC;`; 
      ;// resume_upload_date ASC, date_received ASC, u.id ASC;";
    const params = [deptID];
    return db.query(sql, params);
  }


  async function findAllEndorsementPG(deptid, posid) {
    const db = await dbs();
    var filter = "";

    if (deptid) {
      filter = filter.concat(" AND d.id = $1 ");
    }
    if (posid && posid != "dept") {
      filter = filter.concat(" AND p.id =  $2 ");
    }

    const sql =
      "SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,u.is_done_iqbe_exam, " +
      "willing_to_travel,p.name,resume_upload_date,resume_upload_url,adt.online_exam_start,adt.online_exam_end, " +
      "adt.date_received, u.date_application, (now() - adt.date_endorsement) as running_time, d.id as deptid " +
      "FROM users u JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
      "WHERE u.status = 'endorsement' " +
      filter +
      " ORDER BY adt.date_endorsement ASC, adt.online_exam_end ASC, u.id ASC;";

    if (deptid && posid != "dept") {
      params = [deptid, posid];
      return db.query(sql, params);
    } else if (deptid) {
      params = [deptid];
      return db.query(sql, params);
    } else {
      return db.query(sql);
    }
  }

  async function findAllEndorsement({dateFrom, dateTo, offset = 0, limit = null}) {
    try {
      const db = await dbs(),
            extQuery = (dateFrom && dateTo) ? `AND resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : '',
            sql =
              `SELECT
                u.ID,
                email,
                firstname,
                lastname,
                middlename,
                mobile,
                livestock,
                u.is_done_iqbe_exam,
                willing_to_travel,
                P.NAME,
                resume_upload_date,
                resume_upload_url,
                adt.online_exam_start,
                adt.online_exam_end,
                u.date_application,
                adt.date_received,
                ( now( ) - adt.date_endorsement ) AS running_time,
                adt.date_endorsement,
                adt.date_for_endorsement
              FROM
                users u
                JOIN positions P ON P.ID = u.position_id
                JOIN teams T ON T.ID = P.teams_id
                JOIN departments d ON d.ID = T.department_id
                JOIN applicant_date_trackings adt ON adt.users_id = u.ID 
              WHERE
                (u.status = 'endorsement' OR u.status = 'schedule for assessment')
                AND (adt.date_endorsement IS NOT NULL
                AND adt.date_for_endorsement IS NOT NULL)
                ${extQuery}
              ORDER BY
                u.id DESC
              LIMIT ${limit} OFFSET ${offset}`
      return db.query(sql)
    } catch (error) {
      console.log(error)
    }
  }

  async function findAllForEndorsementFiltered({deptid, posid, dateFrom, dateTo}) {
    let extQuery = '';
    const db = await dbs();
    let filter = "";
    
    if (dateFrom && dateTo) { 
      extQuery = ` AND  u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}' ` 
    }

    if (deptid) {
      filter = filter.concat(` AND d.id = ${deptid} `);
    }
    
    if (posid) {
      filter = filter.concat(` AND p.id =  ${posid} `);
    }

    const sql =
      `SELECT u.id, u.email, u.firstname, u.lastname, u.middlename, u.mobile,livestock,u.position_id, u.is_done_iqbe_exam, 
      u.willing_to_travel, p.name, u.resume_upload_date, u.resume_upload_url, u.date_application, adt.online_exam_start, 
      adt.online_exam_end, adt.date_assessment, (now() - adt.date_for_endorsement) as running_time, u.status as stage, d.id as deptid, adt.date_for_endorsement, p.name as positionname, d.name as deptname
      FROM users u 
      LEFT JOIN positions p ON p.id = u.position_id 
      INNER JOIN teams t ON t.id = p.teams_id 
      INNER JOIN departments d ON d.id = t.department_id 
      LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
      WHERE u.status = 'endorsement'
      ${filter} 
      ${extQuery}
      ORDER BY u.id DESC`;
    return db.query(sql);
  }


  async function findAllJobOrder(deptid, posid) { 

    const db = await dbs();
    var filter = "";

    if (deptid) {
      filter = filter.concat(` AND d.id = ${deptid} `);
    }
    if (posid && posid != "dept") {
      filter = filter.concat(` AND p.id =  ${posid} `);
    }

    const sql =
      "SELECT u.id, u.email, u.firstname, u.lastname, u.middlename, u.mobile,u.livestock, " +
      "u.willing_to_travel, p.name, u.resume_upload_date, u.resume_upload_url, " +
      "u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, adt.date_shortlist, adt.date_joborder, (NOW() - adt.date_joborder) as running_time, d.id as deptid " +
      "FROM users u JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
      "WHERE u.status = 'job offer' " +
      `${filter}` +
      " ORDER BY u.resume_upload_date DESC";
    return db.query(sql);
  }


  async function findAllKeptForReferencePG(deptid, posid, date_from, date_to) {
    let extQuery = ``;
    if (date_from && date_to) { extQuery = `AND u.resume_upload_date BETWEEN '${date_from}' AND '${date_to}' ` }
    const db = await dbs();
    var filter = "";

    if (deptid) {
      filter = filter.concat(" AND d.id = $1 ");
    }
    if (posid && posid != "dept") {
      filter = filter.concat(" AND p.id =  $2 ");
    }

    const sql =
      "SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,position_id,u.filled_up_form,u.is_done_iqbe_exam, " +
      "willing_to_travel,p.name,resume_upload_date,resume_upload_url, u.date_application, " +
      "adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, adt.date_shortlist, adt.date_kept_reference, adt.remarks_kept_reference, " +
      "(NOW() - adt.date_kept_reference) as running_time, d.id as deptid, cat_count.category_count " +
      "FROM users u JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
      "LEFT JOIN ( " +
      "SELECT teams_id, COUNT(teams_id) as category_count " +
      "FROM categories " +
      "GROUP BY teams_id " +
      ") as cat_count ON t.id = cat_count.teams_id " +
      "WHERE u.status = 'kept for reference' " +
      `${filter} ${extQuery}` +
      " ORDER BY u.id DESC"; //u.date_application ASC, u.id ASC;"; 
    if (deptid && posid != "dept") {
      params = [deptid, posid];
      return db.query(sql, params);
    } else if (deptid) {
      params = [deptid];
      return db.query(sql, params);
    } else {
      return db.query(sql);
    }
  }
  async function findAllOnlineExamPG(dateFrom, dateTo) {
    let extQuery = ``;
    if (dateFrom && dateTo) { extQuery = `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}' ` }
    const db = await dbs();
    const sql =
      "SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,position_id, " +
      "willing_to_travel,p.name,resume_upload_date,resume_upload_url,adt.online_exam_start, " +
      "u.date_application, adt.date_received, adt.date_online_exam, checkexam.status, " +
      "rh.remaining_hours, rh.remaining_minutes, (NOW() - adt.online_exam_start) as running_time, d.id as deptid,  " +
      "checkexam.not_answered_categories, checkexam.total_time_exam_seconds, checkexam.total_time_elapsed_seconds " +
      "FROM users u JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
      "LEFT JOIN (" +
      " SELECT cr.user_id, COUNT(cr.is_done) as not_answered_categories, " +
      "u.total_time_exam_seconds, u.total_time_elapsed_seconds, u.status " +
      "FROM categories_random cr " +
      "JOIN users u ON cr.user_id = u.id " +
      "WHERE is_done = 'no' " +
      "GROUP BY cr.user_id, u.total_time_exam_seconds, u.total_time_elapsed_seconds, u.status " +
      ") checkexam ON u.id = checkexam.user_id " +
      "LEFT JOIN( " +
      "SELECT adt.users_id," +
      "EXTRACT(HOUR FROM (adt.online_exam_end - NOW())) as remaining_hours, " +
      "EXTRACT(MINUTE FROM (adt.online_exam_end - NOW())) as remaining_minutes " +
      "FROM applicant_date_trackings adt " +
      ") rh ON u.id = rh.users_id " +
      `WHERE u.status = 'online exam' ${extQuery} ORDER BY u.id DESC;`; //adt.online_exam_start ASC, u.id ASC;";
    return db.query(sql);
  }
  async function findAllOnlineExamDH({deptID, dateFrom, dateTo, offset = 0, limit = null}) {
    try {
      let extQuery = (dateFrom && dateTo) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : ''

      const db = await dbs();
      const sql =
        `SELECT u.id,email,firstname,lastname,middlename,mobile,livestock, 
        willing_to_travel,p.name,resume_upload_date,resume_upload_url,adt.online_exam_start, 
        u.date_application, adt.date_received, adt.date_online_exam, checkexam.status,
        rh.remaining_hours, rh.remaining_minutes, (NOW() - adt.online_exam_start) as running_time, d.id as deptid, 
        checkexam.not_answered_categories, checkexam.total_time_exam_seconds, checkexam.total_time_elapsed_seconds
        FROM users u JOIN positions p ON p.id = u.position_id 
        JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id
        LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id 
        LEFT JOIN (
        SELECT cr.user_id, COUNT(cr.is_done) as not_answered_categories, 
        u.total_time_exam_seconds, u.total_time_elapsed_seconds, u.status 
        FROM categories_random cr
        JOIN users u ON cr.user_id = u.id
        WHERE is_done = 'no'
        GROUP BY cr.user_id, u.total_time_exam_seconds, u.total_time_elapsed_seconds, u.status
        ) checkexam ON u.id = checkexam.user_id
        LEFT JOIN(
        SELECT adt.users_id,
        EXTRACT(HOUR FROM (adt.online_exam_end - NOW())) as remaining_hours,
        EXTRACT(MINUTE FROM (adt.online_exam_end - NOW())) as remaining_minutes
        FROM applicant_date_trackings adt
        ) rh ON u.id = rh.users_id
        WHERE u.status = 'online exam' AND d.id=$1 ${extQuery} 
        ORDER BY u.resume_upload_date DESC
        LIMIT ${limit}
        OFFSET ${offset}`;
      const params = [deptID];
      return db.query(sql, params);
    } catch (error) {
      console.log(error)
    }
  }

  async function fetchAllPendingStatus({dateTo, dateFrom, offset = 0, limit = null}) {
    try {
      const db = await dbs();
      const dateFilter = (dateTo && dateFrom) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : ``;

      let sql = `SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,    
                  willing_to_travel,p.name,resume_upload_date,resume_upload_url,schedule_for_assessment, (NOW() - adt.date_pending) as running_time,
                  online_exam_start,online_exam_end, d.id as deptid, adt.date_assessment as date_interview
                FROM users u 
                LEFT JOIN positions p ON p.id = u.position_id
                LEFT JOIN teams t ON t.id = p.teams_id LEFT JOIN departments d ON d.id = t.department_id
                LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
                WHERE u.status = 'pending for review' 
                ${dateFilter}
                ORDER BY u.id DESC
                LIMIT ${limit}
                OFFSET ${offset}`;
      return db.query(sql);
    } catch (error) {
      console.log(error)
    }
  }

  async function findAllOnlineExamPGDeptAndPosition(id, posId, date_from, date_to) {
    let extQuery = ``;
    if (date_from && date_to) { extQuery = `AND u.resume_upload_date BETWEEN '${date_from}' AND '${date_to}' ` }

    const db = await dbs();
    const sql =
      "SELECT u.id,email,firstname,lastname,middlename,mobile,livestock, " +
      "willing_to_travel,p.name,resume_upload_date,resume_upload_url,adt.online_exam_start, " +
      "u.date_application, adt.date_received, adt.date_online_exam, checkexam.status, " +
      "rh.remaining_hours, rh.remaining_minutes, (NOW() - adt.online_exam_start) as running_time, d.id as deptid,  " +
      "checkexam.not_answered_categories, checkexam.total_time_exam_seconds, checkexam.total_time_elapsed_seconds " +
      "FROM users u JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
      "LEFT JOIN (" +
      " SELECT cr.user_id, COUNT(cr.is_done) as not_answered_categories, " +
      "u.total_time_exam_seconds, u.total_time_elapsed_seconds, u.status " +
      "FROM categories_random cr " +
      "JOIN users u ON cr.user_id = u.id " +
      "WHERE is_done = 'no' " +
      "GROUP BY cr.user_id, u.total_time_exam_seconds, u.total_time_elapsed_seconds, u.status " +
      ") checkexam ON u.id = checkexam.user_id " +
      "LEFT JOIN( " +
      "SELECT adt.users_id," +
      "EXTRACT(HOUR FROM (adt.online_exam_end - NOW())) as remaining_hours, " +
      "EXTRACT(MINUTE FROM (adt.online_exam_end - NOW())) as remaining_minutes " +
      "FROM applicant_date_trackings adt " +
      ") rh ON u.id = rh.users_id " +
      `WHERE u.status = 'online exam' AND d.id=$1 AND p.id=$2 ${extQuery} ORDER BY u.id DESC`; //adt.online_exam_start ASC, u.id ASC;";
    const params = [id, posId];
    return db.query(sql, params);
  }
  async function findAllPendingPG(deptid, posid) {

    const db = await dbs();
    var filter = "";

    if (deptid) {
      filter = filter.concat(` AND d.id = ${deptid} `);
    }
    if (posid && posid != "dept") {
      filter = filter.concat(` AND p.id =  ${posid} `);
    }

    const sql =
      "SELECT u.id,email,firstname,lastname,middlename,mobile,livestock, " +
      "willing_to_travel,p.name,resume_upload_date,resume_upload_url,schedule_for_assessment, (NOW() - adt.date_pending) as running_time, " +
      "online_exam_start,online_exam_end, d.id as deptid, adt.date_assessment as date_interview FROM users u LEFT JOIN positions p ON p.id = u.position_id " +
      "LEFT JOIN teams t ON t.id = p.teams_id LEFT JOIN departments d ON d.id = t.department_id " +
      "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
      //"JOIN interview_assessments ia ON ia.user_id_applicant = u.id " +
      " WHERE u.status = 'pending for review' " +
      `${filter}` +
      "ORDER BY u.id DESC"; //adt.date_pending ASC, u.id ASC;";  

    return db.query(sql);

  }


  async function findAllRejected(dateFrom, dateTo) {
    const db = await dbs();
    let dateFilter = (dateFrom && dateTo) ? ` AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : ''

    const sql =
      `SELECT u.id, u.email, u.firstname, u.lastname, u.middlename, u.mobile, u.livestock,
      u.willing_to_travel, p.name, u.resume_upload_date, u.resume_upload_url, adt.remarks_reject,
      u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, adt.date_shortlist, adt.date_rejected, 
      us.firstname as rb_firstname, us.lastname as rb_lastname, d.id as deptid
      FROM users u 
      LEFT JOIN positions p ON p.id = u.position_id 
      LEFT JOIN teams t ON t.id = p.teams_id 
      LEFT JOIN departments d ON d.id = t.department_id 
      LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
      LEFT JOIN users us ON adt.rejected_by = us.id
      WHERE u.status = 'rejected' 
      ${dateFilter}
       ORDER BY u.id DESC`;
    return db.query(sql);
  }


  async function getAllRejected(params) {
    const db = await dbs(),
        { deptid, posid, from, to } = params,
        deptFilter = (deptid) ? ` AND d.id = ${deptid}` : '',
        posFilter = (posid && deptid) ? ` AND p.id = ${posid}`: '',
        dateFilter = (from && to) ? ` AND u.resume_upload_date BETWEEN '${from}' AND '${to}'`: '',
        sql =
          `SELECT u.id, u.email, u.firstname, u.lastname, u.middlename, u.mobile, u.livestock,
          u.willing_to_travel, p.name, u.resume_upload_date, u.resume_upload_url, adt.remarks_reject,
          u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, adt.date_shortlist, adt.date_rejected, 
          us.firstname as rb_firstname, us.lastname as rb_lastname, d.id as deptid
          FROM users u 
          LEFT JOIN positions p ON p.id = u.position_id 
          LEFT JOIN teams t ON t.id = p.teams_id 
          LEFT JOIN departments d ON d.id = t.department_id 
          LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
          LEFT JOIN users us ON adt.rejected_by = us.id
          WHERE u.status = 'rejected' 
          ${deptFilter}
          ${posFilter}
          ${dateFilter}
          ORDER BY u.id DESC`;
    return db.query(sql);
  }


  async function findAllShortListPG(date_from, date_to) {
    let extQuery = ``;
    if (date_from && date_to) { extQuery = `AND u.resume_upload_date BETWEEN '${date_from}' AND '${date_to}' ` }
    const db = await dbs();

    const sql =
      "SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,u.is_done_iqbe_exam, " +
      "willing_to_travel,u.position_id,p.name,resume_upload_date,resume_upload_url, " +
      "u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, " +
      "adt.date_shortlist, (NOW() - adt.date_shortlist) as running_time, d.id as deptid " +
      "FROM users u JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
      "WHERE u.status = 'shortlisted' " +
      `${extQuery}` +
      " ORDER BY u.id DESC"; //adt.date_shortlist ASC, u.id ASC;";

    if (deptid && posid != "dept") {
      params = [deptid, posid];
      return db.query(sql, params);
    } else if (deptid) {
      params = [deptid];
      return db.query(sql, params);
    } else {
      return db.query(sql);
    }
  }


  async function getAllShortlisted(deptid, posid) {
    const db = await dbs(),
          filter = (deptid && posid) ? ` AND d.id = ${deptid} AND p.id = ${posid}` : ` AND d.id = ${deptid}`,
          sql =
            "SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,u.is_done_iqbe_exam, " +
            "willing_to_travel,u.position_id,p.name,resume_upload_date,resume_upload_url, " +
            "u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, " +
            "adt.date_shortlist, (NOW() - adt.date_shortlist) as running_time, d.id as deptid " +
            "FROM users u JOIN positions p ON p.id = u.position_id " +
            "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
            "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
            "WHERE u.status = 'shortlisted' " +
            `${filter}` +
            " ORDER BY u.resume_upload_date DESC";
    return db.query(sql);
  }


  // until here
  async function onlineExamResult(id, posid) {
    const db = await dbs();
    const sql =
      // "SELECT DISTINCT a.user_id,a.firstname,a.lastname,a.catgname,COALESCE(b.correctanswer,0) AS correctanswer, " +
      // "COALESCE(c.wronganswer,0) AS wronganswer FROM ( " +
      // "SELECT cr.user_id,u.firstname,u.lastname,cr.category_id,c.name " +
      // "AS catgname FROM categories_random cr JOIN categories c " +
      // "ON c.id = cr.category_id JOIN users u ON u.id = cr.user_id WHERE cr.user_id = $1 " +
      // ")a LEFT JOIN (" +
      // "SELECT COUNT(u.id) AS correctanswer, q.categories_id FROM users_scores u JOIN " +
      // "questions q ON q.id = u.question_id WHERE u.is_correct = 'yes' AND user_id = $1 GROUP BY q.categories_id " +
      // ")b ON a.category_id = b.categories_id LEFT JOIN ( " +
      // "SELECT COUNT(u.id) AS wronganswer, q.categories_id FROM users_scores u " +
      // "JOIN questions q ON q.id = u.question_id " +
      // "WHERE u.is_correct = 'no' AND user_id = $1 GROUP BY q.categories_id " +
      // ")c ON a.category_id = c.categories_id WHERE a.catgname <> 'IQ Test' AND a.catgname <> 'Math Test' " +
      // "AND a.catgname <> 'Psychological Test' AND a.catgname <> 'Word Analogy';";
      "SELECT DISTINCT a.user_id,a.firstname,a.lastname,a.catgname,COALESCE(b.correctanswer,0) AS correctanswer, " +
      "COALESCE(c.wronganswer,0) AS wronganswer " +
      "FROM ( " +
      "SELECT cr.user_id,u.firstname,u.lastname, cr.category_id, c.name " +
      "AS catgname " +
      "FROM categories_random cr " +
      "JOIN categories c " +
      "ON c.id = cr.category_id JOIN users u ON u.id = cr.user_id " +
      "WHERE cr.user_id = $1 " +
      ") a " +
      "LEFT JOIN ( " +
      "SELECT COUNT(u.id) AS correctanswer, q.categories_id, p.position " +
      "FROM users_scores u " +
      "JOIN questions q ON q.id = u.question_id " +
      "LEFT JOIN categories c ON q.categories_id = c.id " +
      "LEFT JOIN teams t ON c.teams_id = t.id " +
      "LEFT JOIN ( " +
      "SELECT p.id as position, t.id as team " +
      "FROM positions p " +
      "LEFT JOIN teams t ON p.teams_id = t.id " +
      "WHERE p.id = $2 " +
      ") p ON t.id = p.team " +
      "WHERE u.is_correct = 'yes' AND user_id = $1 AND p.position = $2 " +
      "GROUP BY q.categories_id,  p.position " +
      ") b ON a.category_id = b.categories_id " +
      "LEFT JOIN ( " +
      "SELECT COUNT(u.id) AS wronganswer, q.categories_id, p.position " +
      "FROM users_scores u " +
      "JOIN questions q ON q.id = u.question_id " +
      "LEFT JOIN categories c ON q.categories_id = c.id " +
      "LEFT JOIN teams t ON c.teams_id = t.id " +
      "LEFT JOIN ( " +
      "SELECT p.id as position, t.id as team " +
      "FROM positions p " +
      "LEFT JOIN teams t ON p.teams_id = t.id " +
      " WHERE p.id = $2 " +
      ") p ON t.id = p.team " +
      "WHERE (u.is_correct = 'no' OR u.is_correct IS NULL) AND user_id = $1 AND p.position = $2 " +
      "GROUP BY q.categories_id, p.position " +
      ") c ON a.category_id = c.categories_id " +
      "WHERE a.catgname <> 'IQ Test' AND a.catgname <> 'Math Test' " +
      "AND a.catgname <> 'Psychological Test' AND a.catgname <> 'Word Analogy' " +
      "AND (c.position = $2 OR b.position = $2); ";
    const params = [id, posid];
    return db.query(sql, params);
  }
  // IQBE Exam result
  async function iqbeExamResult(id) {
    const db = await dbs();
    const sql =
      // "SELECT DISTINCT a.user_id,a.firstname,a.lastname,a.catgname,COALESCE(b.correctanswer,0) AS correctanswer, " +
      // "COALESCE(c.wronganswer,0) AS wronganswer FROM ( " +
      // "SELECT cr.user_id,u.firstname,u.lastname,cr.category_id,c.name " +
      // "AS catgname FROM categories_random cr JOIN categories c " +
      // "ON c.id = cr.category_id JOIN users u ON u.id = cr.user_id WHERE cr.user_id = $1 " +
      // ")a LEFT JOIN (" +
      // "SELECT COUNT(u.id) AS correctanswer, q.categories_id FROM users_scores u JOIN " +
      // "questions q ON q.id = u.question_id WHERE u.is_correct = 'yes' AND user_id = $1 GROUP BY q.categories_id " +
      // ")b ON a.category_id = b.categories_id LEFT JOIN ( " +
      // "SELECT COUNT(u.id) AS wronganswer, q.categories_id FROM users_scores u " +
      // "JOIN questions q ON q.id = u.question_id " +
      // "WHERE u.is_correct = 'no' AND user_id = $1 GROUP BY q.categories_id " +
      // ")c ON a.category_id = c.categories_id WHERE a.catgname = 'IQ Test' OR a.catgname = 'Math Test' " +
      // "OR a.catgname = 'Psychological Test' OR a.catgname = 'Word Analogy';";
      `SELECT DISTINCT a.user_id,a.firstname,a.lastname,a.catgname,COALESCE(b.correctanswer,0) AS correctanswer, 
      COALESCE(c.wronganswer,0) AS wronganswer FROM ( 
      SELECT cr.user_id,u.firstname,u.lastname,cr.category_id,c.name
      AS catgname FROM categories_random cr JOIN categories c 
      ON c.id = cr.category_id JOIN users u ON u.id = cr.user_id WHERE cr.user_id = $1
      )a LEFT JOIN (
      SELECT COUNT(u.id) AS correctanswer, q.categories_id FROM users_scores u JOIN 
      questions q ON q.id = u.question_id WHERE u.is_correct = 'yes' AND user_id = $1 GROUP BY q.categories_id 
      )b ON a.category_id = b.categories_id LEFT JOIN ( 
      SELECT COUNT(u.id) AS wronganswer, q.categories_id FROM users_scores u 
      JOIN questions q ON q.id = u.question_id 
      WHERE u.is_correct = 'no' AND user_id = $1 GROUP BY q.categories_id 
      )c ON a.category_id = c.categories_id WHERE a.catgname LIKE '%IQ Test%'`
    const params = [id];
    return db.query(sql, params);
  }
  // use to insert in logs
  async function insertUserLogs(id, ...info) {
    const db = await dbs();
    const sql = "INSERT INTO logs (id, user_id, module, table_affected, activity, date_time) VALUES (DEFAULT, $1, $2, $3, $4, NOW());";
    const params = [
      id,
      info[0].module,
      info[0].table_affected,
      info[0].activity,
    ];
    return db.query(sql, params);
  }
  // use to insert in logs
  async function getIdThruToken(token) {
    const db = await dbs();
    // set time zone for logs
    const sqls = "SET TIMEZONE='Asia/Kuala_Lumpur';";
    await db.query(sqls);
    // end
    const sql =
      "SELECT u.id,u.sign_in_count,t.department_id, t.id as team_id FROM users u LEFT JOIN positions p ON p.id = u.position_id " +
      "LEFT JOIN teams t ON t.id = p.teams_id LEFT JOIN departments d ON d.id = t.department_id WHERE u.token = $1;";
    const params = [token];
    return db.query(sql, params);
  }
  // update referral form
  async function updateReferralForm({ id, ...userInfo }) {
    const db = await dbs();
    const sql =
      "UPDATE referral_forms SET referrer_name=$2, referrer_dept=$3, referrer_position=$4, " +
      "referrer_id=$5, referrer_contact=$6, " +
      "referral_relationship_referrer=$7 WHERE id=$1;";
    const params = [
      id,
      userInfo.referrer_name,
      userInfo.referrer_dept,
      userInfo.referrer_position,
      userInfo.referrer_id,
      userInfo.referrer_contact,
      userInfo.referral_relationship_referrer,
    ];
    return db.query(sql, params);
  }
  // view single referral form
  async function viewOneReferralForm(id) {
    const db = await dbs();
    const sql =
      `select r.referrer_id, u1.firstname as ref_fname, u1.lastname as ref_lname, d.name as "department", p.name as "position", u1.mobile as referrer_contact, r.applicant_id, adt.date_deployed, u2.firstname, u2.lastname, p2.name as "applicantPosition", u2.mobile, r.referral_relationship_referrer
      from referral_forms r
      left join users u1 on r.referrer_id = u1.id
      left join users u2 on r.applicant_id = u2.id
      left join positions p on p.id = u1.position_id
      left join positions p2 on p2.id = u2.position_id
      left join teams t on t.id = p.teams_id
      left join departments d on d.id = t.department_id
      left join applicant_date_trackings adt on adt.users_id = u2.id
      where r.applicant_id = $1`
    const params = [id];
    return db.query(sql, params);
  }
  // view referral form
  async function viewAllReferralForm() {
    const db = await dbs();
    const sql =
      `select r.referrer_id, u1.firstname as ref_fname, u1.lastname as ref_lname, d.name as "department", p.name as "position", u1.mobile as referrer_contact, r.applicant_id, adt.date_deployed, u2.firstname, u2.lastname, p2.name as "applicantPosition", u2.mobile, r.referral_relationship_referrer
      from referral_forms r
      left join users u1 on r.referrer_id = u1.id
      left join users u2 on r.applicant_id = u2.id
      left join positions p on p.id = u1.position_id
      left join positions p2 on p2.id = u2.position_id
      left join teams t on t.id = p.teams_id
      left join departments d on d.id = t.department_id
      left join applicant_date_trackings adt on adt.users_id = u2.id
      `;
    return db.query(sql);
  }
  // make new referral form
  async function addReferralForm({ ...userInfo }) {
    const db = await dbs();
    const sql =
      "INSERT INTO referral_forms(id, referrer_id, applicant_id, time_created, time_updated) VALUES (DEFAULT, $1, $2, $3, NOW(), NOW());";
    const params = [
      userInfo.referrer_id,
      userInfo.applicant_id,
      userInfo.referral_relationship_referrer,
    ];
    return db.query(sql, params);
  }
  // use to show who's logged in in the system
  async function getNameThruToken(token) {
    const db = await dbs();
    const sql = "SELECT	firstname,lastname FROM users WHERE token = $1";
    const params = [token];
    return db.query(sql, params);
  }
  // get position name for received status email
  async function getPositionName(id) {
    const db = await dbs();
    const sql = "SELECT name FROM positions WHERE id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // get email and full name for email
  async function returnEmailAndName(id) {
    const db = await dbs();
    const sql =
      "SELECT u.id, email, firstname, lastname AS names,p.name AS position,d.name AS deptName FROM users u " +
      "LEFT JOIN positions p ON p.id = u.position_id LEFT JOIN teams t ON t.id = p.teams_id " +
      "LEFT JOIN departments d ON d.id = t.department_id WHERE u.id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // 3hrs for IQBE
  async function timeForIQBE(id) {
    const db = await dbs();
    const sql = "UPDATE users SET iqbe_time = 10800 WHERE id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // for position drop down on application form
  async function getSameTeamPositionApplicationForm(id) {
    const db = await dbs();
    const sql =
      "SELECT id AS value ,name AS text FROM positions WHERE teams_id = $1 ORDER BY name;";
    const params = [id];
    return db.query(sql, params);
  }
  // existing user's data during self register in apply
  async function getUsersDataApplicationForm(token) {
    const db = await dbs();
    const sql =
      "SELECT u.id,firstname,middlename,lastname,u.filled_up_form,u.user_image_path, p.name, p.teams_id, u.resume_upload_date " +
      "FROM users u JOIN positions p ON p.id = u.position_id WHERE u.token = $1;";
    const params = [token];
    return db.query(sql, params);
  }
  // fetch data thru id use for print and viewing application form
  async function getUsersDataApplicationFormPrintView(id) {
    const db = await dbs();
    const sql =
      "SELECT a.id, a.firstname,a.middlename,a.lastname,a.mobile,a.name,a.user_image_path,b.name AS names FROM ( " +
      "SELECT u.id,firstname,middlename,mobile,user_image_path,lastname, p.name " +
      "FROM users u JOIN positions p ON p.id = u.position_id WHERE u.id = $1 " +
      ")a   LEFT JOIN ( SELECT u.id, p.name " +
      "FROM users u JOIN positions p ON p.id = u.position_id_second_choice WHERE u.id = $1 " +
      ")b ON a.id = b.id;";

    const params = [id];
    return db.query(sql, params);
  }
  // fetch data of application form
  async function applicationFormData(token) {
    const db = await dbs();
    const sql =
      "SELECT salary_desired,unitroomnumberfloor,buildingname,lotblockphasehousenumber,streetname,villagesubdivision,barangay, " +
      "towndistrict,municipality,cityprovince,place_birth,bday,age,height,weight,gender,is_single_parent,civil_status,citizenship, " +
      "religion,who_referred,when_to_start,person_to_notify,person_relationship,person_address,person_telephone, " +
      "position_id_second_choice,cs_eligible,cs_date_taken,date_application " +
      "FROM users WHERE token = $1;";
    const params = [token];
    return db.query(sql, params);
  }
  // fetch data of application form for print and view
  async function applicationFormDataPrintView(id) {
    const db = await dbs();
    const sql =
      "SELECT salary_desired,unitroomnumberfloor,buildingname,lotblockphasehousenumber,streetname,villagesubdivision,barangay, " +
      "towndistrict,municipality,cityprovince,place_birth,bday,age,height,weight,gender,is_single_parent,civil_status,citizenship, " +
      "religion,who_referred,when_to_start,person_to_notify,person_relationship,person_address,person_telephone, " +
      "position_id_second_choice,cs_eligible,cs_date_taken,date_application " +
      "FROM users WHERE id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // education bg
  async function applicantsEducation(id) {
    const db = await dbs();
    const sql = "SELECT * FROM users_educational_bg WHERE users_id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // relative array
  async function applicantsRelative(id) {
    const db = await dbs();
    const sql = "SELECT * FROM users_relative_in_bfi WHERE user_id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // professional licenses
  async function applicantsProfLicenses(id) {
    const db = await dbs();
    const sql =
      "SELECT * FROM users_professional_licenses WHERE users_id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // org memberships
  async function applicantsOrgMemberships(id) {
    const db = await dbs();
    const sql = "SELECT * FROM users_org_memberships WHERE users_id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // seminars attended
  async function applicantsSeminars(id) {
    const db = await dbs();
    const sql = "SELECT * FROM users_seminar_attends WHERE users_id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // work experience
  async function applicantsWorkExp(id) {
    const db = await dbs();
    const sql =
      "SELECT * FROM users_work_experiences WHERE users_id = $1 ORDER BY end_date DESC;";
    const params = [id];
    return db.query(sql, params);
  }
  // have work with bfi
  async function applicantsWorkWithBfi(id) {
    const db = await dbs();
    const sql = "SELECT * FROM users_have_work_with_bfi WHERE user_id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // other info
  async function applicantsOtherInfo(id) {
    const db = await dbs();
    const sql = "SELECT * FROM users_other_info WHERE users_id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // family bg
  async function applicantsFamilyBg(id) {
    const db = await dbs();
    const sql = "SELECT * FROM users_family_bg WHERE users_id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // if married
  async function applicantsMarried(id) {
    const db = await dbs();
    const sql = "SELECT * FROM users_is_married WHERE users_id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // refernece
  async function applicantsReference(id) {
    const db = await dbs();
    const sql = "SELECT * FROM users_references WHERE users_id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // willingness to work
  async function applicantsWillingness(id) {
    const db = await dbs();
    const sql = "SELECT * FROM users_willingness_to_work WHERE users_id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // end
  // for jeonsoft queries
  async function getEmployeeAddressContactEmergency(id) {
    const db = await dbs();
    const sql =
      "SELECT u.id,firstname,middlename,lastname,unitroomnumberfloor,buildingname,lotblockphasehousenumber,streetname, " +
      "villagesubdivision,barangay,towndistrict,municipality,cityprovince,addresstype, person_to_notify, person_address, " +
      "person_telephone,person_relationship,mobile,gender, civil_status,tin,sss,citizenship,bday,email " +
      "FROM users u JOIN users_willingness_to_work uw ON uw.users_id = u.id WHERE u.id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  async function getEmployeeCertifications(id) {
    const db = await dbs();
    const sql =
      "SELECT users_id,professional_licenses_held,certification_number FROM " +
      "users_professional_licenses WHERE users_id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  async function getEmployeeEducation(id) {
    const db = await dbs();
    const sql =
      "SELECT users_id,educational_attainment,institution_name,institution_address,course_degree, " +
      "SPLIT_PART(years_attended, '-', 1) AS startyear, SPLIT_PART(years_attended, '-', 2) AS endyear " +
      "FROM users_educational_bg WHERE users_id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  async function getEmployeeFamilyBg(id) {
    const db = await dbs();
    const sql =
      "SELECT CONCAT(a.mfirstname,' ',a.mmiddlename,' ',a.mlastname) mothermaiden, " +
      "CONCAT(b.ffirstname,' ',b.fmiddlename,' ',b.flastname) fathersname, " +
      "CONCAT(a.mfirstname,' ',a.mlastname,' ',b.flastname) mothersname, " +
      "c.sfirstname,c.smiddlename,c.slastname FROM ( " +
      "SELECT id,users_id,SPLIT_PART(name, ' ', 1) mfirstname,SPLIT_PART(name, ' ', 2) mmiddlename,SPLIT_PART(name, ' ', 3) mlastname " +
      "FROM users_family_bg WHERE users_id = $1 AND LOWER(relationship) = LOWER('Mother') ) a LEFT JOIN ( " +
      "SELECT id,users_id,SPLIT_PART(name, ' ', 1) ffirstname,SPLIT_PART(name, ' ', 2) fmiddlename,SPLIT_PART(name, ' ', 3) flastname " +
      "FROM users_family_bg WHERE users_id = $1 AND LOWER(relationship) = LOWER('father') " +
      ") b ON b.users_id = a.users_id LEFT JOIN ( SELECT id,users_id,SPLIT_PART(name, ' ', 1) sfirstname,SPLIT_PART(name, ' ', 2) smiddlename,SPLIT_PART(name, ' ', 3) slastname " +
      "FROM users_family_bg WHERE users_id = $1 AND LOWER(relationship) = LOWER('spouse') " +
      ") c ON c.users_id = a.users_id;";
    const params = [id];
    return db.query(sql, params);
  }
  async function getEmployeeWorkExperience(id) {
    const db = await dbs();
    const sql =
      "SELECT users_id,company_name,address,position,last_salary,start_date,end_date,reason_leaving " +
      "FROM users_work_experiences WHERE users_id=$1;";
    const params = [id];
    return db.query(sql, params);
  }
  async function getEmployeePreviousEmployer(id) {
    const db = await dbs();
    const sql =
      "SELECT users_id,company_name,address,extract(year from end_date) yearend FROM users_work_experiences " +
      "WHERE users_id = $1 ORDER BY end_date DESC LIMIT 1;";
    const params = [id];
    return db.query(sql, params);
  }
  async function isExportedToTrue(id) {
    const db = await dbs();
    const sql = "UPDATE users SET is_exported = 't' WHERE id=$1;";
    const params = [id];
    return db.query(sql, params);
  }
  async function displayApplicantsReadyForExport() {
    const db = await dbs();
    const sql =
      "SELECT DISTINCT u.id,email,firstname,lastname,middlename,is_exported FROM users u LEFT JOIN " +
      "users_educational_bg ue ON ue.users_id = u.id " +
      "LEFT JOIN users_family_bg uf ON uf.users_id = u.id LEFT JOIN users_have_work_with_bfi uh ON uh.user_id = u.id " +
      "LEFT JOIN users_is_married um ON um.users_id = u.id LEFT JOIN users_org_memberships uo ON uo.users_id = u.id " +
      "LEFT JOIN users_other_info ui ON ui.users_id = u.id LEFT JOIN users_professional_licenses up ON up.users_id = u.id " +
      "LEFT JOIN users_willingness_to_work uw ON uw.users_id = u.id LEFT JOIN users_work_experiences uwe ON uwe.users_id = u.id " +
      "LEFT JOIN users_references urs ON urs.users_id = u.id LEFT JOIN users_seminar_attends usa ON usa.users_id = u.id " +
      "WHERE u.status = 'deployed' ORDER BY is_exported;";
    return db.query(sql);
  }
  // end jeonsoft queries

  // get token of user logged in in DB
  async function getTokenOfUser(email, password) {
    const db = await dbs();
    const sql = "SELECT id, token FROM users WHERE email = $1 AND password = $2";
    const params = [email, password];
    return db.query(sql, params);
  }
  // --
  // get name of applicant
  // get role of user who logged in
  async function getRoleOfUser(email, password) {
    const db = await dbs();
    //  ,r.access_rights_id 
    const sql =
      `SELECT r.name, r.access_rights_id
      FROM users u 
      JOIN roles r ON r.id = u.role_id
      WHERE u.email = $1 AND u.password = $2;`;
    const params = [email, password];

    return db.query(sql, params);
  }

  async function getRoleOfUserVerify(email, password) {
    const db = await dbs();
    //  ,r.access_rights_id 
    const sql =
      `SELECT r.name, r.access_rights_id,d.name as "DepName"
      FROM users u 
      JOIN roles r ON r.id = u.role_id
      JOIN positions p ON p.id = u.position_id
      JOIN teams t ON t.id = p.teams_id
      JOIN departments d on d.id= t.department_id
      WHERE u.email = $1 AND u.password = $2;`;
    const params = [email, password];

    return db.query(sql, params);
  }

  async function insert({ ...userInfo }) {
    const db = await dbs();

    let sql

    if (userInfo.isQuickApplicant == true) {
      sql =
        "INSERT INTO users (id,email,time_created,time_updated,sign_in_count, " +
        "firstname,lastname,mobile,livestock,willing_to_travel,resume_upload_date, " +
        "resume_upload_url,position_id,status,middlename,is_viewed,is_exported,is_done_online_exam, " +
        "is_done_iqbe_exam,is_viewed_th,is_viewed_oh,religion) " +
        "VALUES (DEFAULT,$1,NOW(),NOW(),0, " +
        "$2,$3,$4,$5,$6,NOW(),$7,$8,'quick applicant',$9,'f','f','f','f','f','f',$10) RETURNING id;";
    }

    else {
      sql =
      "INSERT INTO users (id,email,time_created,time_updated,sign_in_count, " +
      "firstname,lastname,mobile,livestock,willing_to_travel,resume_upload_date, " +
      "resume_upload_url,position_id,status,middlename,is_viewed,is_exported,is_done_online_exam, " +
      "is_done_iqbe_exam,is_viewed_th,is_viewed_oh,religion) " +
      "VALUES (DEFAULT,$1,NOW(),NOW(),0, " +
      "$2,$3,$4,$5,$6,NOW(),$7,$8,'received',$9,'f','f','f','f','f','f',$10) RETURNING id;";
    }

      let positionId = (userInfo.position_id) ? userInfo.position_id : null

    const params = [
      userInfo.email,
      userInfo.firstname,
      userInfo.lastname,
      userInfo.mobile,
      userInfo.livestock,
      userInfo.travel,
      userInfo.resume_url,
      positionId,
      userInfo.middlename,
      userInfo.religion,
    ];

    let newUserId

    await db.query(sql, params)
            .then(res => newUserId = Number(res.rows[0].id))
            .catch(err => console.log(err))
    
    const sql3 = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params3 = [newUserId];
    const data = await db.query(sql3, params3);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_received = NOW() " +
        "WHERE id = $1;";
      const params = [appId];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,date_received) " +
        "VALUES ($1,NOW())";
      const params = [newUserId];
      await db.query(sql, params);
    }
    //
    return {
      applicantID: newUserId
    }
  }

  async function findByEmail(inputEmail) {
    const db = await dbs();
    const sql = "SELECT * FROM users WHERE LOWER(email)=LOWER($1);";
    const params = [inputEmail];
    return db.query(sql, params);
  }
  async function findByFullName(inputFirstName, inputLastname) {
    const db = await dbs();
    const sql =
      "SELECT * FROM users WHERE LOWER(firstname)=LOWER($1) AND LOWER(lastname)=LOWER($2);";
    const params = [inputFirstName, inputLastname];
    return db.query(sql, params);
  }
  async function logins({ ...userInfo }) {
    const db = await dbs();
    const sql =
      "UPDATE users SET sign_in_count = sign_in_count + 1, login_time = NOW(), current_ip = $1, " +
      "token=$2 WHERE email=$3 AND password=$4 AND status='active';";
    const params = [
      userInfo.ip,
      userInfo.token,
      userInfo.email,
      userInfo.password,
    ];
    return db.query(sql, params);
  }

  async function findAllReceivedPGDefault(date_from, dateTo) {
    var extQuery = '';
    if (date_from && dateTo) {
      extQuery = `AND u.resume_upload_date BETWEEN '${date_from}' AND '${dateTo}'`
    }

    const db = await dbs();
    const sql =
      "SELECT u.id,email,firstname,lastname,middlename,mobile,livestock, " +
      "willing_to_travel,p.name,resume_upload_date,resume_upload_url,d.name as deptname, " +
      "adt.date_received, cat_count.category_count, (now() - adt.date_received) as running_time, d.id as deptid " +
      "FROM users u JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
      "LEFT JOIN ( " +
      "SELECT teams_id, COUNT(teams_id) as category_count " +
      "FROM categories " +
      "GROUP BY teams_id " +
      ") as cat_count ON t.id = cat_count.teams_id " +
      `WHERE u.status = 'received' ${extQuery} ORDER BY u.id DESC`; //resume_upload_date ASC, date_received ASC, u.id ASC;";

    return db.query(sql);
  }
  async function changeStatusOnlineExam(id, password) {
    const db = await dbs();
    //
    const sql = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET online_exam_start = NOW(), online_exam_end = (NOW() + interval '24 hour'), date_online_exam = NOW()" +
        "WHERE id = $1;";
      const params = [appId];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id, online_exam_start, online_exam_end, date_online_exam) " +
        "VALUES ($1, NOW(), (NOW() + interval '24 hour'), NOW())";
      const params = [id];
      await db.query(sql, params);
    }
    //
    const sql2 =
      "UPDATE users SET status = 'online exam', password=$2, is_done_online_exam='f', " +
      "is_viewed = 'f' WHERE id = $1;";
    const params2 = [id, password];
    return db.query(sql2, params2);
  }

  
  // retake online exam
  async function onlineExamRetake(id, password) {
    const db = await dbs();
    // date tracking
    const sql = "SELECT id FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET online_exam_start = NOW(),  online_exam_end = (NOW() + interval '24 hour'), date_online_exam = NOW() " +
        "WHERE id = $1;"
      const params = [appId];
      await db.query(sql, params);

      const sql2 = "UPDATE users set is_done_online_exam='f',is_done_iqbe_exam='f' where id=$1;";
      const params2 = [id];
      await db.query(sql2, params2);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id, online_exam_start, online_exam_end, date_online_exam) " +
        "VALUES ($1, NOW(), (NOW() + interval '24 hour'), NOW())";
      const params = [id];
      await db.query(sql, params);
    }
    // end date tracking
    // remove categories
    const sql2 = "DELETE FROM categories_random WHERE user_id = $1";
    const params2 = [id];
    await db.query(sql2, params2);
    // end remove categories
    // remove answers
    const sql3 = "DELETE FROM users_scores WHERE user_id = $1";
    const params3 = [id];
    await db.query(sql3, params3);
    // end remove answers
    // return status to online exam
    const sql4 =
      `UPDATE users SET status = 'online exam', password=$1, is_done_online_exam='f',  is_done_iqbe_exam='f', total_time_elapsed_seconds = null, iqbe_time = null WHERE id = $2;`;
    const params4 = [password, id];
    query = await db.query(sql4, params4);
    return query;
  }


  async function getAllOnlineExam(deptid, posid) {
    const db = await dbs(),
          filter = (deptid && posid) ? ` AND d.id = ${deptid} AND p.id = ${posid}` : ` AND d.id = ${deptid}`,
          sql =
      `SELECT u.id,email,firstname,lastname,middlename,mobile,livestock, 
      willing_to_travel,p.name,resume_upload_date,resume_upload_url,adt.online_exam_start,
      u.date_application, adt.date_received, adt.date_online_exam, checkexam.status, 
      rh.remaining_hours, rh.remaining_minutes, (NOW() - adt.online_exam_start) as running_time, d.id as deptid, 
      checkexam.not_answered_categories, checkexam.total_time_exam_seconds, checkexam.total_time_elapsed_seconds
      FROM users u JOIN positions p ON p.id = u.position_id 
      JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id
      LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
      LEFT JOIN (
      SELECT cr.user_id, COUNT(cr.is_done) as not_answered_categories, 
      u.total_time_exam_seconds, u.total_time_elapsed_seconds, u.status
      FROM categories_random cr
      JOIN users u ON cr.user_id = u.id
      WHERE is_done = 'no'
      GROUP BY cr.user_id, u.total_time_exam_seconds, u.total_time_elapsed_seconds, u.status
      ) checkexam ON u.id = checkexam.user_id
      LEFT JOIN(
      SELECT adt.users_id,
      EXTRACT(HOUR FROM (adt.online_exam_end - NOW())) as remaining_hours,
      EXTRACT(MINUTE FROM (adt.online_exam_end - NOW())) as remaining_minutes
      FROM applicant_date_trackings adt
      ) rh ON u.id = rh.users_id 
      WHERE u.status = 'online exam' 
      ${filter}
      ORDER BY u.id DESC`
    return db.query(sql);
  }


  async function findAllOnlineExam({dateFrom, dateTo, offset = 0, limit = null}) {
    try {
      let dateFilter = (dateFrom && dateTo) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}' ` : ''
      const db = await dbs();
      const sql =
        `SELECT u.id,email,firstname,lastname,middlename,mobile,livestock, 
        willing_to_travel,p.name,resume_upload_date,resume_upload_url,adt.online_exam_start,
        u.date_application, adt.date_received, adt.date_online_exam, checkexam.status, 
        rh.remaining_hours, rh.remaining_minutes, (NOW() - adt.online_exam_start) as running_time, d.id as deptid, 
        checkexam.not_answered_categories, checkexam.total_time_exam_seconds, checkexam.total_time_elapsed_seconds
        FROM users u JOIN positions p ON p.id = u.position_id 
        JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id
        LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
        LEFT JOIN (
        SELECT cr.user_id, COUNT(cr.is_done) as not_answered_categories, 
        u.total_time_exam_seconds, u.total_time_elapsed_seconds, u.status
        FROM categories_random cr
        JOIN users u ON cr.user_id = u.id
        WHERE is_done = 'no'
        GROUP BY cr.user_id, u.total_time_exam_seconds, u.total_time_elapsed_seconds, u.status
        ) checkexam ON u.id = checkexam.user_id
        LEFT JOIN(
        SELECT adt.users_id,
        EXTRACT(HOUR FROM (adt.online_exam_end - NOW())) as remaining_hours,
        EXTRACT(MINUTE FROM (adt.online_exam_end - NOW())) as remaining_minutes
        FROM applicant_date_trackings adt
        ) rh ON u.id = rh.users_id 
        WHERE u.status = 'online exam' 
        ${dateFilter}
        ORDER BY u.id DESC
        LIMIT ${limit}
        OFFSET ${offset}`
      return db.query(sql);
    } catch (error) {
      console.log(sql)
    }
  }


  async function changeStatusEndorsement(id) {
    const db = await dbs();
    //
    const sql = "SELECT id FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_endorsement = NOW() " +
        "WHERE id = $1;";
      const params = [appId];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,date_endorsement) " +
        "VALUES ($1,NOW())";
      const params = [id];
      await db.query(sql, params);
    }
    //
    const sql2 =
      "UPDATE users SET status = 'schedule for assessment' WHERE id = $1;";
    const params2 = [id];
    return db.query(sql2, params2);
  }
  async function changeStatusForEndorsement(id) {
    try {
      
    const db = await dbs();
    //
    const sql = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_for_endorsement = NOW() " +
        "WHERE id = $1;";
      const params = [appId];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,date_for_endorsement) " +
        "VALUES ($1,NOW())";
      const params = [id];
      await db.query(sql, params);
    }
    //
    const sql2 =
      "UPDATE users SET status = 'endorsement', is_viewed = 'f', is_viewed_th = 'f', is_viewed_oh = 'f' WHERE id = $1;";
    const params2 = [id];
    return db.query(sql2, params2);
    } catch (error) {
      console.log(error)
    }
  }

  async function findAllEndorsementDH({deptID, dateFrom, dateTo, offset = 0, limit = null}) {
    try {
      const db = await dbs(),
            dateFilter = (dateFrom && dateTo) ? `AND resume_upload_date BETWEEN '${dateFrom}' and '${dateTo}'` : '',
            sql =
              `SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,u.is_done_iqbe_exam,willing_to_travel,p.name,resume_upload_date,resume_upload_url,adt.online_exam_start,adt.online_exam_end, u.date_application, adt.date_received, (now() - adt.date_endorsement) as running_time, adt.date_endorsement, adt.date_for_endorsement
            FROM users u JOIN positions p ON p.id = u.position_id
            JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id
            LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
            WHERE 
            (u.status = 'endorsement' or u.status = 'schedule for assessment')
            AND (adt.date_endorsement IS NOT NULL
            AND adt.date_for_endorsement IS NOT NULL)
            AND d.id = ${deptID} ${dateFilter}
            ORDER BY u.id DESC
            LIMIT ${limit} OFFSET ${offset}`;
      return db.query(sql);
    } catch (error) {
      console.log(error) 
    }
  }
  async function findAllEndorsementFiltered(deptid, posid) {
    let db = await dbs(),
      deptFilter = (deptid) ? `AND d.id = ${deptid}` : '',
      deptAndPosFilter = (deptid && posid) ? `AND d.id = ${deptid} AND p.id = ${posid}` : '',
      sql =
        `SELECT u.id,u.email,u.firstname,u.lastname,u.middlename,u.mobile,u.livestock,u.is_done_iqbe_exam,
        u.willing_to_travel,p.name,u.resume_upload_date,u.resume_upload_url,adt.online_exam_start,adt.online_exam_end, u.date_application, adt.date_received, (now() - adt.date_endorsement) as running_time, u.status as stage, adt.date_endorsement
        FROM users u JOIN positions p ON p.id = u.position_id
        JOIN teams t
          ON t.id = p.teams_id
        JOIN departments d
          ON d.id = t.department_id
        LEFT JOIN applicant_date_trackings adt
          ON adt.users_id = u.id
        WHERE u.status = 'endorsement'
        AND adt.date_endorsement IS NOT NULL
        AND adt.date_for_endorsement IS NOT NULL
          ${deptAndPosFilter}
          ${deptFilter}
        ORDER BY u.id DESC`
    return db.query(sql)
  }
  async function findAllForEndorsement({deptID, teamID, dateFrom, dateTo, offset = 0, limit = null}) {
    try {

      let deptFilter = (deptID) ? `AND d.id = ${deptID}` : '',
          teamFilter = (teamID) ? `AND t.id = ${teamID}` : '',
          dateFilter = (dateFrom && dateTo) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : ''

      const db = await dbs(),
            sql =
              `SELECT
              u.ID,
              email,
              u.firstname,
              u.lastname,
              u.middlename,
              u.mobile,
              livestock,
              u.willing_to_travel,
              P.NAME AS positionname,
              u.resume_upload_date,
              u.resume_upload_url,
              u.date_application,
              d.NAME AS deptname,
              u.status AS stage,
              adt.date_for_endorsement,
              adt.online_exam_start,
              adt.online_exam_end
            FROM
              users u
              JOIN positions P ON P.ID = u.position_id
              JOIN teams T ON T.ID = P.teams_id
              JOIN departments d ON d.ID = T.department_id
              LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.ID 
            WHERE
              u.status = 'endorsement'
              AND adt.date_for_endorsement IS NOT NULL
              AND adt.date_endorsement IS NULL
              ${deptFilter}
              ${teamFilter}
              ${dateFilter}
            ORDER BY
              u.id DESC
            LIMIT ${limit}
            OFFSET ${offset}`;
      return db.query(sql);
    } catch (error) {
      console.log(error)
    }
  }
  async function changeStatusReject(id, remarks, rejectedBy) {
    const db = await dbs();
    //
    const sql = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET remarks_reject = $2, date_rejected = NOW(), rejected_by = $3 " +
        "WHERE id = $1;";
      const params = [appId, remarks, rejectedBy];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id, remarks_reject, date_rejected, rejected_by) " +
        "VALUES ($1, $2, NOW(), $3)";
      const params = [id, remarks, rejectedBy];
      await db.query(sql, params);
    }
    //
    const sql2 =
      "UPDATE users SET status = 'rejected',is_viewed = 'f' WHERE id = $1;";
    const params2 = [id];
    return db.query(sql2, params2);
  }


  async function findAllReject(deptID, dateFrom, dateTo) {
    const db = await dbs(),
      dateFilter = (dateForm && dateTo) ? ` AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : '',
      sql =
        `SELECT u.id, u.email, u.firstname, u.lastname, u.middlename, u.mobile, u.livestock, 
      u.willing_to_travel, p.name, u.resume_upload_date, u.resume_upload_url, adt.remarks_reject, 
      u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, adt.date_shortlist, adt.date_rejected, 
      us.firstname as rb_firstname, us.lastname as rb_lastname, d.id as deptid, adt.rejected_by
      FROM users u LEFT JOIN positions p ON p.id = u.position_id 
      LEFT JOIN teams t ON t.id = p.teams_id LEFT JOIN departments d ON d.id = t.department_id
      LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id 
      LEFT JOIN users us ON adt.rejected_by = us.id
      WHERE u.status = 'rejected' 
      AND d.id = ${deptID}
      ${dateFilter} 
      ORDER BY u.resume_upload_date DESC`
    return db.query(sql);
  }
  async function changeStatusBlacklist(id, remarks) {
    const db = await dbs();
    //
    const sql = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET remarks_blacklist = $2,date_blacklist = NOW() " +
        "WHERE id = $1;";
      const params = [appId, remarks];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,remarks_blacklist,date_blacklist) " +
        "VALUES ($1,$2,NOW())";
      const params = [id, remarks];
      await db.query(sql, params);
    }
    //
    const sql2 =
      "UPDATE users SET status = 'blacklist',is_viewed = 'f' WHERE id = $1;";
    const params2 = [id];
    return db.query(sql2, params2);
  }

  //filters blacklisted users based on the users department
  async function findAllBlacklistDH(deptId, dateFrom, dateTo) {
    const db = await dbs(),
      extQuery = (dateFrom && dateTo) ? `AND resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : '',
      sql =
        `SELECT u.id,email,firstname,lastname,middlename,mobile,livestock, 
      willing_to_travel,p.name,resume_upload_date,resume_upload_url,adt.remarks_blacklist, 
      u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, adt.date_blacklist, d.id as deptid 
      FROM users u LEFT JOIN positions p ON p.id = u.position_id 
      LEFT JOIN teams t ON t.id = p.teams_id LEFT JOIN departments d ON d.id = t.department_id 
      LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
      WHERE u.status = 'blacklist' AND d.id = $1 ${extQuery}
      ORDER BY u.id DESC;`
    const params = [deptId];
    return db.query(sql, params);
  }


  async function getAllBlacklisted({deptid, posid, from, to, offset = 0, limit = null} ={}) {
    const db = await dbs(),
      deptFilter = (deptid) ? ` AND d.id = ${deptid}` : '',
      posFilter = (posid) ? ` AND p.id = ${posid}`: '',
      dateFilter = (from && to) ? ` AND u.resume_upload_date BETWEENT '${from}' AND '${to}'`: '',
      sql =
        `SELECT
        u.ID,
        u.email,
        u.firstname,
        u.lastname,
        u.middlename,
        u.mobile,
        u.livestock,
        u.willing_to_travel,
        P.NAME,
        u.resume_upload_date,
        u.resume_upload_url,
        adt.remarks_blacklist,
        u.date_application,
        adt.date_received,
        adt.date_online_exam,
        adt.date_endorsement,
        adt.date_assessment,
        adt.date_blacklist 
      FROM
        users u
        INNER JOIN applicant_date_trackings adt ON u.ID = adt.users_id
        INNER JOIN positions P ON P.ID = u.position_id
        INNER JOIN teams t on t.id = p.teams_id
        INNER JOIN departments d on d.id = t.department_id
      WHERE
        u.status = 'blacklist' 
        ${deptFilter}
        ${posFilter}
        ${dateFilter}
      ORDER BY
        u.id desc
      LIMIT ${limit} OFFSET ${offset}`
    return db.query(sql)
  }


  async function findAllBlacklisted({dateFrom, dateTo, offset=0, limit=null}) {
    try {
      const db = await dbs(),
            dateFilter = (dateFrom && dateTo) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : '',
            sql =
                `SELECT
                u.ID,
                u.email,
                u.firstname,
                u.lastname,
                u.middlename,
                u.mobile,
                u.livestock,
                u.willing_to_travel,
                P.NAME,
                u.resume_upload_date,
                u.resume_upload_url,
                adt.remarks_blacklist,
                u.date_application,
                adt.date_received,
                adt.date_online_exam,
                adt.date_endorsement,
                adt.date_assessment,
                adt.date_blacklist 
              FROM
                users u
                INNER JOIN applicant_date_trackings adt ON u.ID = adt.users_id
                INNER JOIN positions P ON P.ID = u.position_id
                INNER JOIN teams t on t.id = p.teams_id
                INNER JOIN departments d on d.id = t.department_id
              WHERE
                u.status = 'blacklist' 
                ${dateFilter}
              ORDER BY
                u.id desc
              LIMIT ${limit} OFFSET ${offset}`
      return db.query(sql)
    } catch (error) {
      console.log(error)
    }
  }


  async function findAllBlacklistedTH({teamID, dateFrom, dateTo, offset=0, limit=null}) {
    try {
      const db = await dbs(),
            dateFilter = (dateFrom && dateTo) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : '',
            sql =
              `SELECT
              u.ID,
              u.email,
              u.firstname,
              u.lastname,
              u.middlename,
              u.mobile,
              u.livestock,
              u.willing_to_travel,
              P.NAME,
              u.resume_upload_date,
              u.resume_upload_url,
              adt.remarks_blacklist,
              u.date_application,
              adt.date_received,
              adt.date_online_exam,
              adt.date_endorsement,
              adt.date_assessment,
              adt.date_blacklist 
            FROM
              users u
              INNER JOIN applicant_date_trackings adt ON u.ID = adt.users_id
              INNER JOIN positions P ON P.ID = u.position_id
              INNER JOIN teams t on t.id = p.teams_id
              INNER JOIN departments d on d.id = t.department_id
            WHERE
              u.status = 'blacklist'
              AND t.id = ${teamID}
              ${dateFilter}
            ORDER BY
              u.id desc
            LIMIT ${limit} OFFSET ${offset}`

      return db.query(sql)
    } catch (error) {
      console.log(error)
    }
  }


  async function findAllBlacklistedDH({deptID, dateFrom, dateTo, offset=0, limit=null}) {
    try {
      const db = await dbs(),
            dateFilter = (dateFrom && dateTo) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : '',
            sql =
                `SELECT
                u.ID,
                u.email,
                u.firstname,
                u.lastname,
                u.middlename,
                u.mobile,
                u.livestock,
                u.willing_to_travel,
                P.NAME,
                u.resume_upload_date,
                u.resume_upload_url,
                adt.remarks_blacklist,
                u.date_application,
                adt.date_received,
                adt.date_online_exam,
                adt.date_endorsement,
                adt.date_assessment,
                adt.date_blacklist 
              FROM
                users u
                INNER JOIN applicant_date_trackings adt ON u.ID = adt.users_id
                INNER JOIN positions P ON P.ID = u.position_id
                INNER JOIN teams t on t.id = p.teams_id
                INNER JOIN departments d on d.id = t.department_id
              WHERE
                u.status = 'blacklist'
                AND d.id = ${deptID}
                ${dateFilter}
              ORDER BY
                u.id desc
              LIMIT ${limit} OFFSET ${offset}`
      return db.query(sql)
    } catch (error) {
      console.log(error)
    }
    
  }


  async function changeStatusAssessment(id) {
    const db = await dbs();
    //
    const sql = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_assessment = NOW() " +
        "WHERE id = $1;";
      const params = [appId];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,date_assessment) " +
        "VALUES ($1,NOW())";
      const params = [id];
      await db.query(sql, params);
    }
    //
    const sql2 = "UPDATE users SET status = 'assessment' WHERE id = $1;";
    //"UPDATE users SET status = 'assessment',is_viewed = 'f', is_viewed_th = 'f', is_viewed_oh = 'f' WHERE id = $1;";
    const params2 = [id];
    return db.query(sql2, params2);
  }
  async function checkStatusOfApplicant(id) {
    const db = await dbs();
    const sql = "SELECT status FROM users WHERE id=$1;";
    const params = [id];
    return db.query(sql, params);
  }


  async function getAllAssessment({deptid, posid, offset = 0, limit = null}) {
    
    const db = await dbs(),
      filter = (deptid && posid) ? ` AND d.id = ${deptid} AND p.id = ${posid}` : `AND d.id = ${deptid}`,
      sql = `
                SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,u.is_done_iqbe_exam,
                willing_to_travel,u.position_id,p.name,resume_upload_date,resume_upload_url,schedule_for_assessment, adt.date_endorsement,
                adt.online_exam_start,adt.online_exam_end, (now() - adt.date_assessment) as running_time, d.id as deptid, u.filled_up_form
                FROM users u 
                JOIN positions p 
                  ON p.id = u.position_id
                JOIN teams t 
                  ON t.id = p.teams_id 
                JOIN departments d 
                  ON d.id = t.department_id
                LEFT JOIN applicant_date_trackings adt 
                  ON adt.users_id = u.id
                WHERE u.status = 'assessment' 
                  ${filter}
                ORDER BY u.id DESC
                LIMIT ${limit} OFFSET ${offset}`
    return db.query(sql)
  }


  async function findAllAssessment({dateFrom, dateTo, offset = 0, limit = null}) {
    const db = await dbs(),
      dateFilter = (dateFrom && dateTo) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : '',
      sql = `
                SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,u.is_done_iqbe_exam,
                willing_to_travel,u.position_id,p.name,resume_upload_date,resume_upload_url,schedule_for_assessment, adt.date_endorsement,
                adt.online_exam_start,adt.online_exam_end, (now() - adt.date_assessment) as running_time, d.id as deptid, u.filled_up_form
                FROM users u 
                JOIN positions p 
                  ON p.id = u.position_id
                JOIN teams t 
                  ON t.id = p.teams_id 
                JOIN departments d 
                  ON d.id = t.department_id
                LEFT JOIN applicant_date_trackings adt 
                  ON adt.users_id = u.id
                WHERE u.status = 'assessment' 
                  ${dateFilter}
                ORDER BY u.id DESC
                LIMIT ${limit} OFFSET ${offset}`
    return db.query(sql)
  }


  async function findAllAssessmentDH({deptID, dateFrom, dateTo, offset = 0, limit = null}) {

    const db = await dbs(),
      dateFilter = (dateFrom && dateTo) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : '',
      sql = `
                SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,u.is_done_iqbe_exam,
                willing_to_travel,u.position_id,p.name,resume_upload_date,resume_upload_url,schedule_for_assessment, adt.date_endorsement,
                adt.online_exam_start,adt.online_exam_end, (now() - adt.date_assessment) as running_time, d.id as deptid, u.filled_up_form
                FROM users u 
                JOIN positions p 
                  ON p.id = u.position_id
                JOIN teams t 
                  ON t.id = p.teams_id 
                JOIN departments d 
                  ON d.id = t.department_id
                LEFT JOIN applicant_date_trackings adt 
                  ON adt.users_id = u.id
                WHERE u.status = 'assessment'
                  AND d.id = ${deptID} 
                  ${dateFilter}
                ORDER BY u.id DESC
                LIMIT ${limit} OFFSET ${offset}`
    return db.query(sql)
  }


  async function findAllAssessmentTH({teamID, dateFrom, dateTo, offset = 0, limit = null}) {

    const db = await dbs(),
      dateFilter = (dateFrom && dateTo) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : '',
      sql = `
                SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,u.is_done_iqbe_exam,
                willing_to_travel,u.position_id,p.name,resume_upload_date,resume_upload_url,schedule_for_assessment, adt.date_endorsement,
                adt.online_exam_start,adt.online_exam_end, (now() - adt.date_assessment) as running_time, d.id as deptid, u.filled_up_form
                FROM users u 
                JOIN positions p 
                  ON p.id = u.position_id
                JOIN teams t 
                  ON t.id = p.teams_id 
                JOIN departments d 
                  ON d.id = t.department_id
                LEFT JOIN applicant_date_trackings adt 
                  ON adt.users_id = u.id
                WHERE u.status = 'assessment'
                  AND t.id = ${teamID} 
                  ${dateFilter}
                ORDER BY u.id DESC
                OFFSET ${offset} LIMIT ${limit}`
    return db.query(sql)
  }


  async function changeStatusPending(id) {
    const db = await dbs();
    //
    const sql = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_pending = NOW() " +
        "WHERE id = $1;";
      const params = [appId];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,date_pending) " +
        "VALUES ($1,NOW())";
      const params = [id];
      await db.query(sql, params);
    }
    //
    const sql2 =
      "UPDATE users SET password = null, status = 'pending for review', is_viewed = 'f' WHERE id = $1;";
    const params2 = [id];
    return db.query(sql2, params2);
  }
  async function findAllPending(id, date_from, date_to) {
    let extQuery = ``;
    if (date_from && date_to) { extQuery = `AND u.resume_upload_date BETWEEN '${date_from}' AND '${date_to}' ` }
    const db = await dbs();
    const sql =
      "SELECT u.id,email,firstname,lastname,middlename,mobile,livestock, " +
      "willing_to_travel,p.name,resume_upload_date,resume_upload_url,schedule_for_assessment, (NOW() - adt.date_pending) as running_time, " +
      "online_exam_start,online_exam_end, d.id as deptid, adt.date_assessment as date_interview FROM users u LEFT JOIN positions p ON p.id = u.position_id " +
      "LEFT JOIN teams t ON t.id = p.teams_id LEFT JOIN departments d ON d.id = t.department_id " +
      "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
      //  " JOIN interview_assessments ia ON ia.user_id_applicant = u.id " +
      " WHERE u.status = 'pending for review' " +
      `AND d.id = $1 ${extQuery} ORDER BY u.id DESC`; //adt.date_pending ASC, u.id ASC;";
    const params = [id];
    return db.query(sql, params);
  }
  async function changeStatusKeptForReference(id, remarks) {
    const db = await dbs();
    //
    const sql = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_kept_reference = NOW(), remarks_kept_reference = $2 " +
        "WHERE id = $1;";
      const params = [appId, remarks];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id, date_kept_reference, remarks_kept_reference) " +
        "VALUES ($1, NOW(), $2)";
      const params = [id];
      await db.query(sql, params);
    }
    //
    const sql2 =
      "UPDATE users SET status = 'kept for reference',is_viewed = 'f' WHERE id = $1;";
    const params2 = [id];
    return db.query(sql2, params2);
  }
  async function getAllkeepforReferences({dateTo, dateFrom, offset = 0, limit = null}) {
    try {
      const db = await dbs();
      const dateFilter = (dateTo && dateFrom) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : ``;
      const sql = `SELECT u.id,u.email,u.firstname,u.lastname,u.mobile,u.livestock,
                    u.position_id,u.filled_up_form,u.is_done_iqbe_exam,
                    u.willing_to_travel,p.name,u.resume_upload_date,u.resume_upload_url, u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, adt.date_shortlist, adt.date_kept_reference, adt.remarks_kept_reference,
                    (NOW() - adt.date_kept_reference) as running_time, d.id as deptid, cat_count.category_count
                    FROM users u JOIN positions p ON p.id = u.position_id
                    JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id
                      LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
                      LEFT JOIN (
                    SELECT teams_id, COUNT(teams_id) as category_count
                    FROM categories
                    GROUP BY teams_id
                        ) as cat_count ON t.id = cat_count.teams_id
                    WHERE u.status = 'kept for reference' ${dateFilter}
                    ORDER BY u.resume_upload_date DESC
                    LIMIT ${limit}
                    OFFSET ${offset}`; //adt.date_shortlist ASC, u.id ASC;";
      return db.query(sql);
    } catch (error) {
      console.log(error)
    }
  }


  async function findAllKeptForReference({deptid, posid, from, to}) {
    const db = await dbs()
    let dateFilter = (from && to) ? ` AND u.resume_upload_date BETWEEN '${from}' AND '${to}'`: '',
        deptFilter = (deptid) ? ` AND d.id = ${deptid}`: '',
        posFilter = (posid) ? ` AND p.id = ${posid}`: '',
        sql =
            `SELECT u.id, u.email, u.firstname, u.lastname, u.middlename, u.mobile, u.livestock, u.position_id, u.filled_up_form, u.is_done_iqbe_exam,
            u.willing_to_travel, p.name, u.resume_upload_date, u.resume_upload_url,
            u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, adt.date_shortlist, adt.date_kept_reference, adt.remarks_kept_reference,
            (NOW() - adt.date_kept_reference) as running_time, d.id as deptid
            FROM users u JOIN positions p ON p.id = u.position_id 
            JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id 
            LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
            WHERE u.status = 'kept for reference'${deptFilter} ${posFilter} ${dateFilter}
          ORDER BY u.id DESC`;
    return db.query(sql);
  }


  async function changeStatusShortList(id) {
    const db = await dbs();
    //
    const sql = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_shortlist = NOW() " +
        "WHERE id = $1;";
      const params = [appId];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,date_shortlist) " +
        "VALUES ($1,NOW())";
      const params = [id];
      await db.query(sql, params);
    }
    //
    const sql2 =
      "UPDATE users SET status = 'shortlisted',is_viewed = 'f', is_viewed_oh = 'f' WHERE id = $1;";
    const params2 = [id];
    return db.query(sql2, params2);
  }
  async function findAllShortList({deptid, posid, dateTo, dateFrom}) {
    const db = await dbs(),
      deptFilter = (deptid) ? ` AND d.id = ${deptid}` : '',
      deptAndPosFilter = (deptid && posid) ? ` AND d.id = ${deptid} AND p.id = ${posid}` : ''
    const Sorting = (dateTo && dateFrom) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : ``;
    const sql = `SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,u.is_done_iqbe_exam,
    willing_to_travel,u.position_id,p.name,resume_upload_date,resume_upload_url,
    u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment,
     adt.date_shortlist, (NOW() - adt.date_shortlist) as running_time, d.id as deptid ,u.status as UserStatus
     FROM users u JOIN positions p ON p.id = u.position_id
  LEFT JOIN teams t ON t.id = p.teams_id LEFT JOIN departments d ON d.id = t.department_id
  LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id 
  WHERE u.status = 'shortlisted'
  ${Sorting}
  ${deptFilter}
  ${deptAndPosFilter}
  ORDER BY u.resume_upload_date DESC
 `;
    return db.query(sql);
  }
  async function changeStatusDeployed(id, date) {
    const db = await dbs();
    //
    const sql = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_deployed = $2 " +
        "WHERE id = $1;";
      const params = [appId, date];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,date_deployed) " +
        "VALUES ($1,$2)";
      const params = [id, date];
      await db.query(sql, params);
    }
    //
    const sql2 =
      "UPDATE users SET status = 'deployed',is_viewed = 'f', is_viewed_th = 'f', is_viewed_oh = 'f' WHERE id = $1;";
    const params2 = [id];
    return db.query(sql2, params2);
  }


  async function findAllDeployed({dateFrom, dateTo, limit = null, offset = 0}) {
    try {
      const db = await dbs(),
            dateFilter = (dateFrom && dateTo) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}' ` : '',
            sql =
              `SELECT
              u.ID,
              u.email,
              u.firstname,
              u.lastname,
              u.middlename,
              u.mobile,
              u.livestock,
              u.is_done_iqbe_exam,
              willing_to_travel,
              P.NAME,
              d.NAME AS deptname,
              u.resume_upload_date,
              u.resume_upload_url,
              u.date_application,
              adt.date_received,
              adt.date_online_exam,
              adt.date_endorsement,
              adt.date_assessment,
              adt.date_shortlist,
              adt.date_deployed,
              ( adt.date_deployed :: TIMESTAMP - u.resume_upload_date :: TIMESTAMP ) AS no_of_days_deployed,
              d.ID AS deptid 
            FROM
              users u
              JOIN positions P ON P.ID = u.position_id
              JOIN teams T ON T.ID = P.teams_id
              JOIN departments d ON d.ID = T.department_id
              LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.ID 
            WHERE
              u.status = 'deployed' 
            ${dateFilter}
            ORDER BY u.id DESC
            LIMIT ${limit} OFFSET ${offset}`;
      return db.query(sql);
    } catch (error) {
      console.log(error)
    }
  }


  async function getAllDeployed(params) {
    const { deptid, posid, from, to, offset=0, limit=null } = params,
      db = await dbs(),
      deptFilter = (deptid) ? ` AND d.id = ${deptid}` : '',
      posFilter = (posid) ? ` AND p.id = ${posid}`: '',
      dateFilter = (from && to) ? ` AND u.resume_upload_date BETWEEN '${from}' AND '${to}'`: '',
      sql =
        `SELECT
        u.ID,
        u.email,
        u.firstname,
        u.lastname,
        u.middlename,
        u.mobile,
        u.livestock,
        u.is_done_iqbe_exam,
        willing_to_travel,
        P.NAME,
        d.NAME AS deptname,
        u.resume_upload_date,
        u.resume_upload_url,
        u.date_application,
        adt.date_received,
        adt.date_online_exam,
        adt.date_endorsement,
        adt.date_assessment,
        adt.date_shortlist,
        adt.date_deployed,
        ( adt.date_deployed :: TIMESTAMP - u.resume_upload_date :: TIMESTAMP ) AS no_of_days_deployed,
        d.ID AS deptid 
      FROM
        users u
        JOIN positions P ON P.ID = u.position_id
        JOIN teams T ON T.ID = P.teams_id
        JOIN departments d ON d.ID = T.department_id
        LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.ID 
      WHERE
        u.status = 'deployed' 
      ${deptFilter}
      ${posFilter}
      ${dateFilter}
      ORDER BY u.id DESC
      LIMIT ${limit} OFFSET ${offset}`;
    return db.query(sql);
  }


  async function findAllDeployedDH({deptID, dateFrom, dateTo, limit = null, offset = 0}) {
    const db = await dbs(),
      dateFilter = (dateFrom && dateTo) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}' ` : '',
      sql =
        `SELECT
        u.ID,
        u.email,
        u.firstname,
        u.lastname,
        u.middlename,
        u.mobile,
        u.livestock,
        u.is_done_iqbe_exam,
        willing_to_travel,
        P.NAME,
        d.NAME AS deptname,
        u.resume_upload_date,
        u.resume_upload_url,
        u.date_application,
        adt.date_received,
        adt.date_online_exam,
        adt.date_endorsement,
        adt.date_assessment,
        adt.date_shortlist,
        adt.date_deployed,
        ( adt.date_deployed :: TIMESTAMP - u.resume_upload_date :: TIMESTAMP ) AS no_of_days_deployed,
        d.ID AS deptid 
      FROM
        users u
        JOIN positions P ON P.ID = u.position_id
        JOIN teams T ON T.ID = P.teams_id
        JOIN departments d ON d.ID = T.department_id
        LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.ID 
      WHERE
        u.status = 'deployed'
        AND d.id = ${deptID}
        ${dateFilter}
      ORDER BY u.id DESC
      LIMIT ${limit} OFFSET ${offset}`;
    return db.query(sql);
  }

  async function findAllDeployedTH({teamID, dateFrom, dateTo, offset = 0, limit = null}) {
    try {
      const db = await dbs(),
            dateFilter = (dateFrom && dateTo) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}' ` : '',
            sql =
              `SELECT
              u.ID,
              u.email,
              u.firstname,
              u.lastname,
              u.middlename,
              u.mobile,
              u.livestock,
              u.is_done_iqbe_exam,
              willing_to_travel,
              P.NAME,
              d.NAME AS deptname,
              u.resume_upload_date,
              u.resume_upload_url,
              u.date_application,
              adt.date_received,
              adt.date_online_exam,
              adt.date_endorsement,
              adt.date_assessment,
              adt.date_shortlist,
              adt.date_deployed,
              ( adt.date_deployed :: TIMESTAMP - u.resume_upload_date :: TIMESTAMP ) AS no_of_days_deployed,
              d.ID AS deptid 
            FROM
              users u
              JOIN positions P ON P.ID = u.position_id
              JOIN teams T ON T.ID = P.teams_id
              JOIN departments d ON d.ID = T.department_id
              LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.ID 
            WHERE
              u.status = 'deployed'
              AND t.id = ${teamID}
              ${dateFilter}
            ORDER BY u.id DESC
            LIMIT ${limit} OFFSET ${offset}`;
      return db.query(sql);
    } catch (error) {
      console.log(error)
    }
  }


  async function getRemainingOnlineExamTime({ ...userInfo }) {
    const db = await dbs();
    const sql =
      `SELECT u.id, u.email, u.password, tm.remaining_hours, tm.remaining_minutes, u.total_time_exam_seconds, u.total_time_elapsed_seconds, tm.exam_start, tm.exam_end
      FROM users u
      LEFT JOIN ( 
      SELECT adt.users_id, 
      EXTRACT(HOUR FROM (adt.online_exam_end - NOW())) as remaining_hours,
      EXTRACT(MINUTE FROM (adt.online_exam_end - NOW())) as remaining_minutes,
      adt.online_exam_start as exam_start,
      adt.online_exam_end as exam_end
      FROM applicant_date_trackings adt
      ) AS tm ON u.id = tm.users_id
      WHERE u.email= $1 AND u.password= $2`;
    const params = [userInfo.email, userInfo.password];
    return db.query(sql, params);
  }
  // login for applicant online exam
  async function loginsApplicant({ ...userInfo }) {
    const db = await dbs();
    const sql =
      "UPDATE users SET sign_in_count = sign_in_count + 1, login_time = NOW(), current_ip = $1, " +
      "token=$2 WHERE email=$3 AND password=$4 AND is_done_online_exam='f';";
    const params = [
      userInfo.ip,
      userInfo.token,
      userInfo.email,
      userInfo.password,
    ];
    return db.query(sql, params);
  }
  // login for applicant IQBE Exam
  async function loginsApplicants({ ...userInfo }) {
    const db = await dbs();
    const sql =
      "UPDATE users SET sign_in_count = sign_in_count + 1, login_time = NOW(), current_ip = $1, " +
      "token=$2 WHERE email=$3 AND password=$4 AND is_done_iqbe_exam='f';";
    const params = [
      userInfo.ip,
      userInfo.token,
      userInfo.email,
      userInfo.password,
    ];
    return db.query(sql, params);
  }
  // login for applicantion form
  async function loginsApplicantApplication({ ...userInfo }) {
    const db = await dbs();
    const sql =
      "UPDATE users SET sign_in_count = sign_in_count + 1, login_time = NOW(), current_ip = $1, " +
      "token=$2 WHERE email=$3 AND password=$4 RETURNING id;";
    const params = [
      userInfo.ip,
      userInfo.token,
      userInfo.email,
      userInfo.password,
    ];
    return db.query(sql, params);
  }
  // change status of user to done online exam
  async function doneOnlineExam(id) {
    const db = await dbs();
    const sql = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET online_exam_end = NOW() " +
        "WHERE id = $1;";
      const params = [appId];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,online_exam_end) " +
        "VALUES ($1,NOW())";
      const params = [id];
      await db.query(sql, params);
    }
    //
    const sql2 = "UPDATE users SET is_done_online_exam = 't' WHERE id = $1;";
    const params2 = [id];
    return db.query(sql2, params2);
  }
  // change status of user to done online exam
  async function doneIQBE(id) {
    const db = await dbs();
    const sql = "UPDATE users SET is_done_iqbe_exam = 't' WHERE id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // get applicant id when login
  async function getApplicantID(email) {
    const db = await dbs();
    const sql = "SELECT id FROM users WHERE email = $1;";
    const params = [email];
    return db.query(sql, params);
  }
  // get random categories base on applicants position applied
  // Change LIMIT base on how many categories to insert
  async function selectRandomCategories1st(userId) {
    const db = await dbs();
    const sql = `SELECT * FROM (
        SELECT DISTINCT c.id,c.name,c.time_limit_on_seconds, 
        (
          SELECT COUNT(*) FROM questions WHERE categories_id = c.id
        ) AS numitems
        FROM users u 
        LEFT JOIN positions p ON p.id = u.position_id 
        LEFT JOIN teams t ON t.id = p.teams_id 
        LEFT JOIN categories c ON c.teams_id = t.id 
        LEFT JOIN questions q ON q.categories_id=c.id
        WHERE u.id = $1 AND c.status='active' and c.name NOT LIKE '%IQ Test%' 
        ) a
        ORDER BY random();`;
    const params = [userId];
    return db.query(sql, params);
  }

  async function selectRandomCategories(userId) {
    const db = await dbs();
    const sql = `SELECT cat.id, cat.name, cat.time_limit_on_seconds, cat2.categories_count as numitems
	        FROM categories cat
		      JOIN (SELECT c.id, COUNT(q.categories_id) categories_count, c."name"
                FROM users_scores sc
                LEFT JOIN questions q ON sc.question_id = q.id
                LEFT JOIN categories c ON q.categories_id = c.id
                WHERE sc.user_id = $1 and c.name NOT LIKE '%IQ Test%' 
                GROUP BY c."name", c.id) 
          cat2 ON cat.id = cat2.id`;
    const params = [userId];
    return db.query(sql, params);
  }
  // insert random categories;
  async function insertRandomCategories(userId, categoryId) {
    const db = await dbs();
    const sql =
      "INSERT INTO categories_random (id, category_id, user_id, is_done, time_created, time_updated) VALUES (DEFAULT,$2,$1,'no',NOW(),NOW());";
    const params = [userId, categoryId];
    return db.query(sql, params);
  }
  // get random questions base on applicants position applied
  // Change LIMIT base on how many questions to insert
  async function selectRandomQuestions(categoryId, limit) {
    const db = await dbs();
    const sql =
      "SELECT id FROM questions WHERE categories_id = $1 ORDER BY random() LIMIT $2;";
    const params = [categoryId, limit];
    return db.query(sql, params);
  }
  // get team and no of items per team for online exam
  async function getTeamNumberOfItems(userId) {
    const db = await dbs();
    const sql =
      "SELECT t.id,t.no_of_items FROM users u JOIN positions p ON p.id = u.position_id JOIN teams t ON " +
      "t.id = p.teams_id WHERE u.id = $1;";
    const params = [userId];
    return db.query(sql, params);
  }
  // update end date and start and time of exam
  async function examEndDate(startTime, endTime, totalTimeInSeconds, userId) {
    const db = await dbs();
    const sql =
      "UPDATE users SET exam_start = $3,exam_end = $2,total_time_exam_seconds=$4 WHERE id = $1;";
    const params = [userId, endTime, startTime, totalTimeInSeconds];
    return db.query(sql, params);
  }
  // get start and end date from db to be used in timer for the online-exam
  async function returnStartEndDate(token) {
    const db = await dbs();
    const sql =
      "SELECT id,total_time_exam_seconds,total_time_elapsed_seconds FROM users WHERE token = $1;";
    const params = [token];
    return db.query(sql, params);
  }
  // get time for IQBE exam
  async function IQBEGetTime(token) {
    const db = await dbs();
    const sql = "SELECT id,iqbe_time FROM users WHERE token = $1;";
    const params = [token];
    return db.query(sql, params);
  }
  // update elapsed time in db
  async function elapsedTimes(times, token) {
    const db = await dbs();
    const sql =
      "UPDATE users SET total_time_elapsed_seconds = $2 WHERE token =$1 returning id, (total_time_exam_seconds - total_time_elapsed_seconds) as remaining_time;";
    const params = [token, times];
    return db.query(sql, params);
  }
  // subtract IQBE remaining time
  async function minusTimeForIQBE(times, token) {
    const db = await dbs();
    const sql = "UPDATE users SET iqbe_time = $2 WHERE token =$1 RETURNING id, iqbe_time;";
    const params = [token, times];
    return db.query(sql, params);
  }
  // insert random questions;
  async function insertRandomQuestions(userId, questionId) {
    const db = await dbs();
    const sql =
      "INSERT INTO users_scores(id, user_id, user_answer, is_correct, question_id, time_created) VALUES (DEFAULT,$1,NULL,NULL,$2,NOW());";
    const params = [userId, questionId];
    return db.query(sql, params);
  }
  async function scheduleForAssessment(id, date, password) {
    const db = await dbs();
    const sql =
      "UPDATE users SET schedule_for_assessment = $2, password=$3, status = 'assessment', is_viewed = 'f', is_viewed_th = 'f', is_viewed_oh = 'f', filled_up_form = false WHERE id = $1;";
    const params = [id, date, password];

    const sql2 =
      "UPDATE applicant_date_trackings SET date_assessment = $1 WHERE users_id = $2";
    const params2 = [date, id];
    await db.query(sql2, params2);

    return db.query(sql, params);
  }
  async function changeStatusJobOrder(id) {
    const db = await dbs();
    //
    const sql = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_joborder = NOW() " +
        "WHERE id = $1;";
      const params = [appId];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,date_joborder) " +
        "VALUES ($1,NOW())";
      const params = [id];
      await db.query(sql, params);
    }
    //
    const sql2 =
      "UPDATE users SET status = 'job offer',is_viewed = 'f' WHERE id = $1;";
    const params2 = [id];
    return db.query(sql2, params2);
  }

  async function changeJobOrder(id, remarks) {
    const db = await dbs();
    //
    const sql = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);
    if (data.rowCount > 0) {
      // update
      const appId = data.rows[0].id;
      const sql =
        "UPDATE applicant_date_trackings SET date_joborder = NOW(), remarks_deploy=$2" +
        "WHERE id = $1;";
      const params = [appId, remarks];
      await db.query(sql, params);
    } else {
      // insert
      const sql =
        "INSERT INTO applicant_date_trackings (users_id,date_joborder,remarks_deploy) " +
        "VALUES ($1,NOW(),$2)";
      const params = [id, remarks];
      await db.query(sql, params);
    }
    //
    const sql2 =
      "UPDATE users SET status = 'job offer',is_viewed = 'f' WHERE id = $1;";
    const params2 = [id];
    return db.query(sql2, params2);
  }
  async function getAllJobOffer({dateTo, dateFrom, offset = 0, limit = null}) {
    try {
      const db = await dbs();
      let dateFilter = (dateTo && dateFrom) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : ``,
          sql = `SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,
            willing_to_travel,p.name,resume_upload_date,resume_upload_url,
            u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment,
            adt.date_shortlist, adt.date_joborder, (NOW() - adt.date_joborder) as running_time, d.id as deptid
            FROM users u JOIN positions p ON p.id = u.position_id
            JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id
            LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
            WHERE u.status = 'job offer'  
            ${dateFilter}
            ORDER BY u.resume_upload_date DESC
            LIMIT ${limit}
            OFFSET ${offset}`;
      return db.query(sql);
    } catch (error) {
      console.log(error)
    }
  }
  async function getJobOfferDh({deptID, dateTo, dateFrom, offset = 0, limit = null}) {
    try {
      const db = await dbs();
      const dateFilter = (dateTo && dateFrom) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}' ` : ``;
      const sql = `		SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,
        willing_to_travel,p.name,resume_upload_date,resume_upload_url,
        u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment,
        adt.date_shortlist, adt.date_joborder, (NOW() - adt.date_joborder) as running_time, d.id as deptid
        FROM users u JOIN positions p ON p.id = u.position_id
        JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id
        LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
        WHERE u.status = 'job offer' AND d.id = $1 
        ${dateFilter}
        ORDER BY u.resume_upload_date DESC
        LIMIT ${limit}
        OFFSET ${offset}`; //adt.date_shortlist ASC, u.id ASC;";
      const params = [deptID];
      return db.query(sql, params);
    } catch (error) {
      console.log(error)
    }  
  }


  // fill up application form
  async function fillUpApplication({ ...userInfo }) {
    try {
      
    const db = await dbs();
    const userId = userInfo.id; // user's id
    // clear data first
    const sql5 = "DELETE FROM users_work_experiences WHERE users_id = $1;";
    const params5 = [userId];
    await db.query(sql5, params5);
    const sql6 = "DELETE FROM users_willingness_to_work WHERE users_id = $1;";
    const params6 = [userId];
    await db.query(sql6, params6);
    const sql7 = "DELETE FROM users_seminar_attends WHERE users_id = $1;";
    const params7 = [userId];
    await db.query(sql7, params7);
    const sql8 = "DELETE FROM users_relative_in_bfi WHERE user_id = $1;";
    const params8 = [userId];
    await db.query(sql8, params8);
    const sql9 = "DELETE FROM users_references WHERE users_id = $1;";
    const params9 = [userId];
    await db.query(sql9, params9);
    const sql10 =
      "DELETE FROM users_professional_licenses WHERE users_id = $1;";
    const params10 = [userId];
    await db.query(sql10, params10);
    const sql11 = "DELETE FROM users_other_info WHERE users_id = $1;";
    const params11 = [userId];
    await db.query(sql11, params11);
    const sql12 = "DELETE FROM users_org_memberships WHERE users_id = $1;";
    const params12 = [userId];
    await db.query(sql12, params12);
    const sql13 = "DELETE FROM users_is_married WHERE users_id = $1;";
    const params13 = [userId];
    await db.query(sql13, params13);
    const sql14 = "DELETE FROM users_have_work_with_bfi WHERE user_id = $1;";
    const params14 = [userId];
    await db.query(sql14, params14);
    const sql15 = "DELETE FROM users_family_bg WHERE users_id = $1;";
    const params15 = [userId];
    await db.query(sql15, params15);
    const sql16 = "DELETE FROM users_educational_bg WHERE users_id = $1;";
    const params16 = [userId];
    await db.query(sql16, params16);
    // end
    const sql =
      "UPDATE users SET salary_desired=$1, place_birth=$12, " +
      "bday=$13, age=$14, height=$15, weight=$16, gender=$17, is_single_parent=$18, civil_status=$19, " +
      "citizenship=$20, religion=$21, who_referred=$22, when_to_start=$23, person_to_notify=$24, " +
      "person_relationship=$25, person_address=$26, person_telephone=$27, " +
      "position_id_second_choice=$28, cs_eligible=$29, cs_date_taken=$30, cs_certificate_image_path=$31, " +
      "date_application=$32, user_image_path=$33, unitroomnumberfloor=$2, buildingname=$3, " +
      "lotblockphasehousenumber=$4, streetname=$5, villagesubdivision=$6, barangay=$7, " +
      "towndistrict=$8, municipality=$9, cityprovince=$10, addresstype=$11, middlename = $35 WHERE id=$34;";
    const params = [
      parseFloat(userInfo.salary_desired), // 1
      // new fields
      userInfo.UnitRoomNumberFloor,
      userInfo.BuildingName,
      userInfo.LotBlockPhaseHouseNumber,
      userInfo.StreetName,
      userInfo.VillageSubdivision,
      userInfo.Barangay,
      userInfo.TownDistrict,
      userInfo.Municipality,
      userInfo.CityProvince, // 10
      userInfo.AddressType,
      //
      userInfo.place_birth,
      userInfo.bday,
      userInfo.age,
      userInfo.height,
      userInfo.weight,
      userInfo.gender,
      userInfo.is_single_parent,
      userInfo.civil_status,
      userInfo.citizenship, // 20
      userInfo.religion,
      userInfo.who_referred,
      userInfo.when_to_start,
      userInfo.person_to_notify,
      userInfo.person_relationship,
      userInfo.person_address,
      userInfo.person_telephone,
      userInfo.position_id_second_choice,
      userInfo.CS_eligible,
      userInfo.CS_date_taken, // 30
      userInfo.CS_certificate_image_path,
      userInfo.date_application,
      userInfo.user_image_path,
      userId,
      userInfo.middlename,
    ];
    await db.query(sql, params);

    // run only if there is data (this is optional)
    if (userInfo.relativeArray) {
      if (userInfo.relativeArray.length > 0) {
        // users relative
        const relativeArray = userInfo.relativeArray;
        // all other array has the same length
        const fullNameArray = relativeArray[0].full_name;
        const positionArray = relativeArray[0].position;
        const branchArray = relativeArray[0].branch;
        const relationshipArray = relativeArray[0].relationship;

        for (let i = 0; i < fullNameArray.length; i++) {
          const f = fullNameArray[i];
          const p = positionArray[i];
          const b = branchArray[i];
          const r = relationshipArray[i];

          // insert into users_relative_in_bfi db
          const sql2 =
            "INSERT INTO users_relative_in_bfi(id, full_name, position, branch, user_id, relationship) VALUES (DEFAULT,$1,$2,$3,$4,$5)";
          const params2 = [f, p, b, userId, r];
          await db.query(sql2, params2);
        }
      }
    }

    // this is required
    // user's educational bg
    const educationalBgArray = userInfo.educationalBgArray;
    // all other array has the same length
    const educational_attainment = educationalBgArray[0].educational_attainment;
    const institution_name = educationalBgArray[0].institution_name;
    const institution_address = educationalBgArray[0].institution_address;
    const course_degree = educationalBgArray[0].course_degree;
    const years_attended = educationalBgArray[0].years_attended;
    const honors_awards = educationalBgArray[0].honors_awards;

    for (let i = 0; i < educational_attainment.length; i++) {
      const e = educational_attainment[i];
      const ins = institution_name[i];
      const ia = institution_address[i];
      const c = course_degree[i];
      const y = years_attended[i];
      const h = honors_awards[i];

      // insert into users_educational_bg db
      const sql2 =
        "INSERT INTO users_educational_bg(id,users_id, educational_attainment, institution_name, institution_address, course_degree, years_attended, honors_awards) VALUES (DEFAULT,$1,$2,$3,$4,$5,$6,$7)";
      const params2 = [userId, e, ins, ia, c, y, h];
      await db.query(sql2, params2);
    }

    // run only if there is data (this is optional)
    if (userInfo.professionalLicensesArray) {
      if (userInfo.professionalLicensesArray.length > 0) {
        // user's professional licenses held
        const professionalLicensesArray = userInfo.professionalLicensesArray;

        // all other array has the same length
        const professional_licenses_held =
          professionalLicensesArray[0].professional_licenses_held;
        const certificate_image_path =
          professionalLicensesArray[0].certificate_image_path;
        const certification_number =
          professionalLicensesArray[0].certification_number;

        for (let i = 0; i < professional_licenses_held.length; i++) {
          const p = professional_licenses_held[i];
          const c = certificate_image_path[i];
          const n = certification_number[i];
          // insert into users_professional_licenses db
          const sql2 =
            "INSERT INTO users_professional_licenses(id, users_id, professional_licenses_held, certificate_image_path, certification_number) VALUES (DEFAULT,$1,$2,$3,$4)";
          const params2 = [userId, p, c, n];
          await db.query(sql2, params2);
        }
      }
    }

    // run only if there is data (this is optional)
    if (userInfo.orgMembershipArray) {
      if (userInfo.orgMembershipArray.length > 0) {
        // user's org memberships
        const orgMembershipArray = userInfo.orgMembershipArray;
        // all other array has the same length
        const association_name = orgMembershipArray[0].association_name;
        const position_title = orgMembershipArray[0].position_title;
        const year_participation = orgMembershipArray[0].year_participation;

        for (let i = 0; i < association_name.length; i++) {
          const a = association_name[i];
          const p = position_title[i];
          const y = year_participation[i];

          // insert into users_org_memberships db
          const sql2 =
            "INSERT INTO users_org_memberships(id, associaton_name, position_title, year_participation, users_id) VALUES (DEFAULT,$1,$2,$3,$4)";
          const params2 = [a, p, y, userId];
          await db.query(sql2, params2);
        }
      }
    }

    console.log(userInfo.seminarsAttendedArray.length)
    // run only if there is data (this is optional)
    if (userInfo.seminarsAttendedArray) {
      if (userInfo.seminarsAttendedArray.length > 0) {
        // user's seminars attended
        let seminarsAttendedArray = userInfo.seminarsAttendedArray;
        // all other array has the same length
        console.log(seminarsAttendedArray)
        const seminar_name = seminarsAttendedArray[0].seminar_name;
        const description = seminarsAttendedArray[0].description;
        const venue = seminarsAttendedArray[0].venue;
        const conducted_by = seminarsAttendedArray[0].conducted_by;
        const dates = seminarsAttendedArray[0].dates;
        const scertificate_image_path =
          seminarsAttendedArray[0].certificate_image_path;

        console.log("HEEHEH")
        for (let i = 0; i < seminar_name.length; i++) {
          try {
          console.log("SEMEN NAME: ", seminar_name)
          const s = seminar_name[i];
          const d = description[i];
          const v = venue[i];
          const c = conducted_by[i];
          const ds = dates[i];
          const sc = scertificate_image_path[i];

          // insert into users_seminar_attends db
          const sql2 =
            "INSERT INTO users_seminar_attends(id, seminar_name, description, venue, conducted_by, dates, users_id, certificate_image_path) VALUES (DEFAULT,$1,$2,$3,$4,$5,$6,$7)";
          const params2 = [s, d, v, c, ds, userId, sc];
          await db.query(sql2, params2).then(res => console.log(res));
          } catch (error) {
            console.log(error)
          }
          
        }
      }
    }

    // run only if there is data (this is optional)
    if (userInfo.workExperienceArray) {
      if (userInfo.workExperienceArray.length > 0) {
        // user's work experience
        const workExperienceArray = userInfo.workExperienceArray;
        // all other array has the same length
        const company_name = workExperienceArray[0].company_name;
        const address = workExperienceArray[0].address;
        const position = workExperienceArray[0].position;
        const start_date = workExperienceArray[0].start_date;
        const end_date = workExperienceArray[0].end_date;
        const last_salary = workExperienceArray[0].last_salary;
        const reason_leaving = workExperienceArray[0].reason_leaving;

        for (let i = 0; i < company_name.length; i++) {
          const cn = company_name[i];
          const a = address[i];
          const p = position[i];
          const sd = start_date[i];
          const e = end_date[i];
          const l = parseFloat(last_salary[i]);
          const r = reason_leaving[i];

          // insert into users_work_experiences db
          const sql2 =
            "INSERT INTO users_work_experiences(id, company_name, address, position, start_date, end_date, last_salary, reason_leaving, users_id) VALUES (DEFAULT,$1,$2,$3,$4,$5,$6,$7,$8)";
          const params2 = [cn, a, p, sd, e, l, r, userId];
          await db.query(sql2, params2);
        }
      }
    }

    // run only if there is data (this is optional)
    if (userInfo.workWithBFIArray) {
      if (userInfo.workWithBFIArray.length > 0) {
        // user's if have worked with BFI
        const workWithBFIArray = userInfo.workWithBFIArray;

        // t or f; t = true; f = false;
        const have_work_with_bfi = workWithBFIArray[0].have_work_with_bfi;
        // all other array has the same length
        const branch_company = workWithBFIArray[0].branch_company;
        const bdates = workWithBFIArray[0].dates;
        const bposition = workWithBFIArray[0].position;
        const department = workWithBFIArray[0].department;
        // if true else false
        if (have_work_with_bfi === "t") {
          for (let i = 0; i < branch_company.length; i++) {
            const b = branch_company[i];
            const bd = bdates[i];
            const bp = bposition[i];
            const d = department[i];

            // insert into users_have_work_with_bfi db
            const sql2 =
              "INSERT INTO users_have_work_with_bfi(id, have_work_with_bfi, branch_company, dates, position, department, user_id) VALUES (DEFAULT,$1,$2,$3,$4,$5,$6)";
            const params2 = [have_work_with_bfi, b, bd, bp, d, userId];
            await db.query(sql2, params2);
          }
        } else {
          // insert into users_have_work_with_bfi db
          const sql2 =
            "INSERT INTO users_have_work_with_bfi(id, have_work_with_bfi, branch_company, dates, position, department, user_id) VALUES (DEFAULT,$1,NULL,NULL,NULL,NULL,$2)";
          const params2 = [have_work_with_bfi, userId];
          await db.query(sql2, params2);
        }
      }
    }

    // user's other info
    const otherInfoArray = userInfo.otherInfoArray;
    const typing_wpm = otherInfoArray[0].typing_wpm;
    const steno_wpm = otherInfoArray[0].steno_wpm;
    const hobbies = otherInfoArray[0].hobbies;
    const language_speak_write = otherInfoArray[0].language_speak_write;
    const machines_can_operate = otherInfoArray[0].machines_can_operate;
    const any_crime_committed = otherInfoArray[0].any_crime_committed;
    const been_hospitalized = otherInfoArray[0].been_hospitalized;
    const computer = otherInfoArray[0].computer;
    const art_work = otherInfoArray[0].art_work;
    const crime_details = otherInfoArray[0].crime_details;
    const hospitalized_details = otherInfoArray[0].hospitalized_details;

    // insert into users_other_info db
    const sql2 =
      "INSERT INTO users_other_info(id, typing_wpm, steno_wpm, hobbies, language_speak_write, machines_can_operate, any_crime_commited, been_hospitalized, users_id, computer, art_work, crime_details, hospitalized_details) VALUES (DEFAULT,$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)";
    const params2 = [
      typing_wpm,
      steno_wpm,
      hobbies,
      language_speak_write,
      machines_can_operate,
      any_crime_committed,
      been_hospitalized,
      userId,
      computer,
      art_work,
      crime_details,
      hospitalized_details,
    ];
    await db.query(sql2, params2);

    // run only if there is data (this is optional)
    if (userInfo.familyBgArray) {
      if (userInfo.familyBgArray.length > 0) {
        // user's family bg
        const familyBgArray = userInfo.familyBgArray;

        // all other array has the same length
        const name = familyBgArray[0].name;
        const relationship = familyBgArray[0].relationship;
        const age = familyBgArray[0].age;
        const occupation = familyBgArray[0].occupation;
        const company_address = familyBgArray[0].company_address;

        for (let i = 0; i < name.length; i++) {
          const n = name[i];
          const r = relationship[i];
          const a = age[i];
          const o = occupation[i];
          const c = company_address[i];

          // insert into users_family_bg db
          const sql2 =
            "INSERT INTO users_family_bg(id, name, relationship, age, occupation, company_address, users_id) VALUES (DEFAULT,$1,$2,$3,$4,$5,$6)";
          const params2 = [n, r, a, o, c, userId];
          await db.query(sql2, params2);
        }
      }
    }

    // run only if there is data (this is optional)
    if (userInfo.isMarriedArray) {
      if (userInfo.isMarriedArray.length > 0) {
        // user is married or not
        const isMarriedArray = userInfo.isMarriedArray;
        const is_married = isMarriedArray[0].is_married;
        const number_children = isMarriedArray[0].number_children;
        const range_age = isMarriedArray[0].range_age;
        const do_you = isMarriedArray[0].do_you;
        const with_who = isMarriedArray[0].with_who;

        let number_childrens = "";
        if (number_children === "") {
          number_childrens = 0;
        } else {
          number_childrens = number_children;
        }

        // insert into users_is_married db
        const sql3 =
          "INSERT INTO users_is_married (id, is_married, number_children, range_age, do_you, with_who, users_id) VALUES (DEFAULT,$1,$2,$3,$4,$5,$6)";
        const params3 = [
          is_married,
          number_childrens,
          range_age,
          do_you,
          with_who,
          userId,
        ];
        await db.query(sql3, params3);
      }
    }

    // run only if there is data (this is optional)
    if (userInfo.referencesArray) {
      if (userInfo.referencesArray.length > 0) {
        // user's references
        const referencesArray = userInfo.referencesArray;
        // all other array has the same length
        const names = referencesArray[0].name;
        const raddress = referencesArray[0].address;
        const telephone = referencesArray[0].telephone;
        const roccupation = referencesArray[0].occupation;
        const how_long_known = referencesArray[0].how_long_known;

        for (let i = 0; i < names.length; i++) {
          const n = names[i];
          const ra = raddress[i];
          const t = telephone[i];
          const ro = roccupation[i];
          const h = how_long_known[i];

          // insert into users_references db
          const sql2 =
            "INSERT INTO users_references(id, name, address, telephone, occupation, how_long_known, users_id) VALUES (DEFAULT,$1,$2,$3,$4,$5,$6)";
          const params2 = [n, ra, t, ro, h, userId];
          await db.query(sql2, params2);
        }
      }
    }

    // run only if there is data (this is optional)
    if (userInfo.willingnessArray) {
      if (userInfo.willingnessArray.length > 0) {
        // user's willingness to work in bfi
        const willingnessArray = userInfo.willingnessArray;
        const explain_why_hire = willingnessArray[0].explain_why_hire;
        const willing_train_apprentice =
          willingnessArray[0].willing_train_apprentice;
        const willing_to_BOND = willingnessArray[0].willing_to_BOND;
        const sss = willingnessArray[0].sss;
        const tin = willingnessArray[0].tin;
        const residence_certificate = willingnessArray[0].residence_certificate;
        const residence_certificate_date_issue =
          willingnessArray[0].residence_certificate_date_issue;
        const residence_certificate_place_issue =
          willingnessArray[0].residence_certificate_place_issue;

        let residence_certificate_date_issues = null;
        if (residence_certificate_date_issue === "") {
          // do nothing
        } else {
          residence_certificate_date_issues = residence_certificate_date_issue;
        }
        // insert into users_willingness_to_work db
        const sql4 =
          `INSERT INTO users_willingness_to_work(id, users_id, explain_why_hire, willing_train_apprentice, "willing_to_BOND", sss, tin, residence_certificate, residence_certificate_date_issue, residence_certificate_place_issue) VALUES (DEFAULT,$1,$2,$3,$4,$5,$6,$7,$8,$9)`;
        const params4 = [
          userId,
          explain_why_hire,
          willing_train_apprentice,
          willing_to_BOND,
          sss,
          tin,
          residence_certificate,
          residence_certificate_date_issues,
          residence_certificate_place_issue,
        ];
        await db.query(sql4, params4);
      }
    }

    const updateFilledUpSql =
      "UPDATE users SET filled_up_form = 't' WHERE id = $1";
    const updateFilledUpSqlParams = [userId];
    await db.query(updateFilledUpSql, updateFilledUpSqlParams);

    const msg = {
      msg: "Successfully filled up the application form. Thank you.",
    };
    return msg;
    } catch (error) {
      console.log(error)
    }
  }
  // notification for received applicants
  // for pg
  async function notificationReceived() {
    const db = await dbs();
    const sql =
      "SELECT COUNT(*) FROM users WHERE  status  = 'received' AND is_viewed = 'f';";
    return db.query(sql);
  }

  // for pg
  async function notificationQuickApplicantPg() {
    const db = await dbs();
    const sql =
      "SELECT COUNT(*) FROM users WHERE  status  = 'quick applicant' AND is_viewed = 'f';";
    return db.query(sql);
  }

  async function notificationQuickApplicantOh() {
    const db = await dbs();
    const sql =
      "SELECT COUNT(*) FROM users WHERE  status  = 'quick applicant' AND is_viewed_oh = 'f';";
    return db.query(sql);
  }

  //returns the total of unread/unchecked notifications for quick applicants (admin)
  async function notificationQuickApplicantAdmin() {
    const db = await dbs();
    const sql =
      "SELECT COUNT(*) FROM users WHERE  status  = 'quick applicant' = 'f';";
    return db.query(sql);
  }

  // notif received applicants filter by dept for dept head
  async function notificationReceivedDp(id) {
    const db = await dbs();
    const sql =
      "SELECT COUNT(u.id) FROM users u JOIN positions p ON p.id = u.position_id JOIN " +
      "teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "WHERE u.status = 'received' AND u.is_viewed = 'f' AND d.id = $1;";
    const params = [id];
    return db.query(sql, params);
  }

  //TH received notification
  async function notificationReceivedTh(id) {
    const db = await dbs();
    const sql =
      "SELECT COUNT(u.id) FROM users u JOIN positions p ON p.id = u.position_id JOIN " +
      "teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "WHERE u.status = 'received' AND u.is_viewed_th = 'f' AND t.id = $1;";
    const params = [id];
    return db.query(sql, params);
  }

  //OH received notification
  async function notificationReceivedOh() {
    const db = await dbs();
    const sql =
      "SELECT COUNT(u.id) FROM users u JOIN positions p ON p.id = u.position_id JOIN " +
      "teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "WHERE u.status = 'received' AND u.is_viewed_oh = 'f'";

    return db.query(sql);
  }

  // change to true when viewed notificationReceivedTrue
  async function notificationReceivedTrue(deptID) {
    const db = await dbs(),
      extQuery = (deptID) ? `AND t.department_id = ${deptID}` : '',
      sql =
        `SELECT u.id FROM users u 
      JOIN positions p ON p.id = u.position_id 
      JOIN teams t ON t.id = p.teams_id WHERE u.status = 'received' ${extQuery}
      AND u.is_viewed = 'f';`
    const data = await db.query(sql);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }

  async function notificationQuickApplicantTrueOh(dateFrom, dateTo) {
    const db = await dbs();
    let extQuery = (dateFrom && dateTo) ? `AND resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : ''
    const sql =
      `SELECT id, 
              email, 
              firstname, 
              lastname, 
              middlename, 
              mobile, 
              livestock, 
              willing_to_travel, 
              resume_upload_date, 
              resume_upload_url, 
              role_id, 
              (NOW() - resume_upload_date) as running_time 
        FROM users 
        WHERE position_id IS NULL AND status = 'quick applicant ${extQuery}' 
        ORDER BY resume_upload_date ASC`;
    const data = await db.query(sql);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed_oh = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }

  async function notificationQuickApplicantTrueAdmin() {
    const db = await dbs();
    const sql =
      `SELECT id, email, firstname, lastname,middlename, mobile, livestock, 
      willing_to_travel, resume_upload_date, resume_upload_url, role_id, (NOW() - resume_upload_date) as running_time
      FROM users WHERE position_id IS NULL AND status = 'quick applicant' ORDER BY id ASC`;
    const data = await db.query(sql);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed_admin = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }
  // change to true when viewed notificationQuickApplicantTrue
  async function notificationQuickApplicantTruePg() {
    const db = await dbs();
    const sql =
      "SELECT id, email, firstname, lastname,middlename, mobile, livestock," +
      "willing_to_travel, resume_upload_date, resume_upload_url, role_id, (NOW() - resume_upload_date) as running_time " +
      "FROM users WHERE position_id IS NULL AND status = 'quick applicant' ORDER BY id ASC";
    const data = await db.query(sql);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }

  // change to true when viewed notificationReceivedTrue team head
  async function notificationReceivedTrueTh(id) {
    const db = await dbs();
    const sql =
      "SELECT u.id FROM users u " +
      "JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id WHERE t.id = $1 AND u.status = 'received' " +
      "AND u.is_viewed_th = 'f';";
    const params = [id];
    const data = await db.query(sql, params);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed_th = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }

  // change to true when viewed notificationReceivedTrue team head
  async function notificationReceivedTrueOh(id) {
    const db = await dbs();
    const sql =
      "SELECT u.id FROM users u " +
      "JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id WHERE u.status = 'received' " +
      "AND u.is_viewed_oh = 'f';";

    const data = await db.query(sql);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed_oh = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }

  async function notificationReceivedTrueAdmin() {
    const db = await dbs(),
      sql =
        `SELECT u.id FROM users u
            JOIN positions p ON p.id = u.position_id
            JOIN teams t ON t.id = p.teams_id WHERE u.status = 'received'
            AND u.is_viewed_admin = 'f'`

    const data = await db.query(sql);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed_admin = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }

  // notification for endorsed applicants
  async function notificationEndorsed() {
    const db = await dbs();
    const sql =
      "SELECT COUNT(*) FROM users WHERE  status  = 'endorsement' AND is_viewed = 'f';";
    return db.query(sql);
  }

  //endorsement notification for oh
  async function notificationEndorsedOh() {
    const db = await dbs();
    const sql =
      "SELECT COUNT(*) FROM users WHERE  status  = 'endorsement' AND is_viewed_oh = 'f';";
    return db.query(sql);
  }

  async function notificationForEndorsed(id) {
    const db = await dbs();
    const sql =
      "SELECT COUNT(u.id) FROM users u JOIN positions p ON p.id = u.position_id JOIN " +
      "teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "WHERE u.status = 'endorsement' AND u.is_viewed = 'f' AND d.id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  async function notificationForEndorsedTh(id) {
    const db = await dbs();
    const sql =
      "SELECT COUNT(u.id) FROM users u JOIN positions p ON p.id = u.position_id JOIN " +
      "teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "WHERE u.status = 'endorsement' AND u.is_viewed_th = 'f' AND t.id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  async function notificationForEndorsedOh() {
    const db = await dbs();
    const sql =
      "SELECT COUNT(u.id) FROM users u JOIN positions p ON p.id = u.position_id JOIN " +
      "teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "WHERE u.status = 'endorsement' AND u.is_viewed_oh = 'f';";

    return db.query(sql);
  }
  // change to true when viewed
  async function notificationForEndorsedTrue() {
    const db = await dbs();
    const sql =
      "UPDATE users SET is_viewed = 't' WHERE  status  = 'endorsement';";
    return db.query(sql);
  }
  // change to true when viewed by th
  async function notificationForEndorsedTrueTh(id) {
    const db = await dbs();
    const sql =
      "SELECT u.id FROM users u " +
      "JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id WHERE t.id = $1 AND u.status = 'endorsement' " +
      "AND u.is_viewed_th = 'f';";
    const params = [id];
    const data = await db.query(sql, params);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed_th = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }
  // change to true when viewed by oh
  async function notificationForEndorsedTrueOh() {
    const db = await dbs();
    const sql =
      "SELECT u.id FROM users u " +
      "JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id WHERE u.status = 'endorsement' " +
      "AND u.is_viewed_oh = 'f';";
    const data = await db.query(sql);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed_oh = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }
  // change to true when viewed
  async function notificationEndorsedTrue() {
    const db = await dbs();
    const sql =
      "UPDATE users SET is_viewed = 't' WHERE  status  = 'endorsement';";
    return db.query(sql);
  }

  // change to true when viewed by oh
  async function notificationEndorsedOhTrue() {
    const db = await dbs();
    const sql =
      "UPDATE users SET is_viewed_oh = 't' WHERE  status  = 'endorsement';";
    return db.query(sql);
  }

  // notif for dept head on assessment applicants
  async function notificationAssessmentDp(id) {
    const db = await dbs();
    const sql =
      "SELECT COUNT(u.id) FROM users u JOIN positions p ON p.id = u.position_id JOIN " +
      "teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "WHERE u.status = 'assessment' AND u.is_viewed = 'f' AND d.id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // notif for operations head on assessment applicants
  async function notificationAssessmentOh() {
    const db = await dbs();
    const sql =
      "SELECT COUNT(u.id) FROM users u JOIN positions p ON p.id = u.position_id JOIN " +
      "teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "WHERE u.status = 'assessment' AND u.is_viewed_oh = 'f'";
    return db.query(sql);
  }
  // notif for team head on assessment applicants
  async function notificationAssessmentTh(id) {
    const db = await dbs();
    const sql =
      "SELECT COUNT(u.id) FROM users u JOIN positions p ON p.id = u.position_id JOIN " +
      "teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "WHERE u.status = 'assessment' AND u.is_viewed_th = 'f' AND t.id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // change to true when viewed
  // filter by dept param here is department id
  async function notificationAssessmentTrue(id) {
    const db = await dbs();
    const sql =
      "SELECT u.id FROM users u " +
      "JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id WHERE t.department_id = $1 AND u.status = 'assessment' " +
      "AND u.is_viewed = 'f';";
    const params = [id];
    const data = await db.query(sql, params);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }
  async function notificationAssessmentTrueTh(id) {
    const db = await dbs();
    const sql =
      "SELECT u.id FROM users u " +
      "JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id WHERE t.id = $1 AND u.status = 'assessment' " +
      "AND u.is_viewed_th = 'f';";
    const params = [id];
    const data = await db.query(sql, params);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed_th = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }

  async function notificationAssessmentTrueAdmin() {
    const db = await dbs(),
      sql = `SELECT u.id
                FROM users u
                JOIN positions p
                  ON p.id = u.position_id
                JOIN teams t
                  ON t.id = p.teams_id
                WHERE u.status = 'assessment'
                  AND u.is_viewed_admin = 'f'`,
      data = await db.query(sql),
      applicants = data.rows
    for (applicant of applicants) {
      let id = applicant.id,
        sql = `UPDATE users SET is_viewed_admin = 't' WHERE id = ${id}`
      await db.query(sql)
    }
  }
  async function notificationAssessmentTrueOh() {
    const db = await dbs();
    const sql =
      "SELECT u.id FROM users u " +
      "JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id WHERE  u.status = 'assessment' " +
      "AND u.is_viewed_oh = 'f';";
    const data = await db.query(sql);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed_oh = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }
  // notif for dept head on assessment applicants
  async function notificationDeployedDp(id) {
    const db = await dbs();
    const sql =
      "SELECT COUNT(u.id) FROM users u JOIN positions p ON p.id = u.position_id JOIN " +
      "teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "WHERE u.status = 'deployed' AND u.is_viewed = 'f' AND d.id = $1;";
    const params = [id];
    return db.query(sql, params);
  }

  // notif for team head on assessment applicants
  async function notificationDeployedTh(id) {
    const db = await dbs();
    const sql =
      "SELECT COUNT(u.id) FROM users u JOIN positions p ON p.id = u.position_id JOIN " +
      "teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "WHERE u.status = 'deployed' AND u.is_viewed_th = 'f' AND t.id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // notif for team head on assessment applicants
  async function notificationDeployedOh() {
    const db = await dbs();
    const sql =
      "SELECT COUNT(u.id) FROM users u JOIN positions p ON p.id = u.position_id JOIN " +
      "teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "WHERE u.status = 'deployed' AND u.is_viewed_oh = 'f' ;";

    return db.query(sql);
  }

  async function notificationDeployedAdmin() {
    const db = await dbs(),
      sql = `SELECT COUNT(u.id) 
              FROM users as u
              JOIN positions p
                ON p.id = u.position_id
              JOIN teams t
                ON t.id = p.teams_id
              JOIN departments d
                ON d.id = t.department_id
              WHERE u.status = 'deployed' AND u.is_viewed_admin = 'f'`
    return db.query(sql)
  }
  // change to true when viewed
  // filter by dept param here is department id
  async function notificationDeployedTrue(id) {
    const db = await dbs();
    const sql =
      "SELECT u.id FROM users u " +
      "JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id WHERE t.department_id = $1 AND u.status = 'deployed' " +
      "AND u.is_viewed = 'f';";
    const params = [id];
    const data = await db.query(sql, params);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }
  // change to true when viewed
  // filter by dept param here is department id
  async function notificationDeployedTrueTh(id) {
    const db = await dbs();
    const sql =
      "SELECT u.id FROM users u " +
      "JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id WHERE t.id = $1 AND u.status = 'deployed' " +
      "AND u.is_viewed_th = 'f';";
    const params = [id];
    const data = await db.query(sql, params);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed_th = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }
  // change to true when viewed by oh
  async function notificationDeployedTrueOh() {
    const db = await dbs();
    const sql =
      "SELECT u.id FROM users u " +
      "JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id WHERE  u.status = 'deployed' " +
      "AND u.is_viewed_oh = 'f';";

    const data = await db.query(sql);
    const applicants = data.rows;
    for (let i = 0; i < applicants.length; i++) {
      const e = applicants[i].id;
      const sql = "UPDATE users SET is_viewed_oh = 't' WHERE  id=$1;";
      const params = [e];
      await db.query(sql, params);
    }
  }
  // all new shortlisted for pg
  async function notificationShorlist() {
    const db = await dbs();
    const sql =
      "SELECT COUNT(*) FROM users WHERE  status  = 'shortlisted' AND is_viewed = 'f';";
    return db.query(sql);
  }
  // all new shortlisted for oh
  async function notificationShorlistOh() {
    const db = await dbs();
    const sql =
      "SELECT COUNT(*) FROM users WHERE  status  = 'shortlisted' AND is_viewed_oh = 'f';";
    return db.query(sql);
  }
  async function notificationShortlistTrue() {
    const db = await dbs();
    const sql =
      "UPDATE users SET is_viewed = 't' WHERE  status  = 'shortlisted';";
    return db.query(sql);
  }
  async function notificationShortlistOhTrue() {
    const db = await dbs();
    const sql =
      "UPDATE users SET is_viewed_oh = 't' WHERE  status  = 'shortlisted';";
    return db.query(sql);
  }
  // ADMIN FUNCTIONS
  async function adminInsert({ ...userInfo }) {
    const db = await dbs();
    const sql =
      "INSERT INTO users (id,email,password,time_created,time_updated, " +
      "firstname,lastname,mobile, " +
      "role_id,position_id,status,middlename) VALUES (DEFAULT,$1,$2,NOW(),NOW(), " +
      "$3,$4,$5,$6,$7,'active',$8) RETURNING id;";
    const params = [
      userInfo.email,
      userInfo.password,
      userInfo.firstname,
      userInfo.lastname,
      userInfo.mobile,
      userInfo.role_id,
      userInfo.position_id,
      userInfo.middlename,
    ];
    return db.query(sql, params);
  }
  async function viewAllUsers() {
    const db = await dbs();
    const sql =
      "SELECT u.id,u.email,u.firstname,u.lastname,u.mobile,u.status, " +
      "p.name AS position, t.name AS team,d.name AS department " +
      "FROM users u JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      " ORDER BY id ASC;";
    return db.query(sql);
  }
  async function viewDetailsSpecificUser(id) {
    const db = await dbs();
    const sql =
      "SELECT u.id,u.email,u.password,u.firstname,u.lastname,u.middlename,u.mobile,u.sign_in_count, " +
      "u.status,p.name AS position, p.description AS positionDescription, p.id AS positionId, " +
      "t.name AS team, t.description AS teamDescription, " +
      "d.name AS department, d.description AS deptDescription, " +
      "r.name AS roles, r.description AS roleDescription, r.id AS roleId " +
      "FROM users u JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "JOIN roles r ON r.id = u.role_id WHERE u.id = $1 " +
      "ORDER BY id ASC;";
    const params = [id];
    return db.query(sql, params);
  }
  async function adminUpdateUser({ id, ...userInfo }) {
    const db = await dbs();
    let sql = "";
    let params = [];

    if (userInfo.password) {
      sql =
        "UPDATE users SET email = $2, password = $3, firstname = $4, lastname = $5, " +
        "mobile = $6, role_id = $7, position_id = $8, middlename = $9, status = $10, sign_in_count = NULL " +
        "WHERE id = $1;";
      params = [
        id,
        userInfo.email,
        userInfo.password,
        userInfo.firstname,
        userInfo.lastname,
        userInfo.mobile,
        userInfo.role_id,
        userInfo.position_id,
        userInfo.middlename,
        userInfo.status,
      ];
    } else {
      sql =
        "UPDATE users SET email = $2, firstname = $3, lastname = $4, " +
        "mobile = $5, role_id = $6, position_id = $7, middlename = $8, status = $9 " +
        "WHERE id = $1;";
      params = [
        id,
        userInfo.email,
        userInfo.firstname,
        userInfo.lastname,
        userInfo.mobile,
        userInfo.role_id,
        userInfo.position_id,
        userInfo.middlename,
        userInfo.status,
      ];
    }

    return db.query(sql, params);
  }

  // IQ AND BEHAVIORAL EXAM
  async function getAllCategoryIdForIQBExam() {
    const db = await dbs();
    // store all category id;
    let data = [];
    // IQ Test
    const sql =
      `SELECT a.id,a.name,a.teams_id,b.item FROM 
      ( SELECT DISTINCT c.id,c.teams_id, c.name FROm questions q 
        JOIN categories c ON c.id = q.categories_id 
        WHERE c.name ='IQ Test' )a JOIN ( SELECT COUNT(*) item,c.id FROm questions q JOIN categories c ON c.id = q.categories_id 
        WHERE c.name ='IQ Test' GROUP BY c.id )b ON b.id = a.id `;
    const result = await db.query(sql);
    data.push(result.rows);
    // // Psychological Test
    // const sql2 =
    //   "SELECT a.id,a.name,b.item FROM ( SELECT DISTINCT c.id,c.name FROm questions q JOIN categories c ON c.id = q.categories_id " +
    //   "WHERE LOWER(c.name) = LOWER('Psychological Test') )a JOIN ( SELECT COUNT(*) item,c.id FROm questions q JOIN categories c ON c.id = q.categories_id " +
    //   "WHERE LOWER(c.name) = LOWER('Psychological Test') GROUP BY c.id )b ON b.id = a.id;";
    // const result2 = await db.query(sql2);
    // data.push(result2.rows);
    // // Math Test
    // const sql3 =
    //   "SELECT a.id,a.name,b.item FROM ( SELECT DISTINCT c.id,c.name FROm questions q JOIN categories c ON c.id = q.categories_id " +
    //   "WHERE LOWER(c.name) = LOWER('Math Test') )a JOIN ( SELECT COUNT(*) item,c.id FROm questions q JOIN categories c ON c.id = q.categories_id " +
    //   "WHERE LOWER(c.name) = LOWER('Math Test') GROUP BY c.id )b ON b.id = a.id;";
    // const result3 = await db.query(sql3);
    // data.push(result3.rows);
    // // Word Analogy
    // const sql4 =
    //   "SELECT a.id,a.name,b.item FROM ( SELECT DISTINCT c.id,c.name FROm questions q JOIN categories c ON c.id = q.categories_id " +
    //   "WHERE LOWER(c.name) = LOWER('Word Analogy') )a JOIN ( SELECT COUNT(*) item,c.id FROm questions q JOIN categories c ON c.id = q.categories_id " +
    //   "WHERE LOWER(c.name) = LOWER('Word Analogy') GROUP BY c.id )b ON b.id = a.id;";
    // const result4 = await db.query(sql4);
    // data.push(result4.rows);

    return data;
  }
  async function selectAllQuestionsIQB(categoryId) {
    const db = await dbs();
    const sql =
      "SELECT id FROM questions WHERE categories_id = $1 ORDER BY random();";
    const params = [categoryId];
    return db.query(sql, params);
  }
  async function insertAllQuestionsIQB(userId, questionId) {
    const db = await dbs();
    const sql =
      "INSERT INTO users_scores(id, user_id, user_answer, is_correct, question_id, time_created) VALUES (DEFAULT,$1,NULL,NULL,$2,NOW());";
    const params = [userId, questionId];
    return db.query(sql, params);
  }

  //date filter for reports
  async function filterReports(queryInfo) {
    const db = await dbs();
    const sql =
      'SELECT * FROM "applicantReports" WHERE ' +
      queryInfo.user +
      " status = $1 AND " +
      queryInfo.date +
      " BETWEEN $2 AND $3";

    const params = [queryInfo.status, queryInfo.startDate, queryInfo.endDate];
    return db.query(sql, params);
  }
  //for th
  async function getJobOrderTh({teamID, dateTo, dateFrom, offset = 0, limit = null}) {
    try {
      const db = await dbs();
      const dateFilter = (dateTo && dateFrom) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : ``;
      const sql = `SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,
        willing_to_travel,p.name,resume_upload_date,resume_upload_url,
        u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, adt.date_shortlist, adt.date_joborder, (NOW() - adt.date_joborder) as running_time, d.id as deptid
        FROM users u JOIN positions p ON p.id = u.position_id
        JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id
        LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
        WHERE u.status = 'job offer' AND t.id = $1 ${dateFilter}
        ORDER BY u.resume_upload_date DESC
        LIMIT ${limit}
        OFFSET ${offset}`;

      const params = [teamID]
      return db.query(sql, params);
    } catch (error) {
      console.log(error)
    }
  }
  async function findAllJobOrderTh(id, date_from, date_to) {
    let extQuery = ``;
    if (date_from && date_to) { extQuery = `AND u.resume_upload_date BETWEEN '${date_from}' AND '${date_to}' ` }
    const db = await dbs();
    const sql =
      "SELECT u.id,email,firstname,lastname,middlename,mobile,livestock, " +
      "willing_to_travel,p.name,resume_upload_date,resume_upload_url, " +
      "u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, adt.date_shortlist, adt.date_joborder, (NOW() - adt.date_joborder) as running_time, d.id as deptid " +
      "FROM users u JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
      `WHERE u.status = 'joborder' AND t.id = $1 ${extQuery} ORDER BY u.id DESC`; //adt.date_joborder ASC, u.id ASC, u.id ASC;";
    const params = [id];
    return db.query(sql, params);
  }

  async function findAllReceivedTh(id, date_from, dateTo) {
    let extQuery = '';
    if (date_from && dateTo) { extQuery = `AND u.resume_upload_date BETWEEN '${date_from}' AND '${dateTo}' ` }

    const db = await dbs();
    const sql =
      "SELECT u.id,email,firstname,lastname,middlename,mobile,livestock, " +
      "willing_to_travel,p.name,resume_upload_date,resume_upload_url, d.name as deptname," +
      "adt.date_received, cat_count.category_count, (now() - adt.date_received) as running_time, d.id as deptid  " +
      "FROM users u JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
      "LEFT JOIN ( " +
      "SELECT teams_id, COUNT(teams_id) as category_count " +
      "FROM categories " +
      "GROUP BY teams_id " +
      ") as cat_count ON t.id = cat_count.teams_id " +
      `WHERE u.status = 'received' AND t.id = $1 ${extQuery} ORDER BY u.id DESC` //resume_upload_date ASC, date_received ASC, u.id ASC;";
    const params = [id];
    return db.query(sql, params);
  }

  async function findAllOnlineExamTH({teamID, dateFrom, dateTo, offset = 0, limit = null}) {
    try {
      let extQuery = ``;
      if (dateFrom && dateTo) { extQuery = `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}' ` }

      const db = await dbs();
      const sql =
        "SELECT u.id,email,firstname,lastname,middlename,mobile,livestock, " +
        "willing_to_travel,p.name,resume_upload_date,resume_upload_url,adt.online_exam_start, " +
        "u.date_application, adt.date_received, adt.date_online_exam, checkexam.status, " +
        "rh.remaining_hours, rh.remaining_minutes, (NOW() - adt.online_exam_start) as running_time, d.id as deptid,  " +
        "checkexam.not_answered_categories, checkexam.total_time_exam_seconds, checkexam.total_time_elapsed_seconds " +
        "FROM users u JOIN positions p ON p.id = u.position_id " +
        "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
        "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
        "LEFT JOIN (" +
        " SELECT cr.user_id, COUNT(cr.is_done) as not_answered_categories, " +
        "u.total_time_exam_seconds, u.total_time_elapsed_seconds, u.status " +
        "FROM categories_random cr " +
        "JOIN users u ON cr.user_id = u.id " +
        "WHERE is_done = 'no' " +
        "GROUP BY cr.user_id, u.total_time_exam_seconds, u.total_time_elapsed_seconds, u.status " +
        ") checkexam ON u.id = checkexam.user_id " +
        "LEFT JOIN( " +
        "SELECT adt.users_id," +
        "EXTRACT(HOUR FROM (adt.online_exam_end - NOW())) as remaining_hours, " +
        "EXTRACT(MINUTE FROM (adt.online_exam_end - NOW())) as remaining_minutes " +
        "FROM applicant_date_trackings adt " +
        ") rh ON u.id = rh.users_id " +
        `WHERE u.status = 'online exam' AND t.id = $1 ${extQuery} 
        ORDER BY u.id DESC
        LIMIT ${limit}
        OFFSET ${offset}`; //adt.online_exam_start ASC, u.id ASC;";
      const params = [teamID];
      return db.query(sql, params);
    } catch (error) {
      console.log(error)
    }
  }

  async function findAllEndorsementTh({teamID, dateFrom, dateTo, offset = 0, limit = null}) {
    try {
      const db = await dbs(),
            extQuery = (dateFrom && dateTo) ? `AND resume_upload_url BETWEEN '${dateFrom}' AND '${dateTo}'` : '',
            sql =
              `SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,u.is_done_iqbe_exam,
              willing_to_travel,p.name,resume_upload_date,resume_upload_url,adt.online_exam_start,adt.online_exam_end, u.date_application, adt.date_received, (now() - adt.date_endorsement) as running_time, adt.date_endorsement, adt.date_for_endorsement
              FROM users u JOIN positions p ON p.id = u.position_id, d.id as deptid
              JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id
              LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
              WHERE (u.status = 'endorsement' OR u.status = 'schedule for assessment')
              AND (adt.date_endorsement IS NOT NULL
              AND adt.date_for_endorsement IS NOT NULL)
              AND t.id = $1
              ${extQuery} 
              ORDER BY u.id DESC
              LIMIT ${limit} OFFSET ${offset}`; //adt.date_endorsement ASC, adt.online_exam_end ASC, u.id ASC;";
    const params = [teamID];
    return db.query(sql, params);
    } catch (error) {
      console.log(error)
    }
  }


  async function findAllShortListTh(teamID, dateTo, dateFrom) {
    const db = await dbs();
    const Sorting = (dateTo && dateFrom) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}' ORDER BY u.id DESC` : `ORDER BY u.id DESC`;

    const sql = `SELECT u.id, email, firstname, lastname, middlename, mobile, livestock,
    willing_to_travel, p.name, resume_upload_date, resume_upload_url, schedule_for_assessment, (NOW() - adt.date_pending) as running_time,
    online_exam_start, online_exam_end, d.id as deptid, adt.date_assessment as date_interview FROM users u LEFT JOIN positions p ON p.id = u.position_id
    LEFT JOIN teams t ON t.id = p.teams_id LEFT JOIN departments d ON d.id = t.department_id
    LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
    WHERE u.status = 'shortlisted' AND p.teams_id = $1 ${Sorting}`; //adt.date_shortlist ASC, u.id ASC;";

    let params = [teamID];
    return db.query(sql, params);
  }

  async function findAllKeptForReferenceDh({deptID, dateTo, dateFrom, offset = 0, limit = null}) {
    try {
      const db = await dbs();
      const Sorting = (dateTo && dateFrom) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : ``;
      const sql = `SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,
                    position_id,u.filled_up_form,u.is_done_iqbe_exam,
                      willing_to_travel,p.name,resume_upload_date,resume_upload_url,
                      u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, adt.date_shortlist, adt.date_kept_reference, adt.remarks_kept_reference,
                      (NOW() - adt.date_kept_reference) as running_time, d.id as deptid, cat_count.category_count
                    FROM users u JOIN positions p ON p.id = u.position_id
                    JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id
                    LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
                    LEFT JOIN (
                    SELECT teams_id, COUNT(teams_id) as category_count
                    FROM categories
                    GROUP BY teams_id
                      ) as cat_count ON t.id = cat_count.teams_id
                    WHERE u.status = 'kept for reference' AND d.id = $1 
                      ${Sorting}
                    ORDER BY u.id DESC
                    LIMIT ${limit}
                    OFFSET ${offset}`; //adt.date_shortlist ASC, u.id ASC;";
      const params = [deptID]
      return db.query(sql, params);
    } catch (error) {
      console.log(error)
    }
  }
  async function findAllKeptForReferenceTh({teamID, dateTo, dateFrom, offset = 0, limit = null}) {
    try {
      const db = await dbs();
      const dateFilter = (dateTo && dateFrom) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}' ` : ``;
      const sql = `SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,position_id,u.filled_up_form,u.is_done_iqbe_exam,
        willing_to_travel,p.name,resume_upload_date,resume_upload_url,
        u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, adt.date_shortlist, adt.date_kept_reference, adt.remarks_kept_reference,
        (NOW() - adt.date_kept_reference) as running_time, d.id as deptid, cat_count.category_count
        FROM users u JOIN positions p ON p.id = u.position_id
        JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id
        LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
        LEFT JOIN (
        SELECT teams_id, COUNT(teams_id) as category_count
        FROM categories
        GROUP BY teams_id
        ) as cat_count ON t.id = cat_count.teams_id
        WHERE u.status = 'kept for reference' AND t.id = $1 
        ${dateFilter}
        ORDER BY u.id DESC
        LIMIT ${limit}
        OFFSET ${offset}`; //adt.date_shortlist ASC, u.id ASC;";
      const params = [teamID]

      return db.query(sql, params);
    } catch (error) {
      console.log(error)
    }
  }
  async function findAllRejectDh(deptID, dateFrom, dateTo) {
    const db = await dbs();
    const Sorting = (dateTo && dateFrom) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}' ORDER BY u.id DESC` : `ORDER BY u.id DESC`;

    const sql =
      `SELECT u.id, u.email, u.firstname, u.lastname, u.middlename, u.mobile, u.livestock, 
      u.willing_to_travel, p.name, u.resume_upload_date, u.resume_upload_url, adt.remarks_reject,
      u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, adt.date_shortlist, adt.date_rejected,
      us.firstname as rb_firstname, us.lastname as rb_lastname, d.id as deptid
      FROM users u JOIN positions p ON p.id = u.position_id 
      JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id 
      LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
      LEFT JOIN users us ON adt.rejected_by = us.id 
      WHERE u.status = 'rejected' AND d.id = ${deptID} ${Sorting}`;

    return db.query(sql);
  }
  async function findAllRejectTh(teamID, dateFrom, dateTo) {
    const db = await dbs();
    const Sorting = (dateTo && dateFrom) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}' ORDER BY u.id DESC` : `ORDER BY u.id DESC`;

    const sql =
      "SELECT u.id, u.email, u.firstname, u.lastname, u.middlename, u.mobile, u.livestock, " +
      "u.willing_to_travel, p.name, u.resume_upload_date, u.resume_upload_url, adt.remarks_reject, " +
      "u.date_application, adt.date_received, adt.date_online_exam, adt.date_endorsement, adt.date_assessment, adt.date_shortlist, adt.date_rejected, " +
      "us.firstname as rb_firstname, us.lastname as rb_lastname, d.id as deptid " +
      "FROM users u JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
      "LEFT JOIN users us ON adt.rejected_by = us.id " +
      `WHERE u.status = 'rejected' AND t.id = ${teamID} ${Sorting}`; // adt.date_rejected ASC, u.id ASC;";
    return db.query(sql);
  }


  //get date for date trackings
  async function findAllPendingDh({deptID, dateTo, dateFrom, offset = 0, limit = null}) {
    try {
      const db = await dbs();
      const dateFilter = (dateTo && dateFrom) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : ``;
      const sql = ` SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,
       willing_to_travel,p.name,resume_upload_date,resume_upload_url,schedule_for_assessment, (NOW() - adt.date_pending) as running_time,
       online_exam_start,online_exam_end, d.id as deptid, adt.date_assessment as date_interview FROM users u LEFT JOIN positions p ON p.id = u.position_id
       LEFT JOIN teams t ON t.id = p.teams_id LEFT JOIN departments d ON d.id = t.department_id
      LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
   WHERE u.status = 'pending for review' AND d.id = $1 
   ${dateFilter}
   ORDER BY u.id DESC
   LIMIT ${limit}
   OFFSET ${offset}`;
      let params = [deptID]
      return db.query(sql, params);
    } catch (error) {
      console.log(error.message)
    }
  }

  async function findAllShortListDh(deptID, dateTo, dateFrom) {
    try {
      const db = await dbs();
      const Sorting = (dateTo && dateFrom) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}' ORDER BY u.id DESC` : `ORDER BY u.id DESC`;
      const sql = ` SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,
       willing_to_travel,p.name,resume_upload_date,resume_upload_url,schedule_for_assessment, (NOW() - adt.date_pending) as running_time,
       online_exam_start,online_exam_end, d.id as deptid, adt.date_assessment as date_interview FROM users u LEFT JOIN positions p ON p.id = u.position_id
       LEFT JOIN teams t ON t.id = p.teams_id LEFT JOIN departments d ON d.id = t.department_id
      LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
   WHERE u.status = 'shortlisted' AND d.id = $1 ${Sorting}`;

      let params = [deptID];
      return db.query(sql, params);
    } catch (error) {
      console.log(error.message)
    }
  }
  async function findAllPendingTh({teamID, dateTo, dateFrom, offset = 0, limit = null}) {
    try {
      const db = await dbs();
      const Sorting = (dateTo && dateFrom) ? `AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : ``;
      const sql = ` SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,
                    willing_to_travel,p.name,resume_upload_date,resume_upload_url,schedule_for_assessment, (NOW() - adt.date_pending) as running_time,
                    online_exam_start,online_exam_end, d.id as deptid, adt.date_assessment as date_interview FROM users u LEFT JOIN positions p ON p.id = u.position_id
                    LEFT JOIN teams t ON t.id = p.teams_id LEFT JOIN departments d ON d.id = t.department_id
                    LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id
                    WHERE u.status = 'pending for review' AND p.teams_id = $1 
                    ${Sorting}
                    ORDER BY u.id DESC
                    LIMIT ${limit}
                    OFFSET ${offset}`;
      let params = [teamID]
      return db.query(sql, params);
    } catch (error) {
      console.log(error.message)
    }
  }

  async function findAllForEndorsementTh(id, date_from, date_to) {
    var extQuery = '';
    if (date_from && date_to) { extQuery = ` AND  u.resume_upload_date BETWEEN '${date_from}' AND '${date_to}' ` }

    const db = await dbs();
    const sql =
      "SELECT u.id,email,firstname,lastname,middlename,mobile,livestock,position_id, u.status,u.is_done_iqbe_exam, " +
      "willing_to_travel,p.name, resume_upload_date, resume_upload_url, u.date_application," +
      "adt.date_received, adt.online_exam_start, adt.online_exam_end, adt.date_for_endorsement, (now() - adt.date_for_endorsement) as running_time, d.id as deptid " +
      "FROM users u JOIN positions p ON p.id = u.position_id " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "LEFT JOIN applicant_date_trackings adt ON adt.users_id = u.id " +
      `WHERE u.status = 'endorsement' 
          AND t.id = $1  
          AND adt.date_for_endorsement IS NOT NULL
          AND adt.date_endorsement IS NOT NULL
      ${extQuery} ORDER BY u.id DESC`; //adt.online_exam_end ASC, u.id ASC;";
    const params = [id];
    return db.query(sql, params);
  }

  async function findAllApplicants({dateFrom, dateTo, offset = 0, limit = null }) {
    try {
      let dateFilter = (dateFrom && dateTo) ? ` AND u.resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : ''

      const db = await dbs(),
            sql = 
            `SELECT
              u.ID,
              u.date_application,
              u.firstname,
              u.middlename,
              u.lastname,
              u.email,
              u.mobile,
              u.livestock,
              u.willing_to_travel,
              ( NOW( ) - u.resume_upload_date ) AS running_time,
              u.resume_upload_date,
              u.resume_upload_url,
              u.status AS stage,
              p.name
            FROM
              users u
            LEFT JOIN positions p ON p.id = u.position_id
            LEFT JOIN teams t ON t.id = p.teams_id
            LEFT JOIN departments d ON d.id = t.department_id
            WHERE
              u.status NOT LIKE 'rejected' AND
              u.status NOT LIKE 'blacklist' AND 
              u.status NOT LIKE 'active' AND
              u.status NOT LIKE 'inactive'
              ${dateFilter}
            ORDER BY u.resume_upload_date DESC
            LIMIT ${limit} OFFSET ${offset}`;
      return db.query(sql)
    } catch (error) {
      console.log(error)
    }
  }

  async function getAllApplicants(deptid, posid=undefined, offset=0, limit=null) {
    const db = await dbs(),
      filter = (deptid && posid) ? ` AND d.id = ${deptid} AND p.id = ${posid}` : ` AND d.id = ${deptid}`
      sql = `SELECT
          u.ID,
          u.date_application,
          u.firstname,
          u.middlename,
          u.lastname,
          u.email,
          u.mobile,
          u.livestock,
          u.willing_to_travel,
          ( NOW( ) - u.resume_upload_date ) AS running_time,
					u.resume_upload_date,
          u.resume_upload_url,
          u.status AS stage,
          p.name
        FROM
          users u
        LEFT JOIN positions p ON p.id = u.position_id
        LEFT JOIN teams t ON t.id = p.teams_id
        LEFT JOIN departments d ON d.id = t.department_id 
        WHERE
          u.resume_upload_date IS NOT NULL AND
          u.status NOT LIKE 'rejected' AND
          u.status NOT LIKE 'blacklist' AND 
					u.status NOT LIKE 'active' AND
					u.status NOT LIKE 'inactive' 
          ${filter}
        ORDER BY
          u.resume_upload_date DESC
        LIMIT ${limit} OFFSET ${offset}` ;
    return db.query(sql)
  }

  async function findAllApplicantsDH({deptID, dateFrom, dateTo, offset = 0, limit = null}) {
    try {
      let dateFilter = (dateFrom && dateTo) ? `AND ( u.resume_upload_date BETWEEN '${dateFrom}'                                         AND '${dateTo}' )` : ''
      const db = await dbs(),
          sql = `
            SELECT
              u.ID,
              u.date_application,
              u.firstname,
              u.middlename,
              u.lastname,
              u.email,
              u.mobile,
              u.livestock,
              u.willing_to_travel,
              ( NOW() - u.resume_upload_date ) AS running_time,
              u.resume_upload_date,
              u.resume_upload_url,
              u.status AS stage,
              p.name
            FROM
              users u
              left JOIN positions P ON u.position_id = P.
              ID left JOIN teams T ON P.teams_id = T.
              ID left JOIN departments d ON d.ID = T.department_id
            WHERE 
              u.resume_upload_date IS NOT NULL AND
              u.status NOT LIKE 'blacklist' AND
              u.status NOT LIKE 'rejected' AND
              u.status NOT LIKE 'active' AND
              u.status NOT LIKE 'inactive'  AND
              d.ID = ${deptID} ${dateFilter}
            ORDER BY
              u.resume_upload_date DESC
            LIMIT ${limit} OFFSET ${offset}`
      return db.query(sql)
    } catch (error) {
      console.log(error)
    }
  }
  //find all applicant where belong to th

  async function findAllApplicantsTH({teamID, dateFrom, dateTo, limit = null, offset = 0}) {
    try {
      const db = await dbs();
      let dateFilter = (dateFrom && dateTo) ? `AND ( resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}' )` : ``

      const sql = `SELECT
                    u.ID,
                    u.date_application,
                    u.firstname,
                    u.middlename,
                    u.lastname,
                    u.email,
                    u.mobile,
                    u.livestock,
                    u.willing_to_travel,
                    ( NOW() - u.resume_upload_date ) AS running_time,
                    u.resume_upload_date,
                    u.resume_upload_url,
                    u.status AS stage,
                    p.name
                  FROM
                    users u
                    LEFT JOIN positions P ON u.position_id = P.ID 
                    LEFT JOIN teams T ON P.teams_id = T.ID 
                    LEFT JOIN departments d ON d.ID = T.department_id 
                  WHERE  
                    u.resume_upload_date IS NOT NULL AND
                    u.status NOT LIKE 'blacklist' AND
                    u.status NOT LIKE 'rejected' AND
                    u.status NOT LIKE 'active' AND
                    u.status NOT LIKE 'inactive' AND
                    T.ID = ${teamID} ${dateFilter}  
                  ORDER BY
                    u.resume_upload_date DESC
                  LIMIT ${limit} OFFSET ${offset}`;
      return db.query(sql);
    } catch (error) {
      console.log(error)
    }
  }

  async function findAllfetchedQuickApplicants(deptid, posid, dateFrom, dateTo) {
    const dateFilter = (dateFrom && dateTo) ? `AND resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : ``;
    const db = await dbs(),
      deptFilter = (deptid) ? ` AND d.id = ${deptid}` : '',
      deptAndPosFilter = (deptid && posid) ? ` AND d.id= ${deptid} AND p.id = ${posid}` : '',
      sql =
        `SELECT  u.ID,
          u.date_application,
          u.firstname,
          u.middlename,
          u.lastname,
          u.email,
          u.mobile,
          u.livestock,
          u.willing_to_travel,
          ( NOW( ) - u.resume_upload_date ) AS running_time,
					u.resume_upload_date,
          u.resume_upload_url,
          u.status AS stage,
          u.position_id as name
           FROM users u
            WHERE 
              u.status = 'quick applicant'
              ${deptFilter}
              ${deptAndPosFilter}
              ${dateFilter}
            ORDER BY resume_upload_date DESC`;
    return db.query(sql);
  }


  async function findAllQuickApplicants(deptid, posid) {
    const db = await dbs(),
      deptFilter = (deptid) ? ` AND d.id = ${deptid}` : '',
      deptAndPosFilter = (deptid && posid) ? ` AND d.id= ${deptid} AND p.id = ${posid}` : '',
      sql =
        `SELECT u.id, u.email, u.firstname, u.lastname, u.middlename, u.mobile, u.livestock, u.willing_to_travel, u.resume_upload_date, u.resume_upload_url, (NOW() - u.resume_upload_date) as running_time, u.status, u.position_id
            FROM users u 
            WHERE 
              u.status = 'quick applicant'
              ${deptFilter}
              ${deptAndPosFilter}
            ORDER BY u.id DESC`;
    return db.query(sql);
  }
  //find applicant sorted by date 
  async function findAllQuickApplicantsByDate(dateFrom, dateTo) {
    const db = await dbs(),
      extQuery = (dateFrom && dateTo) ? `AND resume_upload_date BETWEEN '${dateFrom}' AND '${dateTo}'` : '',
      sql =
        `SELECT u.id, u.email, u.firstname, u.lastname, u.middlename, u.mobile, u.    livestock, u.willing_to_travel, u.resume_upload_date, u.resume_upload_url, (NOW() - u.resume_upload_date) as running_time, u.status
    FROM users u 
    WHERE u.status = 'quick applicant' ${extQuery}
    ORDER BY u.id DESC`;
    //modified by rumel
    // const sql = "SELECT id, email, firstname, lastname,middlename, mobile, livestock, " +
    //   "willing_to_travel, resume_upload_date, resume_upload_url, role_id,   resume_upload_date as running_time " +
    //   "FROM users WHERE position_id IS NULL AND status = 'quick applicant' AND resume_upload_date BETWEEN  $1 AND  $2 ORDER BY id ASC";
    const res = await db.query(sql);
    return res
  }
  //
  async function changeStatusReceived(id) {
    const db = await dbs();
    //
    const sql = "SELECT * FROM applicant_date_trackings WHERE users_id = $1;";
    const params = [id];
    const data = await db.query(sql, params);

    if (data.rowCount > 0) {
      // update
      const sql = `
        UPDATE applicant_date_trackings SET date_received = NOW(), date_assessment = null, date_online_exam = null, online_exam_start = null, online_exam_end = null  
        WHERE users_id = $1;`;

      await db.query(sql, params);
    }

    else {

      const sql = `
        INSERT INTO applicant_date_trackings (users_id, date_received)
        VALUES ($1, NOW())
      `;

      await db.query(sql, params);
    }

    let deleteScores = `
            DELETE FROM users_scores where user_id = $1;
    `
    await db.query(deleteScores, params)

    let deleteCategoryResults = `    
            DELETE FROM categories_random where user_id = $1;
    `
    await db.query(deleteCategoryResults, params)
  
    const sql2 =
      `UPDATE users SET status = 'received', is_viewed = 'f', is_viewed_oh = 'f', is_done_online_exam = 'f',  exam_start = null, exam_end = null, total_time_exam_seconds = null, total_time_elapsed_seconds = null, iqbe_time = null WHERE id = $1;`;

    return db.query(sql2, params);
  }

  async function ifTEst(info) {
    const db = await dbs();
    const sql =
      `SELECT u.id, d.name as op_dept from users u
			JOIN positions p ON u.position_id = p.id 
			JOIN teams t ON p.teams_id = t.id 
      JOIN departments d ON t.department_id = d.id 
			WHERE d.name='TEST' and u.id=$1`;
    const params = [info];
    return db.query(sql, params);
  }

  //get dh email given position id
  async function getDhEmail(posId) {
    const db = await dbs();
    const sql =
      `SELECT p.id as position_id, p.name as position, 
      t.name as team, d.name as department, dh.role, dh.id, dh.email, dh.firstname, dh.lastname
      FROM positions p
      LEFT JOIN teams t ON p.teams_id = t.id
      LEFT JOIN departments d ON t.department_id = d.id 
      LEFT JOIN( 
      SELECT u.id, u.email, u.firstname, u.lastname, 
      r.name as role, p.name as position, d.id as dept_id, d.name as department 
      FROM users u 
      JOIN roles r ON u.role_id = r.id 
      LEFT JOIN positions p ON u.position_id = p.id 
      LEFT JOIN teams t ON p.teams_id = t.id
      LEFT JOIN departments d ON t.department_id = d.id
      ) dh ON d.id = dh.dept_id
      WHERE p.id = $1 AND (dh.role = 'Department Head' OR dh.role = 'Administrator')`;
    const params = [posId];
    return db.query(sql, params);
  }

  async function getDhEmailWithId(userId) {
    const db = await dbs();
    const sql =
      "SELECT dh.email, dh.firstname, dh.lastname, p.name as position, t.name as team, d.name as department, r.name as role, u.position as applicant_position " +
      "FROM users dh " +
      "LEFT JOIN roles r ON dh.role_id = r.id " +
      "LEFT JOIN positions p ON dh.position_id = p.id " +
      "LEFT JOIN teams t ON p.teams_id = t.id " +
      "LEFT JOIN departments d ON t.department_id = d.id " +
      "LEFT JOIN " +
      " (" +
      "SELECT u.id, u.email, u.firstname, u.lastname, d.id as department, p.name as position " +
      "FROM users u " +
      "LEFT JOIN positions p ON u.position_id = p.id " +
      "LEFT JOIN teams t ON p.teams_id = t.id " +
      "LEFT JOIN departments d ON t.department_id = d.id " +
      ") u ON u.department = d.id " +
      "WHERE u.id = $1 AND r.name = 'Department Head'";
    const params = [userId];
    return db.query(sql, params);
  }

  async function getUserDetails(id) {
    const db = await dbs();
    const sql =
      `SELECT u.id, u.email, u.firstname, u.lastname, p.name as position, r.name as role, d.id as deptid, d.name as department, t.id as teamid, t.name as team
      FROM users u 
      LEFT JOIN positions p on p.id = u.position_id 
      INNER JOIN teams t on t.id = p.teams_id
      INNER JOIN departments d on d.id = t.department_id
      INNER JOIN roles r on r.id = u.role_id WHERE u.id = $1`;
    const params = [id];
    return db.query(sql, params);
  }

  async function getOperationsHeadEmail(deptIDs) {
    const db = await dbs();
    sql =
      "SELECT u.id, u.firstname, u.lastname, u.position_id, u.email, r.name as role, d.name as op_dept " +
      "FROM users u " +
      "LEFT JOIN positions p ON u.position_id = p.id " +
      "LEFT JOIN teams t ON p.teams_id = t.id " +
      "LEFT JOIN departments d ON t.department_id = d.id " +
      "LEFT JOIN roles r ON u.role_id = r.id " +
      `WHERE r.name = 'Operations Head' and d.id in (${deptIDs})`

    return db.query(sql);
  }

  async function getApplicantDateTrackings(id) {
    const db = await dbs();
    sql =
      "SELECT users_id, " +
      "(date_received::TIMESTAMP - u.resume_upload_date::TIMESTAMP) as days_as_quick_applicant, " +
      "(date_online_exam::TIMESTAMP - date_received::TIMESTAMP) as days_received, " +
      "(date_endorsement::TIMESTAMP - date_online_exam::TIMESTAMP) as days_online_exam, " +
      "(date_assessment::TIMESTAMP - date_endorsement::TIMESTAMP) as days_endorsement, " +
      "(date_pending::TIMESTAMP - date_assessment::TIMESTAMP) as days_assessment, " +
      "(date_shortlist::TIMESTAMP - date_pending::TIMESTAMP) as days_pending, " +
      "(date_joborder::TIMESTAMP - date_shortlist::TIMESTAMP) as days_shortlist, " +
      "(date_deployed::TIMESTAMP - date_joborder::TIMESTAMP) as days_joborder, " +
      "(date_deployed::TIMESTAMP - u.resume_upload_date::TIMESTAMP) as recruitment_duration, " +
      "date_received, date_online_exam, date_endorsement, date_assessment, date_pending, date_shortlist, date_joborder, date_deployed " +
      "FROM applicant_date_trackings " +
      "JOIN users u ON applicant_date_trackings.users_id = u.id " +
      "WHERE u.status = 'deployed' AND u.id = $1";
    params = [id];
    return db.query(sql, params);
  }

  async function getApplicantAssessmentSchedule(id) {
    const db = await dbs();
    const sql =
      "SELECT date_assessment FROM applicant_date_trackings WHERE users_id = $1";
    params = [id];
    return db.query(sql, params);
  }

  //-----------------butch part-------------------
  //filter the kept for ref. tab
  async function filterKeptfroRef(queryInfo) {
    const db = await dbs();
    var filter = "";

    if (queryInfo.deptid) {
      filter = filter.concat(" AND department = $3 ");
    }
    if (queryInfo.posid && queryInfo.posid != "") {
      filter = filter.concat(" AND position =  $4 ");
    }

    const sql =
      'SELECT * FROM "applicantReports" WHERE ' +
      "status='kept for reference' AND date_kept_reference BETWEEN $1 AND $2 " +
      filter;

    if (queryInfo.deptid && queryInfo.posid != "") {
      params = [
        queryInfo.startDate,
        queryInfo.endDate,
        queryInfo.deptid,
        queryInfo.posid,
      ];
      return db.query(sql, params);
    } else if (queryInfo.deptid) {
      params = [queryInfo.startDate, queryInfo.endDate, queryInfo.deptid];
      return db.query(sql, params);
    } else {
      return db.query(sql);
    }
  }

  //filter the blacklist tab
  async function filterBlacklist(queryInfo) {
    const db = await dbs();
    var filter = "";

    if (queryInfo.deptid) {
      filter = filter.concat(" AND department = $3 ");
    }
    if (queryInfo.posid && queryInfo.posid != "") {
      filter = filter.concat(" AND position =  $4 ");
    }

    const sql =
      'SELECT * FROM "applicantReports" WHERE ' +
      "status='blacklist' AND date_blacklist BETWEEN $1 AND $2 " +
      filter;

    if (queryInfo.deptid && queryInfo.posid != "") {
      params = [
        queryInfo.startDate,
        queryInfo.endDate,
        queryInfo.deptid,
        queryInfo.posid,
      ];
      return db.query(sql, params);
    } else if (queryInfo.deptid) {
      params = [queryInfo.startDate, queryInfo.endDate, queryInfo.deptid];
      return db.query(sql, params);
    } else {
      return db.query(sql);
    }
  }
  //filter the rejected tab
  async function filterRejected(queryInfo) {
    const db = await dbs();
    var filter = "";

    if (queryInfo.deptid) {
      filter = filter.concat(" AND department = $3 ");
    }
    if (queryInfo.posid && queryInfo.posid != "") {
      filter = filter.concat(" AND position =  $4 ");
    }

    const sql =
      'SELECT * FROM "applicantReports" WHERE ' +
      "status='rejected' AND date_rejected BETWEEN $1 AND $2 " +
      filter;

    if (queryInfo.deptid && queryInfo.posid != "") {
      params = [
        queryInfo.startDate,
        queryInfo.endDate,
        queryInfo.deptid,
        queryInfo.posid,
      ];
      return db.query(sql, params);
    } else if (queryInfo.deptid) {
      params = [queryInfo.startDate, queryInfo.endDate, queryInfo.deptid];
      return db.query(sql, params);
    } else {
      return db.query(sql);
    }
  }
  //filter the deployed tab
  async function filterDeployed(queryInfo) {
    const db = await dbs();
    var filter = "";

    if (queryInfo.deptid) {
      filter = filter.concat(" AND department = $3 ");
    }
    if (queryInfo.posid && queryInfo.posid != "") {
      filter = filter.concat(" AND position =  $4 ");
    }

    const sql =
      `SELECT * FROM "applicantReports" WHERE ` +
      `status='deployed' AND date_deployed BETWEEN $1 AND $2 ` +
      filter;
    if (queryInfo.deptid && queryInfo.posid != "") {
      params = [
        queryInfo.startDate,
        queryInfo.endDate,
        queryInfo.deptid,
        queryInfo.posid,
      ];
      return db.query(sql, params);
    } else if (queryInfo.deptid) {
      params = [queryInfo.startDate, queryInfo.endDate, queryInfo.deptid];
      return db.query(sql, params);
    } else {
      return db.query(sql);
    }
  }

  // check if user id has already categories random
  async function checkCatRandom(id) {
    const db = await dbs();
    const sql = "SELECT * FROM categories_random where user_id=$1;";
    const params = [id];
    return db.query(sql, params);
  }
  //check for question type if essay or problem solving
  async function onlineExamResultQTYPE(id, posid) {
    const db = await dbs();
    const sql = `SELECT DISTINCT a.user_id,a.firstname,a.lastname,a.catgname,b.question_type,b.id,b.is_correct,b.questions,b.user_answer 
                    FROM ( 
                    SELECT cr.user_id,u.firstname,u.lastname, cr.category_id, c.name 
                        AS catgname 
                        FROM categories_random cr 
                      LEFT JOIN categories c 
                        ON c.id = cr.category_id JOIN users u ON u.id = cr.user_id 
                        WHERE cr.user_id = $1
                    ) a 
                      LEFT JOIN ( 
                      SELECT  q.categories_id, p.position ,qt.question_type,q.questions,u.user_answer,u.is_correct,u.id
                        FROM users_scores u 
                        JOIN questions q ON q.id = u.question_id 
                        LEFT JOIN categories c ON q.categories_id = c.id 
                        LEFT JOIN teams t ON c.teams_id = t.id 
                        LEFT JOIN questions_type qt ON qt.id=q.question_type_id
                      
                      LEFT JOIN ( 
                          SELECT p.id as position, t.id as team 
                          FROM positions p 
                          LEFT JOIN teams t ON p.teams_id = t.id 
                          WHERE p.id = $2
                        ) p ON t.id = p.team 
                        WHERE user_id = $1 AND p.position = $2
                        GROUP BY q.categories_id,u.user_answer,  p.position ,qt.question_type,q.questions,u.is_correct,u.id
                    ) b 
                    ON a.category_id = b.categories_id 
                  WHERE a.catgname <> 'IQ Test' AND a.catgname <> 'Math Test' 
                  AND a.catgname <> 'Psychological Test' AND a.catgname <> 'Word Analogy' 
                  AND (b.position = $2) AND b.question_type ='Essay' OR b.question_type='Problem Solving';`;
    const params = [id, posid];
    return db.query(sql, params);
  }

  //check for question type if essay or problem solving
  async function IQTestResultQTYPE(id) {
    const db = await dbs();
    const sql = `
    SELECT DISTINCT a.user_id,a.firstname,a.lastname,a.catgname,b.question_type,b.id,b.is_correct,b.questions,b.user_answer 
                    FROM ( 
                    SELECT cr.user_id,u.firstname,u.lastname, cr.category_id, c.name 
                        AS catgname 
                        FROM categories_random cr 
                      LEFT JOIN categories c 
                        ON c.id = cr.category_id JOIN users u ON u.id = cr.user_id 
                        WHERE cr.user_id = $1
                    ) a 
                      LEFT JOIN ( 
                      SELECT  q.categories_id, p.position ,qt.question_type,q.questions,u.user_answer,u.is_correct,u.id
                        FROM users_scores u 
                        JOIN questions q ON q.id = u.question_id 
                        LEFT JOIN categories c ON q.categories_id = c.id 
                        LEFT JOIN teams t ON c.teams_id = t.id 
                        LEFT JOIN questions_type qt ON qt.id=q.question_type_id
                      
                      LEFT JOIN ( 
                          SELECT p.id as position, t.id as team 
                          FROM positions p 
                          LEFT JOIN teams t ON p.teams_id = t.id 
                          
                        ) p ON t.id = p.team 
                        WHERE user_id = $1 AND c.name ='IQ Test'
                        GROUP BY q.categories_id,u.user_answer,  p.position ,qt.question_type,q.questions,u.is_correct,u.id
                    ) b 
                    ON a.category_id = b.categories_id 
                  WHERE a.catgname = 'IQ Test'
                  AND b.question_type ='Essay' OR b.question_type='Problem Solving';`
    const params = [id];
    return db.query(sql, params);
  }

  // give score for essay / problem solving
  async function rateAnswer(info) {
    const db = await dbs();
    const sql = `UPDATE users_scores set is_correct=$2 WHERE id=$1; `;
    const params = [info.id, info.is_correct];
    return db.query(sql, params);
  }

  async function findScoreById(id) {
    const db = await dbs();
    const sql = `SELECT * FROM users_scores WHERE id=$1; `;
    const params = [id];
    return db.query(sql, params);
  }

  async function updatePositionToRecieved(id) {
    const db = await dbs();
    const sql = `UPDATE users set "status"='received' WHERE id=$1; `;
    const params = [id];
    return db.query(sql, params);
  }

  async function findUser(id) {
    const db = await dbs();
    const sql = `SELECT id,position_id,status FROM users where id=$1;`;
    const params = [id];
    return db.query(sql, params);
  }
  async function find(id) {
    const db = await dbs();
    const sql = `SELECT * FROM users where id=$1;`;
    const params = [id];
    return db.query(sql, params);
  }

  async function findbyPosition(id) {
    const db = await dbs();
    const sql = `select teams_id from positions WHERE id=$1;`;
    const params = [id];
    return db.query(sql, params);
  }

  //updateResume
  async function updateResume(info) {
    const db = await dbs();
    const sql = `UPDATE  users SET resume_upload_url=$1 where mobile=$2;`;
    const params = [info.resume_upload_url, info.mobile];
    return db.query(sql, params);
  }

  // monitorAnswer in realtime
  async function monitorAnswer(token) {
    const db = await dbs();
    const sql = `SELECT COALESCE(COUNT(*),0) as correct_answers  FROM users_scores WHERE user_id=(SELECT id FROM users WHERE token=$1) and user_answer IS NOT NULL;`;
    const params = [token];
    return db.query(sql, params);
  }

  async function findUserEmail(id) {
    const db = await dbs();
    const sql = `SELECT * FROM users where id=$1; `;
    const params = [id];
    return db.query(sql, params);
  }

  async function updateUserEmail(info) {
    const db = await dbs();
    const sql = `UPDATE users SET email=$2 where id=$1; `;
    const params = [info.id, info.email];
    return db.query(sql, params);
  }

  async function viewAllIQBE() {
    const db = await dbs();
    const sql = `
    SELECT a.id,a.name,a.description,a.teams_id,a.team,b.item FROM 
		( SELECT DISTINCT c.id,c.teams_id,t.name as team, c.name,c.description FROM questions q 
			JOIN categories c ON c.id = q.categories_id
			JOIN teams t ON t.id = c.teams_id
      WHERE c.name ='IQ Test' )a JOIN ( SELECT COUNT(*) item,c.id FROm questions q JOIN categories c ON c.id = q.categories_id 
      WHERE c.name ='IQ Test' GROUP BY c.id )b ON b.id = a.id `;

    return db.query(sql);
  }

  async function findApplicantById(applicantID, deptID, teamID) {
    const db = await dbs(),
      dhFilter = (deptID) ? `AND d.id = ${deptID}` : '',
      thFilter = (teamID) ? `AND t.id = ${teamID}` : '',
      sql = `
            SELECT
              u.id,
              u.firstname,
              u.lastname,
              u.middlename,
              u.status as stage,
              u.livestock,
              u.willing_to_travel,
              p.id as positionid,
              p.name as positionname,
              u.email,
              u.mobile,
              t.name as teamname,
              d.name as departmentname,
              u.date_application,
              adt.online_exam_start,
              adt.online_exam_end,
              adt.date_endorsement,
              adt.date_assessment,
              adt.date_deployed,
              u.resume_upload_date,
              u.resume_upload_url,
              (NOW() - resume_upload_date) as running_time,
              u.token
            FROM
              users u
              LEFT JOIN applicant_date_trackings adt ON u.ID = adt.users_id
              LEFT JOIN positions P ON P.ID = u.position_id
              LEFT JOIN teams t ON t.id = p.teams_id
              LEFT JOIN departments d ON d.id = t.department_id
            WHERE
              p.id IS NOT NULL
              AND u.id = ${applicantID} 
              ${dhFilter} 
              ${thFilter}`
    return db.query(sql)
  }

  async function findApplicantWithoutPositionById(applicantID) {
    let db = await dbs(),
      sql = `
          SELECT
            u.id,
            u.firstname,
            u.lastname,
            u.middlename,
            u.status as stage,
            u.livestock,
            u.willing_to_travel,
            u.email,
            u.mobile,
            u.date_application,
            adt.online_exam_start,
            adt.online_exam_end,
            adt.date_endorsement,
            adt.date_assessment,
            adt.date_deployed,
            u.resume_upload_date,
            u.resume_upload_url,
            (NOW() - resume_upload_date) as running_time
          FROM
            users u
            INNER JOIN applicant_date_trackings adt ON u.ID = adt.users_id
          WHERE
            u.id = ${applicantID}`

    return db.query(sql)
  };
  
  async function addApplicantNotification({ userID, applicantID, comment, actionID }) {
    const db = await dbs(),
          sql = `
          INSERT INTO user_notifications (user_id, action_id, applicant_id, comment, datetime, is_viewed_by_admin, is_viewed_by_pg, is_viewed_by_dh, is_viewed_by_oh, is_viewed_by_th) VALUES ($1, $2, $3, $4, NOW(), 'f', 'f', 'f', 'f', 'f') RETURNING id`,
          params = [userID, actionID, applicantID, comment]
    return db.query(sql, params)
  };

  async function updateNotificationViewedBy({notificationID, userID}) {
    const db = await dbs(),
          sql = `UPDATE user_notifications SET is_viewed_by = CONCAT_WS('-',is_viewed_by, $1::text) where id = $2`,
          params = [userID, notificationID]
    return db.query(sql, params)
  }

  async function getApplicantNotification ({role, deptID, teamID, dateFrom, dateTo}) {
    let filter, dateFilter

    dateFilter = (dateFrom && dateTo) ? ` AND DATE(datetime) BETWEEN '${dateFrom}' AND '${dateTo}'` : ''

    switch (role) {
      case ("Administrator"):
      case ("People Group"):
      case (undefined):
        filter = ``
        break 
      case ("Operations Head"):
      case ("Department Head"):
        filter = ` WHERE d.id = ${deptID}`
        break
      case ("Team Head"):
        filter = ` WHERE t.id = ${teamID}`
        break
    }

    const db = await dbs(),
          sql = `
                  SELECT
                  un.id,
                  un.user_id,
                  DATE(un.datetime) as date,
                  u1.firstname as u_fn,
                  u1.lastname as u_ln,
                  u1.email as "userEmail",
                  u1.mobile as "userMobile",
                  r1.name as "userRole",
                  p1.name as "userPosition",
                  t1.name as "userTeam",
                  d1.name as "userDepartment",
                  un.applicant_id,
                  u2.firstname as app_fn,
                  u2.lastname as "app_ln",
                  u2.email as "applicantEmail",
                  u2.mobile as "applicantMobile",
                  p2.name as "applicantPosition",
                  t2.name as "applicantTeam",
                  d2.name as "applicantDepartment",
                  un.comment,
                  un.action_id,
                  A.action_name as action,
                  un.is_viewed_by_admin,
                  un.is_viewed_by_pg,
                  un.is_viewed_by_oh,
                  un.is_viewed_by_dh,
                  un.is_viewed_by_th,
                  un.is_viewed_by
                FROM
                  "user_notifications" un
                  LEFT JOIN users u1 ON u1.ID = un.user_id
                  LEFT JOIN users u2 ON u2.ID = un.applicant_id
                  LEFT JOIN actions A ON un.action_id = A.ID
                  INNER JOIN roles r1 on r1.id = u1.role_id
                  INNER JOIN positions p1 on u1.position_id = p1.ID
                  INNER JOIN positions p2 ON u2.position_id = p2.ID 
                  INNER JOIN teams t1 ON p1.teams_id = t1.ID
                  INNER JOIN teams t2 ON p2.teams_id = t2.id
                  INNER JOIN departments d1 ON d1.ID = t1.department_id
                  INNER JOIN departments d2 ON d2.ID = t2.department_id
                WHERE un.applicant_id IN (select u.id from users u inner join positions p on p.id = u.position_id inner join teams t on p.teams_id = t.id inner join departments d on d.id = t.department_id ${filter})
                ${dateFilter}
                ORDER BY datetime DESC
                `
    return db.query(sql)
  }

  async function updateApplicantNotification({entryID, role}) {
    let isViewed
    switch(role) {
      case "Administrator":
        isViewed = "is_viewed_by_admin = 't'"
        break
      case "People Group":
        isViewed = "is_viewed_by_pg = 't'"
        break
      case "Department Head":
        isViewed = "is_viewed_by_dh = 't'"
        break
      case "Team Head":
        isViewed = "is_viewed_by_th = 't'"
        break
      case "Operations Head":
        isViewed = "is_viewed_by_oh = 't'"
        break
    }
    const db = await dbs(),
          sql = `
                UPDATE user_notifications
                SET ${isViewed}
                WHERE id = ${entryID}
                  RETURNING id
          `
    return db.query(sql)
  }

  async function passwordVerification(email, password) {
    const db = await dbs()
    let sql = 'SELECT id from users where email = $1 and password = $2',
        params = [email, password]
    return db.query(sql, params)
  }

  async function fetchApplicantsWithoutReferral() {
    const db = await dbs()
    let sql = `select u.id, u.firstname, u.lastname, p.name as position from users u JOIN positions p on p.id = u.position_id  where u.id NOT IN (select applicant_id from referral_forms) order by u.id desc`

    return db.query(sql)
  }

  async function fetchDepartmentTeamPositionBy(userID) {
    const db = await dbs()

    let sql = `
              select d.id as departmentid, d.name as department, p.id as positionid, p.  name as position, t.id as teamid, t.name as team from users u inner join positions p on u.position_id = p.id inner join teams t on t.id = p.teams_id inner join departments d on d.id = t.department_id where u.id = $1`,
        params = [userID]
    return db.query(sql, params)
  }

  async function fetchDepartmentHeadDetailsBy(positionId) {
    const db = await dbs()

    let sql = `
              select u.id, u.email, u.firstname, u.lastname from users u
              inner join positions p on u.position_id = p.id
              inner join teams t on t.id = p.teams_id
              inner join departments d on d.id = t.department_id
              where role_id = 3 and d.id = (select d.id from departments d inner join teams t on d.id = t.department_id inner join positions p on p.teams_id = t.id where p.id = $1)`
        params = [positionId]
    return db.query(sql, params)
  }

  async function fetchPGDetails() {
    const db = await dbs()

    let sql = `
    SELECT ID,
          NAME 
    FROM
      departments 
    WHERE
      NAME LIKE'%PG%' 
      OR NAME LIKE'People Group'
        `
  return db.query(sql)
  }

  async function fetchUsers(input) {

    const db = await dbs()
    let filter = (typeof input === 'number') ? `AND u.id = ${input}` : ` AND u.firstname LIKE '%${input}%' OR u.lastname LIKE '%${input}%'`

    let sql = `
          SELECT
            u.ID,
            u.firstname,
            u.lastname,
            u.middlename,
            u.mobile,
            u.email,
            r.id as "roleID",
            r.NAME AS role,
            p.id as "positionID",
            P.NAME AS position,
            t.id as "teamID",
            T.NAME AS team,
            d.id as "departmentID",
            d.NAME AS department 
          FROM
            users u
          LEFT JOIN positions P ON P.ID = u.position_id
          LEFT JOIN roles r ON r.ID = u.role_id
          INNER JOIN teams T ON T.ID = P.teams_id
          INNER JOIN departments d ON d.ID = T.department_id 
          WHERE
            u.role_id IS NOT NULL
            ${filter}
        `
        
    return db.query(sql)
  }

  async function getApplicantHistory(user_id) {

    const db = await dbs()

    let sql = `
                SELECT * FROM applicant_date_trackings where user_id = $1`,
        params = [user_id]
    
    return db.query(sql, params)
            .catch(err => console.error(err.hint))
  }

  async function checkUserHasIQBE(user_id) {
    const db = await dbs()

    let sql = ` SELECT
                  us.ID 
                FROM
                  users_scores us
                  LEFT JOIN questions q ON q.ID = us.question_id
                  INNER JOIN categories C ON C.ID = q.categories_id
                  INNER JOIN users u ON u.ID = us.user_id 
                WHERE
                  C.NAME LIKE'IQ %' 
                  AND u.ID = $1`

    let params = [user_id]

    return db.query(sql, params)
            .catch(err => console.log(err))
  }

}


module.exports = db
