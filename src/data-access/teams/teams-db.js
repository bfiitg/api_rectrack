const db = ({ dbs }) => {
  return Object.freeze({
    insert,
    findByName,
    findAll,
    changeStatusInactive,
    updates,
    viewOneTeam,
    findAllFilterDept,
    checkIfNameExists
  });
  // check if the name exists in other data when updating
  async function checkIfNameExists(id, name) {
    const db = await dbs();
    const sql = "SELECT * FROM teams WHERE name = $2 AND id <> $1;";
    const params = [id, name];
    return db.query(sql, params);
  }
  // select all team base on department
  async function findAllFilterDept(id) {
    const db = await dbs();
    const sql =
      "SELECT t.id,t.name AS teamname,t.description,d.name AS deptName FROM teams t JOIN departments " +
      "d ON d.id = t.department_id WHERE t.department_id = $1 AND t.status='active' ORDER BY teamname;";
    const params = [id];
    return db.query(sql, params);
  }
  async function viewOneTeam(id) {
    const db = await dbs();
    const sql =
      "SELECT t.*,d.name AS deptname FROM teams t JOIN departments d ON d.id = t.department_id " +
      "WHERE t.status = 'active' AND t.id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  async function insert({ ...info }) {
    const db = await dbs();
    const sql =
      "INSERT INTO teams VALUES (DEFAULT,$1,$2,$3,'active',$4,NOW(),NOW());";
    const params = [
      info.name,
      info.description,
      info.department_id,
      info.no_of_items
    ];
    return db.query(sql, params);
  }
  async function findByName(name) {
    const db = await dbs();
    const sql = "SELECT * FROM teams WHERE name=$1;";
    const params = [name];
    return db.query(sql, params);
  }
  async function findAll() {
    const db = await dbs();
    const sql =
      "SELECT t.id,t.name AS teamName,t.description,d.name AS deptName,  t.status as status  FROM teams t JOIN departments " +
      "d ON d.id = t.department_id " +
      //sort only for all active teams
      //"WHERE t.status='active'" + 
      " ORDER BY teamName;";
    return db.query(sql);
  }
  async function changeStatusInactive(id) {
    const db = await dbs();
    const sql =
      "UPDATE teams SET time_updated = NOW(), status = 'inactive' WHERE id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  async function updates({ id, ...info }) {
    const db = await dbs();
    const sql =
      "UPDATE teams SET name = $2, description = $3, time_updated = NOW(), " +
      "department_id = $4, status = $5, no_of_items = $6 WHERE id = $1;";
    const params = [
      id,
      info.name,
      info.description,
      info.department_id,
      info.status,
      info.no_of_items
    ];
    return db.query(sql, params);
  }
};

module.exports = db;
