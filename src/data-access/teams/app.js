const makeDb = require("../db");
const db = require("./teams-db");

const teamsDb = makeDb({ db });

module.exports = teamsDb;
