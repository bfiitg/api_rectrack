const db = ({dbs}) => {
    return Object.freeze({
        findAllModules,
        findAllModulesActive,
        findModuleByDescription,
        findModuleById,
        addModule,
        updateModule,
        checkModuleUpdate
    });

    //find all models
    async function findAllModules(){
        const db = await dbs();
        const sql = "SELECT * FROM modules;";
        return db.query(sql);
    }

    //find all models active
    async function findAllModulesActive(){
        const db = await dbs();
        const sql = `SELECT * FROM modules WHERE "status"='active';`;
        return db.query(sql);
    }
    //find module by description
    async function findModuleByDescription(module_name){
        const db = await dbs();
        const sql = `SELECT * FROM modules WHERE module_name=$1;`;
        const params = [module_name];
        return db.query(sql, params)
    }

      //find module by description with statsu
      async function checkModuleUpdate(info){
        const db = await dbs();
        const sql = `SELECT * FROM modules WHERE module_name=$1 AND status=$2;`;
        const params = [info.module_name, info.status];
        return db.query(sql, params)
    }
    //find module by description
    async function findModuleById(id){
        const db = await dbs();
        const sql = `SELECT * FROM modules WHERE id=$1;`;
        const params = [id];
        return db.query(sql, params)
    }
    //add module
    async function addModule(info){
        const db = await dbs();
        const sql = `INSERT INTO modules(id, module_name, status, time_created, time_updated) VALUES (DEFAULT,$1,$2,NOW(),NOW());`;
        const params = [info.module_name, info.status];
        return db.query(sql, params)
    }
    //update module
    async function updateModule(id,info){
        const db = await dbs();
        const sql = `UPDATE modules SET module_name = $2, time_updated = NOW(), `+
                    `status = $3 WHERE id = $1;`;
        const params = [id,info.module_name, info.status];
        return db.query(sql, params)
    }

};
module.exports = db;