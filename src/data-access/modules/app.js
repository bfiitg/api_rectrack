const makeDb = require('../db');
const db = require('./modules-db');

const moduleDb = makeDb({ db });

module.exports = moduleDb;