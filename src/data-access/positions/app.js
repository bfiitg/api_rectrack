const makeDb = require("../db");
const db = require("./positions-db");

const positionsDb = makeDb({ db });

module.exports = positionsDb;
