const db = ({ dbs }) => {
  return Object.freeze({
    insert,
    findByName,
    findAllVacant,
    findAll,
    toNotVacant,
    toVacant,
    findAllNotVacant,
    findOne,
    updating,
    findByNameUpdate,
    findAllPositionFilterByDept
  });
  async function findAllPositionFilterByDept(id) {
    const db = await dbs();
    const sql =
      "SELECT p.id,p.name FROM positions p JOIN teams t ON t.id = p.teams_id " +
      "JOIN departments d ON d.id = t.department_id WHERE d.id = $1 AND p.status = 'active';";
    const params = [id];
    return db.query(sql, params);
  }
  async function findByNameUpdate(id, name) {
    const db = await dbs();
    const sql = "SELECT * FROM positions WHERE name=$1 AND id <> $2;";
    const params = [name, id];
    return db.query(sql, params);
  }
  async function updating({ id, ...info }) {
    const db = await dbs();
    const sql =
      "UPDATE positions SET name = $1, description = $2, time_updated = NOW(), " +
      "teams_id = $3, is_vacant = $5, is_office_staff = $4, status=$6 WHERE id = $7;";
    const params = [
      info.name,
      info.description,
      info.teams_id,
      info.is_office_staff,
      info.is_vacant,
      info.status,
      id
    ];
    return db.query(sql, params);
  }
  async function findOne(id) {
    const db = await dbs();
    const sql =
      "SELECT p.*,t.name AS teamname FROM positions p JOIN teams t ON t.id = p.teams_id " +
      "WHERE p.id = $1 AND p.status = 'active';";
    const params = [id];
    return db.query(sql, params);
  }
  async function insert({ ...info }) {
    const db = await dbs();
    const sql =
      "INSERT INTO positions(id, name, description, teams_id, status, is_vacant, is_office_staff, time_created, time_updated) VALUES (DEFAULT,$1,$2,$3,'active',$5,$4,NOW(),NOW());";
    const params = [
      info.name,
      info.description,
      info.teams_id,
      info.is_office_staff,
      info.is_vacant
    ];
    return db.query(sql, params);
  }
  async function findByName(name) {
    const db = await dbs();
    const sql = "SELECT * FROM positions WHERE name=$1;";
    const params = [name];
    return db.query(sql, params);
  }
  async function findAllVacant() {
    const db = await dbs();
    const sql =
      "SELECT p.id,p.name AS positionName,p.description AS positionDesc,t.name AS teamName,d.name AS deptName " +
      "FROM positions p JOIN teams t ON t.id = p.teams_id JOIN departments d " +
      "ON d.id = t.department_id WHERE p.is_vacant ='t' AND p.status = 'active';";
    return db.query(sql);
  }
  async function findAll() {
    const db = await dbs();
    const sql =
      "SELECT p.id,p.name AS positionName,p.description,t.name AS teamName,d.name AS deptName ,p.is_vacant FROM positions p " +
      "JOIN teams t ON t.id = p.teams_id JOIN departments d ON d.id = t.department_id " +
      "WHERE p.status = 'active' ORDER BY positionName;";
    return db.query(sql);
  }
  async function toNotVacant(id) {
    const db = await dbs();
    const sql = "UPDATE positions SET is_vacant = 'f' WHERE id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  async function toVacant(id) {
    const db = await dbs();
    const sql = "UPDATE positions SET is_vacant = 't' WHERE id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  async function findAllNotVacant() {
    const db = await dbs();
    const sql =
      "SELECT p.id,p.name AS positionName,p.description AS positionDesc,t.name AS teamName,d.name AS deptName " +
      "FROM positions p JOIN teams t ON t.id = p.teams_id JOIN departments d " +
      "ON d.id = t.department_id WHERE p.is_vacant ='f' AND p.status='active';";
    return db.query(sql);
  }
};

module.exports = db;
