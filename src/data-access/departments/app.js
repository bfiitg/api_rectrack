const makeDb = require("../db");
const db = require("./departments-db");

const departmentDb = makeDb({ db });

module.exports = departmentDb;
