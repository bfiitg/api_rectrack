const db = ({ dbs }) => {
  return Object.freeze({
    insert,
    findByName,
    findAll,
    changeStatusInactive,
    updates,
    findOne,
    checkIfNameExists
  });
  // check if the name exists in other data when updating
  async function checkIfNameExists(id, name) {
    const db = await dbs();
    const sql = "SELECT * FROM departments WHERE name = $2 AND id <> $1;";
    const params = [id, name];
    return db.query(sql, params);
  }
  async function findOne(id) {
    const db = await dbs();
    const sql =
      "SELECT d.id,d.name,d.description,d.status,u.id AS userid,u.firstname,u.lastname " +
      "FROM departments d LEFT JOIN users u ON u.id = d.user_id WHERE d.status = 'active' AND d.id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  async function insert({ ...info }) {
    const db = await dbs();
    const sql =
      "INSERT INTO departments(id, name, description, user_id, status, time_created, time_updated) VALUES (DEFAULT,$1,$2,$3,'active',NOW(),NOW());";
    const params = [info.name, info.description, info.user_id];
    return db.query(sql, params);
  }
  async function findByName(name) {
    const db = await dbs();
    const sql = "SELECT * FROM departments WHERE name=$1;";
    const params = [name];
    return db.query(sql, params);
  }
  async function findAll() {
    const db = await dbs();
    const sql =
      "SELECT d.id,d.name,d.description,d.status,u.id AS userid,u.firstname,u.lastname,d.status as status " +
      "FROM departments d LEFT JOIN users u ON u.id = d.user_id "
      //select either status is active or inactive
      //+"WHERE d.status = 'active';";
    return db.query(sql);
  }
  async function changeStatusInactive(id) {
    const db = await dbs();
    const sql =
      "UPDATE departments SET time_updated = NOW(), status = 'inactive' WHERE id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  async function updates({ id, ...info }) {
    const db = await dbs();
    const sql =
      "UPDATE departments SET name = $2, description = $3, time_updated = NOW(), " +
      "user_id = $4, status = $5 WHERE id = $1;";
    const params = [id, info.name, info.description, info.user_id, info.status];
    return db.query(sql, params);
  }
};

module.exports = db;
