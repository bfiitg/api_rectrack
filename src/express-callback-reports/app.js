const zip = require("express-zip");
const fs = require("fs");

const makeExpressCallbackReportDownload = controller => {
  return (req, res) => {
    const httpRequest = {
      body: req.body,
      query: req.query,
      params: req.params,
      ip: req.ip,
      method: req.method,
      path: req.path,
      headers: {
        "Content-Type": req.get("Content-Type"),
        Referer: req.get("referer"),
        "User-Agent": req.get("User-Agent"),
        "Access-Control-Allow-Origin": "*"
      }
    };
    controller(httpRequest)
      .then(httpResponse => {
        if (httpResponse.headers) {
          res.set("Access-Control-Allow-Origin", "*");
          res.set("Accept-Ranges", "bytes");
          res.set(httpResponse.headers);
        }
        // download EmployeeAddresses-2019-07-23 10.4
        // const fileExt = fileExtension();
        const fileExt = fileExtension(); // real time
        const fileExts = fileExtensions(); // one minute late
        // EmployeeAddresses
        const paths = `./reports/assessment-${fileExt}-report.xls`;
        fs.access(paths, fs.F_OK, err => {
          if (err) {
            // file doesn't exist
            // export the one minute late
            res.zip(
              [
                {
                  path: `./reports/assessment-${fileExts}-report.xls`,
                  name: "Assessment.xls"
                },
                {
                  path: `./reports/endorsement-${fileExts}-report.xls`,
                  name: "Endorsement.xls"
                },
                {
                  path: `./reports/joboffer-${fileExts}-report.xls`,
                  name: "Job-Offer.xls"
                },
                {
                  path: `./reports/online-exam-${fileExts}-report.xls`,
                  name: "Online-Exam.xls"
                },
                {
                  path: `./reports/pending-${fileExts}-report.xls`,
                  name: "Pending.xls"
                },
                {
                  path: `./reports/quick-applicants-${fileExts}-report.xls`,
                  name: "Quick-Applicant.xls"
                },
                {
                  path: `./reports/received-${fileExts}-report.xls`,
                  name: "Received.xls"
                },
                {
                  path: `./reports/shortlist-${fileExts}-report.xls`,
                  name: "Shortlist.xls"
                }
              ],
              `reports-${fileExts}.zip`
            );
          }
          // file exist
          // export real time
          res.zip(
            [
              {
                path: `./reports/assessment-${fileExts}-report.xls`,
                name: "Assessment.xls"
              },
              {
                path: `./reports/endorsement-${fileExts}-report.xls`,
                name: "Endorsement.xls"
              },
              {
                path: `./reports/joboffer-${fileExts}-report.xls`,
                name: "Job-Offer.xls"
              },
              {
                path: `./reports/online-exam-${fileExts}-report.xls`,
                name: "Online-Exam.xls"
              },
              {
                path: `./reports/pending-${fileExts}-report.xls`,
                name: "Pending.xls"
              },
              {
                path: `./reports/quick-applicants-${fileExts}-report.xls`,
                name: "Quick-Applicant.xls"
              },
              {
                path: `./reports/received-${fileExts}-report.xls`,
                name: "Received.xls"
              },
              {
                path: `./reports/shortlist-${fileExts}-report.xls`,
                name: "Shortlist.xls"
              }
            ],
            `reports-${fileExt}.zip`
          );
        });
      })
      .catch(e => res.sendStatus(500));
  };
};

// real time
const fileExtension = () => {
  const date_ob = new Date();
  // current date
  const date = ("0" + date_ob.getDate()).slice(-2);

  // current month
  const month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

  // current year
  const year = date_ob.getFullYear();

  // current hours
  const hours = date_ob.getHours();

  // current minutes
  const minutes = date_ob.getMinutes();

  const fileExt = year + "-" + month + "-" + date;
  return fileExt;
};

// less one minute
const fileExtensions = () => {
  const date_ob = new Date();
  // current date
  const date = ("0" + date_ob.getDate()).slice(-2);

  // current month
  const month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

  // current year
  const year = date_ob.getFullYear();

  // current hours
  const hours = date_ob.getHours();

  // current minutes
  const minute = date_ob.getMinutes();
  const minutes = minute - 1; // less one minute here

  const fileExt = year + "-" + month + "-" + date;
  return fileExt;
};

module.exports = makeExpressCallbackReportDownload;
