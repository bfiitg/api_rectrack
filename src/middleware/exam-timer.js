const userDb = require('../../src/data-access/users/app')

const socketExamTimer = ( io ) => {
   io.on('connection', socket => {

      let applicantID

      socket.on('updateOnlineExamElapsedTime', async(data) => {
         const { token, time } = data[0]
         let remainingTime
         
         await userDb.elapsedTimes(time, token)
                     .then(res => {
                        applicantID = res.rows[0].id
                        remainingTime = res.rows[0].remaining_time
                     })
         
         let result = {
            applicantID: applicantID,
            remainingTime: remainingTime
         }
         
         io.emit('updateOnlineExamElapsedTime', result) 
      })


   })
}

module.exports = socketExamTimer