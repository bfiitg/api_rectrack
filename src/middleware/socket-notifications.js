const userDb = require('../../src/data-access/users/app'),
      actionsDb = require('../../src/data-access/actions/app'),
      { decrypt } = require('../../src/entities/users/app')

const socketNotification = ( io ) => {
   io.on('connection', socket => {
      
      socket.on('fetchNotifications', async(data) => {
         try {
            if (data[0]) {
               const {role, deptID, teamID} = data[0]
               let results, listOfNotifs = []
         
               await userDb.getApplicantNotification({role, deptID, teamID})
                              .then(res => {
                                 results = res.rows
                              })

               if (results) {
                  for (let result of results) {
                     let uFirstName = decrypt(result.u_fn),
                     uLastName = decrypt(result.u_ln),
                     appFirstName = decrypt(result.app_fn),
                     appLastName = decrypt(result.app_ln)
                  
                     listOfNotifs.push({
                        notificationID: result.id,
                        date: result.date,
                        userID: result.user_id,
                        user: `${uFirstName} ${uLastName}`,
                        userEmail : decrypt(result.userEmail),
                        userMobile: result.userMobile,
                        userPosition: result.userPosition,
                        userTeam: result.userTeam,
                        userDepartment: result.userDepartment,
                        action: result.action,
                        desc: result.comment,
                        applicantID: result.applicant_id,
                        applicant: `${appFirstName} ${appLastName}`,
                        applicantEmail: decrypt(result.applicantEmail),
                        applicantMobile: result.applicantMobile,
                        applicantPosition: result.applicantPosition,
                        applicantTeam: result.applicantTeam,
                        applicantDepartment: result.applicantDepartment,
                        isViewedBy: result.is_viewed_by
                     })
                  }
               }
            
            io.emit("fetchNotifications", listOfNotifs)
            }

         } catch (error) {
            
            console.log("Socket notification error: ", error)
            
         }
         
      })

      
      socket.on('notificationAlerts', async(data) => {
         try {

            let actionCheck = await actionsDb.select(data[0].actionID)

            if (actionCheck.rowCount === 0) {
               throw new Error("Please enter a valid action id.")
            }
            
            let role = data[0].user.role,
               prompt,
               info = {
               userID: data[0].user.id,
               applicantID: data[0].applicant.id,
               comment: data[0].desc,
               actionID: data[0].actionID
            }

            await userDb.addApplicantNotification(info)
                        .then(res => {
                           prompt = (res.rowCount > 0) ? 'Sucess' : 'Fail'
                        })

            io.emit('notificationAlerts', {prompt, data})

         } catch (error) {
            console.log(error)
         }
         
      })

      socket.on('updateNotification', async(data) => {
         try {
            const { notificationID, userID } = data[0]

            if (!userID) {
               throw new Error("Please provide a user id.")
            }
            
            let prompt

            await userDb.updateNotificationViewedBy({notificationID, userID})
                        .then(res => {
                           prompt = (res.rowCount > 0) ? 'Sucessfully updated notification' : 'Failed to update notification'
                        })

            io.emit('updateNotification', prompt)

         } catch (error) {
            console.log(error)
         }
         
      })
   })
}  

module.exports = socketNotification