module.exports = {
  validateIP(req, res, next) {
    let clientIP = req.header('origin');

    const whiteList = [
      "https://rectrack-dev.jltechsol.com",
      "rectrack-dev.jltechsol.com",
      "https://forms-dev.jltechsol.com",
      "forms-dev.jltechsol.com",
      "https://rectrack.biotechfarms.net",
      "https://forms.biotechfarms.net",
      "http://rectrack.biotechfarms.com",
      "http://forms.biotechfarms.com",
      "http://rectrack.biotechfarms.com:5001",
      "localhost:5090",
      "http://127.0.0.1:8080",
      "172.16.4.182:1000",
      "172.16.4.182:2000",
      "172.16.4.182:1010",
      "172.16.4.182:2010",
      "http://172.16.4.182:2011",
      "172.16.4.201:5091",
      "172.16.4.201:5092",
    ];

    if (!whiteList.includes(clientIP)) {
      return res.status(403).send(clientIP);
    }

    next();
  },
};
