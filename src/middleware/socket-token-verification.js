const userDb = require('../data-access/users/app')

const socketTokenVerification = ( io ) => {
   io.on('connection', socket => {
      try {
         
      socket.on('verifyToken', async(data) => {
         const { token } = data[0]
         let validToken
         
         await userDb.checkToken(token)
                     .then(res => {
                        validToken = (res.rowCount > 0) ? true : false
                     })

         io.emit("verifyToken", validToken)
      })
      } catch (error) {
         console.log(error)
      }
   })
}

module.exports = socketTokenVerification