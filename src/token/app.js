const jwt = require("jsonwebtoken");
require("dotenv").config();
const userDb = require("../data-access/users/app");

// generate token for users
const makeToken = async (email, password) => {
  const user = {
    email: email,
    password: password,
  };
  // to do

  const tokens = await new Promise((resolve) => {
    jwt.sign(
      { user },
      process.env.ENCRYPTION_KEY /*, { expiresIn: "30s" }*/,
      (err, token) => {
        if (err) console.log(`Error on making token: `, err);
        resolve(token);
      }
    );
  });

  return tokens;
};

// get token from header
const getToken = async (req, res, next) => {
  const bearerHeader = req.headers["authorization"];

  if (typeof bearerHeader !== "undefined") {
    // Base on the format must split the string
    // to get the token; split in space
    const bearer = bearerHeader.split(" ");

    // at index 0 is the "Bearer" word
    // at index 1 is the token
    const bearerToken = bearer[1];

    req.token = bearerToken;
    // next function; just like that
    const userData = await getUserDataThruToken(req.token);
    if (userData) {
      const queryToken = await userDb.getTokenOfUser(
        userData.user.email,
        userData.user.password
      );
      if (queryToken.rowCount == 0) return res.sendStatus(403);
      const dbToken = queryToken.rows[0].token;

      if (dbToken === req.token) {
        jwt.verify(req.token, process.env.ENCRYPTION_KEY, (err, authData) => {
          if (err) {
            return res.sendStatus(403);
          } else {
            next();
          }
        });
      } else {
        return res.sendStatus(403);
      }
    } else {
      return res.sendStatus(403);
    }
  } else {
    //forbidden
    return res.sendStatus(403);
  }
};

//
const paramToken = async (req, res, next) => {
  const queryToken = req.query.token;
  if (typeof queryToken !== "undefined") {
    req.token = queryToken;
    // next function; just like that
    const userData = await getUserDataThruToken(req.token);
    if (userData) {
      const queryToken = await userDb.getTokenOfUser(
        userData.user.email,
        userData.user.password
      );
      const dbToken = queryToken.rows[0].token;
      if (dbToken === req.token) {
        jwt.verify(req.token, process.env.ENCRYPTION_KEY, (err, authData) => {
          if (err) {
            res.sendStatus(403);
          } else {
            next();
          }
        });
      } else {
        res.sendStatus(403);
      }
    } else {
      res.sendStatus(403);
    }
  } else {
    //forbidden
    res.sendStatus(403);
  }
};

const getUserDataThruToken = async (token) => {
  let authData = "";
  await jwt.verify(token, process.env.ENCRYPTION_KEY, (err, authorizedData) => {
    if (!err) {
      authData = authorizedData;
    }
  });
  return authData;
};

module.exports = { paramToken, makeToken, getToken, getUserDataThruToken };
