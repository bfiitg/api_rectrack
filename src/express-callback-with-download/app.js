const zip = require("express-zip");
const fs = require("fs");

const makeExpressCallbackDownload = controller => {
  return (req, res) => {
    const httpRequest = {
      body: req.body,
      query: req.query,
      params: req.params,
      ip: req.ip,
      method: req.method,
      path: req.path,
      headers: {
        "Content-Type": req.get("Content-Type"),
        Referer: req.get("referer"),
        "User-Agent": req.get("User-Agent"),
        "Access-Control-Allow-Origin": "*"
      }
    };
    controller(httpRequest)
      .then(httpResponse => {
        if (httpResponse.headers) {
          res.set("Access-Control-Allow-Origin", "*");
          res.set("Accept-Ranges", "bytes");
          res.set(httpResponse.headers);
        }
        // download EmployeeAddresses-2019-07-23 10.4
        // const fileExt = fileExtension();
        const fileExt = fileExtension(); // real time
        const fileExts = fileExtensions(); // one minute late
        // EmployeeAddresses
        const paths = `./jeonsoft/EmployeeAddresses-${fileExt}.csv`;
        fs.access(paths, fs.F_OK, err => {
          if (err) {
            // file doesn't exist
            // export the one minute late
            res.zip(
              [
                {
                  path: `./jeonsoft/EmployeeAddresses-${fileExts}.csv`,
                  name: "EmployeeAddresses.csv"
                },
                {
                  path: `./jeonsoft/EmployeeCertifications-${fileExts}.csv`,
                  name: "EmployeeCertifications.csv"
                },
                {
                  path: `./jeonsoft/EmployeeEducation-${fileExts}.csv`,
                  name: "EmployeeEducation.csv"
                },
                {
                  path: `./jeonsoft/EmployeeEmergencyContact-${fileExts}.csv`,
                  name: "EmployeeEmergencyContact.csv"
                },
                {
                  path: `./jeonsoft/EmployeeFamilyBackground-${fileExts}.csv`,
                  name: "EmployeeFamilyBackground.csv"
                },
                {
                  path: `./jeonsoft/EmployeeMobileNumbers-${fileExts}.csv`,
                  name: "EmployeeMobileNumbers.csv"
                },
                {
                  path: `./jeonsoft/EmployeePreviousEmployer-${fileExts}.csv`,
                  name: "EmployeePreviousEmployer.csv"
                },
                {
                  path: `./jeonsoft/Employees-${fileExts}.csv`,
                  name: "Employees.csv"
                },
                {
                  path: `./jeonsoft/EmployeeWorkExperience-${fileExts}.csv`,
                  name: "EmployeeWorkExperience.csv"
                }
              ],
              `jeonsoft-${fileExts}.zip`
            );
          }
          // file exist
          // export real time
          res.zip(
            [
              {
                path: `./jeonsoft/EmployeeAddresses-${fileExt}.csv`,
                name: "EmployeeAddresses.csv"
              },
              {
                path: `./jeonsoft/EmployeeCertifications-${fileExt}.csv`,
                name: "EmployeeCertifications.csv"
              },
              {
                path: `./jeonsoft/EmployeeEducation-${fileExt}.csv`,
                name: "EmployeeEducation.csv"
              },
              {
                path: `./jeonsoft/EmployeeEmergencyContact-${fileExt}.csv`,
                name: "EmployeeEmergencyContact.csv"
              },
              {
                path: `./jeonsoft/EmployeeFamilyBackground-${fileExt}.csv`,
                name: "EmployeeFamilyBackground.csv"
              },
              {
                path: `./jeonsoft/EmployeeMobileNumbers-${fileExt}.csv`,
                name: "EmployeeMobileNumbers.csv"
              },
              {
                path: `./jeonsoft/EmployeePreviousEmployer-${fileExt}.csv`,
                name: "EmployeePreviousEmployer.csv"
              },
              {
                path: `./jeonsoft/Employees-${fileExt}.csv`,
                name: "Employees.csv"
              },
              {
                path: `./jeonsoft/EmployeeWorkExperience-${fileExt}.csv`,
                name: "EmployeeWorkExperience.csv"
              }
            ],
            `jeonsoft-${fileExt}.zip`
          );
        });
      })
      .catch(e => res.sendStatus(500));
  };
};

// real time
const fileExtension = () => {
  const date_ob = new Date();
  // current date
  const date = ("0" + date_ob.getDate()).slice(-2);

  // current month
  const month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

  // current year
  const year = date_ob.getFullYear();

  // current hours
  const hours = date_ob.getHours();

  // current minutes
  const minutes = date_ob.getMinutes();

  const fileExt = year + "-" + month + "-" + date + " " + hours + "." + minutes;
  return fileExt;
};

// less one minute
const fileExtensions = () => {
  const date_ob = new Date();
  // current date
  const date = ("0" + date_ob.getDate()).slice(-2);

  // current month
  const month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

  // current year
  const year = date_ob.getFullYear();

  // current hours
  const hours = date_ob.getHours();

  // current minutes
  const minute = date_ob.getMinutes();
  const minutes = minute - 1; // less one minute here

  const fileExt = year + "-" + month + "-" + date + " " + hours + "." + minutes;
  return fileExt;
};

module.exports = makeExpressCallbackDownload;
