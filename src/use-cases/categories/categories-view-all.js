const categoriesSelectAll = ({ categoriesDb }) => {
  return async function selects(id, roleId) {
    if (!id || roleId == 1) {
      const result = await categoriesDb.selectAll();
      return result.rows;
    } else {
      const result = await categoriesDb.selectByDept(id);
      return result.rows;
    }
  };
};

module.exports = categoriesSelectAll;
