const makeCategory = require("../../entities/categories/app");

const makeAddCategory = ({ categoriesDb }) => {
  return async function posts(info) {
    const result = makeCategory(info);
    const categoryExists = await categoriesDb.findByName(result.getName());

    const rows = categoryExists.rows;

    if (categoryExists.rowCount !== 0) {
      const result = {
        msg: "Category already exists!",
        command: categoryExists.command,
        rows
      };
      return result;
    }

    
    return categoriesDb.insert({
      name: result.getName(),
      description: result.getDescription(),
      teams_id: result.getTeam(),
      time_limit_on_seconds: result.getTimeLimit()
    });
  };
};

module.exports = makeAddCategory;
