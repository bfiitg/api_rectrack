const categorySelectAllFilterTeam = ({ categoriesDb }) => {
  return async function selects(id) {
    const result = await categoriesDb.selectAllFilterTeam(id);
    return result.rows;
  };
};

module.exports = categorySelectAllFilterTeam;
