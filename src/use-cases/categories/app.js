const categoriesDb = require("../../data-access/categories/app");

const categorySelectOne = require("./categories-view-one");
const makeAddCategory = require("./categories-add");
const categoriesSelectAll = require("./categories-view-all");
const updateCategory = require("./categories-update");
const categorySelectAllFilterTeam = require("./categories-view-filter-team");

const categorySelectAllFilterTeams = categorySelectAllFilterTeam({
  categoriesDb
});
const categorySelectOnes = categorySelectOne({ categoriesDb });
const addCategory = makeAddCategory({ categoriesDb });
const categoriesSelectAlls = categoriesSelectAll({ categoriesDb });
const updateCategorys = updateCategory({ categoriesDb });

const services = Object.freeze({
  addCategory,
  categoriesSelectAlls,
  categorySelectOnes,
  updateCategorys,
  categorySelectAllFilterTeams
});

module.exports = services;
module.exports = {
  addCategory,
  categoriesSelectAlls,
  categorySelectOnes,
  updateCategorys,
  categorySelectAllFilterTeams
};
