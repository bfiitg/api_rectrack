const categorySelectOne = ({ categoriesDb }) => {
  return async function selects(id) {
    const result = await categoriesDb.selectOne(id);
    return result.rows;
  };
};

module.exports = categorySelectOne;
