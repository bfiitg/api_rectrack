const makeCategory = require("../../entities/categories/app");

const updateCategory = ({ categoriesDb }) => {
  return async function put({ id, ...info } = {}) {
    const result = makeCategory(info);
    const categoryExists = await categoriesDb.findByNameUpdate(
      result.getName(),
      id
    );
    const rows = categoryExists.rows;
    if (categoryExists.rowCount !== 0) {
      throw new Error("Category already exists!");
    }
    //
    return categoriesDb.updating({
      id: id,
      name: result.getName(),
      description: result.getDescription(),
      teams_id: result.getTeam(),
      time_limit_on_seconds: result.getTimeLimit(),
      status: result.getStatus()
    });
  };
};

module.exports = updateCategory;
