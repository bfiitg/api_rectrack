const teamsSelectAll = ({ teamsDb }) => {
    return async function selects() {
      const result = await teamsDb.findAll();
      return result.rows;
    };
  };
  
  module.exports = teamsSelectAll;
  