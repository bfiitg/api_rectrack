const makeTeam = require("../../entities/teams/app");

const adminEditTeam = ({ teamsDb }) => {
  return async function put({ id, ...info } = {}) {
    const result = makeTeam(info);
    //
    const teamName = result.getName();
    const count = await teamsDb.checkIfNameExists(id, teamName);
    const dataCount = count.rowCount;
    
    if (dataCount > 0) {
      // exists
      throw new Error("Team name already exists.");
    } else {
      await teamsDb.updates({
        id: id,
        name: result.getName(),
        description: result.getDescription(),
        department_id: result.getDepartment(),
        status: result.getStatus(),
        no_of_items: result.getNoItems()
      });
      const data = {
        msg: "Updated successfully"
      };
      return data;
    }
  };
};

module.exports = adminEditTeam;
