const teamChangeStatusInactive = ({ teamsDb }) => {
    return async function puts({ id } = {}) {
      const updated = await teamsDb.changeStatusInactive(id);
      return { ...updated };
    };
  };
  
  module.exports = teamChangeStatusInactive;
  