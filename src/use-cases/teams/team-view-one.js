const teamSelectOne = ({ teamsDb }) => {
  return async function selects(id) {
    const result = await teamsDb.viewOneTeam(id);
    return result.rows;
  };
};

module.exports = teamSelectOne;
