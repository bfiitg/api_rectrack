const makeTeam = require("../../entities/teams/app");

const makeAddTeams = ({ teamsDb }) => {
  return async function posts(info) {
    const result = makeTeam(info);
    const teamExists = await teamsDb.findByName(result.getName());
    const rows = teamExists.rows;
    if (teamExists.rowCount !== 0) {
      const result = {
        msg: "Team already exists!",
        command: teamExists.command,
        rows
      };
      return result;
    }
    //
    return teamsDb.insert({
      name: result.getName(),
      description: result.getDescription(),
      department_id: result.getDepartment(),
      no_of_items: result.getNoItems()
    });
  };
};

module.exports = makeAddTeams;
