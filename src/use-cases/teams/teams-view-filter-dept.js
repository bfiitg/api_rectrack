const teamsSelectAllFilterDept = ({ teamsDb }) => {
  return async function selects(id) {
    const result = await teamsDb.findAllFilterDept(id);
    return result.rows;
  };
};

module.exports = teamsSelectAllFilterDept;
