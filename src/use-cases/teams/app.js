const teamsDb = require("../../data-access/teams/app");

const makeAddTeams = require("./teams-add");
const teamsSelectAll = require("./teams-view-all");
const teamChangeStatusInactive = require("./teams-change-status-inactive");
const adminEditTeam = require("./teams-edit");
const teamSelectOne = require("./team-view-one");
const teamsSelectAllFilterDept = require("./teams-view-filter-dept");

const teamsSelectAllFilterDepts = teamsSelectAllFilterDept({ teamsDb });
const teamSelectOnes = teamSelectOne({ teamsDb });
const addTeams = makeAddTeams({ teamsDb });
const teamsSelectAlls = teamsSelectAll({ teamsDb });
const teamChangeStatusInactives = teamChangeStatusInactive({ teamsDb });
const adminEditTeams = adminEditTeam({ teamsDb });

const services = Object.freeze({
  addTeams,
  teamsSelectAlls,
  teamChangeStatusInactives,
  adminEditTeams,
  teamSelectOnes,
  teamsSelectAllFilterDepts
});

module.exports = services;
module.exports = {
  addTeams,
  teamsSelectAlls,
  teamChangeStatusInactives,
  adminEditTeams,
  teamSelectOnes,
  teamsSelectAllFilterDepts
};
