const userViewQuestion = ({ questionsDb }) => {
  return async function get(id) {
    const questions = await questionsDb.selectSingleQuestion(id);
    const result = questions.rows;
    const results = [];

    for (let i = 0; i < result.length; i++) {
      const choices = result[i].choices_or_imagepath;
      // split @ used as identifier
      // to separate from img path

      // if there is choices
      if (choices) {
        // if (split[0] === "") {
        //   ch = split[1];
        // }
        if (i === 0) {
          results.push({
            id: result[i].id,
            catgId: result[i].categories_id,
            questions: result[i].questions,
            correct_answer: result[i].correct_answer,
            question_type_id: result[i].question_type_id,
            details: [
              {
                choice: choices
              }
            ]
          });
        } else {
          if (result[i].id !== result[i - 1].id) {
            results.push({
              id: result[i].id,
              questions: result[i].questions,
              correct_answer: result[i].correct_answer,
              details: [
                {
                  choice: choices
                }
              ]
            });
          } else {
            const x = results.length - 1;
            results[x].details.push({
              choice: choices
            });
          }
        }
      } else {
        results.push({
          id: result[i].id,
          catgId: result[i].categories_id,
          questions: result[i].questions,
          question_type_id: result[i].question_type_id,
        });
      }
    }
    return results;
   
  };
};

module.exports = userViewQuestion;
