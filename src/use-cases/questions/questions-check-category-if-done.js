const checkUsersCategoryIfDone = ({ questionsDb }) => {
  return async function selects(categoryId) {
    const done = await questionsDb.checkifCategoryIsDone(categoryId);
    const isDone = done.rows[0].is_done;
    return isDone;
  };
};

module.exports = checkUsersCategoryIfDone;
