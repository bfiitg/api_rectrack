const { makeQuestionEssayEntities } = require("../../entities/questions/app");

const makeAddQuestionsEssay = ({ questionsDb }) => {
  return async function posts(info) {
   
    const result = makeQuestionEssayEntities(info)
    //    
    
    return questionsDb.insertQuestionEssay({
      questions: result.getQuestion(),
      categories_id: result.getCategories(),
      question_type_id: result.getQuestionType(),
    });

  };
};

module.exports = makeAddQuestionsEssay;
