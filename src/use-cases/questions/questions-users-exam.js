const selectUsersRandomQuestions = ({ questionsDb,userDb }) => {
  return async function selects(categoryId, token) {
    const results = [];
    // --------

    const result = await questionsDb.displayUsersRandomQuestions(
      categoryId,
      token
    );
    const monitor = await userDb.monitorAnswer(token)
    const monitoring=monitor.rows[0].coalesce

    for (let i = 0; i < result.rows.length; i++) {

      const questionId = result.rows[i].question_id;
      const question = result.rows[i].questions;  
      const correctAnswer = result.rows[i].correct_answer;
      const question_type = result.rows[i].question_type;
      const choices = await questionsDb.displayUsersRandomQuestionsChoices(
        questionId
      );
      results.push({
        questionId: questionId,
        question: question,
        correctAnswer: correctAnswer,
        question_type:question_type,
        choices: choices.rows,
        monitor:monitoring
      });
    }
    return results;
  };
};

module.exports = selectUsersRandomQuestions;
