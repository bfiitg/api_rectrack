const { toLowerCased, decrypt } = require("../../entities/users/app");
const { getUserDataThruToken } = require("../../token/app");

const selectUsersRandomQuestionsIQBE = ({ questionsDb }) => {
  return async function selects(categoryId, token) {
    // get auth data thru token
    const user = await getUserDataThruToken(token);
    const userDetails = {
      email: user.user.email,
      password: user.user.password
    };
    // get token in db to compare in token in the body
    const tokens = await questionsDb.getTokenOfUser(
      userDetails.email,
      userDetails.password
    );
    const dbToken = tokens.rows[0].token;
    if (dbToken === token) {
      const id = await questionsDb.getUserId(
        userDetails.email,
        userDetails.password
      );

      //   user id
      const userId = id.rows[0].id;

      //   here
      const results = [];
      // --------
      const result = await questionsDb.displayUsersRandomQuestionsIQBE(
        categoryId,
        token
      );

      // still have questions left
      if (result.rows.length !== 0) {
        for (let i = 0; i < result.rows.length; i++) {
          const questionId = result.rows[i].question_id;
          const question = result.rows[i].questions;
          const correctAnswer = result.rows[i].correct_answer;
          const question_type = result.rows[i].question_type;
          const choices = await questionsDb.displayUsersRandomQuestionsChoices(
            questionId
          );
          results.push({
            questionId: questionId,
            question: question,
            correctAnswer: correctAnswer,
            question_type:question_type,
            choices: choices.rows
          });
        }
      } else {
        // no more questions
        //   then category is done
        await questionsDb.changeToIsDone(userId, categoryId);
      }

      return results;
      // til here
    } else {
      const msg = {
        msg: "Your session has expired. Please contact us."
      };
      return msg;
    }
  };
};

module.exports = selectUsersRandomQuestionsIQBE;
