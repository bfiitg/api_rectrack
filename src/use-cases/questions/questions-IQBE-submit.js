const { toLowerCased, decrypt } = require("../../entities/users/app");
const { getUserDataThruToken } = require("../../token/app");

const userSubmitAnswerIQBE = ({ questionsDb }) => {
  return async function puts(info) {
    const token = info.token; // token in the body
    // get auth data thru token
    const user = await getUserDataThruToken(token);
    const userDetails = {
      email: user.user.email,
      password: user.user.password
    };
    // get token in db to compare in token in the body
    const tokens = await questionsDb.getTokenOfUser(
      userDetails.email,
      userDetails.password
    );
    const dbToken = tokens.rows[0].token;

    // compare if equal both token
    if (dbToken === token) {
      // get user id thru token
      const id = await questionsDb.getUserId(
        userDetails.email,
        userDetails.password
      );

      //   user id
      const userId = id.rows[0].id;

      const categoryIdArray = info.categoryId;
      for (let i = 0; i < categoryIdArray.length; i++) {
        const done = await questionsDb.checkifCategoryIsDone(
          categoryIdArray[i]
        );
        const isDone = done.rows[0].is_done;

        //   change is_done to yes
        if (isDone === "no") {
          // update user_scores db of user answer and if it is correct
          const answers = info.answers;
          for (let i = 0; i < answers.length; i++) {
            const questionId = answers[i].questionId;
            const answer = answers[i].answer;

            const answerData = await questionsDb.getCorrectAnswerPerQuestion(
              questionId
            );

            const correctAnswer = answerData.rows[0].correct_answer;

            // check answer
            // get the id from questions_choices from correct answer in questions db
            // for no image
            const id = await questionsDb.getIdFromCorrectAnswer(
              correctAnswer.toString() + "@"
            );
            const img = await questionsDb.getIdFromCorrectAnswer(
              correctAnswer.toString()
            );
            let bool = false; // determine if correct or wrong
            for (let i = 0; i < id.rows.length; i++) {
              const e = id.rows[i];
              const ids = e.id;
              if (ids.toString() === answer) {
                //  correct answer
                bool = true;
                break;
              }
            }
            for (let i = 0; i < img.rows.length; i++) {
              const e = img.rows[i];
              const ids = e.id;
              if (ids.toString() === answer) {
                //  correct answer
                bool = true;
                break;
              }
            }
            if (bool === true) {
              // if correct
              await questionsDb.ifAnswerIsCorrect(questionId, userId, answer);
            } else {
              // wrong
              await questionsDb.ifAnswerIsWrong(questionId, userId, answer);
            }
            // end check answer
          }

          //   change categories_random is done to 'yes'
          // only change to yes when no more questions left
          // await questionsDb.changeToIsDone(userId, categoryIdArray[i]);

          const msg = {
            msg: "Submitted successfully.. moving on the next set of questions."
          };
          return msg;
        }
      }
    } else {
      const msg = {
        msg: "Your session has expired. Please contact us."
      };
      return msg;
    }
  };
};

module.exports = userSubmitAnswerIQBE;
