const { makeQuestions } = require("../../entities/questions/app");
const { makeQuestionEssayEntities } = require("../../entities/questions/app");

const editQuestion = ({ questionsDb, questionTypeDb }) => {
  return async function put({ id, ...info } = {}) {
    const check = await questionTypeDb.findQuestionTypeByQTYPE(
      info.question_type_id
    );

    if (check.rows[0].question_type === "Multiple choice") {
      const result = makeQuestions(info);

      //
      const updated = await questionsDb.updateSelectedQuestion({
        id: id,
        questions: result.getQuestion(),
        correct_answer: result.getCorrectAnswer(),
        categories_id: result.getCategories(),
        question_type_id: result.getQuestionType(),
        details: result.getDetails()
      });
      return { ...updated };
    } else {
      const result2 = makeQuestionEssayEntities(info);
      const updated = await questionsDb.updateSelectedQuestion({
        id: id,
        questions: result2.getQuestion(),
        categories_id: result2.getCategories(),
        question_type_id: result2.getQuestionType()
      });
      return { ...updated,
      msg:"Updated successfully.." };
    }
  };
};

module.exports = editQuestion;
