const {
  makeQuestionEntityImports,
  toLowerCased
} = require("../../entities/questions/app");

const makeAddQuestionsImport = ({ questionsDb,questionTypeDb }) => {
  return async function posts(info) {
    const check = await questionTypeDb.findQuestionTypeByQTYPE(
      info.question_type_id
    );

    if (check.rows[0].question_type === "Multiple choice") {
    const result = makeQuestionEntityImports(info);
    //
    const data = await result.getData(); // extract data from xlsx
  
    return questionsDb.insertImportQuestions({
      question_type_id: result.getQuestionType(),
      categories_id: result.getCategories(),
      details: data
    });
  }else{
    throw new Error("Question type must be Multiple choice...")
  }
}
};

module.exports = makeAddQuestionsImport;
