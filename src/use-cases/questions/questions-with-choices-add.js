const { makeQuestions } = require("../../entities/questions/app");

const makeAddQuestions = ({ questionsDb }) => {
  return async function posts(info) {

    const result = makeQuestions(info);
    //   
    
    return questionsDb.insertWithChoices({
      questions: result.getQuestion(),
      correct_answer: result.getCorrectAnswer(),
      categories_id: result.getCategories(),
      question_type_id: result.getQuestionType(),
      details: result.getDetails()
    });
    /* TEST REMOVE
    const answer = result.getCorrectAnswer();
    if (answer === "true" || answer === "false") {
      return questionsDb.insertTrueFalse({
        questions: result.getQuestion(),
        correct_answer: result.getCorrectAnswer(),
        categories_id: result.getCategories()
      });
    } else {
      return questionsDb.insertWithChoices({
        questions: result.getQuestion(),
        correct_answer: result.getCorrectAnswer(),
        categories_id: result.getCategories(),
        details: result.getDetails()
      });
    }*/
  };
};

module.exports = makeAddQuestions;
