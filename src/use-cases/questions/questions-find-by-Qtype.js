const viewQType = ({ questionTypeDb }) => {
  return async function get(question_type_id) {
        const data = await questionTypeDb.findQuestionTypeByQTYPE(question_type_id);
        return data.rows[0].question_type
};
}
module.exports = viewQType;
