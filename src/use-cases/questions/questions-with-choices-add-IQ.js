const { makeQuestionIQ } = require("../../entities/questions/app");

const makeAddQuestionsIQ = ({ questionsDb }) => {
  return async function posts(info) {

    const result = makeQuestionIQ(info);
    //  
    
    return questionsDb.insertWithChoicesIQ({
      questions: result.getQuestion(),
      correct_answer: result.getCorrectAnswer(),
      question_type_id: result.getQuestionType(),
      details: result.getDetails()
    });
    /* TEST REMOVE
    const answer = result.getCorrectAnswer();
    if (answer === "true" || answer === "false") {
      return questionsDb.insertTrueFalse({
        questions: result.getQuestion(),
        correct_answer: result.getCorrectAnswer(),
        categories_id: result.getCategories()
      });
    } else {
      return questionsDb.insertWithChoices({
        questions: result.getQuestion(),
        correct_answer: result.getCorrectAnswer(),
        categories_id: result.getCategories(),
        details: result.getDetails()
      });
    }*/
  };
};

module.exports = makeAddQuestionsIQ;