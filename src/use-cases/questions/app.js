const questionsDb = require("../../data-access/questions/app");
const questionTypeDb = require("../../data-access/questions_type/app");
const userDb = require("../../data-access/users/app")

const makeAddQuestions = require("./questions-with-choices-add");
const selectUsersRandomQuestions = require("./questions-users-exam");
const checkUsersCategoryIfDone = require("./questions-check-category-if-done");
const userSubmitAnswer = require("./questions-user-submit");
const makeAddQuestionsImport = require("./questions-import");
const userViewQuestion = require("./questioins-view-per-category");
const editQuestion = require("./questions-edit");
const adminViewQuestions = require("./questions-display");
const selectUsersRandomQuestionsIQBE = require("./questions-IQBE");
const userSubmitAnswerIQBE = require("./questions-IQBE-submit");
//butch's part
const makeAddQuestionsEssay = require("./question-add-essay");
const viewQType = require("./questions-find-by-Qtype");
const viewAllIQTest = require("./question-view-all-IQBE")
const makeAddQuestionsIQ = require("./questions-with-choices-add-IQ")
//for test
const userSubmitAnswerTest = require('./questions-user-submit-test');
const { request } = require("../../app");

const userSubmitAnswerIQBEs = userSubmitAnswerIQBE({ questionsDb });
const addQuestions = makeAddQuestions({ questionsDb });
const selectUsersRandomQuestion = selectUsersRandomQuestions({ questionsDb,userDb });
const checkUsersCategoryIfDones = checkUsersCategoryIfDone({ questionsDb });
const userSubmitAnswers = userSubmitAnswer({ questionsDb });
const makeAddQuestionsImports = makeAddQuestionsImport({ questionsDb,questionTypeDb});
const userViewQuestions = userViewQuestion({
  questionsDb
});
const editQuestions = editQuestion({ questionsDb,questionTypeDb});
const adminViewQuestionss = adminViewQuestions({ questionsDb });
const selectUsersRandomQuestionsIQBEs = selectUsersRandomQuestionsIQBE({
  questionsDb
});
//butch part
const addQuestionsEssayUseCase = makeAddQuestionsEssay({questionsDb});
const viewQTypeUseCase = viewQType({questionTypeDb});
const viewAllIQTestUseCase = viewAllIQTest({questionsDb})
const makeAddQuestionsIQUseCase = makeAddQuestionsIQ({questionsDb})
//for test
const userSubmitAnswersTest = userSubmitAnswerTest({ questionsDb });

const services = Object.freeze({
  addQuestions,
  selectUsersRandomQuestion,
  checkUsersCategoryIfDones,
  userSubmitAnswers,
  makeAddQuestionsImports,
  userViewQuestions,
  editQuestions,
  adminViewQuestionss,
  selectUsersRandomQuestionsIQBEs,
  userSubmitAnswerIQBEs,
  userSubmitAnswersTest,
  //butch part
  addQuestionsEssayUseCase,
  viewQTypeUseCase,
  viewAllIQTestUseCase,
  makeAddQuestionsIQUseCase
});

module.exports = services;
module.exports = {
  addQuestions,
  selectUsersRandomQuestion,
  checkUsersCategoryIfDones,
  userSubmitAnswers,
  makeAddQuestionsImports,
  userViewQuestions,
  editQuestions,
  adminViewQuestionss,
  selectUsersRandomQuestionsIQBEs,
  userSubmitAnswerIQBEs,
  userSubmitAnswersTest,
  //butch part
  addQuestionsEssayUseCase,
  viewQTypeUseCase,
  viewAllIQTestUseCase,
  makeAddQuestionsIQUseCase
};
