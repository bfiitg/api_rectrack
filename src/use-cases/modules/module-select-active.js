const selectModulesActive = ({moduleDb}) => {
    return async function selectAll(info){
        const result = await moduleDb.findAllModulesActive(info.id);
  
        return result.rows
    }
  }
  module.exports=selectModulesActive;