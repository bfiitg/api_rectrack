const e = require("express");
const makeModule = require("../../entities/modules/app")

const makeAddModules = ({moduleDb}) => {
    return async function posts(info){
        const result = makeModule(info.info);
        const moduleExists = await moduleDb.findModuleByDescription(result.getModule_name())
    
        if(moduleExists.rows.length !=0 ){
            const result = {
                msg: "Module already exists!",
                rows:moduleExists.rows
            }
            return result;
        }
        else{
            const insert =  moduleDb.addModule({
              module_name:result.getModule_name(),
                status:result.getStatus()
            })

            
            return insert
        }
        
    }
}

module.exports = makeAddModules