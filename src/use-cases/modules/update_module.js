const e = require("express");
const makeModule = require("../../entities/modules/app")

const updateModules = ({ moduleDb }) => {
    return async function put(info) {
        const result = makeModule(info.info);
        const moduleExists = await moduleDb.checkModuleUpdate(
            {
                module_name: result.getModule_name(),
                status: result.getStatus()
            }
        )
        if (moduleExists.rows.length != 0) {
            const result = {
                msg: "Module already exists!",
                rows: moduleExists.rows
            }
            return result;
        }
        else {
            const update = await moduleDb.updateModule(info.id, {
                module_name: result.getModule_name(),
                status: result.getStatus()
            })


            return update
        }

    }
}

module.exports = updateModules