const selectModules = ({moduleDb}) => {
    return async function selectAll(info){
        const result = await moduleDb.findModuleById(info.id);
  
        return result.rows
    }
  }
  module.exports=selectModules;