const selectAllModules = ({moduleDb}) => {
  return async function selectAll(){
      const result = await moduleDb.findAllModules();

      return result.rows
  }
}
module.exports=selectAllModules;