const moduleDb = require("../../data-access/modules/app")

const makeAddModules = require("./add_module");
const selectAllModules = require("./list_modules");
const updateModules = require("./update_module");
const selectModules = require("./module-select")
const selectModulesActive = require("./module-select-active")

const makeAddModulesUseCase = makeAddModules({moduleDb});
const selectAllModulesUseCase = selectAllModules({moduleDb});
const updateModulesUseCase = updateModules({moduleDb});
const selectModulesUseCase = selectModules({moduleDb});
const selectModulesActiveUseCase = selectModulesActive({moduleDb})

const service = Object.freeze({
    makeAddModulesUseCase,
    selectAllModulesUseCase,
    updateModulesUseCase,
    selectModulesUseCase,
    selectModulesActiveUseCase
})

module.exports = service;
module.exports = {
    makeAddModulesUseCase,
    selectAllModulesUseCase,
    updateModulesUseCase,
    selectModulesUseCase,
    selectModulesActiveUseCase
}