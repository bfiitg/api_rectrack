const positionsDb = require("../../data-access/positions/app");

const positionSelectAllByDept = require("./position-select-all-by-dept");
const makeAddPositions = require("./positions-add");
const positionsDisplayVacant = require("./positions-display-vacant");
const positionSelectsAll = require("./positions-select-all");
const positionNotVacant = require("./positions-to-not-vacant");
const positionVacant = require("./positions-to-vacant");
const positionsDisplayNotVacant = require("./positions-display-not-vacant");
const positionSelectOne = require("./position-select-one");
const updatePositions = require("./positions-update");

const positionSelectAllByDepts = positionSelectAllByDept({ positionsDb });
const updatePositionss = updatePositions({ positionsDb });
const positionSelectOnes = positionSelectOne({ positionsDb });
const addPositions = makeAddPositions({ positionsDb });
const positionsDisplayVacants = positionsDisplayVacant({ positionsDb });
const positionSelectsAlls = positionSelectsAll({ positionsDb });
const positionNotVacants = positionNotVacant({ positionsDb });
const positionVacants = positionVacant({ positionsDb });
const positionsDisplayNotVacants = positionsDisplayNotVacant({ positionsDb });

const services = Object.freeze({
  addPositions,
  positionsDisplayVacants,
  positionSelectsAlls,
  positionNotVacants,
  positionVacants,
  positionsDisplayNotVacants,
  positionSelectOnes,
  updatePositionss,
  positionSelectAllByDepts
});

module.exports = services;
module.exports = {
  addPositions,
  positionsDisplayVacants,
  positionSelectsAlls,
  positionNotVacants,
  positionVacants,
  positionsDisplayNotVacants,
  positionSelectOnes,
  updatePositionss,
  positionSelectAllByDepts
};
