const makePosition = require("../../entities/positions/app");

const makeAddPositions = ({ positionsDb }) => {
  return async function posts(info) {
    const result = makePosition(info);
    const positionExists = await positionsDb.findByName(result.getName());
    const rows = positionExists.rows;
    if (positionExists.rowCount !== 0) {
      const result = {
        msg: "Position already exists!",
        command: positionExists.command,
        rows
      };
      return result;
    }
    //
    return positionsDb.insert({
      name: result.getName(),
      description: result.getDescription(),
      teams_id: result.getTeam(),
      is_office_staff: result.getOfficeStaff(),
      is_vacant: result.getVacant()
    });
  };
};

module.exports = makeAddPositions;
