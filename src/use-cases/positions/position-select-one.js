const positionSelectOne = ({ positionsDb }) => {
  return async function selects(id) {
    const result = await positionsDb.findOne(id);
    return result.rows;
  };
};

module.exports = positionSelectOne;
