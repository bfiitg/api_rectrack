const positionsDisplayVacant = ({ positionsDb }) => {
    return async function selects() {
      const result = await positionsDb.findAllVacant();
      return result.rows;
    };
  };
  
  module.exports = positionsDisplayVacant;
  