const makePosition = require("../../entities/positions/app");

const updatePositions = ({ positionsDb }) => {
  return async function put({ id, ...info } = {}) {
    const result = makePosition(info);
    const positionExists = await positionsDb.findByNameUpdate(
      id,
      result.getName()
    );
    const rows = positionExists.rows;
    if (positionExists.rowCount !== 0) {
      throw new Error("Position already exists.");
    }
    //
    return positionsDb.updating({
      id: id,
      name: result.getName(),
      description: result.getDescription(),
      teams_id: result.getTeam(),
      is_office_staff: result.getOfficeStaff(),
      is_vacant: result.getVacant(),
      status: result.getStatus()
    });
  };
};

module.exports = updatePositions;
