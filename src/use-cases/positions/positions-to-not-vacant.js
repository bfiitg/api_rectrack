const positionNotVacant = ({ positionsDb }) => {
  return async function put({ id } = {}) {
    //
    const updated = await positionsDb.toNotVacant(id);
    return { ...updated };
  };
};

module.exports = positionNotVacant;
