const positionSelectsAll = ({ positionsDb }) => {
  return async function selects() {
    const result = await positionsDb.findAll();
    return result.rows;
  };
};

module.exports = positionSelectsAll;
