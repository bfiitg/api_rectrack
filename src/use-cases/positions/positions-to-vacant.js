const positionVacant = ({ positionsDb }) => {
    return async function put({ id } = {}) {
      //
      const updated = await positionsDb.toVacant(id);
      return { ...updated };
    };
  };
  
  module.exports = positionVacant;
  