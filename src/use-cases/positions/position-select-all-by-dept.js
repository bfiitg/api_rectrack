const positionSelectAllByDept = ({ positionsDb }) => {
  return async function selects(id) {
    const result = await positionsDb.findAllPositionFilterByDept(id);
    return result.rows;
  };
};

module.exports = positionSelectAllByDept;
