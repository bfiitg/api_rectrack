const positionsDisplayNotVacant = ({ positionsDb }) => {
  return async function selects() {
    const result = await positionsDb.findAllNotVacant();
    return result.rows;
  };
};

module.exports = positionsDisplayNotVacant;
