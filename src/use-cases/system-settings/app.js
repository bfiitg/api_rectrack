const systemSettingsDb = require('../../data-access/system-settings/app');

const getSystemColors = require('./system-settings-get-colors');
const updateSystemSettings = require('./update-system-settings');
const getCurrentSystemSettings = require('./get-current-system-settings');

const getSystemColorsUseCase = getSystemColors({ systemSettingsDb });
const updateSystemSettingsUseCase = updateSystemSettings({ systemSettingsDb });
const getCurrentSystemSettingsUseCase = getCurrentSystemSettings({ systemSettingsDb });

const systemSettingsService = Object.freeze({
    getSystemColorsUseCase,
    updateSystemSettingsUseCase,
    getCurrentSystemSettingsUseCase
});

module.exports = systemSettingsService;
module.exports = {
    getSystemColorsUseCase,
    updateSystemSettingsUseCase,
    getCurrentSystemSettingsUseCase
}