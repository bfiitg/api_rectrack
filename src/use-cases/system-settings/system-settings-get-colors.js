const getSystemColors = ({ systemSettingsDb }) => {
    return async function getColors(){
        const result = await systemSettingsDb.getColors();
        return result.rows;
    }
}

module.exports = getSystemColors;