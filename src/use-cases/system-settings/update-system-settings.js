
const updateSystemSettingsUseCase = ({ systemSettingsDb }) => {
    return async function updateSettings(settings){

        const updated = await systemSettingsDb.setSystemSettings(
                settings.logoPath, 
                settings.hexColor
            );

        const currentSettings = await systemSettingsDb.getCurrentSystemSettings();
        return { ...currentSettings.rows };

    }
}

module.exports = updateSystemSettingsUseCase;