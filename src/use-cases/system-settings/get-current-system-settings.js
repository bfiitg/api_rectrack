const getCurrentSystemSettingsUseCase = ({ systemSettingsDb }) => {
    return async function getColors(){
        const result = await systemSettingsDb.getCurrentSystemSettings();
        return result.rows;
    }
}

module.exports = getCurrentSystemSettingsUseCase;