const { decrypt } = require("../../entities/users/app");

const departmentSelectAll = ({ departmentDb }) => {
  return async function selects() {
    const result = await departmentDb.findAll();
    const departments = result.rows;
    const results = [];
    for (let department of departments) {
      results.push({
        id: department.id,
        name: department.name,
        description: department.description,
        userId: department.userid,
        deptHead: (department.firstname && department.lastname) ? decrypt(department.firstname) + " " + decrypt(department.lastname) : '',
        status: department.status,
      })
    }
    return results;
  };
};

module.exports = departmentSelectAll;
