const departmentDb = require("../../data-access/departments/app");

const makeAddDept = require("./department-add");
const departmentSelectAll = require("./department-view-all");
const deptChangeStatusInactive = require("./department-change-status-inactive");
const adminEditDept = require("./department-edit");
const departmentSelectOne = require("./department-view-one");

const departmentSelectOnes = departmentSelectOne({ departmentDb });
const addDept = makeAddDept({ departmentDb });
const departmentSelectAlls = departmentSelectAll({ departmentDb });
const deptChangeStatusInactives = deptChangeStatusInactive({ departmentDb });
const adminEditDepts = adminEditDept({ departmentDb });

const services = Object.freeze({
  addDept,
  departmentSelectAlls,
  deptChangeStatusInactives,
  adminEditDepts,
  departmentSelectOnes
});

module.exports = services;
module.exports = {
  addDept,
  departmentSelectAlls,
  deptChangeStatusInactives,
  adminEditDepts,
  departmentSelectOnes
};
