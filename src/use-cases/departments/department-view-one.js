const { decrypt } = require("../../entities/users/app");

const departmentSelectOne = ({ departmentDb }) => {
  return async function selects(id) {
    const result = await departmentDb.findOne(id);
    const departments = result.rows;
    const results = [];
    for (let department of departments) {
      results.push({
        id: department.id,
        name: department.name,
        description: department.description,
        userId: department.userid,
        deptHead: (department.firstname && department.lastname) ? decrypt(department.firstname) + " " + decrypt(department.lastname) : '',
        status: department.status,
      })
    }
    return results;
  };
};

module.exports = departmentSelectOne;
