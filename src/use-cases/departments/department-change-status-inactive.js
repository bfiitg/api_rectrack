const deptChangeStatusInactive = ({ departmentDb }) => {
  return async function puts({ id } = {}) {
    const updated = await departmentDb.changeStatusInactive(id);
    return { ...updated };
  };
};

module.exports = deptChangeStatusInactive;
