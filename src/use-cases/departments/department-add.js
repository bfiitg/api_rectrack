const makeDepartment = require("../../entities/departments/app");

const makeAddDept = ({ departmentDb }) => {
  return async function posts(info) {
    const result = makeDepartment(info);
    const deptExists = await departmentDb.findByName(result.getName());
    const rows = deptExists.rows;
    if (deptExists.rowCount !== 0) {
      const result = {
        msg: "Department already exists!",
        command: deptExists.command,
        rows
      };
      return result;
    }
    //
    return departmentDb.insert({
      name: result.getName(),
      description: result.getDescription(),
      user_id: result.getUser()
    });
  };
};

module.exports = makeAddDept;
