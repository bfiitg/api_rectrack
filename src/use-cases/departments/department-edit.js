const makeDepartment = require("../../entities/departments/app");

const adminEditDept = ({ departmentDb }) => {
  return async function put({ id, ...info } = {}) {
    const result = makeDepartment(info);
    //
    const deptName = result.getName();

    const count = await departmentDb.checkIfNameExists(id, deptName);
    const dataCount = count.rowCount;
    if (dataCount > 0) {
      // exists
      throw new Error("Department name already exists.");
    } else {
      await departmentDb.updates({
        id: id,
        name: result.getName(),
        description: result.getDescription(),
        user_id: result.getUser(),
        status: result.getStatus()
      });
      const data = {
        msg: "Updated successfully"
      };
      return data;
    }
  };
};

module.exports = adminEditDept;
