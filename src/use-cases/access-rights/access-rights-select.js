const e = require("express");

const selectAccessRights = ({ accessRightsDb, actionsDb, moduleDb }) => {
    return async function selectAll(info) {
        const result = await accessRightsDb.select(info);
        const item = result.rows;
        const moduleResult = await moduleDb.findAllModules();
        const moduleItem = moduleResult.rows;
        // console.log(item)
        let access = []
        for (let a = 0; a < item.length; a++) {
            const value = {};
            let actions = [];
            let ArrayResult = [];
            let ResultArray = [];
            const element = item[a];
            const convert = element.actions
            const array = JSON.parse("[" + convert + "]");

            value.id = element.id
            value.discription = element.discription
            value.time_created = element.time_created
            value.time_updated = element.time_updated
            value.status = element.status

            for (let i = 0; i < array.length; i++) {
                const element_ = array[i];
                const ActionRes = await actionsDb.select(element_);
                actions.push(ActionRes.rows[a]);
            }


            const SortedActions = actions.sort(function (a, b) {
                return (a.module_id < b.module_id) ? -1 : (a.module_id > b.module_id) ? 1 : 0;
            });

            moduleItem.forEach(e => {

                for (let pp = 0; pp < SortedActions.length; pp++) {
                    const element___ = SortedActions[pp];
                    if (element___) {
                        if (e.id == element___.module_id) {

                            ArrayResult.push({
                                module_id: e.id,
                                moduleName: e.module_name,
                                access: [element___]
                            })
                        }
                    }
                }
                // throw new Error('error')

                // // for (let o = 0; o < SortedActions.length; o++) {
                //     const element__ = SortedActions[o];
                //     if (e.id == element__.module_id) {
                //         ArrayResult.push({
                //             module_id: e.id,
                //             moduleName: e.module_name,
                //             access: [element__]
                //         })
                //     }
                // }
            });

            let group = ArrayResult.reduce((r, a) => {
                r[a.module_id] = [...r[a.module_id] || [], a];
                return r;
            }, {});

            moduleItem.forEach(e => {
                if (group[e.id + '']) {
                    let Marrays = [];
                    const sortingModules = group[e.id + ''];
                    for (let u = 0; u < sortingModules.length; u++) {
                        const element___ = sortingModules[u];
                        Marrays.push(...element___.access);
                    }
                    ResultArray.push({
                        module_id: e.id,
                        moduleName: e.module_name,
                        access: [...Marrays]
                    })
                }
            });
            value.actions = ResultArray

            // let group = ArrayResult.reduce((r, a) => {
            //     // console.log("a", a);
            //     // console.log('r', r);
            //     r[a.module_id] = [...r[a.module_id] || [], a];
            //     return r;
            //    }, {});
            //    console.log("group", group['2'][2]);
            //    console.log(value)

            // console.log(ArrayResult)


            // console.log(SortedActions)
            // for (let a = 0; a < array.length; a++) {
            //     const act = {}
            //     const element = array[a];

            //     const findAction = await actionsDb.select(element)
            //     act.actions = findAction.rows

            //     value.actions = act
            //     actions.push(act)
            // } 
            access.push(value)
        }
        return (access)
    }
}
module.exports = selectAccessRights;