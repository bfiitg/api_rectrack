const makeAccessRights = require("../../entities/access-rights/app")

const updateAccessRights = ({accessRightsDb}) => {
    return async function posts(info){
        const result = makeAccessRights(info.info);
        const Exists = await accessRightsDb.checkUpdate({
            discription:result.getDiscription(),
            status:result.getStatus(),
            actions:result.getActions(),
        })
        if(Exists.rows.length !=0){
            const result = {
                error:400,
                msg: "Already exists!",
                rows:Exists.rows
            }
            return result;
        }
        else{
            const update =  accessRightsDb.update(info.id,{
                discription:result.getDiscription(),
                actions:result.getActions(),
                status:result.getStatus()
            })

            
            return update
        }
        
    }
}

module.exports = updateAccessRights