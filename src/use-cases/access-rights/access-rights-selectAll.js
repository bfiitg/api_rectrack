const e = require("express");

const selectAllAccessRights = ({ accessRightsDb, actionsDb,moduleDb }) => {
    return async function selectAll() {
        const result = await accessRightsDb.selectAll();
        const item = result.rows 
        // console.log(item)
        let access = []
        for (let a = 0; a < item.length; a++) {
            const value = {}
            let actions =[]
            const element = item[a];
            const convert = element.actions
            const array = JSON.parse("[" + convert + "]");

            value.id = element.id
            value.discription = element.discription
            value.time_created = element.time_created
            value.time_updated = element.time_updated
            value.status = element.status

            for (let a = 0; a < array.length; a++) {
                const act = {}
                const element = array[a];

                const findAction = await actionsDb.select(element)
                act.actions = findAction.rows

                value.actions = act
                actions.push(act)
            }

        
            value.actions=actions

            access.push(value)

        }
           return(access)
    }
}
module.exports = selectAllAccessRights;