
const additionalAccessRights = ({ accessRightsDb }) => {
    return async function posts(info) {
        if (!info.info.module_id) { throw new Error('Please enter module id') }
        if (!info.info.action_name) { throw new Error('Please enter action name') }
        const data = {
            module_id: info.info.module_id,
            action_name: info.info.action_name,
        };
        const checkaction = await accessRightsDb.checkUpdatetoDelete(info.id)
        const findModuleName = await accessRightsDb.checkUpdate(data)
        if (!checkaction.rows.length) {
            return result = {
                error: 400,
                msg: "failed to fetch or invalid format of discription!",
            }
        }
        else if (!findModuleName.rows.length) {
            return result = {
                error: 400,
                msg: "failed to fetch or invalid format of action name or module id!",
            }

        } else {
            const modules = findModuleName.rows[0].id + '';
            const checkfetchData = checkaction.rows[0];
            const arr = checkfetchData.actions.split(",");

            const index = arr.indexOf(modules);
            if (index > -1) {
                arr.splice(index, 1);
            }
            arr.push(modules)
            const ActionUpdate = arr.join(",");
            const resultss = await accessRightsDb.updateActions(ActionUpdate, info.id)
            return { success: "Success" }

        }
    }
}

module.exports = additionalAccessRights