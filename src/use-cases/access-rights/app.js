const accessRightsDb = require("../../data-access/access-rights/app");
const actionsDb = require("../../data-access/actions/app")
const moduleDb = require("../../data-access/modules/app")

const selectAllAccessRights = require("./access-rights-selectAll");
const selectAccessRights = require("./access-rights-select");
const makeAddAccessRights = require("./access-rights-add");
const updateAccessRights = require("./access-rights-update");
const removeAccessRights = require("./access-rights-remove");
const additionalAccessRights = require("./access-rights-addAction")
const selectAllAccessRightsUseCase = selectAllAccessRights({ accessRightsDb, actionsDb, moduleDb });
const selectAccessRightsUseCase = selectAccessRights({ accessRightsDb, actionsDb, moduleDb });
const makeAddAccessRightsUseCase = makeAddAccessRights({ accessRightsDb });
const updateAccessRightsUseCase = updateAccessRights({ accessRightsDb });
const removeAccessRightsUseCase = removeAccessRights({ accessRightsDb })
const additionalAccessRightsUseCase = additionalAccessRights({ accessRightsDb });
const services = Object.freeze({
    selectAllAccessRightsUseCase,
    selectAccessRightsUseCase,
    makeAddAccessRightsUseCase,
    updateAccessRightsUseCase,
    removeAccessRightsUseCase,
    additionalAccessRightsUseCase
})

module.exports = services;

module.exports = {
    selectAllAccessRightsUseCase,
    selectAccessRightsUseCase,
    makeAddAccessRightsUseCase,
    updateAccessRightsUseCase,
    removeAccessRightsUseCase,
    additionalAccessRightsUseCase
}