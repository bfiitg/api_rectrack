const makeAccessRights = require("../../entities/access-rights/app")

const makeAddAccessRights = ({accessRightsDb}) => {
    return async function posts(info){

        const result = makeAccessRights(info.info);
        const Exists = await accessRightsDb.findByDiscription(result.getDiscription())
        
        if(Exists.rows.length !=0){
            const result = {
                error:400,
                msg: "Already exists!",
                rows:Exists.rows
            }
            return result;
        }
        else{
            const insert =  accessRightsDb.insert({
                discription:result.getDiscription(),
                actions:result.getActions(),
                status:result.getStatus()
            })

            
            return insert
        }
        
    }
}

module.exports = makeAddAccessRights