const makeAction = require("../../entities/actions/app")

const makeAddAction = ({actionsDb}) => {
    return async function posts(info){
        const result = makeAction(info.info);
        const actionExists = await actionsDb.check(result.getAction_name())
        if(actionExists.rows.length !=0 ){
            const result = {
                msg: "Action already exists!",
                rows:actionExists.rows
            }
            return result;
        }else{
            const insert =  actionsDb.insert({
                action_name:result.getAction_name(),
                module_id:result.getModule_id(),
                status:result.getStatus()
            })

            
            return insert
        }
        
    }
}

module.exports = makeAddAction