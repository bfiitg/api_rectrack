const selectActions = ({actionsDb}) => {
    return async function selectAll(info){
        const result = await actionsDb.select(info.id);

        return result.rows
    }
}
module.exports=selectActions;