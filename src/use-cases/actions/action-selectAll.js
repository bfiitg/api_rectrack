const selectAllActions = ({actionsDb}) => {
    return async function selectAll(){
        const result = await actionsDb.selectAll();

        return result.rows
    }
}
module.exports=selectAllActions;