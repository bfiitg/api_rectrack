const e = require("express");
const makeAction = require("../../entities/actions/app")

const updateAction = ({actionsDb}) => {
    return async function put(info){
        const result = makeAction(info.info);
        const actionExists = await actionsDb.checkupdate({
             action_name:result.getAction_name(),
             module_id:result.getModule_id(),
             status:result.getStatus()})

        if(actionExists.rows.length !=0 ){
            const result = {
                code:400,
                msg: "Action already exists!",
                rows:actionExists.rows
            }
            return result;
        }else{
            const update = await actionsDb.update(info.id,{
                
                action_name:result.getAction_name(),
                module_id:result.getModule_id(),
                status:result.getStatus()
            })

            
            return update
        }
        
    }
}

module.exports = updateAction