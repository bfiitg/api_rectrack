const actionsDb = require("../../data-access/actions/app")

const selectAllActions = require("./action-selectAll");
const makeAddAction = require("./action-add");
const updateAction = require("./action-edit");
const selectActions = require("./action-select")

const selectAllActionsUseCase = selectAllActions({actionsDb});
const makeAddActionUseCase = makeAddAction({actionsDb});
const updateActionUseCase = updateAction({actionsDb});
const selectActionsUseCase = selectActions({actionsDb});

const services = Object.freeze({
    selectAllActionsUseCase,
    makeAddActionUseCase,
    updateActionUseCase,
    selectActionsUseCase,
})


module.exports=services;

module.exports = {
    selectAllActionsUseCase,
    makeAddActionUseCase,
    updateActionUseCase,
    selectActionsUseCase,
}