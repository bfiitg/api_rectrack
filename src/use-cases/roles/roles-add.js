const makeRole = require("../../entities/roles/app");

const makeAddRole = ({ rolesDb }) => {
  return async function posts(rolesInfo) {
    const result = makeRole(rolesInfo);
    const roleExists = await rolesDb.findByName(result.getName());
    const rows = roleExists.rows;
    if (roleExists.rowCount !== 0) {
      const result = {
        msg: "Role already exists!",
        command: roleExists.command,
        rows
      };
      return result;
    }
    //
    return rolesDb.insert({
      name: result.getName(),
      description: result.getDescription(),
      status: result.getStatus()
    });
  };
};

module.exports = makeAddRole;
