const rolesSelectOne = ({ rolesDb }) => {
    return async function selects(id) {
      const res = await rolesDb.selectOneRoleWithAccess(id)
      const result = await rolesDb.selectOneRole(id);
      
      if(res.rows.length==0){
        return result.rows;
      }else{
        return res.rows
      }
      
    };
  };
  
  module.exports = rolesSelectOne;
  