const makeRole = require("../../entities/roles/app");

const adminEditRoles = ({ rolesDb }) => {
  return async function put({ id, ...info } = {}) {
    const result = makeRole(info);
    //
    const roleExists = await rolesDb.findByNameUpdate(result.getName(), id);
    const rows = roleExists.rows;
    if (roleExists.rowCount !== 0) {
      throw new Error("Role name already exists.");
    }

    const updated = await rolesDb.editRole({
      id: id,
      name: result.getName(),
      description: result.getDescription(),
      status: result.getStatus()
    });
    return { ...updated };
  };
};

module.exports = adminEditRoles;
