const rolesSelectsAll = ({ rolesDb }) => {
  return async function selects() {
    let view = []
    // const res = await rolesDb.selectAllRoleWithAccess()
    const result = await rolesDb.selectAllRoles();

    // for (const item of res.rows) {
    //   view.push(item)
    // }

    if(result.rows.length){
      for (const item of result.rows) {
        if(item.access_rights_id==null){
            view.push(item)
        }
      }
    }

    return view
  };
};

module.exports = rolesSelectsAll;
