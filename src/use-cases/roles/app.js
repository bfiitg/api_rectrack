const rolesDb = require("../../data-access/roles/app");

const makeAddRole = require("./roles-add");
const rolesSelectsAll = require("./roles-select-all");
const rolesSelectOne = require("./roles-select-one");
const adminEditRoles = require("./roles-edit");

const rolesSelectOnes = rolesSelectOne({ rolesDb });
const addRole = makeAddRole({ rolesDb });
const rolesSelectAll = rolesSelectsAll({ rolesDb });
const adminEditRoless = adminEditRoles({ rolesDb });

const rolesService = Object.freeze({
  addRole,
  rolesSelectAll,
  rolesSelectOnes,
  adminEditRoless
});

module.exports = rolesService;
module.exports = {
  addRole,
  rolesSelectAll,
  rolesSelectOnes,
  adminEditRoless
};
