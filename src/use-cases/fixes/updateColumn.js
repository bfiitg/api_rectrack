const updateColumn = ({ fixesDb }) => {
    return async function updateCol(info) {
      const question_type_id = info.question_type_id;
      const categories_id = info.categories_id
      const test = await fixesDb.updateCol(question_type_id,categories_id);
      return {
        message: "success"
      };
    };
  };
  module.exports = updateColumn;
  