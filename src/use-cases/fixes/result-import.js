const parser = new (require("simple-excel-to-json")).XlsParser();
const fpath = "./questions/List_of_Newly_Hired_Applicants.xlsx";
const { encrypt } = require("../../entities/users/app");

const resultImport = ({fixesDb}) =>{
   return async function imports(){
    const doc = parser.parseXls2Json(fpath);
    const data = doc[0]
   // let applicants = [];
    //print the data of the first sheet
    let inserted = [];
    let alreadyRecorded = [];

    for(let i = 0; i < data.length; i++){

        //check if applicant exists
        let checkApplicant = await fixesDb.getApplicantId({
            email: encrypt(data[i].email),
            firstname: encrypt(data[i].first_name),
            middlename: encrypt(data[i].middle_name),
            lastname: encrypt(data[i].last_name)
        });

        if(!checkApplicant.rows.length){
            //get position id
            let fetchPositionId = await fixesDb.getPositionId(data[i].deptName.trim(), data[i].position.trim());
            let position = fetchPositionId.rows;
            let position_id;

            if(!position.length){
                return {
                    msg: "position not found",
                    position: {
                        position: data[i].position.trim(),
                        dept: data[i].deptName.trim()
                    }
                }
            } else{
                
            position_id = position[0].position_id;
            }
            
            //insert to users table
            await fixesDb.insertUsers({
                email: encrypt(data[i].email),
                firstname: encrypt(data[i].first_name),
                middlename: encrypt(data[i].middle_name),
                lastname: encrypt(data[i].last_name),
                mobile: data[i].mobile,
                livestock: data[i].livestock,
                willing_to_travel: data[i].willing_to_travel,
                position_id: position_id,
                resume_upload_date: data[i].resume_upload_date,
                resume_upload_url: data[i].resume_upload_url
            });

            //get applicant id
            let fetchApplicant = await fixesDb.getApplicantId({
                email: encrypt(data[i].email),
                firstname: encrypt(data[i].first_name),
                middlename: encrypt(data[i].middle_name),
                lastname: encrypt(data[i].last_name)
            });
            let applicant = fetchApplicant.rows;
            let users_id = applicant[0].id;

            //insert date trackings
            let insertADT = await fixesDb.insertADT({
                users_id: users_id,
                date_received: data[i].date_received,
                date_deployed: data[i].date_deployed
            });

            inserted.push({
                    email: (data[i].email),
                    firstname: (data[i].first_name),
                    middlename: (data[i].middle_name),
                    lastname: (data[i].last_name),
                    position: data[i].position,
                    dept: data[i].deptName
                });
        } else{
            alreadyRecorded.push({
                email: (data[i].email),
                firstname: (data[i].first_name),
                middlename: (data[i].middle_name),
                lastname: (data[i].last_name),
                position: data[i].position,
                dept: data[i].deptName
            })
        } 
    }  

     return {
         inserted: inserted,
         alreadyRecorded: alreadyRecorded
     };
   }
}


module.exports = resultImport
