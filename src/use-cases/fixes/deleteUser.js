const deleteUserUseCase = ({ fixesDb }) => {
    return async function deleteUsers(info) {
      const id = info.id;
      const test = await fixesDb.deleteUser(id);
      return {
        message: "success"
      };
    };
  };
  module.exports = deleteUserUseCase;