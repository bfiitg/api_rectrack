const deleteDuplicate = ({ fixesDb }) => {
    return async function Data() {
      const test = await fixesDb.selectDuplicate();
     
      for(i=0;i<test.rows.length;i++){
      
        const fix = await fixesDb.selectId(test.rows[i].mobile)
        for(o=1;o<fix.rows.length;o++){
         const del = await fixesDb.deleteDuplicate(fix.rows[o].id) 
        }
      }
      return {
        message: "DELETED"
      };
    };
  };
  module.exports = deleteDuplicate;
  