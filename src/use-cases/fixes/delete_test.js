const deleteTestUseCase = ({ fixesDb }) => {
  return async function deleteTestData(info) {
    const dept = info.deptname;
    const test = await fixesDb.deleteTest(dept);
    return {
      message: "success"
    };
  };
};
module.exports = deleteTestUseCase;
