const updateToOnlineExam = ({ fixesDb }) => {
    return async function updateCol(info) {
    const del = await fixesDb.resetOnlineExamRecord(info.id);
      const test = await fixesDb.updateToOnlineExam(info.id,info.info.mobile);

      return {
        message: "success"
      };
    };
  };
  module.exports = updateToOnlineExam;
  