const insertDeployedApplicant = ({
  fixesDb,
  readDataFromExcel,
  encrypt,
  path
}) => {
  return async function insert(info) {
    const paths = path.resolve(`year-end-report/deployed.csv`);

    const applicants = await readDataFromExcel(paths); // array of applicants
    let count = 0; // count number of inserted applicant
    let skippedApplicant = [];
    let insertedAppplicant = [];

    for await (let e of applicants) {
      const data = retriveApplicants(e, encrypt);

      let counter = 0; //increase if anything exist

      // check if email already exist; if exist do not insert
      const emailExist = await fixesDb.findByEmail(data.email);
      if (emailExist.rowCount !== 0) {
        e.where = "email exist";
        skippedApplicant.push(e);
        counter++;
      }
      // check if full name exist; if exist do not insert
      const userExist = await fixesDb.findByFullName(
        data.first_name,
        data.last_name
      );
      if (userExist.rowCount !== 0) {
        e.where = "name exist";
        skippedApplicant.push(e);
        counter++;
      }

      if (counter === 0) {
        insertedAppplicant.push(e);
        // insert to db
        await fixesDb.insertDeployedApplicants({ data });
        count++;
      }
    }

    const data = {
      msg: `Inserted ${count} applicants.`,
      skippedApplicant,
      insertedAppplicant,
      allApplicants: applicants
    };

    return data;
  };
};
module.exports = insertDeployedApplicant;

const retriveApplicants = (applicants, encrypt) => {
  const data = {
    email: encrypt(applicants.email),
    first_name: encrypt(applicants.first_name),
    last_name: encrypt(applicants.last_name),
    mobile: applicants.mobile ? applicants.mobile.toString() : "",
    livestock: applicants.livestocks ? applicants.livestocks : "",
    willing_to_travel: applicants.willing_to_travel
      ? applicants.willing_to_travel
      : "",
    position: applicants.position,
    date_applied: applicants.date_applied,
    date_deployed: applicants.date_deployed,
    resume_url: applicants.resume_url ? applicants.resume_url : "",
    status: "Deployed"
  };
  return data;
};
