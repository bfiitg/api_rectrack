const userClearOnlineExam = ({ fixesDb }) => {
    return async function deleteUsers(info) {

      const id = info.id;
      const test = await fixesDb.resetOnlineExamRecord(id);

      return {
        message: "success"
      };
    };
  };
  module.exports = userClearOnlineExam;