const { decrypt } = require("../../entities/users/app");
//function to find all applicants without `rejected_by` records
const findRejectorsUseCase = ({ fixesDb }) => {
    return async function getRejectors(){
        //get all rejected applicants with null rejected_by
        const applicants = await fixesDb.getAllRejectedWithoutRejectedBy();
        const result = applicants.rows;
        let count = 0;

        //for each applicant, find his reject log and get the user id of the rejector
        for(let i = 0; i < result.length; i++){
            let name = "";
            let mn = result[i].middlename;
            let middlename = "";
           
            if (mn != null && mn != "") {
                middlename = decrypt(mn);
                name = decrypt(result[i].firstname) + " " + middlename + " " + decrypt(result[i].lastname);
            }
            else if(mn == ""){
                name = decrypt(result[i].firstname) + "  " + decrypt(result[i].lastname);
            } 
            else{
                name = decrypt(result[i].firstname) + " " + decrypt(result[i].lastname);
            }

            //get logs
            let applicantRejector = await fixesDb.getRejectorFromLogs(name);
            let fetched = applicantRejector.rows;
            
            let info = {
                rejected_by: fetched[0].user_id,
                users_id: result[i].users_id
            };
    
            //update applicant rejector record
            let updated = fixesDb.updateRejectedBy(info);
            if(updated){
                count ++
            }
        }

        return {
            message: 'updated ' + count + ' applicants'
        };
    }
}

module.exports = findRejectorsUseCase;