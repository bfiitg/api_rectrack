const fixesDb = require("../../data-access/fixes/app");
const moment = require("moment");
const { encrypt } = require("../../entities/users/crypting");
const path = require("path");
// for importing excel file
const csvjson = require("csvjson");
const fs = require("fs");
const options = {
  delimiter: ",",
  quote: '"'
};
const readDataFromExcel = async path => {
  const data = fs.readFileSync(path, {
    encoding: "utf8"
  });

  const res = await csvjson.toObject(data, options);
  return res;
};

// end reading data from excel

//#######################
const findRejectors = require("./find-rejected-applicant-rejector");
const resultImport = require("./result-import");
const delete_Tests = require("./delete_test");
const insertDeployedApplicant = require("./insert-applicant-deployed");
const insertRejectedApplicant = require("./insert-applicant-rejected");
const insertReferenceApplicant = require("./insert-applicant-reference");
const insertBlacklistApplicant = require("./insert-applicant-blacklist");

const alterTabel = require("./alter-table-in-database");
const alterTabel1 = require("./alter-table-in-database_1");
const alterTabel2 = require("./alter-table-in-database_2")
const updateColumn = require("./updateColumn");

const deleteDuplicate = require("./delete_duplicate")
const deleteUser = require("./deleteUser")
const userClearOnlineExam = require("./resetOnlineExamRecord")

const updateToDeploy = require("./update-user-to-deploy")
const updateToOnlineExam = require("./update-user-to-online")

// ######################
const findRejectorsUseCase = findRejectors({ fixesDb });
const resultImportUseCase = resultImport({ fixesDb });
const delete_testUseCase = delete_Tests({ fixesDb });
const insertDeployedApplicants = insertDeployedApplicant({
  fixesDb,
  readDataFromExcel,
  encrypt,
  path
});
const insertRejectedApplicants = insertRejectedApplicant({
  fixesDb,
  readDataFromExcel,
  encrypt,
  path
});
const insertReferenceApplicants = insertReferenceApplicant({
  fixesDb,
  readDataFromExcel,
  encrypt,
  path
});
const insertBlacklistApplicants = insertBlacklistApplicant({
  fixesDb,
  readDataFromExcel,
  encrypt,
  path
});
const alterTabels = alterTabel({fixesDb})
const alterTabel1s = alterTabel1({fixesDb})
const alterTabel2s = alterTabel2({fixesDb})
const deleteDuplicateUseCase = deleteDuplicate({fixesDb})

const updateColumns = updateColumn({fixesDb})
const deleteUserUseCase = deleteUser({fixesDb})
const userClearOnlineExamUserCase = userClearOnlineExam({fixesDb}) 

const updateToDeployUseCase = updateToDeploy({fixesDb})
const updateToOnlineExamUseCase = updateToOnlineExam({fixesDb}) 

const services = Object.freeze({
  findRejectorsUseCase,
  resultImportUseCase,
  delete_testUseCase,
  insertDeployedApplicants,
  insertRejectedApplicants,
  insertReferenceApplicants,
  insertBlacklistApplicants,

  alterTabels,
  alterTabel1s,
  alterTabel2s,
  updateColumns,

  deleteDuplicateUseCase,
  deleteUserUseCase,
  userClearOnlineExamUserCase,

  updateToDeployUseCase,
  updateToOnlineExamUseCase
});

module.exports = services;
module.exports = {
  findRejectorsUseCase,
  resultImportUseCase,
  delete_testUseCase,
  insertDeployedApplicants,
  insertRejectedApplicants,
  insertReferenceApplicants,
  insertBlacklistApplicants,

  alterTabels,
  alterTabel1s,
  alterTabel2s,
  updateColumns,

  deleteDuplicateUseCase,
  deleteUserUseCase,
  userClearOnlineExamUserCase,

  updateToDeployUseCase,
  updateToOnlineExamUseCase
};
