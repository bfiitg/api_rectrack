const { makeUpdateQuestionType } = require("../../entities/questions_type/app");

const putUpdateQuestion = ({ questionTypeDb }) => {
  return async function makeEditModule({ id, ...info }) {
    const result = makeUpdateQuestionType(info);

    const checkid = await questionTypeDb.findQuestionTypeById(id);
    if (checkid.rowCount == 0) {
      throw new Error("Not Found");
    }

    {
      const content = questionTypeDb.updateQuestionType({
        id: id,
        question_type:result.getQuestionType()
      });

      return {
        msg: "Updated successfully"
      };
    }
  };
};

module.exports = putUpdateQuestion;
