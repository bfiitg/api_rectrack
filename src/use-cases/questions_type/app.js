const questionTypeDb = require("../../data-access/questions_type/app")


const postQuestionType = require("./add_question_type");
const putUpdateQuestion = require("./update_question_type");
const selectAllQuestionType = require("./view_all_question_type");
const changeActive = require("./changeToActive");
const changeInactive = require("./changeToInactive");
const selectQuestionType = require("./find_question_typeById");


const addQuestionTypeUseCase = postQuestionType({questionTypeDb});
const updateQuestionTypeUseCase = putUpdateQuestion({questionTypeDb});
const selectAllQuestionTypeUseCase = selectAllQuestionType({questionTypeDb});
const changeActiveUseCase = changeActive({questionTypeDb});
const changeInactiveUseCase = changeInactive({questionTypeDb})
const selectQuestionTypeUseCase = selectQuestionType({questionTypeDb})

const service = Object.freeze({
    addQuestionTypeUseCase,
    updateQuestionTypeUseCase,
    selectAllQuestionTypeUseCase,
    changeActiveUseCase,
    changeInactiveUseCase,
    selectQuestionTypeUseCase,
})

module.exports = service;
module.exports = {
    addQuestionTypeUseCase,
    updateQuestionTypeUseCase,
    selectAllQuestionTypeUseCase,
    changeActiveUseCase,
    changeInactiveUseCase,
    selectQuestionTypeUseCase
}