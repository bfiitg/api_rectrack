const changeInactive = ({ questionTypeDb }) => {
  return async function changeStatus({ id }) {
    const checkid = await questionTypeDb.findQuestionTypeById(id);
    if (checkid.rowCount == 0) {
      throw new Error("Not Found!");
    }
    const check = await questionTypeDb.checkInactive(id);
    if (check.rowCount !== 0) {
      throw new Error("Already Inactive");
    }
    const change = await questionTypeDb.changeStatusInactive({
      id: id
    });
    return {
      msg: "Success!"
    };
  };
};
module.exports = changeInactive;
