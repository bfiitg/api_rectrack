const { makeQuestionType } = require("../../entities/questions_type/app");

const postQuestionType = ({ questionTypeDb }) => {
  return async function makePostModule({ ...info }) {
    const results = makeQuestionType(info);

    const doExists = await questionTypeDb.findQuestionByType(
      results.getQuestionType()
    );
    const rows = doExists;

    if (rows.rowCount !== 0) {
     return{msg:"Question Type already exists!"}
    }

    const content = questionTypeDb.addQuestionType({
      question_type: results.getQuestionType()
    });

    return {
      msg: "added successfully"
    };
  };
};

module.exports = postQuestionType;
