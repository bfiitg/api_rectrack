const changeActive = ({ questionTypeDb }) => {
  return async function changeStatus({ id }) {
    const checkid = await questionTypeDb.findQuestionTypeById(id);
    if (checkid.rowCount == 0) {
      throw new Error("Not Found!");
    }
    const check = await questionTypeDb.checkActive(id);
    if (check.rowCount !== 0) {
      throw new Error("Already Active");
    }

    const change = await questionTypeDb.changeStatusActive({
      id: id
    });
    return {
      msg: "Success!"
    };
  };
};
module.exports = changeActive;
