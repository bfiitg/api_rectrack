const questionTypeSelectOne = ({ questionTypeDb }) => {
    return async function selects(id) {
      const result = await questionTypeDb.findQuestionTypeById(id);
      return result.rows;
    };
  };
  
  module.exports = questionTypeSelectOne;