const selectAllQuestionType = ({ questionTypeDb }) => {
  return async function selects() {
    const result = await questionTypeDb.findAllQuestionType();
    const data = result.rows;

    if (result.rowCount !== 0) {
      const results = [];
      for (let i = 0; i < data.length; i++) {
        const e = data[i];
        results.push({
          id: e.id,
          question_type: e.question_type,
          status: e.status
        });
      }
      return results;
    } else {
      throw new Error("No data");
    }
  };
};
module.exports = selectAllQuestionType;
