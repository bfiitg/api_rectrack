const interviewDb = require("../../data-access/interview-assessment/app");
const userDb = require("../../data-access/users/app");

const addInterviewAssessment = require("./interview-assessment-add");
const interviewAssessmentView = require("./interview-assessment-view");
const editInterviewAssessment = require("./interview-assessment-edit");
const interviewAssessmentViewAll = require("./interview-assessment-view-all");
const interviewAssessmentViewsRate = require("./interview-assessment-view-rates");
const interviewAssessmentViewsRateRater = require("./interview-assessment-view-rates-rated")

const addInterviewAssessments = addInterviewAssessment({ interviewDb, userDb });
const interviewAssessmentViews = interviewAssessmentView({ interviewDb });
const editInterviewAssessments = editInterviewAssessment({ interviewDb });
const interviewAssessmentViewAlls = interviewAssessmentViewAll({ interviewDb });
const interviewAssessmentViewsRates = interviewAssessmentViewsRate({interviewDb}); 
const interviewAssessmentViewsRateRaters = interviewAssessmentViewsRateRater({interviewDb})

const services = Object.freeze({
  addInterviewAssessments,
  interviewAssessmentViews,
  editInterviewAssessments,
  interviewAssessmentViewAlls,
  interviewAssessmentViewsRates,
  interviewAssessmentViewsRateRaters
});

module.exports = services;
module.exports = {
  addInterviewAssessments,
  interviewAssessmentViews,
  editInterviewAssessments,
  interviewAssessmentViewAlls,
  interviewAssessmentViewsRates,
  interviewAssessmentViewsRateRaters
};
