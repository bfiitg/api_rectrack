const interviewAssessmentViewsRatesAll = ({ interviewDb }) => {
    return async function get(interviewInfo) {
      const result = await interviewDb.getApplicantRaters(interviewInfo);
      return result.rows[0];
    };
    
  };
  module.exports = interviewAssessmentViewsRatesAll;
