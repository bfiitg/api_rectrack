const interviewAssessmentViewsRates = ({ interviewDb }) => {
    return async function get(interviewInfo) {
      const result = await interviewDb.checkIAFrated(interviewInfo);
      return result.rows[0];
    };
    
  };
  module.exports = interviewAssessmentViewsRates;
