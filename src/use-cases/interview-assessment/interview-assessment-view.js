const { decrypt } = require("../../entities/users/app");

const interviewAssessmentView = ({ interviewDb }) => {
  return async function get(id) {
    const users = await interviewDb.viewAssessmentOfEmployee(id);
    const result = users.rows;
    const results = [];

    for (let i = 0; i < result.length; i++) {
      results.push({
        id: result[i].id,
        applicantFn: decrypt(result[i].applicantfn),
        applicantLn: decrypt(result[i].applicantln),
        interviewerName: decrypt(result[i].interviewerfn) + " " + decrypt(result[i].interviewerln),
        //interviewerLn: decrypt(result[i].interviewerln),
        date_interview: result[i].date_interview,
        communication_skills: result[i].communication_skills,
        communication_skills_note: result[i].communication_skills_note,
        confidence: result[i].confidence,
        confidence_note: result[i].confidence_note,
        physical_appearance: result[i].physical_appearance,
        physical_appearance_note: result[i].physical_appearance_note,
        knowledge_skills: result[i].knowledge_skills,
        knowledge_skills_note: result[i].knowledge_skills_note,
        asking_rate: result[i].asking_rate,
        availability: result[i].availability,
        others: result[i].others,
        gen_remarks_recommendations: result[i].gen_remarks_recommendations,
        user_id_interviewer: result[i].user_id_interviewer
      });
    }
    return results;
  };
};

module.exports = interviewAssessmentView;
