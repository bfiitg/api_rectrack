const {
  makeInterviewAssessments
} = require("../../entities/interview-assessment/app");

const addInterviewAssessment = ({ interviewDb, userDb }) => {
  return async function posts(info) {
    const result = makeInterviewAssessments(info);
    //
    // const userExist = await interviewDb.findByApplicantId(
    //   result.getUserIdApplicant()
    // );
    // const uRows = userExist.rows;
    // if (userExist.rowCount !== 0) {
    //   throw new Error("User already have record assessment!");
    // }
    //

    const fetchAssessmentSchedule = await userDb.getApplicantAssessmentSchedule(result.getUserIdApplicant());
    const fetched = fetchAssessmentSchedule.rows;
    

    return interviewDb.insert({
      user_id_applicant: result.getUserIdApplicant(),
      date_interview: fetched[0].date_assessment,
      communication_skills: result.getCommunicationSkills(),
      communication_skills_note: result.getCommunicationSkillsNote(),
      confidence: result.getConfidence(),
      confidence_note: result.getConfidenceNote(),
      physical_appearance: result.getPhysicalAppearance(),
      physical_appearance_note: result.getPhysicalAppearanceNote(),
      knowledge_skills: result.getKnowledge(),
      knowledge_skills_note: result.getKnowledgeNote(),
      asking_rate: result.getAskingRate(),
      availability: result.getAvailability(),
      others: result.getOthers(),
      gen_remarks_recommendations: result.getRemarks(),
      user_id_interviewer: result.getUserIdInterviewer()
    });
  };
};

module.exports = addInterviewAssessment;
