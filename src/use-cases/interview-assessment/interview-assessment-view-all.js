const { decrypt } = require("../../entities/users/app");

const interviewAssessmentViewAll = ({ interviewDb }) => {
  return async function get() {
    const users = await interviewDb.selectAll();
    const result = users.rows;
    const results = [];
    for (let i = 0; i < result.length; i++) {
      results.push({
        id: result[i].id,
        applicantFn: decrypt(result[i].applicantfn),
        applicantLn: decrypt(result[i].applicantln),
        interviewerFn: decrypt(result[i].interviewerfn),
        interviewerLn: decrypt(result[i].interviewerln),
        date_interview: result[i].date_interview
      });
    }
    return results;
  };
};

module.exports = interviewAssessmentViewAll;
