const {
  makeInterviewAssessmentEdits
} = require("../../entities/interview-assessment/app");

const editInterviewAssessment = ({ interviewDb }) => {
  return async function puts({ id, ...info } = {}) {
    const result = makeInterviewAssessmentEdits(info);
    //
    return interviewDb.updates({
      id: id,
      date_interview: result.getDateInterview(),
      communication_skills: result.getCommunicationSkills(),
      communication_skills_note: result.getCommunicationSkillsNote(),
      confidence: result.getConfidence(),
      confidence_note: result.getConfidenceNote(),
      physical_appearance: result.getPhysicalAppearance(),
      physical_appearance_note: result.getPhysicalAppearanceNote(),
      knowledge_skills: result.getKnowledge(),
      knowledge_skills_note: result.getKnowledgeNote(),
      asking_rate: result.getAskingRate(),
      availability: result.getAvailability(),
      others: result.getOthers(),
      gen_remarks_recommendations: result.getRemarks(),
      user_id_interviewer: result.getUserIdInterviewer()
    });
  };
};

module.exports = editInterviewAssessment;
