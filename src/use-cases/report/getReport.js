const fs = require("fs");
const moment = require('moment')
const { decrypt } = require("../../entities/users/app");

const getReport = ({ userDb }) => {
  return async function selects(info) {
    const path = "./reports";
    let result;
    let execute;
    switch (info.stage.stage) {
      case 'quick-applicants':
        result = await userDb.findAllQuickApplicants();
        execute = quickApp()
        break;
      case 'received':
        if (!info.depId.depId) {
          return { msg: "depId is null or undefined" }
        }
        result = await userDb.findAllReceivedPGDeptOnly(info.depId.depId);
        execute = received()
        break;
      case 'online-exam':
        if (!info.depId.depId) {
          return { msg: "depId is null or undefined" }
        }
        result = await userDb.findAllOnlineExam(info.depId.depId)
        execute = onlineExam()
        break;
      case 'endorsement':
        if (!info.depId.depId) {
          return { msg: "depId is null or undefined" }
        }
        result = await userDb.findAllForEndorsement(info.depId.depId)
        execute = endorsement()
        break;
      case 'assessment':
        if (!info.depId.depId) {
          return { msg: "depId is null or undefined" }
        }
        result = await userDb.findAllAssessment(info.depId.depId)
        execute = assessment()
        break;
      case 'pending':
        if (!info.depId.depId) {
          return { msg: "depId is null or undefined" }
        }
        result = await userDb.findAllPending(info.depId.depId)
        execute = pending()
        break;
      case 'shortlist':
        if (!info.depId.depId) {
          return { msg: "depId is null or undefined" }
        }
        result = await userDb.findAllShortList(info.depId.depId)
        execute = shortlist()
        break;
      case 'joboffer':
        if (!info.depId.depId) {
          return { msg: "depId is null or undefined" }
        }
        result = await userDb.findAllJobOrder(info.depId.depId)
        execute = joboffer()
        break;
    };

    await fs.access(path, fs.F_OK, err => {
      if (err) {
        // file doesn't exist
        // so write new file
        fs.mkdir(path, err => {
          if (err) {
            console.log(err.message);
          } else {
            console.log("Make the folder..");
          }
        });
        return;
      }
    });


    function quickApp() {
      try {
        const writeStream = fs.createWriteStream(`${path}/${info.stage.stage}-${moment().format('YYYY-MM-DD')}-report.xls`);
        const header = "EMAIL" + "\t" +
          "FIRST NAME" + "\t" +
          "MIDDLE NAME" + "\t" +
          "LAST NAME" + "\t" +
          "MOBILE" + "\t" +
          "LIVESTOCKS" + "\t" +
          "WILLING TO TRAVEL" + "\t" +
          "APPLICATION DATE" + "\n";
        writeStream.write(header);

        let test = [];
        for (i = 0; i < result.rows.length; i++) {

          const data = result.rows[i];

          const toPush =
            decrypt(data.email) +
            "\t" +
            decrypt(data.firstname) +
            "\t" +
            (data.middlename ? decrypt(data.middlename) : '') +
            "\t" +
            decrypt(data.lastname) +
            "\t" +
            (data.mobile) +
            "\t" +
            (data.livestock) +
            "\t" +
            (data.willing_to_travel) +
            "\t" +
            (moment(data.resume_upload_date.toString()).format('L')) +
            "\n";
          test.push(toPush);
          writeStream.write(test[i]);
        }
        writeStream.close();
      } catch (error) {
        return "Error"
      }

    }

    function received() {
      try {
        const writeStream = fs.createWriteStream(`${path}/${info.stage.stage}-${moment().format('YYYY-MM-DD')}-report.xls`);
        const header =
          "EMAIL" + "\t" +
          "FIRST NAME" + "\t" +
          "MIDDLE NAME" + "\t" +
          "LAST NAME" + "\t" +
          "MOBILE" + "\t" +
          "LIVESTOCKS" + "\t" +
          "WILLING TO TRAVEL" + "\t" +
          "POSITION" + "\t" +
          "DEPARTMENT" + "\t" +
          "APPLICATION DATE" + "\n";
        writeStream.write(header);
        let test = [];
        for (i = 0; i < result.rows.length; i++) {

          const data = result.rows[i];

          const toPush =
            decrypt(data.email) +
            "\t" +
            decrypt(data.firstname) +
            "\t" +
            (data.middlename ? decrypt(data.middlename) : '') +
            "\t" +
            decrypt(data.lastname) +
            "\t" +
            (data.mobile) +
            "\t" +
            (data.livestock) +
            "\t" +
            (data.willing_to_travel) +
            "\t" +
            (data.name) +
            "\t" +
            (data.deptname) +
            "\t" +
            (moment(data.resume_upload_date.toString()).format('L')) +
            "\n";
          test.push(toPush);
          writeStream.write(test[i]);

        }
        writeStream.close();

      } catch (error) {
        return "Error"
      }

    }

    function onlineExam() {
      try {
        const writeStream = fs.createWriteStream(`${path}/${info.stage.stage}-${moment().format('YYYY-MM-DD')}-report.xls`);
        const header =
          "EMAIL" + "\t" +
          "FIRST NAME" + "\t" +
          "MIDDLE NAME" + "\t" +
          "LAST NAME" + "\t" +
          "MOBILE" + "\t" +
          "LIVESTOCKS" + "\t" +
          "WILLING TO TRAVEL" + "\t" +
          "POSITION" + "\t" +
          "APPLICATION DATE" + "\t" +
          "EXAM START DATE" + "\n";
        writeStream.write(header);
        let test = [];
        for (i = 0; i < result.rows.length; i++) {

          const data = result.rows[i];

          const toPush =
            decrypt(data.email) +
            "\t" +
            decrypt(data.firstname) +
            "\t" +
            (data.middlename ? decrypt(data.middlename) : '') +
            "\t" +
            decrypt(data.lastname) +
            "\t" +
            (data.mobile) +
            "\t" +
            (data.livestock) +
            "\t" +
            (data.willing_to_travel) +
            "\t" +
            (data.name) +
            "\t" +
            (moment(data.resume_upload_date.toString()).format('L')) +
            "\t" +
            (moment(data.online_exam_start.toString()).format('L')) +
            "\n";
          test.push(toPush);
          writeStream.write(test[i]);

        }
        writeStream.close();
      } catch (error) {
        return "Error"
      }
    }

    function endorsement() {
      try {
        const writeStream = fs.createWriteStream(`${path}/${info.stage.stage}-${moment().format('YYYY-MM-DD')}-report.xls`);
        const header =
          "EMAIL" + "\t" +
          "FIRST NAME" + "\t" +
          "MIDDLE NAME" + "\t" +
          "LAST NAME" + "\t" +
          "MOBILE" + "\t" +
          "LIVESTOCKS" + "\t" +
          "WILLING TO TRAVEL" + "\t" +
          "POSITION" + "\t" +
          "APPLICATION DATE" + "\t" +
          "EXAM START DATE" + "\t" +
          "EXAM END DATE" + "\n";
        writeStream.write(header);
        let test = [];
        for (i = 0; i < result.rows.length; i++) {

          const data = result.rows[i];

          const toPush =
            decrypt(data.email) +
            "\t" +
            decrypt(data.firstname) +
            "\t" +
            (data.middlename ? decrypt(data.middlename) : '') +
            "\t" +
            decrypt(data.lastname) +
            "\t" +
            (data.mobile) +
            "\t" +
            (data.livestock) +
            "\t" +
            (data.willing_to_travel) +
            "\t" +
            (data.name) +
            "\t" +
            (moment(data.resume_upload_date.toString()).format('L')) +
            "\t" +
            (moment(data.online_exam_start.toString()).format('L')) +
            "\t" +
            (moment(data.online_exam_end.toString()).format('L')) +
            "\n";
          test.push(toPush);
          writeStream.write(test[i]);

        }
        writeStream.close();
      } catch (error) {
        return "Error"
      }

    }

    function assessment() {
      try {
        const writeStream = fs.createWriteStream(`${path}/${info.stage.stage}-${moment().format('YYYY-MM-DD')}-report.xls`);
        const header =
          "EMAIL" + "\t" +
          "FIRST NAME" + "\t" +
          "MIDDLE NAME" + "\t" +
          "LAST NAME" + "\t" +
          "MOBILE" + "\t" +
          "LIVESTOCKS" + "\t" +
          "WILLING TO TRAVEL" + "\t" +
          "POSITION" + "\t" +
          "APPLICATION DATE" + "\t" +
          "EXAM START DATE" + "\t" +
          "EXAM END DATE" + "\t" +
          "ENDORSEMENT DATE" + "\t" +
          "ASSESSMENT SCHEDULE" + "\n";
        writeStream.write(header);
        let test = [];
        for (i = 0; i < result.rows.length; i++) {

          const data = result.rows[i];

          const toPush =
            decrypt(data.email) +
            "\t" +
            decrypt(data.firstname) +
            "\t" +
            (data.middlename ? decrypt(data.middlename) : '') +
            "\t" +
            decrypt(data.lastname) +
            "\t" +
            (data.mobile) +
            "\t" +
            (data.livestock) +
            "\t" +
            (data.willing_to_travel) +
            "\t" +
            (data.name) +
            "\t" +
            (moment(data.resume_upload_date.toString()).format('L')) +
            "\t" +
            (moment(data.online_exam_start.toString()).format('L')) +
            "\t" +
            (moment(data.online_exam_end.toString()).format('L')) +
            "\t" +
            (moment(data.date_endorsement.toString()).format('L')) +
            "\t" +
            (moment(data.schedule_for_assessment.toString()).format('L')) +
            "\n";
          test.push(toPush);
          writeStream.write(test[i]);

        }
        writeStream.close();
      } catch (error) {
        return "Error"
      }

    }

    function pending() {
      try {
        const writeStream = fs.createWriteStream(`${path}/${info.stage.stage}-${moment().format('YYYY-MM-DD')}-report.xls`);
        const header =
          "EMAIL" + "\t" +
          "FIRST NAME" + "\t" +
          "MIDDLE NAME" + "\t" +
          "LAST NAME" + "\t" +
          "MOBILE" + "\t" +
          "LIVESTOCKS" + "\t" +
          "WILLING TO TRAVEL" + "\t" +
          "POSITION" + "\t" +
          "APPLICATION DATE" + "\t" +
          "EXAM START DATE" + "\t" +
          "EXAM END DATE" + "\t" +
          "DATE OF INTERVIEW" + "\n";
        writeStream.write(header);
        let test = [];
        for (i = 0; i < result.rows.length; i++) {

          const data = result.rows[i];

          const toPush =
            decrypt(data.email) +
            "\t" +
            decrypt(data.firstname) +
            "\t" +
            (data.middlename ? decrypt(data.middlename) : '') +
            "\t" +
            decrypt(data.lastname) +
            "\t" +
            (data.mobile) +
            "\t" +
            (data.livestock) +
            "\t" +
            (data.willing_to_travel) +
            "\t" +
            (data.name) +
            "\t" +
            (moment(data.resume_upload_date.toString()).format('L')) +
            "\t" +
            (moment(data.online_exam_start.toString()).format('L')) +
            "\t" +
            (moment(data.online_exam_end.toString()).format('L')) +
            "\t" +
            (moment(data.date_interview.toString()).format('L')) +
            "\n";
          test.push(toPush);
          writeStream.write(test[i]);
        }
        writeStream.close();
      } catch (error) {
        return "Error"
      }

    }

    function shortlist() {
      try {
        const writeStream = fs.createWriteStream(`${path}/${info.stage.stage}-${moment().format('YYYY-MM-DD')}-report.xls`);
        const header =
          "EMAIL" + "\t" +
          "FIRST NAME" + "\t" +
          "MIDDLE NAME" + "\t" +
          "LAST NAME" + "\t" +
          "MOBILE" + "\t" +
          "LIVESTOCKS" + "\t" +
          "WILLING TO TRAVEL" + "\t" +
          "POSITION" + "\t" +
          "APPLICATION DATE" + "\n";
        writeStream.write(header);
        let test = [];
        for (i = 0; i < result.rows.length; i++) {

          const data = result.rows[i];

          const toPush =
            decrypt(data.email) +
            "\t" +
            decrypt(data.firstname) +
            "\t" +
            (data.middlename ? decrypt(data.middlename) : '') +
            "\t" +
            decrypt(data.lastname) +
            "\t" +
            (data.mobile) +
            "\t" +
            (data.livestock) +
            "\t" +
            (data.willing_to_travel) +
            "\t" +
            (data.name) +
            "\t" +
            (moment(data.resume_upload_date.toString()).format('L')) +
            "\n";
          test.push(toPush);
          writeStream.write(test[i]);

        }
        writeStream.close();
      } catch (error) {
        return "Error"
      }

    }

    function joboffer() {
      try {
        const writeStream = fs.createWriteStream(`${path}/${info.stage.stage}-${moment().format('YYYY-MM-DD')}-report.xls`);
        const header =
          "EMAIL" + "\t" +
          "FIRST NAME" + "\t" +
          "MIDDLE NAME" + "\t" +
          "LAST NAME" + "\t" +
          "MOBILE" + "\t" +
          "LIVESTOCKS" + "\t" +
          "WILLING TO TRAVEL" + "\t" +
          "POSITION" + "\t" +
          "APPLICATION DATE" + "\n";
        writeStream.write(header);
        let test = [];
        for (i = 0; i < result.rows.length; i++) {

          const data = result.rows[i];

          const toPush =
            decrypt(data.email) +
            "\t" +
            decrypt(data.firstname) +
            "\t" +
            (data.middlename ? decrypt(data.middlename) : '') +
            "\t" +
            decrypt(data.lastname) +
            "\t" +
            (data.mobile) +
            "\t" +
            (data.livestock) +
            "\t" +
            (data.willing_to_travel) +
            "\t" +
            (data.name) +
            "\t" +
            (moment(data.resume_upload_date.toString()).format('L')) +
            "\n";
          test.push(toPush);
          writeStream.write(test[i]);

        }
        writeStream.close();
      } catch (error) {
        return "Error"
      }

    }

    return {
      msg: "Success"
    };
  };
};

module.exports = getReport;
