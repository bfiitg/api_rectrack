const userDb = require("../../data-access/users/app");

const getReport = require("./getReport")
const dlReports = require("./dlReports")

const getReportUseCase = getReport({ userDb });
const dlReportsUseCase = dlReports({})


const reportService = Object.freeze({
    getReportUseCase,
    dlReportsUseCase
});

module.exports = reportService;
module.exports = {
    getReportUseCase,
    dlReportsUseCase
};