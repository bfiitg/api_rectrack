require('dotenv').config()

const blacklistEmail = ({ decrypt, userDb, sendEmail }) => {
   return async function send({ applicantID }){
      let firstName, lastName, email, emailResult

      await userDb.find(applicantID)
                  .then(res => {
                     firstName = decrypt(res.rows[0].firstname)
                     lastName = decrypt(res.rows[0].lastname)
                     email = decrypt(res.rows[0].email)
                  })


      let mail = {
         from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
         to: email,
         subject: "BFI : Application Status - Blacklisted",
         html:
         `Dear ${firstName} ${lastName}, <br/><br/>` +
         "Thank you for sending your resume and for your interest in joining Biotech Farms IT Team. As we reviewed your profile," +
         "we are regretful to inform you that your application was not selected for further consideration due to LIVESTOCK BIOSECURITY as per Company Policy.<br/><br/>" +
         "However, we are excited to inform you as well that your application will be forwarded to our Company Affiliates for further review and assessment.<br/><br/>" +
         "Thank you for your interest in Biotech Farms, Always Better – Improving Lives, Beyond World-Class Products. Better wishes on your job search.<br/><br/>" +
         "Regards,<br/><br/>" +
         "Biotech Farms Inc." +
         "<br/><br/>" +
         "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
         "This mailbox does not allow incoming messages.</div>"
      }

      await sendEmail(mail)
               .then(res => {
                  emailResult = res
               })
               .catch(err => console.log(err))

      return emailResult
   }
}

module.exports = blacklistEmail