require('dotenv').config()

const keptForReferenceEmail = ({ decrypt, userDb, sendEmail }) => {
   return async function send({applicantID} = {}) {
      let firstName, lastName, email, emailResult

      await userDb.find(applicantID)
                  .then(res => {
                     firstName = decrypt(res.rows[0].firstname)
                     lastName = decrypt(res.rows[0].lastname)
                     email = decrypt(res.rows[0].email)
                  })

      let mail = {
         from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
         to: email,
         subject: "BFI : Application Status - Kept for Reference",
         html:
         `Dear ${firstName} ${lastName}, <br/><br/>` +
         "We appreciate your interest in applying at Biotech Farms Inc.<br/><br/> " +
         "We would like to note that competition for jobs at Biotech Farms Inc. is always strong and that we  <br/>"+
         "often have to make difficult choices between many high-caliber candidates. Since we've gotten the  <br/>"+
         "opportunity to know more about you, we will keep your application for future openings that better fit your profile.<br/><br/>" +
         "Thank you for your interest for applying in Biotech Farms, Always Better – Improving Lives, Beyond World-Class Products. Better wishes on your pursuit of employment.<br/><br/>" +
         "Regards,<br/><br/>" +
         "Biotech Farms Inc." +
         "<br/><br/>" +
         "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
         "This mailbox does not allow incoming messages.</div>"
      }

      await sendEmail(mail)
               .then(res => {
                  emailResult = res
               })

      return emailResult
   }
}

module.exports = keptForReferenceEmail