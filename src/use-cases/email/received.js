const { restart } = require('nodemon')

require('dotenv').config()

const receivedEmail = ({decrypt, userDb, sendEmail}) => {
   return async function send({applicantID}){
      let firstName, lastName, email, position, emailResult, dhEmailResult = [], positionID, dhFirstName, dhLastName, dhEmail

      await userDb.find(applicantID)
                  .then(res => { 
                     firstName = decrypt(res.rows[0].firstname)
                     lastName = decrypt(res.rows[0].lastname)
                     email = decrypt(res.rows[0].email)
                     positionID = (res.rows[0].position_id === null) ? '' : res.rows[0].position_id
                  })

      if (positionID.trim().length !== 0) {

         await userDb.getPositionName(positionID)
                     .then(res => {
                        position = res.rows[0].name
                     })

         await userDb.fetchDepartmentHeadDetailsBy(positionID)
                     .then(async (res) => {
                        for (let departmentHead of res.rows) {
                           dhFirstName = decrypt(departmentHead.firstname)
                           dhLastName = decrypt(departmentHead.lastname)
                           dhEmail = decrypt(departmentHead.email)
                        }

                        let emailForDeptHead = {
                           from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
                           to: dhEmail,
                           subject: "BFI : New Applicant Received",
                           html:
                              // `Dear ${dhFirstName} ${dhLastName}, <br/><br/>` +
                              `A new application to your department from ${firstName} ${lastName} for the position of ${position} has been received.` +
                              "<br/><br/>" +
                              "Regards,<br/><br/>" +
                              "Biotech Farms Inc." +
                              "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
                              "This mailbox does not allow incoming messages.</div>"
                           }
               
                        await sendEmail(emailForDeptHead)
                        .then(res => {
                           dhEmailResult.push(res)
                        })
         })
         
      }

      let emailForApplicant = {
         from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
         to: email,
         subject: "BFI : Application Status - Received",
         html:
           `Dear ${firstName} ${lastName}, <br/><br/>` +
           "This letter is to let you know that we have received your application. We appreciate your interest in Biotech Farms Inc. " +
           `the position of ${position} for which you applied. We are reviewing applications currently. If you are selected to take the online exam, ` +
           "expect to receive a separate email with the link of the online exam on the next business day. Otherwise, you will still receive updates on your application status.<br/><br/> " +
           "We are grateful for the time you spent applying in Biotech Farms, Always Better – Improving Lives, Beyond World-Class Products.<br/><br/> " +
           "Regards,<br/><br/>" +
           "Biotech Farms Inc." +
           "<br/><br/>" +
           "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
           "This mailbox does not allow incoming messages.</div>"
      }

      await sendEmail(emailForApplicant)
               .then(res => {
                  emailResult = res
               })

      return {
         emailResult,
         dhEmailResult
      }
   }
}

module.exports = receivedEmail