require('dotenv').config()

const assessmentFormEmail = ({ decrypt, userDb, sendEmail, moment }) => {
   return async function send({ applicantID, date, link, name, contact, location } = {}) {
      let firstName, lastName, email, emailResult, password, dates = moment(date).format("ddd, MMM DD, YYYY hh:mm A")
      
      await userDb.find(applicantID)
                  .then(res => {
                     firstName = decrypt(res.rows[0].firstname)
                     lastName = decrypt(res.rows[0].lastname)
                     email = decrypt(res.rows[0].email)
                     password = decrypt(res.rows[0].password)
                  })

      let mail = {
         from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
         to: email,
         subject:
            "BFI : Application Status - Fill up Application form for Assessment",
         html:
            `Dear ${firstName} ${lastName}, <br/><br/>` +
            "Thank you for sending your resume and for your interest in joining Biotech Farms IT Team. " +
            `Please look for ${name} (${contact}) at ${location} on ${dates} for your assessment.<br/>` +
            "To proceed on the next step please fill up our application form, please follow the instructions below.<br/><br/>" +
            `To login, just click this link: ${link} and use the following credentials:<br/><br/>` +
            `<b>Email: </b> ${email}<br/>` +
            `<b>Password: </b> ${password}<br/><br/>` +
            `<i>** <b>Important Reminder</b>: You will also use this credentials for the IQ and Behavioral Exam.</i><br/>` +
            `<i>&nbsp;&nbsp;&nbsp;&nbsp;Please print a copy of your application form and bring it during the assessment.</i>` +
            "<br/><br/>" +
            "Regards,<br/><br/>" +
            "Biotech Farms Inc." +
            "<br/><br/>" +
            "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
            "This mailbox does not allow incoming messages.</div>"
      }

      await sendEmail(mail)
               .then(res => {
                  emailResult = res
               })

      return emailResult
   }
}

module.exports = assessmentFormEmail