require('dotenv').config()

const returnToReceivedEmail = ({ decrypt, userDb, sendEmail }) => {
   return async function send({ applicantID } = {}) {
      let firstName, lastName, email, position, emailResult
      
      await userDb.find(applicantID)
                  .then(res => {
                     firstName = decrypt(res.rows[0].firstname)
                     lastName = decrypt(res.rows[0].lastname)
                     email = decrypt(res.rows[0].email)
                  })

      await userDb.fetchDepartmentTeamPositionBy(applicantID)
                  .then(res => {
                     position = res.rows[0].position
                  })
      
      let mail = {
         from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
         to: email,
         subject: "BFI : Application Status - Job Offer",
         html:
            `Dear ${firstName} ${lastName}, <br/><br/>` +
   
            `Greetings from Biotechfarms Inc.<br>
            
            We're glad to invite you to apply as ${position}. Please contact us at 09976688673 for confirmation so we can send you the online exam link. Thank you. <br>
            <br>
            <div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. <br>
            This mailbox does not allow incoming messages.</div> `
      }

      await sendEmail(mail)
               .then(res => {
                  emailResult = res
               })

      return emailResult

   }
}

module.exports = returnToReceivedEmail