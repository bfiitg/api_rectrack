require('dotenv').config()

const endorsedToOtherDepartment = ({decrypt,userDb,sendEmail}) => {
   return async function send({applicantID, endorserID, positionID}) {
      let applicantFirstName, applicantLastName, endorserFirstName, endorserLastName, dhEmail, dhEmailResult, dhFirstName, dhLastName, ohEmailResult, departmentIdsArray = [], deptIDs, position, department, ccEmails = []

      await userDb.find(applicantID)
                  .then(res => {
                     applicantFirstName = decrypt(res.rows[0].firstname)
                     applicantLastName = decrypt(res.rows[0].lastname)
                     applicantEmail = decrypt(res.rows[0].email)
                     applicantPositionID = decrypt(res.rows[0].position_id)
                  })

      await userDb.getPositionName(positionID)
                  .then(res => {
                     position = res.rows[0].name
                  })

      await userDb.find(endorserID)
                  .then(res => {
                     endorserFirstName = decrypt(res.rows[0].firstname)
                     endorserLastName = decrypt(res.rows[0].lastname)
                     endorserEmail = decrypt(res.rows[0].email)
                     department = res.rows[0].department
                  })

      await userDb.fetchDepartmentTeamPositionBy(endorserID)
                  .then(res => {
                     department = res.rows[0].department
                     departmentIdsArray.push(res.rows[0].departmentid)
                  })

      await userDb.fetchDepartmentHeadDetailsBy(positionID)
                  .then(async (res) => {

                     for (row of res.rows) {

                        dhFirstName = decrypt(row.firstname)
                        dhLastName = decrypt(row.lastname)
                        dhEmail = decrypt(row.email)

                        let departmentMail = {
                           from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
                           to: dhEmail,
                           subject: "BFI : Applicant Endorsement",
                           html: `Dear ${dhFirstName} ${dhLastName}, <br/><br/> Application from <b>${applicantFirstName} ${applicantLastName}</b> for the position of ${position} has been endorsed to ${department} Department by <b>${endorserFirstName} ${endorserLastName}</b>.<br>
                           <br><br>Regards,<br>
                           Biotech Farms Inc.
                           <br/><br/>
                           <div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email.
                           This mailbox does not allow incoming messages.</div>`
                        }
                        
                  
                        await sendEmail(departmentMail)
                        .then(res => {
                           dhEmailResult = res
                        })
                     }
                  })
      
      await userDb.fetchPGDetails()
                  .then(res => {
                     for (row of res.rows) {
                        departmentIdsArray.push(row.id)
                     }
                  })

      deptIDs = departmentIdsArray.join()

      await userDb.getOperationsHeadEmail(deptIDs)
                  .then(res => {
                     for(opHead of res.rows) {
                        ccEmails.push(decrypt(opHead.email))
                     }
                  })
      
      let ohEmail = {
         from: `Biotech Farms Inc.`,
         to: ccEmails.join(),
         subject: 'BFI : Applicant Deployment',
         html: 
         `Dear Sir/Ma'am, <br><br>
         <b>${endorserFirstName} ${endorserLastName} endorsed ${applicantFirstName} ${applicantLastName} to the ${department} department for the position of ${position}.</b><br/> 
         <br><br>Regards,<br>
         Biotech Farms Inc.
         <br/><br/>
         <div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. 
         This mailbox does not allow incoming messages.</div>`
      }

      await sendEmail(ohEmail)
               .catch(err => console.log(err))
      
      
      return "Successfully endorsed applicant."
   }
}

module.exports = endorsedToOtherDepartment