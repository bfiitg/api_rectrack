require('dotenv').config()

const shortlistedEmail = ({ decrypt, userDb, sendEmail }) => {
   return async function send({applicantID}) {
      let firstName, lastName, email, emailResult
      await userDb.find(applicantID)
                  .then(res => {
                     firstName = decrypt(res.rows[0].firstname)
                     lastName = decrypt(res.rows[0].lastname)
                     email = decrypt(res.rows[0].email)
                  })

      let mail = {
         from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
         to: email,
         subject: "BFI : Application Status - Shortlisted",
         html:
           `Dear ${firstName} ${lastName}, <br/><br/>` +
           "<b>CONGRATULATIONS</b><br/>" +
           "You have passed the actual technical assessment and your resume will be forwarded to " +
           "the management for final evaluation. Expect to receive a text message or call within a few days.<br/>" +
           "We are grateful for the time you spent applying in Biotech Farms, Always Better – Improving Lives, Beyond World-Class Products.<br/><br/>" +
           "Regards,<br/><br/>" +
           "Biotech Farms Inc." +
           "<br/><br/>" +
           "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
           "This mailbox does not allow incoming messages.</div>"
      }

      await sendEmail(mail)
               .then(res => {
                  emailResult = res
               })

      return emailResult
   }
}

module.exports = shortlistedEmail