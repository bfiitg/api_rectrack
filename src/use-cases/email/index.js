const { sendEmail } = require('./nodemailer'),
      { encrypt, decrypt, generateString } = require('../../entities/users/app'),
      userDb = require('../../data-access/users/app'),
      moment = require('moment'),
      { makeToken } = require('../../token/app')

const assessmentEmail = require('./assessment'),
      blacklistEmail = require('./blacklist'),
      deployedEmail = require('./deployed'),
      jobOfferEmail = require('./job-offer'),
      keptForReferenceEmail = require('./kept-for-reference'),
      onlineExamEmail = require('./online-exam'),
      returnToReceivedEmail = require('./return-to-received'),
      rejectedEmail = require('./rejected'),
      shortlistedEmail = require('./shortlist'),
      userCredentialsEmail = require('./user-credentials'),
      assessmentFormEmail = require('./assessment-form'),
      receivedEmail = require('./received'),
      endorsedToOtherDepartment = require('./endorsed-to-other-dept'),
      reapplyApplicantEmail = require('./reapply-applicant'),
      forgotPasswordEmail = require('./forgot-password'),
      examRetakeEmail = require('./exam-retake')

const assessmentEmail_USE = assessmentEmail({decrypt, userDb, sendEmail}),
      blacklistEmail_USE = blacklistEmail({decrypt, userDb, sendEmail}),
      deployedEmail_USE = deployedEmail({decrypt, userDb, sendEmail, moment}),
      jobOfferEmail_USE = jobOfferEmail({decrypt, userDb, sendEmail}),
      keptForReferenceEmail_USE = keptForReferenceEmail({decrypt, userDb, sendEmail}),
      onlineExamEmail_USE = onlineExamEmail({decrypt, userDb, sendEmail}),
      returnToReceivedEmail_USE = returnToReceivedEmail({decrypt, userDb, sendEmail}),
      rejectedEmail_USE = rejectedEmail({decrypt, userDb, sendEmail}),
      shortlistedEmail_USE = shortlistedEmail({decrypt, userDb, sendEmail}),
      userCredentialsEmail_USE = userCredentialsEmail({decrypt, userDb, sendEmail}),
      assessmentFormEmail_USE = assessmentFormEmail({decrypt, userDb, sendEmail, moment}),
      receivedEmail_USE = receivedEmail({decrypt, userDb, sendEmail}),
      endorsedToOtherDepartment_USE = endorsedToOtherDepartment({decrypt,userDb,sendEmail}),
      reapplyApplicantEmail_USE = reapplyApplicantEmail({decrypt,userDb, sendEmail}),
      forgotPasswordEmail_USE = forgotPasswordEmail({encrypt, decrypt, userDb, sendEmail, makeToken}),
      examRetakeEmail_USE = examRetakeEmail({decrypt, userDb, sendEmail})

const emailService = Object.freeze({
   assessmentEmail_USE,
   blacklistEmail_USE,
   deployedEmail_USE,
   jobOfferEmail_USE,
   keptForReferenceEmail_USE,
   onlineExamEmail_USE,
   returnToReceivedEmail_USE,
   rejectedEmail_USE,
   shortlistedEmail_USE,
   userCredentialsEmail_USE,
   assessmentFormEmail_USE,
   receivedEmail_USE,
   endorsedToOtherDepartment_USE,
   reapplyApplicantEmail_USE,
   forgotPasswordEmail_USE,
   examRetakeEmail_USE
})

module.exports = emailService
module.exports = {
   assessmentEmail_USE,
   blacklistEmail_USE,
   deployedEmail_USE,
   jobOfferEmail_USE,
   keptForReferenceEmail_USE,
   onlineExamEmail_USE,
   returnToReceivedEmail_USE,
   rejectedEmail_USE,
   shortlistedEmail_USE,
   userCredentialsEmail_USE,
   assessmentFormEmail_USE,
   receivedEmail_USE,
   endorsedToOtherDepartment_USE,
   reapplyApplicantEmail_USE,
   forgotPasswordEmail_USE,
   examRetakeEmail_USE
}