require('dotenv').config()

const forgotPasswordEmail = ({encrypt, decrypt, userDb, sendEmail, makeToken}) => {
   return async function send({email, baseUrl}) {
      let firstName, password, emailResult, token, applicantID

      await userDb.findByEmail(encrypt(email))
                  .then(res => {
                     applicantID = res.rows[0].id
                     firstName = decrypt(res.rows[0].firstname)
                     password = res.rows[0].password
                  })
                  .catch(err => {
                     throw new Error("Email doesn't exist.")
                  })

      token = makeToken(encrypt(email), password)

      let mail = {
         from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
         to: email,
         subject: "BFI : RecTrack Forgot Password",
         html: `Hi ${firstName},<br/><br/>
         You have requested to reset your password for your account on RecTrack System. Please click the button to reset it.<br/><br/>
 
         <a href="${baseUrl}?token=${token}&id=${applicantID}" target="_blank"
         style="background: green; color: white; padding: 10px;">Reset</a>
 
         <br/><br/>
         Regards,<br/><br/>
         Biotech Farms Inc.
         <br/><br/>
         <div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email.
         This mailbox does not allow incoming messages.</div>`,
      }

      await sendEmail(mail)
               .then(res => {
                  emailResult = res
               })

      return emailResult
   }
}

module.exports = forgotPasswordEmail