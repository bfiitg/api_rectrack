require("dotenv").config();
const nodemailer = require("nodemailer");
let transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: process.env.EMAIL_PORT,
  auth: {
    user: process.env.EMAIL_USER,
    pass: process.env.EMAIL_PASS
  },
  pool: true
});
async function sendEmail(mailOptions) {
  const result = await new Promise((resolve) => {
    transporter.sendMail(mailOptions, (err, data) => {
      let msg;
      console.log("mail result: ", data);
      if (err) {
        msg = {
          status: 400,
          data: `Not sent ${err}`,
        };
        resolve(msg);
      }
      msg = {
        status: 200,
        data: "Sent successfully",
      };
      resolve(msg);
    });
  });
  
  return result;
}
module.exports = { sendEmail };