require('dotenv').config()

const jobOfferEmail = ({ decrypt, userDb, sendEmail }) => {
   return async function send({ applicantID, date, time, location, contactPerson, contactNumber } = {}){
      let firstName, lastName, email, emailResult

      await userDb.find(applicantID)
                  .then(res => {
                     firstName = decrypt(res.rows[0].firstname)
                     lastName = decrypt(res.rows[0].lastname)
                     email = decrypt(res.rows[0].email)
                  })

      let mail = {
         from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
         to: email,
         subject: "BFI : Application Status - Job Offer",
         html:
         `Dear ${firstName} ${lastName}, <br/><br/>` +

         "Greetings from Biotechfarms Inc.<br>" +
         `Maam/Sir, please be informed that you will have your job offer on ` +
         `${date}; ${time} at  ${location}. Please look for ${contactPerson}. <br><br>` +
         `Kindly reply your complete name for confirmation of your attendance on this number: ${contactNumber}.<br><br>` +
         `Note: List of requirements will also be given. Thank you and God bless.` +
         "<br/><br/>" +
         "Regards,<br/><br/>" +
         "Biotech Farms Inc." +
         "<br/><br/>" +
         "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
         "This mailbox does not allow incoming messages.</div>"
      }

      await sendEmail(mail)
               .then(res => {
                  emailResult = res
               })

      return emailResult
   }
}

module.exports = jobOfferEmail