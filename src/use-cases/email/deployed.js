require('dotenv').config()

const deployedEmail = ({ decrypt, userDb, sendEmail, moment }) => {
   return async function send({ applicantID, date, contactNumber, dateOrientation, dateDeadline, location, time} = {}) {
      let firstName, lastName, email, applicantEmailResult, positionID, departmentIdsArray = [], deptIDs, ccEmails = [], department, position

      await userDb.find(applicantID)
                  .then(res => {
                     firstName = decrypt(res.rows[0].firstname)
                     lastName = decrypt(res.rows[0].lastname)
                     email = decrypt(res.rows[0].email)
                     positionID = res.rows[0].position_id
                  })

      await userDb.fetchDepartmentTeamPositionBy(applicantID)
                  .then(res => {
                     department = res.rows[0].department
                     departmentIdsArray.push(res.rows[0].departmentid)
                     position = res.rows[0].position
                  })

      let mail = {
         from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
         to: email,
         subject: "BFI : Application Status - For Deployment",
         html: `Dear ${firstName} ${lastName}, <br/><br/>` +
           `Greetings from Biotech Farms Inc.<br/><br/>` +
           `Congratulations!<br/><br/>` +
           `Your deployment date will be on: ${date} <br/>` +
           "Please see list of requirements:<br/><br/>" +
           "1. HEPA TEST - please feedback the result to me if you are done.<br/>" +
           "2. Xray, urinalysis, facalysis result<br/>" +
           "3. DRUG TEST-JCD only(Beside LTO Koronadal)<br/>" +
           "4. Cedulla - photocopy<br/>" +
           "5. Brgy. Clearance - original and photocopy<br/>" +
           "6. Police clearance<br/>" +
           "7. Certificate of Employment-previous job<br/>" +
           "8. TOR-photocopy<br/>" +
           "9. MDR/PHilhealth ID -3 copies(If you have no philhealth,<br/>" +
           "we will be the one to process)<br/>" +
           "10. PAGIBIG mid number-3 copies<br/>" +
           "11. SSS number-3 copies<br/>" +
           "12. Tin number-3 copies<br/>" +
           "13. Birth certificate-3 copies<br/>" +
           "14. 2x2 and 1x1 pic-2 copies each<br/>" +
           "15. Health card<br/>" +
           "16. Occupational Tax<br/>" +
           "17. Bind your requirements in green expanding envelope<br/><br/>" +
           `*If the hepa test result is reactive please call this number ${contactNumber}. If not, you can proceed with the rest of the requirements. <br/>` +
           ` Note: Please finish before ${dateDeadline}. Orientation will be on ${dateOrientation} <br/>` +
           `at ${location} ${time}. Wear close shoes, pants and tshirt. Thank you.<br/><br/>` +
           "Regards,<br/><br/>" +
           "Biotech Farms Inc." +
           "<br/><br/>" +
           "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
           "This mailbox does not allow incoming messages.</div>"
      }
      await sendEmail(mail)
               .then(res => {
                  applicantEmailResult = res
               })

      //temporarily disabled emailing of department head as that functionality is not yet implemented in the frontend
      let dhEmail, dhFirstName, dhLastName, formatDate = moment(date).format("LL");

      await userDb.fetchDepartmentHeadDetailsBy(positionID)
                  .then(async (res) => {

                     for(let row of res.rows) {
                        dhEmail = decrypt(row.email)
                        dhFirstName = decrypt(row.firstname)
                        dhLastName = decrypt(row.lastname)

                        let dhMail = {
                           from: `Biotech Farms Inc.`,
                           to: dhEmail,
                           subject: "BFI : Applicant Deployment",
                           html: `Dear ${dhFirstName} ${dhLastName}, <br/><br/>` +
                                 `<b>${firstName} ${lastName}</b> will be officially deployed to ${department} Department on <b>${formatDate}</b>.<br/>` +
                                 "<br><br>Regards,<br>" +
                                 "Biotech Farms Inc." +
                                 "<br/><br/>" +
                                 "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
                                 "This mailbox does not allow incoming messages.</div>"
                        }
                  
                        await sendEmail(dhMail)
                                 .catch(err => console.log(err))
                     }
                  })

      await userDb.fetchPGDetails()
                  .then(res => {
                     for (row of res.rows) {
                        departmentIdsArray.push(row.id)
                     }
                  })

      deptIDs = departmentIdsArray.join()

      await userDb.getOperationsHeadEmail(deptIDs)
                  .then(res => {
                     for(let opHead of res.rows) {
                        ccEmails.push(decrypt(opHead.email))
                     }
                  })
                  
      let ohEmail = {
         from: `Biotech Farms Inc.`,
         to: ccEmails.join(),
         subject: 'BFI : Applicant Deployment',
         html: 
         ` Dear Sir/Ma'am, <br><br>
         <b>${firstName} ${lastName} will be officially deployed to the ${department} department for the position of ${position} on ${formatDate}.</b><br/> 
         <br><br>Regards,<br>
         Biotech Farms Inc.
         <br/><br/>
         <div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email.
         This mailbox does not allow incoming messages.</div>`
      }

      sendEmail(ohEmail)
               .catch(err => console.log(err))

      return applicantEmailResult
   }
}

module.exports = deployedEmail