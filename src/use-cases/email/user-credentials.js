require('dotenv').config()

const userCredentialsEmail = ({ decrypt, userDb, sendEmail }) => {
   return async function send({applicantID, link} = {}) {
      let firstName, lastName, email, emailResult
      await userDb.find(applicantID)
                  .then(res => {
                     firstName = decrypt(res.rows[0].firstname)
                     lastName = decrypt(res.rows[0].lastname)
                     email = decrypt(res.rows[0].email)
                     password = decrypt(res.rows[0].password)
                  })

      let mail = {
         from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
         to: email,
         subject: "BFI RecTrack Account Credentials",
         html:
         `Dear ${firstName} ${lastName}, <br/><br/>` +
         "You can access the Biotech Recruitement Tracking System (RecTrack) <br/><br/> " +
         `just click this link: ${link} using the following credentials.<br/><br/>` +
         `<b>Email: </b> ${email}<br/>` +
         `<b>Password: </b> ${password}<br/><br/>` +
         "Regards,<br/><br/>" +
         "IT Team<br/>" +
         "Biotech Farms Inc."
      }

      await sendEmail(mail)
               .then(res => {
                  emailResult = res
               })

      return emailResult
   }
}

module.exports = userCredentialsEmail