require('dotenv').config()

const reapplyApplicantEmail = ({decrypt,userDb, sendEmail}) => {
   return async function send({applicantID}) {

      let firstName, lastName, email, emailResult
      await userDb.find(applicantID)
                  .then(res => {
                     firstName = decrypt(res.rows[0].firstname)
                     lastName = decrypt(res.rows[0].lastname)
                     email = decrypt(res.rows[0].email)
                  })

      let mail = {
         from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
         to: email,
         subject: "BFI : Application Status - Received",
         html:
          `Dear ${firstName} ${lastName}, <br/><br/>` +
          "Your application has been reprocessed and has been received again. " +
          "Please check your email from time to time for updates.<br/><br/>" +
          "Regards,<br/><br/>" +
          "Biotech Farms Inc." +
          "<br/><br/>" +
          "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
          "This mailbox does not allow incoming messages.</div>",
      }

      await sendEmail(mail)
               .then(res => {
                  emailResult = res
               })

      return emailResult
   }
}

module.exports = reapplyApplicantEmail