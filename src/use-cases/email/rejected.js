require('dotenv').config()

const rejectedEmail = ({ decrypt, userDb, sendEmail }) => {
   return async function send({ applicantID }) {
      let firstName, lastName, email, emailResult
      await userDb.find(applicantID)
                  .then(res => {
                     firstName = decrypt(res.rows[0].firstname)
                     lastName = decrypt(res.rows[0].lastname)
                     email = decrypt(res.rows[0].email)
                  })

      let mail = {
         from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
         to: email,
         subject: "BFI : Application Status - Rejected",
         html:
         `Dear ${firstName} ${lastName}, <br/><br/>` +
         "We appreciate your interest in applying at Biotech Farms Inc and the time you’ve invested.<br/><br/>" +
         "Unfortunately, our team did not select you for further consideration.<br/><br/>" +
         "We would like to note that competition for jobs at Biotech Farms Inc. is always strong and that we often <br/>"+
         "have to make difficult choices between many high-caliber candidates.<br/><br/>"+
         "Thank you for your enthusiasm for Biotech Farms, Always Better – Improving Lives, Beyond World-Class Products. Better wishes on your pursuit of employment.<br/><br/>" +
         "Regards,<br/><br/>" +
         "Biotech Farms Inc." +
         "<br/><br/>" +
         "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
         "This mailbox does not allow incoming messages.</div>"
      }

      await sendEmail(mail)
               .then(res => {
                  emailResult = res
               })

      return emailResult
   }
}

module.exports = rejectedEmail