require('dotenv').config()

const examRetakeEmail = ({decrypt, userDb, sendEmail}) => {
   return async function send({applicantID, onlineExamLink, iqbeExamLink} = {}) {
      try {
         
      let firstName, lastName, email, emailResult, password, deptName
      await userDb.find(applicantID)
                  .then(res => {
                     firstName = decrypt(res.rows[0].firstname)
                     lastName = decrypt(res.rows[0].lastname)
                     email = decrypt(res.rows[0].email)
                     password = decrypt(res.rows[0].password)
                  })
      
      await userDb.fetchDepartmentTeamPositionBy(applicantID)
                  .then(res => {
                     deptName = res.rows[0].department
                  })

      let mail = {
         from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
         to: email,
         subject: "BFI : Application Status - Online Exam Retake",
         html:
         `Dear ${firstName} ${lastName}, <br/><br/>` +
         `Thank you for sending your resume and for your interest in joining ${deptName}. ` +
         "To proceed on the next step (ONLINE EXAM), please follow the instructions below. Good luck and God bless.<br/><br/>" +
         `To login, just click this link: ${onlineExamLink} and use the following credentials:<br/><br/>` +
         `For IQ Test, just click this link: ${iqbeExamLink} and use the the same credentials:<br/><br/>` +
         `<b>Email: </b> ${email}<br/>` +
         `<b>Password: </b> ${password}<br/><br/>` +
         "Note: Please take the exam within 24 hours upon receipt of this mail.<br/><br/>" +
         "Regards,<br/><br/>" +
         "Biotech Farms Inc."+
         "<br/><br/>" +
         "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
         "This mailbox does not allow incoming messages.</div>"
      }

      await sendEmail(mail)
               .then(res => {
                  emailResult = res
               })

      return emailResult
      } catch (error) {
         console.log(error)
      }
   }
}

module.exports = examRetakeEmail