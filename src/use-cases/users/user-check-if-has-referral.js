const referralCheckIfExist = ({ userDb }) => {
  return async function selects(id) {
    const result = await userDb.checkIfHasReferral(id);
    return result.rowCount;
  };
};

module.exports = referralCheckIfExist;
