const { decrypt } = require("../../entities/users/app");

const findApplicantById = ({ userDb, getApplicantNames, interviewDb }) => {
   return async function find({ info }) {
      let message, applicantData, applicantInformation = [] 
      
      if (!info.applicantID) {
         throw new Error("Please provide a valid applicant id.")
      }

      try {
         const applicantCheck = await userDb.find(info.applicantID), applicantCheckResults = applicantCheck.rows

         if (applicantCheck.rowCount == 0) {
            throw new Error("Applicant doesn't exist.")
         }
         
         switch (info.role) {
            case "Administrator":
            case "People Group":
               applicantData = (applicantCheckResults[0].position_id !== null) ? await userDb.findApplicantById(info.applicantID) : await userDb.findApplicantWithoutPositionById(info.applicantID)
               break;
            case "Department Head":
            case "Operations Head":
               applicantData = await userDb.findApplicantById(info.applicantID, info.deptID)
               break;
            case "Team Head":
               applicantData = await userDb.findApplicantById(info.applicantID, info.teamID)
               break;
         }

         let applicant = applicantData.rows,
             applicationForm,
             applicantIQBEResults,
             applicantOnlineExamResults,
             assessmentResult,
             firstName = decrypt(applicant[0].firstname),
             lastName = decrypt(applicant[0].lastname),
             fullName = (applicant[0].middlename) ? `${firstName} ${decrypt(applicant[0].middlename)} ${lastName}` : `${firstName} ${lastName}`,
             email = decrypt(applicant[0].email),
             applicationDate = (applicant[0].date_application) ? applicant[0].date_application :  applicant[0].resume_upload_date,
             assessmentInformation = []

            
         await interviewDb.viewAssessmentOfEmployee(info.applicantID)
                           .then(res => {
                              assessmentResult = res.rows
                           })
                           .catch(err => console.log(err))

         await getApplicantNames(applicant[0].token)
                  .then(res => {
                     applicationForm = (res) ? res : ""
                  })
                  .catch(err => console.log(err))

         await userDb.iqbeExamResult(info.applicantID)
                     .then(res => applicantIQBEResults = (res.rowCount > 0) ? res.rows : "Applicant has no IQBE result.")
                     .catch(err => console.log(err))

         await userDb.onlineExamResult(info.applicantID, applicant[0].positionid)
                     .then(res => {
                        applicantOnlineExamResults = (res.rowCount > 0) ? res.rows : "Applicant has no online exam result."})
                     .catch(err => console.log(err))

      
         for (let assessmentInfo of assessmentResult) {
            let interviwerFirstName = decrypt(assessmentInfo.interviewerfn),
               interviwerLastName = decrypt(assessmentInfo.interviewerln)

            assessmentInformation.push({
               interviewerID: assessmentInfo.user_id_interviewer,
               interviewer: `${interviwerFirstName} ${interviwerLastName}`,
               interviewDate: assessmentInfo.date_interview,
               communicationSkills: assessmentInfo.communication_skills,
               communicationSkillsNote: assessmentInfo.communication_skills_note,
               confidence: assessmentInfo.confidence,
               confidenceNote: assessmentInfo.confidence_note,
               physicalAppearance: assessmentInfo.physical_appearance,
               physicalAppearanceNote: assessmentInfo.physical_appearance_note,
               knowledgeSkills: assessmentInfo.knowledge_skills,
               knowledgeSkillsNote: assessmentInfo.knowledge_skills_note,
               askingRate: assessmentInfo.asking_rate,
               availability: assessmentInfo.availability,
               others: assessmentInfo.others,
               generalRemarksRecommendation: assessmentInfo.general_remarks_recommendation
            })
         }
         
         applicantInformation.push({
            id: info.applicantID,
            name: fullName,
            stage: applicant[0].stage,
            livestock: applicant[0].livestock,
            willingToTravel: applicant[0].willing_to_travel,
            positionID: applicant[0].positionid,
            position: applicant[0].positionname,
            email: email,
            mobile: applicant[0].mobile,
            team: applicant[0].teamname,
            department: applicant[0].departmentname,
            applicationDate: applicationDate,
            onlineExamStartDate: applicant[0].online_exam_start,
            onlineExamEndDate: applicant[0].online_exam_end,
            onlineExamResult: applicantOnlineExamResults,
            iqbeResult: applicantIQBEResults,
            assessmentInformation,
            applicationForm,
            endorsementDate: applicant[0].date_endorsement,
            assessmentDate: applicant[0].date_assessment,
            deploymentDate: applicant[0].date_deployed,
            resumeUrl: applicant[0].resume_upload_url,
            runningTime: applicant[0].running_time
         })
         message = "Successfully fetched applicant information"
      } catch (error) {
         console.log(error)
         return message = "Failed to fetch applicant information."
      }
      return {applicantInformation, message}
   };
};

module.exports = findApplicantById;