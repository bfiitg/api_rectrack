const userChangeStatusDeployed = ({
  userDb
}) => {
  return async function puts({
    id,
    date
  } = {}) {

    if (!date || date == "Invalid date") {
      throw new Error("Please enter a valid date for deployement.");
    }


    const update = await userDb.changeStatusDeployed(id, date)

    let result = (update.rowCount > 0) ? "Update successful." : "Update failed."

    return {
      result
    };
  };
};

module.exports = userChangeStatusDeployed;