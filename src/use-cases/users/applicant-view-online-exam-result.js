const { decrypt } = require("../../entities/users/app");

const applicantOnlineExamResult = ({ userDb }) => {
  return async function get(id, whichExam, posid) {
    let users;
    let users2;
    
    if (whichExam === "IQBE") {
      users = await userDb.iqbeExamResult(id);
      users2 = await userDb.IQTestResultQTYPE(id)
    } else {
      users = await userDb.onlineExamResult(id, posid);
      users2 = await userDb.onlineExamResultQTYPE(id,posid)

    }

    const result2 = users2.rows;
    const result2s= [];
    

    let userIdQtype = ""; // id of applicant
    let applicantFnameQtype = ""; // applicant's first name
    let applicantLnameQtype = ""; // applicant's last name
    // let question_type=""
    for(let e = 0; e<result2.length;e++){
      const i = result2[e];

      userIdQtype = i.user_id;
      applicantFnameQtype = decrypt(i.firstname);
      applicantLnameQtype = decrypt(i.lastname);

      

      result2s.push({
        us_id:i.id,
        catgname: i.catgname,
        questions:i.questions,
        user_answer: i.user_answer,
        score:i.is_correct,
        question_type:i.question_type
      });
    }
    const data2 = {
      userIdQtype,
      applicantFnameQtype,
      applicantLnameQtype,
      result2s
    };
    

    const result = users.rows;
    const results = [];
   
    let userId = ""; // id of applicant
    let applicantFname = ""; // applicant's first name
    let applicantLname = ""; // applicant's last name
    for (let i = 0; i < result.length; i++) {
      const e = result[i];
      userId = e.user_id;
      applicantFname = decrypt(e.firstname);
      applicantLname = decrypt(e.lastname);


      results.push({
        catgname: e.catgname,
        correctanswer: e.correctanswer,
        wronganswer: e.wronganswer,
       
      });
    }
    const data = {
      userId,
      applicantFname,
      applicantLname,
      results
    };
   
    return {data,data2}
  };
};

module.exports = applicantOnlineExamResult;
