const { decrypt } = require("../../entities/users/app");
const fs = require("fs");

const adminExportCSVJeonsoft = ({ userDb, papa }) => {
  return async function posts(userInfo) {
    const array = userInfo.data;
    const path = "./jeonsoft";

    const date_ob = new Date();
    // current date
    const date = ("0" + date_ob.getDate()).slice(-2);

    // current month
    const month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

    // current year
    const year = date_ob.getFullYear();

    // current hours
    const hours = date_ob.getHours();

    // current minutes
    const minutes = date_ob.getMinutes();

    const fileExt =
      year + "-" + month + "-" + date + " " + hours + "." + minutes;
    // check if valid path or not; true if exists false if not
    // if directory doesn't exits.. make it..
    await fs.access(path, fs.F_OK, err => {
      if (err) {
        // file doesn't exist
        // so write new file
        fs.mkdir(path, err => {
          if (err) {
            console.log(err.message);
          } else {
            console.log("Make the folder..");
          }
        });
        return;
      }
    });

    // check first if there is no empty employee code
    // return error if there is empty
    for (let i = 0; i < array.length; i++) {
      const e = array[i].EmployeeCode;
      if (!e) {
        const err = {
          msg: "Please fill up all employee code."
        };
        return err;
      }
    }

    // then if there is no empty query from db to get the data
    // to be exported

    for (let i = 0; i < array.length; i++) {
      const e = array[i];
      const employeeCode = e.EmployeeCode;
      const id = e.id;

      // EmployeeAddresses
      // EmployeeEmergecyContact
      // EmployeeMobileNumbers
      // Employees
      const data = await userDb.getEmployeeAddressContactEmergency(id);
      const employeeArray = data.rows;
      employeeArray.push({ employeeCode: employeeCode });
      // EmployeeCertifications
      const certificate = await userDb.getEmployeeCertifications(id);
      const certificateArray = certificate.rows;

      // EmployeeEducation
      const education = await userDb.getEmployeeEducation(id);
      const educationArray = education.rows;

      // EmployeeFamilyBackground
      const family = await userDb.getEmployeeFamilyBg(id);
      const familyArray = family.rows;

      // EmployeeWorkExperience
      const workExp = await userDb.getEmployeeWorkExperience(id);
      const workExpArray = workExp.rows;

      // PreviousEmployer
      const previousEmployer = await userDb.getEmployeePreviousEmployer(id);
      const previousEmployerArray = previousEmployer.rows;

      // EmployeeAddresses
      const filePath = `${path}/EmployeeAddresses-${fileExt}.csv`;

      const mName = employeeArray[0].middlename
        ? decrypt(employeeArray[0].middlename)
        : employeeArray[0].middlename;

      const EmployeeAddressescsv = papa.unparse({
        fields: [
          "EmployeeCode",
          "EmployeeName",
          "UnitRoomNumberFloor",
          "BuildingName",
          "LotBlockPhaseHouseNumber",
          "StreetName",
          "VillageSubdivision",
          "Barangay",
          "TownDistrict",
          "Municipality",
          "CityProvince",
          "AddressType"
        ],
        data: [
          [
            employeeArray[1].employeeCode,
            decrypt(employeeArray[0].firstname) +
              " " +
              mName +
              " " +
              decrypt(employeeArray[0].lastname),
            employeeArray[0].unitroomnumberfloor,
            employeeArray[0].buildingname,
            employeeArray[0].lotblockphasehousenumber,
            employeeArray[0].streetname,
            employeeArray[0].villagesubdivision,
            employeeArray[0].barangay,
            employeeArray[0].towndistrict,
            employeeArray[0].municipality,
            employeeArray[0].cityprovince,
            employeeArray[0].addresstype
          ]
        ]
      });
      const EmployeeAddressescsvs = papa.unparse({
        fields: ["", "", "", "", "", "", "", "", "", "", "", ""],
        data: [
          [
            employeeArray[1].employeeCode,
            decrypt(employeeArray[0].firstname) +
              " " +
              mName +
              " " +
              decrypt(employeeArray[0].lastname),
            employeeArray[0].unitroomnumberfloor,
            employeeArray[0].buildingname,
            employeeArray[0].lotblockphasehousenumber,
            employeeArray[0].streetname,
            employeeArray[0].villagesubdivision,
            employeeArray[0].barangay,
            employeeArray[0].towndistrict,
            employeeArray[0].municipality,
            employeeArray[0].cityprovince,
            employeeArray[0].addresstype
          ]
        ]
      });
      try {
        await fs.access(filePath, fs.F_OK, err => {
          if (err) {
            // file doesn't exist
            // so write new file
            fs.writeFile(filePath, EmployeeAddressescsv, function(err, res) {
              if (err) {
                throw err;
              }
            });
            return;
          }
          // file exist
          // just append
          fs.appendFile(filePath, EmployeeAddressescsvs, function(err, res) {
            if (err) {
              throw err;
            }
          });
        });
      } catch (e) {
        const err = {
          msg: "Error in exporting data.. Please try again."
        };
        return err;
      }

      // Employees
      const efilePath = `${path}/Employees-${fileExt}.csv`;
      const Employeescsv = papa.unparse({
        fields: [
          "EmployeeCode",
          "LastName",
          "FirstName",
          "MiddleName",
          "NameSuffix",
          "Nickname",
          "Active",
          "MobileNo",
          "TelNo",
          "AccessCode",
          "ImmediateSuperiorCode",
          "ImmediateSuperiorName",
          "WebkioskRole",
          "ZIPCodeAR",
          "ZIPCodeAL",
          "AddressForeign",
          "ZIPCodeAF",
          "BirthDate",
          "DateHired",
          "DateRegular",
          "DateSeparated",
          "Rehirable",
          "TaxCode",
          "AlphanumericTaxCode",
          "DaysInYear",
          "PaymentType",
          "Region",
          "Rate",
          "Allowance",
          "COLA",
          "DateEffective",
          "EmploymentType",
          "PositionCode",
          "Gender",
          "MaritalStatus",
          "PayrollFrequency",
          "TINNo",
          "SSSNo",
          "PagIbigNo",
          "HDMFID",
          "PhilHealthNo",
          "EmailAddress",
          "IsTax",
          "IsSSS",
          "IsHDMF",
          "IsPHIC",
          "IsUnion",
          "IsEmailPayslip",
          "TimeSource",
          "Parameter",
          "ShiftScheduleTemplate",
          "ShiftScheduleTemplateGroup",
          "PayElementSchedule",
          "LeaveAccrualSchedule",
          "LeaveForfeitSchedule",
          "PayoutType",
          "BankAccount",
          "BankAccountNo",
          "BankBranchCode",
          "BankAccountType",
          "CostCenterCode",
          "DepartmentCode",
          "Location",
          "PayGroup",
          "Team",
          "Project",
          "CustomGroup1",
          "CustomGroup2",
          "CustomGroup3",
          "CustomGroup4",
          "CustomGroup5",
          "CustomGroup6",
          "CustomGroup7",
          "CustomGroup8",
          "CustomField1",
          "CustomField2",
          "CustomField3",
          "CustomField4",
          "CustomField5",
          "CustomField6",
          "CustomField7",
          "CustomField8",
          "CustomDateField1",
          "CustomDateField2",
          "CustomDateField3",
          "CustomDateField4",
          "CustomDateField5",
          "CustomDateField6",
          "CustomDateField7",
          "CustomDateField8",
          "CustomCheckbox1",
          "CustomCheckbox2",
          "CustomCheckbox3",
          "CustomCheckbox4",
          "CustomCheckbox5",
          "CustomCheckbox6",
          "CustomCheckbox7",
          "CustomCheckbox8",
          "MinTakeHomePayBasis",
          "MinTakeHomePay",
          "MinTakeHomePayType",
          "ContractStartDate",
          "ContractEndDate",
          "IsCleared",
          "DateCleared",
          "Citizenship",
          "SecurityUserLevel",
          "SecurityUserGroup",
          "Branch",
          "CTCNo"
        ],
        data: [
          [
            employeeArray[1].employeeCode,
            decrypt(employeeArray[0].lastname),
            decrypt(employeeArray[0].firstname),
            mName,
            "",
            "",
            "1", // Active
            employeeArray[0].mobile,
            "",
            employeeArray[1].employeeCode,
            "",
            "",
            "", // web kiosk role
            "",
            "",
            "",
            "",
            employeeArray[0].bday.toLocaleDateString(),
            "",
            "",
            "",
            "",
            "S",
            "",
            "",
            "Daily paid",
            "XII",
            "0",
            "0",
            "", // COLA
            "",
            "",
            "",
            employeeArray[0].gender,
            employeeArray[0].civil_status,
            "",
            employeeArray[0].tin,
            employeeArray[0].sss,
            "",
            "",
            "",
            decrypt(employeeArray[0].email), // email
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "Daily WH/W-OT",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            employeeArray[0].citizenship,
            "",
            "",
            "",
            ""
          ]
        ]
      });
      const Employeescsvs = papa.unparse({
        fields: [
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          ""
        ],
        data: [
          [
            employeeArray[1].employeeCode,
            decrypt(employeeArray[0].lastname),
            decrypt(employeeArray[0].firstname),
            mName,
            "",
            "",
            "1", // Active
            employeeArray[0].mobile,
            "",
            employeeArray[1].employeeCode,
            "",
            "",
            "", // web kiosk role
            "",
            "",
            "",
            "",
            employeeArray[0].bday.toLocaleDateString(),
            "",
            "",
            "",
            "",
            "S",
            "",
            "",
            "Daily paid",
            "XII",
            "0",
            "0",
            "", // COLA
            "",
            "",
            "",
            employeeArray[0].gender,
            employeeArray[0].civil_status,
            "",
            employeeArray[0].tin,
            employeeArray[0].sss,
            "",
            "",
            "",
            decrypt(employeeArray[0].email), // email
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "Daily WH/W-OT",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            employeeArray[0].citizenship,
            "",
            "",
            "",
            ""
          ]
        ]
      });
      try {
        await fs.access(efilePath, fs.F_OK, err => {
          if (err) {
            // file doesn't exist
            // so write new file
            fs.writeFile(efilePath, Employeescsv, function(err, res) {
              if (err) {
                throw err;
              }
            });
            return;
          }
          // file exist
          // just append
          fs.appendFile(efilePath, Employeescsvs, function(err, res) {
            if (err) {
              throw err;
            }
          });
        });
      } catch (e) {
        const err = {
          msg: "Error in exporting data.. Please try again."
        };
        return err;
      }

      //EmployeeCertifications
      const certificateFilePath = `${path}/EmployeeCertifications-${fileExt}.csv`;
      // if no data; just push fields
      if (certificateArray.length === 0) {
        certificateArray.push({
          professional_licenses_held: "",
          certification_number: ""
        });
      }
      const EmployeeCertificationscsv = papa.unparse({
        fields: [
          "EmployeeCode",
          "EmployeeName",
          "Certification",
          "CertificationNo",
          "Renewed",
          "Expires"
        ],
        data: [
          [
            employeeArray[1].employeeCode,
            decrypt(employeeArray[0].firstname) +
              " " +
              mName +
              " " +
              decrypt(employeeArray[0].lastname),
            certificateArray[0].professional_licenses_held,
            certificateArray[0].certification_number,
            "",
            ""
          ]
        ]
      });
      const EmployeeCertificationscsvs = papa.unparse({
        fields: ["", "", "", "", "", ""],
        data: [
          [
            employeeArray[1].employeeCode,
            decrypt(employeeArray[0].firstname) +
              " " +
              mName +
              " " +
              decrypt(employeeArray[0].lastname),
            certificateArray[0].professional_licenses_held,
            certificateArray[0].certification_number,
            "",
            ""
          ]
        ]
      });
      try {
        await fs.access(certificateFilePath, fs.F_OK, err => {
          if (err) {
            // file doesn't exist
            // so write new file
            fs.writeFile(
              certificateFilePath,
              EmployeeCertificationscsv,
              function(err, res) {
                if (err) {
                  throw err;
                }
              }
            );
            return;
          }
          // file exist
          // just append
          fs.appendFile(
            certificateFilePath,
            EmployeeCertificationscsvs,
            function(err, res) {
              if (err) {
                throw err;
              }
            }
          );
        });
      } catch (e) {
        const err = {
          msg: "Error in exporting data.. Please try again."
        };
        return err;
      }

      let counter = 0; // used to get out from the loop
      for (let x = 0; x < educationArray.length; x++) {
        const e = educationArray[x];

        const school = e.institution_name;
        const academic = e.educational_attainment;
        const education = e.course_degree;
        const address = e.institution_address;
        const startYear = e.startyear;
        const endYear = e.endyear;

        //   EmployeeEducation
        const educationFilePath = `${path}/EmployeeEducation-${fileExt}.csv`;
        //
        const EmployeeEducationcsv = papa.unparse({
          fields: [
            "EmployeeCode",
            "EmployeeName",
            "School",
            "AcademicLevel",
            "Education",
            "CertificateIssued",
            "Address",
            "YearStart",
            "YearEnd",
            "GPA",
            "MajorGPA",
            "Remarks"
          ],
          data: [
            [
              employeeArray[1].employeeCode,
              decrypt(employeeArray[0].firstname) +
                " " +
                mName +
                " " +
                decrypt(employeeArray[0].lastname),
              school,
              academic,
              education,
              "",
              address,
              startYear,
              endYear,
              "",
              "",
              ""
            ]
          ]
        });
        const EmployeeEducationcsvs = papa.unparse({
          fields: ["", "", "", "", "", "", "", "", "", "", "", ""],
          data: [
            [
              employeeArray[1].employeeCode,
              decrypt(employeeArray[0].firstname) +
                " " +
                mName +
                " " +
                decrypt(employeeArray[0].lastname),
              school,
              academic,
              education,
              "",
              address,
              startYear,
              endYear,
              "",
              "",
              ""
            ]
          ]
        });

        try {
          await fs.access(educationFilePath, fs.F_OK, err => {
            if (err) {
              // file doesn't exist
              // so write new file
              if (counter !== 0) {
                fs.appendFile(
                  educationFilePath,
                  EmployeeEducationcsvs,
                  function(err, res) {
                    if (err) {
                      throw err;
                    }
                  }
                );
              } else {
                counter++;
                fs.writeFile(educationFilePath, EmployeeEducationcsv, function(
                  err,
                  res
                ) {
                  if (err) {
                    throw err;
                  }
                });
              }
              return;
            }
            // file exist
            // just append
            fs.appendFile(educationFilePath, EmployeeEducationcsvs, function(
              err,
              res
            ) {
              if (err) {
                throw err;
              }
            });
          });
        } catch (e) {
          const err = {
            msg: "Error in exporting data.. Please try again."
          };
          return err;
        }
      }

      //EmployeeEmergecyContact
      const emergencyFilePath = `${path}/EmployeeEmergencyContact-${fileExt}.csv`;
      const EmployeeEmergecyContactcsv = papa.unparse({
        fields: [
          "EmployeeCode",
          "EmployeeName",
          "ContactPerson",
          "Address",
          "ContactNo",
          "Relationship"
        ],
        data: [
          [
            employeeArray[1].employeeCode,
            decrypt(employeeArray[0].firstname) +
              " " +
              mName +
              " " +
              decrypt(employeeArray[0].lastname),
            employeeArray[0].person_to_notify,
            employeeArray[0].person_address,
            employeeArray[0].person_telephone,
            employeeArray[0].person_relationship
          ]
        ]
      });
      const EmployeeEmergecyContactcsvs = papa.unparse({
        fields: ["", "", "", "", "", ""],
        data: [
          [
            employeeArray[1].employeeCode,
            decrypt(employeeArray[0].firstname) +
              " " +
              mName +
              " " +
              decrypt(employeeArray[0].lastname),
            employeeArray[0].person_to_notify,
            employeeArray[0].person_address,
            employeeArray[0].person_telephone,
            employeeArray[0].person_relationship
          ]
        ]
      });
      try {
        await fs.access(emergencyFilePath, fs.F_OK, err => {
          if (err) {
            // file doesn't exist
            // so write new file
            fs.writeFile(
              emergencyFilePath,
              EmployeeEmergecyContactcsv,
              function(err, res) {
                if (err) {
                  throw err;
                }
              }
            );
            return;
          }
          // file exist
          // just append
          fs.appendFile(
            emergencyFilePath,
            EmployeeEmergecyContactcsvs,
            function(err, res) {
              if (err) {
                throw err;
              }
            }
          );
        });
      } catch (e) {
        const err = {
          msg: "Error in exporting data.. Please try again."
        };
        return err;
      }

      //
      //EmployeeFamilyBackground
      const familyFilePath = `${path}/EmployeeFamilyBackground-${fileExt}.csv`;
      if (familyArray.length === 0) {
        familyArray.push({
          mothermaiden: "",
          fathersname: "",
          mothersname: "",
          sfirstname: "",
          smiddlename: "",
          slastname: ""
        });
      }
      const EmployeeFamilyBackgroundcsv = papa.unparse({
        fields: [
          "EmployeeCode",
          "EmployeeName",
          "MothersMaidenName",
          "FathersName",
          "MothersName",
          "SpouseFirstName",
          "SpouseMiddleName",
          "SpouseLastName"
        ],
        data: [
          [
            employeeArray[1].employeeCode,
            decrypt(employeeArray[0].firstname) +
              " " +
              mName +
              " " +
              decrypt(employeeArray[0].lastname),
            familyArray[0].mothermaiden,
            familyArray[0].fathersname,
            familyArray[0].mothersname,
            familyArray[0].sfirstname,
            familyArray[0].smiddlename,
            familyArray[0].slastname
          ]
        ]
      });
      const EmployeeFamilyBackgroundcsvs = papa.unparse({
        fields: ["", "", "", "", "", "", "", ""],
        data: [
          [
            employeeArray[1].employeeCode,
            decrypt(employeeArray[0].firstname) +
              " " +
              mName +
              " " +
              decrypt(employeeArray[0].lastname),
            familyArray[0].mothermaiden,
            familyArray[0].fathersname,
            familyArray[0].mothersname,
            familyArray[0].sfirstname,
            familyArray[0].smiddlename,
            familyArray[0].slastname
          ]
        ]
      });
      try {
        await fs.access(familyFilePath, fs.F_OK, err => {
          if (err) {
            // file doesn't exist
            // so write new file
            fs.writeFile(familyFilePath, EmployeeFamilyBackgroundcsv, function(
              err,
              res
            ) {
              if (err) {
                throw err;
              }
            });
            return;
          }
          // file exist
          // just append
          fs.appendFile(familyFilePath, EmployeeFamilyBackgroundcsvs, function(
            err,
            res
          ) {
            if (err) {
              throw err;
            }
          });
        });
      } catch (e) {
        const err = {
          msg: "Error in exporting data.. Please try again."
        };
        return err;
      }
      //
      //EmployeeMobileNumbers
      const mobileFilePath = `${path}/EmployeeMobileNumbers-${fileExt}.csv`;
      const EmployeeMobileNumberscsv = papa.unparse({
        fields: [
          "EmployeeCode",
          "EmployeeName",
          "AreaCode",
          "Number",
          "IsUseForJSM",
          "Remarks"
        ],
        data: [
          [
            employeeArray[1].employeeCode,
            decrypt(employeeArray[0].firstname) +
              " " +
              mName +
              " " +
              decrypt(employeeArray[0].lastname),
            "",
            employeeArray[0].mobile,
            "",
            ""
          ]
        ]
      });
      const EmployeeMobileNumberscsvs = papa.unparse({
        fields: ["", "", "", "", "", ""],
        data: [
          [
            employeeArray[1].employeeCode,
            decrypt(employeeArray[0].firstname) +
              " " +
              mName +
              " " +
              decrypt(employeeArray[0].lastname),
            "",
            employeeArray[0].mobile,
            "",
            ""
          ]
        ]
      });
      try {
        await fs.access(mobileFilePath, fs.F_OK, err => {
          if (err) {
            // file doesn't exist
            // so write new file
            fs.writeFile(mobileFilePath, EmployeeMobileNumberscsv, function(
              err,
              res
            ) {
              if (err) {
                throw err;
              }
            });
            return;
          }
          // file exist
          // just append
          fs.appendFile(mobileFilePath, EmployeeMobileNumberscsvs, function(
            err,
            res
          ) {
            if (err) {
              throw err;
            }
          });
        });
      } catch (e) {
        const err = {
          msg: "Error in exporting data.. Please try again."
        };
        return err;
      }

      let workCounter = 0;
      if (workExpArray.length === 0) {
        workExpArray.push({
          company_name: "",
          address: "",
          position: "",
          last_salary: "",
          start_date: "",
          end_date: "",
          reason_leaving: ""
        });
      }
      for (let y = 0; y < workExpArray.length; y++) {
        //
        //EmployeeWorkExperience
        const workExpFilePath = `${path}/EmployeeWorkExperience-${fileExt}.csv`;
        const EmployeeWorkExperiencecsv = papa.unparse({
          fields: [
            "EmployeeCode",
            "EmployeeName",
            "CompanyName",
            "CompanyAddress",
            "TIN",
            "Position",
            "EmploymentType",
            "Salary",
            "Manager",
            "DateHired",
            "DateSeparated",
            "Duties",
            "ReasonForLeaving",
            "Remarks"
          ],
          data: [
            [
              employeeArray[1].employeeCode,
              decrypt(employeeArray[0].firstname) +
                " " +
                mName +
                " " +
                decrypt(employeeArray[0].lastname),
              workExpArray[y].company_name,
              workExpArray[y].address,
              "",
              workExpArray[y].position,
              "",
              workExpArray[y].last_salary,
              "",
              workExpArray[y].start_date,
              workExpArray[y].end_date,
              "",
              workExpArray[y].reason_leaving,
              ""
            ]
          ]
        });
        const EmployeeWorkExperiencecsvs = papa.unparse({
          fields: ["", "", "", "", "", "", "", "", "", "", "", "", "", ""],
          data: [
            [
              employeeArray[1].employeeCode,
              decrypt(employeeArray[0].firstname) +
                " " +
                mName +
                " " +
                decrypt(employeeArray[0].lastname),
              workExpArray[y].company_name,
              workExpArray[y].address,
              "",
              workExpArray[y].position,
              "",
              workExpArray[y].last_salary,
              "",
              workExpArray[y].start_date,
              workExpArray[y].end_date,
              "",
              workExpArray[y].reason_leaving,
              ""
            ]
          ]
        });
        //
        try {
          await fs.access(workExpFilePath, fs.F_OK, err => {
            if (err) {
              // file doesn't exist
              // so write new file
              if (workCounter !== 0) {
                fs.appendFile(
                  workExpFilePath,
                  EmployeeWorkExperiencecsvs,
                  function(err, res) {
                    if (err) {
                      throw err;
                    }
                  }
                );
              } else {
                workCounter++;
                fs.writeFile(
                  workExpFilePath,
                  EmployeeWorkExperiencecsv,
                  function(err, res) {
                    if (err) {
                      throw err;
                    }
                  }
                );
              }
              return;
            }
            // file exist
            // just append
            fs.appendFile(workExpFilePath, EmployeeWorkExperiencecsvs, function(
              err,
              res
            ) {
              if (err) {
                throw err;
              }
            });
          });
        } catch (e) {
          const err = {
            msg: "Error in exporting data.. Please try again."
          };
          return err;
        }
      }

      //PreviousEmployer
      const previousEmployerFilePath = `${path}/EmployeePreviousEmployer-${fileExt}.csv`;
      if (previousEmployerArray.length === 0) {
        previousEmployerArray.push({
          company_name: "",
          address: "",
          yearend: ""
        });
      }
      const PreviousEmployercsv = papa.unparse({
        fields: [
          "EmployeeCode",
          "EmployeeName",
          "Employer's Name",
          "Employer's TIN",
          "Employer's Address",
          "Employer's ZIP Code",
          "Year",
          "25 Gross Taxable Compensation Income",
          "37 13th Month Pay and Other Benefits (Non-Taxable)",
          "38 De Minimis Benefits (Non-Taxable)",
          "39 SSS, GSIS, PHIC & Pag-ibig Contributions & Union Dues",
          "40 Salaries& Other Forms of Compensation (Non-Taxable)",
          "51 Taxable 13th Month Pay and Other Benefits",
          "27 Premium Paid on Health and/or Hospital Insurance",
          "31 Total Amount of Taxes Withheld"
        ],
        data: [
          [
            employeeArray[1].employeeCode,
            decrypt(employeeArray[0].firstname) +
              " " +
              mName +
              " " +
              decrypt(employeeArray[0].lastname),
            previousEmployerArray[0].company_name,
            "",
            previousEmployerArray[0].address,
            "",
            previousEmployerArray[0].yearend,
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
          ]
        ]
      });
      const PreviousEmployercsvs = papa.unparse({
        fields: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
        data: [
          [
            employeeArray[1].employeeCode,
            decrypt(employeeArray[0].firstname) +
              " " +
              mName +
              " " +
              decrypt(employeeArray[0].lastname),
            previousEmployerArray[0].company_name,
            "",
            previousEmployerArray[0].address,
            "",
            previousEmployerArray[0].yearend,
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
          ]
        ]
      });
      try {
        await fs.access(previousEmployerFilePath, fs.F_OK, err => {
          if (err) {
            // file doesn't exist
            // so write new file
            fs.writeFile(
              previousEmployerFilePath,
              PreviousEmployercsv,
              function(err, res) {
                if (err) {
                  throw err;
                }
              }
            );
            return;
          }
          // file exist
          // just append
          fs.appendFile(
            previousEmployerFilePath,
            PreviousEmployercsvs,
            function(err, res) {
              if (err) {
                throw err;
              }
            }
          );
        });
      } catch (e) {
        const err = {
          msg: "Error in exporting data.. Please try again."
        };
        return err;
      }

      // update is_exported to true
      await userDb.isExportedToTrue(id);
    }
    const err = {
      msg: "Exported successfully.."
    };
    return err;
  };
};

module.exports = adminExportCSVJeonsoft;
