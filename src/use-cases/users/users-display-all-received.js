const { decrypt } = require("../../entities/users/app");

const receivedUser = ({ userDb }) => {
  return async function get(deptid, posid, {info}) {

    try {
      const { role, deptID, teamID, dateFrom, dateTo } = info
      let getReceivedApplicants;

    if (deptid && !posid) {
      getReceivedApplicants = await userDb.getAllReceived(deptid).catch(err => console.log(err))
    }
    if (deptid && posid) {
      getReceivedApplicants = await userDb.getAllReceived(deptid, posid).catch(err => console.log(err))
    }

    switch (role) {
      case "Administrator":
      case "Operations Head":
      case "People Group":
        getReceivedApplicants = await userDb.findAllReceived(dateFrom, dateTo)
        break;
      case "Department Head":
        getReceivedApplicants = await userDb.findAllReceivedPGDeptOnly({deptID, dateFrom, dateTo});
        break;
      case "Team Head":
        getReceivedApplicants = await userDb.findAllReceivedTh(teamID, dateFrom, dateTo)
        break;
    }

    let receivedApplicants = getReceivedApplicants.rows,
          listOfReceivedApplicants = [];

    if (receivedApplicants) {
      for (let receivedApplicant of receivedApplicants) {
        let firstName = decrypt(receivedApplicant.firstname),
            lastName = decrypt(receivedApplicant.lastname),
            fullName = (receivedApplicant.middlename) ? `${firstName} ${decrypt(receivedApplicant.middlename)} ${lastName}` : `${firstName} ${lastName}`,
            email = decrypt(receivedApplicant.email)
  
            listOfReceivedApplicants.push({
              id: receivedApplicant.id,
              email: email,
              name: fullName,
              mobile: receivedApplicant.mobile,
              livestock: receivedApplicant.livestock,
              willing_to_travel: receivedApplicant.willing_to_travel,
              position: receivedApplicant.name,
              resume_upload_date: receivedApplicant.resume_upload_date,
              resume_upload_url: receivedApplicant.resume_upload_url,
              deptName: receivedApplicant.deptname,
              date_received: receivedApplicant.date_received,
              no_of_categories: receivedApplicant.category_count,
              running_time: receivedApplicant.running_time,
              deptId: receivedApplicant.deptid
        })
      }
    }
    
    return listOfReceivedApplicants;
    } catch (error) {
      console.log(error)
    }
    
  };
};
module.exports = receivedUser;
