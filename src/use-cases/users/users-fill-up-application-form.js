const { userApplicationFormEntitys } = require("../../entities/users/app");
const { encrypt } = require("../../entities/users/app");

const userFillUpApplication = ({ userDb }) => {
  return async function put({ id, ...userInfo } = {}) {
    const result = userApplicationFormEntitys(userInfo);
    //
    //   
    let middlename = result.getMiddlename();
    if(middlename){
      middlename = encrypt(middlename);
    }
  
    return userDb.fillUpApplication({
      id: id,
      salary_desired: result.getSalaryDesired(),
      // new fields
      UnitRoomNumberFloor: result.getUnitRoomNumberFloor(),
      BuildingName: result.getBuildingName(),
      LotBlockPhaseHouseNumber: result.getLotBlockPhaseHouseNumber(),
      StreetName: result.getStreetName(),
      VillageSubdivision: result.getVillageSubd(),
      Barangay: result.getBarangay(),
      TownDistrict: result.getTownDistrict(),
      Municipality: result.getMunicipality(),
      CityProvince: result.getCityProvince(),
      AddressType: result.getAddressType(),
      //
      middlename: middlename,
      place_birth: result.getPlaceBirth(),
      bday: result.getBday(),
      age: result.getAge(),
      height: result.getHeight(),
      weight: result.getWeight(),
      gender: result.getGender(),
      is_single_parent: result.getSingleParent(),
      civil_status: result.getCivilStatus(),
      citizenship: result.getCitizenship(),
      religion: result.getReligion(),
      who_referred: result.getWhoReferred(),
      when_to_start: result.getWhenToStart(),
      person_to_notify: result.getPersonToNotify(),
      person_relationship: result.getPersonRelationship(),
      person_address: result.getPersonAddress(),
      person_telephone: result.getPersonTelephone(),
      position_id_second_choice: result.getSecondChoice(),
      CS_eligible: result.getCSEligible(),
      CS_date_taken: result.getCSDateTaken(),
      CS_certificate_image_path: result.getCSImagePath(),
      date_application: result.getDateApplication(),
      user_image_path: result.getUserImagePath(),
      relativeArray: result.getRelative(),
      educationalBgArray: result.getEducation(),
      professionalLicensesArray: result.getProfessionalLicense(),
      orgMembershipArray: result.getOrgMembership(),
      seminarsAttendedArray: result.getSeminars(),
      workExperienceArray: result.getWorkExperience(),
      workWithBFIArray: result.getWorkWithBFI(),
      otherInfoArray: result.getOtherInfo(),
      familyBgArray: result.getFamilyBg(),
      isMarriedArray: result.getIsMarried(),
      referencesArray: result.getReferences(),
      willingnessArray: result.getWillingness()
    });
  };
};

module.exports = userFillUpApplication;
