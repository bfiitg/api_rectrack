process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0
const { encrypt, decrypt } = require("../../entities/users/app");
const { accessSpreadSheet } = require("../../../google-spreadsheet-worker/app");

const googleSpreadSheet = ({ userDb, sendEmail, moment }) => {
  return async function posts() {
    const applicants = await accessSpreadSheet();
    let insertedApplicants = []; // hold inserted applicants

    let count = 0; //count of data inserted

    if (applicants.length > 0) {

      for await (let e of applicants) {
        // query to get position id; must be same string
        const position = await userDb.returnPositionIdFromName(e.position);
        // only if positioin exists in db
        if (position.rowCount > 0) {
  
          const posId = position.rows[0].id; //position id
          let duplicateCounter = 0; //increases when email or applicant exists
          //   check if email exists
          const emailExist = await userDb.findByEmail(encrypt(e.email));
  
          if (emailExist.rowCount > 0) {
            duplicateCounter++;
          }
  
          const userExist = await userDb.findByFullName(
            encrypt(e.firstname),
            encrypt(e.lastname)
          );
          if (userExist.rowCount > 0) {
            duplicateCounter++;
          }
  
          // email composition
          const mailOptions = {
            from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
            to: e.email,
            subject: "BFI : Application Status - Received",
            html:
              `Dear ${e.firstname} ${e.lastname}, <br/><br/>` +
              "This letter is to let you know that we have received your application. We appreciate your interest in Biotech Farms Inc. " +
              `the position of ${e.position} for which you applied. We are reviewing applications currently. If you are selected to take the online exam, ` +
              "expect to receive a separate email with the link of the online exam on the next business day. Otherwise, you will still receive updates on your application status.<br/><br/> " +
              "We are grateful for the time you spent applying in Biotech Farms, Always Better – Improving Lives, Beyond World-Class Products.<br/><br/> " +
              "Regards,<br/><br/>" +
              "Biotech Farms Inc." +
              "<br/><br/>" +
              "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
              "This mailbox does not allow incoming messages.</div>",
          };
  
          // end
          // insert query
          if (duplicateCounter === 0) {
            count++; //increment
            await userDb.insertApplicantFromGoogleSheet({
              email: encrypt(e.email),
              firstname: encrypt(e.firstname),
              lastname: encrypt(e.lastname),
              mobile: e.mobile,
              livestock: e.livestock,
              travel: e.travel,
              resume_url: e.resume,
              position_id: posId,
            })
                    .then(res => {
                      insertedApplicants.push(e);
                    })
                    .catch(err => {
                      console.log(err)
                    })
  
            await sendEmail(mailOptions);
  
            // email composition for applicant
  
            //get dept head info
            //get dept head email
            const deptHeadInfo = await userDb.getDhEmail(posId);
            const deptHead = deptHeadInfo.rows;
  
            const dhMailOptions = `A new application to your department from ${e.firstname} ${e.lastname} for the position of ${e.position} has been received.
              <br/><br/>
              Regards,<br/><br/>
              Biotech Farms Inc.
              <div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. 
              This mailbox does not allow incoming messages.</div>`;
  
            for (let i = 0; i < deptHead.length; i++) {
              var email = decrypt(deptHead[i].email);
              var firstName = decrypt(deptHead[i].firstname);
              var lastName = decrypt(deptHead[i].lastname);
  
              // email composition
              const mailOption = {
                from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
                to: email,
                subject: "BFI : Applicant Received",
                html: `Dear ${firstName} ${lastName}, <br/><br/>` + dhMailOptions,
              };
  
              await sendEmail(mailOption);
            }
            //   end email
            //
            // return;
          } else {
            // make object
            const applicant = {
              email: encrypt(e.email),
              firstname: encrypt(e.firstname),
              lastname: encrypt(e.lastname),
              position_id: posId,
              mobile: e.mobile,
              resume_url: e.resume,
              reapply_count: 1,
            };
            // get the status must be kept for ref, reject or blacklist
            let status = null;
            const getStatus = await userDb.getStatusApplicant({ applicant });
            if (getStatus.rowCount > 0) status = getStatus.rows[0].status;
  
            const allStatus = ["blacklist", "rejected", "kept for reference"];
            const isInclude = allStatus.includes(status);
  
            // if true only; insert to re-applied applicant; per position applied!
            if (isInclude) {
              // if applicant already exist in the table; increment only reapply count
              const check = await userDb.checkApplicantIfReApplied({ applicant });
              if (check.rowCount > 0) {
                // reapplied applicant exists
                const data = check.rows[0];
                const { id, reapply_count, created_at } = data;
  
                const counter = reapply_count + 1;
  
                // increment only count; only when applied date and created date are not equal
                const appliedDate = formatdate(moment, e.timestamp);
                const createdAt = formatdate(moment, created_at);
  
                if (createdAt !== appliedDate) {
                  const toUpdate = {
                    id,
                    reapply_count: counter,
                  };
  
                  // increment
                  await userDb.reAppliedIncrementCount({ toUpdate });
                }
              } else {
                // doesn't exist; so insert
                await userDb.reAppliedApplicant({ applicant });
              }
            }
          }
        }
      }
    }

    const datas = {
      msg: `Inserted ${count} applicants.`,
      applicants,
      insertedApplicants,
    };
    return datas;
  };
};


const formatdate = (moment, date) => {
  return moment(date).format("M/DD/YYYY");
};

module.exports = googleSpreadSheet;
