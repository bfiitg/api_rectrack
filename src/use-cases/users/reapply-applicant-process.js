const { decrypt } = require("../../entities/users/app");

const processReApply = ({ userDb, sendEmail }) => {
  return async function put({ id, ...info } = {}) {
    // id is the PK to update on "reapplied_applicants" table
    //
    // **
    // get the applicant data
    const data = await userDb.getReAppliedApplicantName({ id });
    if (data.rowCount <= 0)
      throw new Error(`No data retrieved, please try again.`);
    const applicant = data.rows[0]; // object

    // get the corresponding record to the "users" table
    const user = await userDb.getUserData({ applicant });
    if (user.rowCount <= 0)
      throw new Error(`User data doesn't exist, please try again.`);

    // user id to update status and position
    const userId = user.rows[0].id;
    const posId = user.rows[0].position_id; // previous position

    // get all pending re apply application; change isprocessed to true
    const pending = await userDb.getAllPendingReApply({ applicant });
    const pendingId = []; // push only id not equal to the param id
    for (let i = 0; i < pending.rows.length; i++) {
      const e = pending.rows[i].id;
      if (e !== id) pendingId.push(e);
    }
    // append data
    applicant.previous_position = posId;
    applicant.userId = userId;
    applicant.pendingId = pendingId;

    const res = await userDb.processReApplyApplicant({ applicant, id });
    let msg = `Error on processing re-apply of applicant.`;
    if (res) {
      // send email here
      const { firstname, lastname, email } = applicant;
      const fn = decrypt(firstname);
      const ln = decrypt(lastname);
      const emails = decrypt(email);
      // email composition
      const mailOptions = {
        from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
        to: emails,
        subject: "BFI : Application Status - Received",
        html:
          `Dear ${fn} ${ln}, <br/><br/>` +
          "You're application has been reprocessed and has been received again. " +
          "Please check your email from time to time for updates.<br/><br/>" +
          "Regards,<br/><br/>" +
          "Biotech Farms Inc." +
          "<br/><br/>" +
          "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
          "This mailbox does not allow incoming messages.</div>",
      };

      // send email; remove await
      sendEmail(mailOptions);
      // end

      msg = `Re-apply of applicant has been processed successfully.`;
      return msg;
    } else {
      throw new Error(msg);
    }
  };
};

module.exports = processReApply;
