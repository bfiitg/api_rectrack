const { decrypt } = require("../../entities/users/app");

const getQuickApplicantsUseCase = ({ userDb }) => {
    return async function get(deptid, posid, {info}) {
    let getQuickApplicants,
        listOfQuickApplicants = [],
        message

        try {

            if(deptid) {
                getQuickApplicants = await userDb.findAllQuickApplicants(deptid)
            }

            if(deptid && posid) {
                getQuickApplicants = await userDb.findAllQuickApplicants(deptid, posid)
            }

            getQuickApplicants = (info.dateFrom && info.dateTo) ? 
                                    await userDb.findAllQuickApplicantsByDate(info.dateFrom, info.dateTo) : 
                                    await userDb.findAllQuickApplicants()
            
            let quickApplicants = getQuickApplicants.rows

            for (let quickApplicant of quickApplicants) {
                let firstName = decrypt(quickApplicant.firstname),
                    lastName = decrypt(quickApplicant.lastname),
                    fullName = (quickApplicant.middlename) ? `${firstName} ${decrypt(quickApplicant.middlename)} ${lastName}` : `${firstName} ${lastName}`,
                    email = decrypt(quickApplicant.email),
                    applicationDate = (quickApplicant.date_application) ? quickApplicant.date_application : quickApplicant.resume_upload_date

                listOfQuickApplicants.push({
                    id: quickApplicant.id,
                    email: email,
                    name: fullName,
                    mobile: quickApplicant.mobile,
                    livestock: quickApplicant.livestock,
                    willing_to_travel: quickApplicant.willing_to_travel,
                    resume_upload_date: quickApplicant.resume_upload_date,
                    resume_upload_url: quickApplicant.resume_upload_url,
                    date_application: applicationDate,
                    running_time: quickApplicant.running_time
                })
            }
            return listOfQuickApplicants;
        } catch (error) {
            console.log(error)
            return message = `Error date format, Please specify Thank you.`
        }
    };
};

module.exports = getQuickApplicantsUseCase;