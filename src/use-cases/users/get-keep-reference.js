const { decrypt } = require("../../entities/users/app");
const KeepReferencesUseCases = ({ userDb }) => {

    return async function post(info) {
        const { id, role, deptID, teamID, dateTo, dateFrom } = info;
        let result;
        const DataArray = [];
        const AccessRole_getAllkeepforReferences = (dateTo && dateFrom) ? await userDb.getAllkeepforReferences(id, dateTo, dateFrom) : await userDb.getAllkeepforReferences(id, dateTo, dateFrom);
        const AccessRole_findAllKeptForReferenceTh = (dateTo && dateFrom) ? await userDb.findAllKeptForReferenceTh(teamID, dateTo, dateFrom) : await userDb.findAllKeptForReferenceTh(teamID, dateTo, dateFrom);
        const AccessRole_findAllKeptForReferenceDh = (dateTo && dateFrom) ? await userDb.findAllKeptForReferenceDh(deptID, dateTo, dateFrom) : await userDb.findAllKeptForReferenceDh(deptID, dateTo, dateFrom);
        if (role == 'Administrator' || role == 'People Group' || role == 'Operations Head') { result = AccessRole_getAllkeepforReferences.rows }
        else if (role == 'Team Head') { result = AccessRole_findAllKeptForReferenceTh.rows } else if (role == 'Department Head') { result = AccessRole_findAllKeptForReferenceDh.rows }

        for (let i = 0; i < result.length; i++) {
            const e = result[i];
            DataArray.push({
                id: e.id,
                email: decrypt(e.email),
                name: `${decrypt(e.firstname)} ${decrypt(e.middlename)} ${decrypt(e.lastname)}`,
                mobile: e.mobile,
                livestock: e.livestock,
                willing_to_travel: e.willing_to_travel,
                done_IQBE: e.is_done_iqbe_exam,
                position: e.name,
                resume_upload_date: e.resume_upload_date,
                resume_upload_url: e.resume_upload_url,
                date_application: e.date_application,
                date_received: e.date_received,
                date_online_exam: e.date_online_exam,
                date_endorsement: e.date_endorsement,
                date_assessment: e.date_assessment,
                date_blacklist: e.date_blacklist,
                date_kept_reference: e.date_kept_reference,
                remarks_kept_reference: e.remarks_kept_reference,
                running_time: e.running_time,
                deptId: e.deptid,
                category_count: e.category_count,
                position_id: e.position_id,
                filled_up_form: e.filled_up_form
            });

        }

        return DataArray;
    }
}

module.exports = KeepReferencesUseCases;