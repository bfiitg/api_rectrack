const userChangeStatusEndorsement = ({ userDb }) => {
    return async function puts({ id } = {}) {
      const updated = await userDb.changeStatusEndorsement(id);
      return { ...updated };
    };
  };
  
  module.exports = userChangeStatusEndorsement;
  