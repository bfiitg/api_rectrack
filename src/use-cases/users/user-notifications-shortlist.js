const notificationShortlist = ({ userDb }) => {
  return async function get(role) {

    if(role == 'Operations Head'){
      const users = await userDb.notificationShorlistOh();
      return users.rows;
    }

    const users = await userDb.notificationShorlist();
    return users.rows;
  };
};

module.exports = notificationShortlist;
