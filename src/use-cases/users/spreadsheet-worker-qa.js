const { encrypt, decrypt } = require("../../entities/users/app");
const {
  accessSpreadSheetQa,
} = require("../../../google-spreadsheet-worker-qa/app");

const googleSpreadSheetforQa = ({ userDb, sendEmail, moment }) => {
  return async function posts() {
    const applicants = await accessSpreadSheetQa();
    let insertedApplicants = []; // hold inserted applicants


    let count = 0; //count of data inserted
    // loop thru all applicants for today only

    if (applicants.length > 0) {

      for await (let e of applicants) {
        // query to get position id; must be same string
  
        let duplicateCounter = 0; //increases when email or applicant exists
        //   check if email exists
        const emailExist = await userDb.findByEmail(encrypt(e.email));
        if (emailExist.rowCount > 0) {
          // if email exists; don't insert.
          duplicateCounter++;
        }
        //   check if user exists
        const userExist = await userDb.findByFullName(
          encrypt(e.firstname),
          encrypt(e.lastname)
        );
        if (userExist.rowCount > 0) {
          // if user exists; don't insert.
          duplicateCounter++;
        }
  
        // email composition
        const mailOptions = {
          from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
          to: e.email,
          subject: "BFI : Application Status - Received",
          html:
            `Dear ${e.firstname} ${e.lastname}, <br/><br/>` +
            "This letter is to let you know that we have received your application. We appreciate your interest in Biotech Farms Inc. " +
            `We are reviewing applications currently and will be endorsing you to one of our departments where we see your qualifications to be fit. If you are selected to take the online exam, ` +
            "expect to receive a separate email with the link of the online exam on the next business day. Otherwise, you will still receive updates on your application status.<br/><br/> " +
            "We are grateful for the time you spent applying in Biotech Farms, Always Better – Improving Lives, Beyond World-Class Products.<br/><br/> " +
            "Regards,<br/><br/>" +
            "Biotech Farms Inc." +
            "<br/><br/>" +
            "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
            "This mailbox does not allow incoming messages.</div>",
        };
  
        // end
        // insert query
        if (duplicateCounter == 0) {
          count++; //increment
          insertedApplicants.push(e);
          await userDb.insertApplicantFromGoogleSheet({
            email: encrypt(e.email),
            firstname: encrypt(e.firstname),
            lastname: encrypt(e.lastname),
            mobile: e.mobile,
            livestock: e.livestock,
            travel: e.travel,
            resume_url: e.resume,
          });
  
          //   send email
          await sendEmail(mailOptions);
          //   end email
          //
        } else {
          // make object
          const applicant = {
            email: encrypt(e.email),
            firstname: encrypt(e.firstname),
            lastname: encrypt(e.lastname),
            mobile: e.mobile,
            position_id: null,
            resume_url: e.resume,
            reapply_count: 1,
          };
          // get the status must be kept for ref, reject or blacklist
          let status = null;
          const getStatus = await userDb.getStatusApplicant({ applicant });
          if (getStatus.rowCount > 0) status = getStatus.rows[0].status;
  
          const allStatus = ["blacklist", "rejected", "kept for reference"];
          const isInclude = allStatus.includes(status);
  
          // if true only; insert to re-applied applicant; per position applied!
          if (isInclude) {
            // if applicant already exist in the table; increment only reapply count
            const check = await userDb.checkApplicantIfReAppliedQA({ applicant });
            if (check.rowCount > 0) {
              // reapplied applicant exists
              const data = check.rows[0];
              const { id, reapply_count, created_at } = data;
  
              const counter = reapply_count + 1;
  
              // increment only count; only when applied date and created date are not equal
              const appliedDate = formatdate(moment, e.timestamp);
              const createdAt = formatdate(moment, created_at);
  
              if (createdAt !== appliedDate) {
                const toUpdate = {
                  id,
                  reapply_count: counter,
                };
  
                // increment
                await userDb.reAppliedIncrementCount({ toUpdate });
              }
            } else {
              // doesn't exist; so insert
              await userDb.reAppliedApplicant({ applicant });
            }
          }
        }
      }
    }

    const datas = {
      msg: `Inserted ${count} quick applicants.`,
      applicants,
      insertedApplicants,
    };
    return datas;
  };
};

const formatdate = (moment, date) => {
  return moment(date).format("M/DD/YYYY");
};
module.exports = googleSpreadSheetforQa;
