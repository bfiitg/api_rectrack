const { decrypt } = require("../../entities/users/app");

const fetchDataReferralForm = ({ userDb }) => {
  return async function get(id) {
    const users = await userDb.fetchExistingDataReferral(id);
    const result = users.rows;
    const results = [];
    for (let i = 0; i < result.length; i++) {
      results.push({
        firstname: decrypt(result[i].firstname),
        lastname: decrypt(result[i].lastname),
        mobile: result[i].mobile,
        positionApplied: result[i].name,
        whoReferred: result[i].who_referred
      });
    }
    return results;
  };
};

module.exports = fetchDataReferralForm;
