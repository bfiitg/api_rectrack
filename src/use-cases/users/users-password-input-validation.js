const inputPassword = ({ userDb, encrypt }) => {
   return async function validate({ email, password }) {
      if(!email || email.trim().length === 0) {
         throw new Error("Please provide an email.")
      }

      if (!password || password.trim().length === 0) {
         throw new Error("Please provide a password.")
      }

      let prompt

      await userDb.passwordVerification(encrypt(email), encrypt(password))
               .then( res => {
                  prompt = (res.rowCount > 0) ? "Successfully verified." : "Incorrect password."
               })
               .catch( err => {
                  console.log("Error: ", err)
               })
      return prompt
   }
}

module.exports = inputPassword