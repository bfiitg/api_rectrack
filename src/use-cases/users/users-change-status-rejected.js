const userChangeStatusRejected = ({ userDb }) => {
  return async function put({ id, remarks, rejectedBy } = {}) {

    if (!remarks) {
      throw new Error("Please enter some remarks.")
    }
    if (!rejectedBy) {
      throw new Error("Please enter who rejected the applicant.")
    }

    // change status
    const updated = await userDb.changeStatusReject(id, remarks, rejectedBy);
    let result = (updated.rowCount > 0) ? "Update success." : "Update failed."

    return {
      result
    };
  };
};

module.exports = userChangeStatusRejected;
