const { updateResumeEntitys } = require("../../entities/users/app");

const applicantResumeUpdate = ({ userDb }) => {
  return async function put(info) {
    const result = updateResumeEntitys(info);

    const datas = {
      msg: "Updated resume successfully.",
      data: await userDb.updateResume({
        resume_upload_url: result.getResume_upload_url(),
        mobile: result.getMobile()
      })
    };
    return datas;
  };
};

module.exports = applicantResumeUpdate;
