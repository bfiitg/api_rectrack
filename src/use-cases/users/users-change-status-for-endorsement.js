const userChangeStatusForEndorsement = ({ userDb }) => {
  return async function puts({ id } = {}) {
    try {
      const updated = await userDb.changeStatusForEndorsement(id);
      return { ...updated };
    } catch (error) {
      console.log(error)
    }
  };
};

module.exports = userChangeStatusForEndorsement;
