const { decrypt } = require("../../entities/users/app");
const moment = require("moment")

const referralFormView = ({ userDb }) => {
  return async function get() {
    const users = await userDb.viewAllReferralForm();
    const result = users.rows;
    const results = [];
    for (let i = 0; i < result.length; i++) {
      results.push({
        id: result[i].id,
        referrer_name: `${decrypt(result[i].ref_fname)} ${decrypt(result[i].ref_lname)}`,
        referrer_dept: result[i].department,
        referrer_position: result[i].position,
        referrer_id: result[i].referrer_id,
        referrer_contact: result[i].referrer_contact,
        applicant_id: result[i].applicant_id,
        referral_name:
          decrypt(result[i].firstname) + " " + decrypt(result[i].lastname),
        referral_contact: result[i].mobile,
        referral_position_applying: result[i].applicantPosition,
        referral_relationship_referrer: result[i].referral_relationship_referrer,
        referral_date_deploy:moment(result[i].date_deployed).format('L'),
        referral_duration:moment().diff(moment(result[i].date_deployed),'days') +" days"
        // moment().format('L')-moment(result[i].date_deployed).format('L')
      });
    }
    return results;
  };
};

module.exports = referralFormView;
