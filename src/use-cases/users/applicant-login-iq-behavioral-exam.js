const { applicantEntityLogin, encrypt } = require("../../entities/users/app");

const loginApplicantIQB = ({ userDb }) => {
  return async function posts(userInfo) {
    const result = applicantEntityLogin(userInfo);
    const email = result.getEmail();
    const password = result.getPassword();
    //
    let userToken = "";

    await result.getToken().then(data => {
      userToken = data;
    });

    const search  = await userDb.getApplicantID(email);
    const find = await userDb.find(search.rows[0].id)

    //validation if user is done on online exam
    if(find.rows[0].is_done_online_exam === false){
      throw new Error("You need to take an online exam before taking IQ test.")
    }

    const loginExist = await userDb.loginsApplicants({
      email: result.getEmail(),
      password: encrypt(result.getPassword()),
      ip: result.getIP(),
      token: userToken
    });
    
    if (loginExist.rowCount === 0 || !find.rows[0].id) {
      throw new Error("Invalid account.")
    }

    let checkUserHasIQBE, categoryArr = []
    
    await userDb.checkUserHasIQBE(search.rows[0].id)
                .then(res => {
                  checkUserHasIQBE = (res.rowCount) ? true : false
                })

    const categories = await userDb.getAllCategoryIdForIQBExam();

    if (!checkUserHasIQBE) {
      // get applicant id
      const userId = await userDb.getApplicantID(email);
      const id = userId.rows[0].id;

      const data = await userDb.find(id)
      const u = (data.rows[0].position_id);

      const find = await userDb.findbyPosition(u)

      for (let i = 0; i < categories.length; i++) {
        const e = categories[i];

        for (let x = 0; x < e.length; x++) {
          const categoryId = e[x].id;
          // insert into db categories random
          await userDb.insertRandomCategories(id, categoryId);

          // get questions
          const questions = await userDb.selectAllQuestionsIQB(categoryId);
          const questionsArray = questions.rows;
          if (questionsArray.length !== 0) {
            for (let i = 0; i < questionsArray.length; i++) {
              let questionId = questionsArray[i].id;
              // insert into db question random
              await userDb.insertAllQuestionsIQB(id, questionId);
            }
          }
        }
      }
      // update 10,800 seconds = 3 hrs for the exam
      await userDb.timeForIQBE(id);
    }
    
    for (cat of shuffle(categories)) {
      categoryArr = cat
    }

    const results = {
      applicantID: search.rows[0].id, 
      msg: "Login successful",
      token: userToken,
      remainingTime: (find.rows[0].iqbe_time) ? find.rows[0].iqbe_time : 0,
      categoryArr
    };

    return results;
  };
};

function shuffle(array) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

module.exports = loginApplicantIQB;
