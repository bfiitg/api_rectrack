const notificationEndorsed = ({ userDb }) => {
  return async function get(role) {
    
    if(role.role == 'Operations Head'){
      const users = await userDb.notificationEndorsedOh();
      return users.rows;
    }
    const users = await userDb.notificationEndorsed();
    return users.rows;
  };
};

module.exports = notificationEndorsed;
