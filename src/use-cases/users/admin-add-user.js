const { adminMakeUser, decrypt } = require("../../entities/users/app");

const adminAddUser = ({ userDb }) => {
  return async function posts(userInfo) {
    const result = adminMakeUser(userInfo);

    let emailValidation = await userDb.findByEmail(result.getEmail())

    if (emailValidation.rowCount > 0) {
      throw new Error('Email already exists.')
    }

    let nameValidation = await userDb.findByFullName(
      result.getFirstName(),
      result.getLastName()
    )

    if (nameValidation.rowCount > 0) {
      throw new Error('Applicant has existing records in the database.')
    }

    let applicantID, rowCount

    await userDb.adminInsert({
      email: result.getEmail(),
      password: result.getPassword(),
      firstname: result.getFirstName(),
      lastname: result.getLastName(),
      mobile: result.getMobile(),
      role_id: result.getRole(),
      position_id: result.getPosition(),
      middlename: result.getMiddleName()
    })
      .then(res => {;
        applicantID = res.rows[0].id
        rowCount = res.rowCount
      })
      .catch(err => console.log(err))

    let prompt = (rowCount > 0) ? `User added successfully` : `Failed to add user.`

    return {prompt, applicantID};
  };
};

module.exports = adminAddUser;
