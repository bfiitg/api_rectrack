const userDoneOnlineExam = ({ userDb }) => {
  return async function puts({ id } = {}) {
    // done online exam
    const updated = await userDb.doneOnlineExam(id);
    // change status for endorsement
    await userDb.changeStatusForEndorsement(id);

    let result = (updated.rowCount > 0) ? 'OK' : 'Failed.'
    return result;
  };
};

module.exports = userDoneOnlineExam;
