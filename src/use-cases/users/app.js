// for jeonsoft export csv
const papa = require("papaparse");
const moment = require("moment");
const path = require("path");
const fs = require("fs");

// end

// for auto email
const { sendEmail } = require("../email/nodemailer");
const { makeToken } = require("../../token/app");

const { decrypt, encrypt } = require("../../entities/users/app");

const userDb = require("../../data-access/users/app");
const accessRightsDb = require("../../data-access/access-rights/app");
const actionsDb = require("../../data-access/actions/app");
const moduleDb = require("../../data-access/modules/app");
const interviewDb = require("../../data-access/interview-assessment/app")

const changePassword = require("./first-time-login");
const onlineExamRetake = require("./users-retake-online-exam");
const referralCheckIfExist = require("./user-check-if-has-referral");
const notificationForEndorsed = require("./user-notifications-for-endorsement");
const logsSelectAll = require("./logs-view");
const googleSpreadSheetforQa = require("./spreadsheet-worker-qa");
const googleSpreadSheet = require("./spreadsheet-worker");
const checkTokens = require("./get-token-if-not-equal-logout");
const userUpdateDeployedDate = require("./applicant-update-deployed-date");
const notificationShortlist = require("./user-notifications-shortlist");
const notificationDeployed = require("./user-notifications-deployed");
const notificationAssessment = require("./user-notifications-assessment");
const applicantUpdatePosition = require("./applicant-update-position");
const assessmentShowListPositions = require("./assessment-show-list-positions");
const fetchDataReferralForm = require("./users-fetch-referral-form");
const getApplicantNamePV = require("./users-application-form-get-data-print");
const applicantOnlineExamResult = require("./applicant-view-online-exam-result");
const logsInsert = require("./logs-insert");
const updateReferral = require("./update-referral-form");
const referralFormViewOne = require("./view-referral-form-single");
const referralFormView = require("./view-referral-form");
const forEndorsedUser = require("./users-display-all-for-endorsement");
const userChangeStatusForEndorsement = require("./users-change-status-for-endorsement");
const userDoneIQBE = require("./users-change-status-done-IQBE");
const userDoneOnlineExam = require("./users-change-status-done-online-exam");
const imageUpload = require("./image-upload");
const applicationFormLogin = require("./applicant-login-application-form");
const makeAddUser = require("./users-add");
const loginUser = require("./users-login");
const receivedUser = require("./users-display-all-received");
const userChangeStatusOnlineExam = require("./users-change-status-onlineexam");
const userChangeStatusBlacklist = require("./users-change-status-blacklist");
const onlineExamUser = require("./users-display-all-onlineExam");
const userChangeStatusEndorsement = require("./users-change-status-endorsement");
const endorsedUser = require("./users-display-all-endorsed");
const userChangeStatusRejected = require("./users-change-status-rejected");
const rejectedUser = require("./users-display-all-rejected");
const blacklistedUser = require("./users-display-all-blacklist");
const userChangeStatusAssessment = require("./users-change-status-assessment");
const assessmentUser = require("./users-display-all-assessment");
const userChangeStatusPending = require("./users-change-status-pending");
const pendingUser = require("./users-display-all-pending");
const userChangeStatusReference = require("./users-change-status-keptReference");
const referenceUser = require("./users-display-all-keptReference");
const userChangeStatusShortList = require("./users-change-status-shortlist");
const shortlistUser = require("./users-display-all-shortlist");
const userChangeStatusDeployed = require("./users-change-status-deployed");
const deployedUser = require("./users-display-all-deployed");
const loginApplicant = require("./applicant-login");
const userScheduleAssessment = require("./users-set-schedule-for-assessment");
const userChangeStatusJobOrder = require("./users-change-status-jobOrder");
const jobOrderUser = require("./users-display-all-jobOrder");
const userFillUpApplication = require("./users-fill-up-application-form");
const notificationReceived = require("./user-notifications-received");
const notificationEndorsed = require("./user-notifications-endorsed");
const loginApplicantIQB = require("./applicant-login-iq-behavioral-exam");
const getApplicantName = require("./users-application-form-get-name");
// ADMIN
const adminViewAllUser = require("./admin-view-all-users");
const adminViewDetailsUser = require("./admin-view-details-user");
const adminAddUser = require("./admin-add-user");
const adminEditUser = require("./admin-edit-user");
// export csv for jeonsoft
const adminExportCSVJeonsoft = require("./jeonsoft-csv-export");
const displayAllApplicantsJeonSoft = require("./jeonsoft-display-applicants");
const jeonsoftClientDownload = require("./jeonsoft-client-download");
// end
const getStartEndTime = require("./get-start-end-time-online-exam");
const IQBESubtractTime = require("./get-time-for-IQBE");
const getLoggedInUserFirstname = require("./get-logged-in-user-name");
const makeAddReferral = require("./make-new-referral-form");

const filterReports = require("./user-filter-reports");
const getQuickApplicants = require("./users-display-allQuickApplicants");
const getAllApplicants = require('./users-display-all-applicants')
const endorseToOtherDepartment = require("./user-endorse-to-other-dept");
const getApplicantDateTrackings = require("./user-get-applicant-date-trackings");

const filterReportAlls = require("./user-filter-reports-all");
const countQuickApplicants = require("./users-count-allQuickApplicants");
const applicantRateAnswer = require("./applicant-rate-answer");

const applicantResumeUpdates = require("./users-update-resume");
const getReapplied = require("./select-reapplied");
const processReApply = require("./reapply-applicant-process");

const updateToKeptRef = require("./applicant-update-deployed-to-kept")
const updateJO = require("./applicant-update-deployed-to-JO")
const viewAllIQTest = require("./view-all-IQ-test");
const getAllShortListUseCases = require("./get-all-Shortlisted")


const KeepReferencesUseCases = require("./get-keep-reference")

const jobControllerUseCases = require("./job-order")

const findApplicantById = require('./users-find-applicant-by-id')

// ====== new

const fetchApplicantUseCases = require("./fetch-pending-users");

const getApplicantNotification = require('./users-notification-get')
const addApplicantNotification = require('./users-notification-add')
const updateApplicantNotification = require('./users-notification-update')
const inputPassword = require('./users-password-input-validation')
const fetchApplicantsWithoutReferral = require('./users-display-no-referrals')
const returnApplicantToReceived = require('./users-change-status-return-to-received')
const findUser = require('./find-user')

// ###########################################

const fetchApplicantUseCase = fetchApplicantUseCases({ userDb, sendEmail });

const processReApplys = processReApply({ userDb, sendEmail });
const getReapplieds = getReapplied({ userDb });
const changePasswords = changePassword({ userDb, sendEmail, makeToken });
const onlineExamRetakes = onlineExamRetake({ userDb });
const referralCheckIfExists = referralCheckIfExist({ userDb });
const notificationForEndorseds = notificationForEndorsed({ userDb });
const logsSelectAlls = logsSelectAll({ userDb });
const googleSpreadSheetforQas = googleSpreadSheetforQa({
  userDb,
  sendEmail,
  moment,
});
const googleSpreadSheets = googleSpreadSheet({ userDb, sendEmail, moment });
const checkTokenss = checkTokens({ userDb });
const userUpdateDeployedDates = userUpdateDeployedDate({ userDb });
const notificationShortlists = notificationShortlist({ userDb });
const notificationDeployeds = notificationDeployed({ userDb });
const notificationAssessments = notificationAssessment({ userDb });
const applicantUpdatePositions = applicantUpdatePosition({ userDb });
const assessmentShowListPositionss = assessmentShowListPositions({ userDb });
const fetchDataReferralForms = fetchDataReferralForm({ userDb });
const getApplicantNamePVs = getApplicantNamePV({ userDb });
const applicantOnlineExamResults = applicantOnlineExamResult({ userDb });
const logsInserts = logsInsert({ userDb });
const updateReferrals = updateReferral({ userDb });
const referralFormViewOnes = referralFormViewOne({ userDb });
const referralFormViews = referralFormView({ userDb });
const forEndorsedUsers = forEndorsedUser({ userDb });
const userChangeStatusForEndorsements = userChangeStatusForEndorsement({
  userDb,
});
const getLoggedInUserFirstnames = getLoggedInUserFirstname({ userDb });
const userDoneIQBEs = userDoneIQBE({ userDb });
const getStartEndTimes = getStartEndTime({ userDb });
const userDoneOnlineExams = userDoneOnlineExam({ userDb });
const imageUploads = imageUpload({});
const applicationFormLogins = applicationFormLogin({ userDb });
const getApplicantNames = getApplicantName({ userDb });
const addUser = makeAddUser({ userDb, sendEmail });
const userLogin = loginUser({
  userDb,
  fs,
  path,
  accessRightsDb,
  actionsDb,
  moduleDb,
});
const receivedUsers = receivedUser({ userDb });
const usersChangeStatusOnlineExam = userChangeStatusOnlineExam({
  userDb
});
const usersChangeStatusBlacklist = userChangeStatusBlacklist({
  userDb
});
const onlineExamUsers = onlineExamUser({ userDb });
const usersChangeStatusEndorsement = userChangeStatusEndorsement({ userDb });
const endorsedUsers = endorsedUser({ userDb });
const usersChangeStatusRejected = userChangeStatusRejected({
  userDb
});
const rejectedUsers = rejectedUser({ userDb });
const blacklistedUsers = blacklistedUser({ userDb });
const userChangeStatusAssessments = userChangeStatusAssessment({
  userDb,
});
const assessmentUsers = assessmentUser({ userDb });
const userChangeStatusPendings = userChangeStatusPending({ userDb });
const pendingUsers = pendingUser({ userDb });
const userChangeStatusReferences = userChangeStatusReference({
  userDb,
  sendEmail,
});
const referenceUsers = referenceUser({ userDb });
const userChangeStatusShortLists = userChangeStatusShortList({
  userDb
});
const shortlistUsers = shortlistUser({ userDb });
const userChangeStatusDeployeds = userChangeStatusDeployed({
  userDb
});
const loginApplicants = loginApplicant({ userDb });
const userScheduleAssessments = userScheduleAssessment({
  userDb
});
const userChangeStatusJobOrders = userChangeStatusJobOrder({
  userDb
});
const jobOrderUsers = jobOrderUser({ userDb });
const userFillUpApplications = userFillUpApplication({ userDb });
const notificationReceiveds = notificationReceived({ userDb });
const notificationEndorseds = notificationEndorsed({ userDb });
const loginApplicantIQBs = loginApplicantIQB({ userDb });
// ADMIN
const adminViewAllUsers = adminViewAllUser({ userDb });
const adminViewDetailsUsers = adminViewDetailsUser({ userDb });
const adminAddUsers = adminAddUser({ userDb });
const adminEditUsers = adminEditUser({ userDb });
// export csv for jeonsoft
const adminExportCSVJeonsofts = adminExportCSVJeonsoft({
  userDb,
  papa,
});
const displayAllApplicantsJeonSofts = displayAllApplicantsJeonSoft({ userDb });
const jeonsoftClientDownloads = jeonsoftClientDownload({});
// end
const IQBESubtractTimes = IQBESubtractTime({ userDb });
const makeAddReferrals = makeAddReferral({ userDb });
const filterReportsUseCase = filterReports({ userDb });
const getQuickApplicantsUseCase = getQuickApplicants({ userDb });
const getAllApplicantsUseCase = getAllApplicants({ userDb })
const endorseToOtherDepartmentUseCase = endorseToOtherDepartment({
  userDb,
});
const getApplicantDateTrackingsUseCase = getApplicantDateTrackings({ userDb });
const filterReportAllUseCase = filterReportAlls({ userDb });
//count number of quickapplicant
const countQuickApplicantsUseCase = countQuickApplicants({ userDb });
//rate answers for essay and problem solving
const rateAnswerUseCase = applicantRateAnswer({ userDb });
//update applicant resume
const applicantResumeUpdateUseCase = applicantResumeUpdates({ userDb });

//deploy accidentally deployed user
const updateToKeptRefUseCase = updateToKeptRef({ userDb, sendEmail });
//deploy deployed but for some reason did not show up
const updateJOUseCase = updateJO({ userDb, sendEmail });

const viewAllIQTestUseCase = viewAllIQTest({ userDb });

const findApplicantById_USECASE = findApplicantById({ userDb, getApplicantNames, interviewDb })

const getAllShortListUseCase = getAllShortListUseCases({ userDb });

const KeepReferencesUseCase = KeepReferencesUseCases({ userDb })

const jobControllerUseCase = jobControllerUseCases({ userDb })

const deployedUsers = deployedUser({ userDb })

const getApplicantNotif_USECASE = getApplicantNotification({ userDb, decrypt })
const addApplicantNotif_USECASE = addApplicantNotification({ userDb }) 
const updateApplicantNotif_USECASE = updateApplicantNotification({ userDb })


const passwordValidation_USECASE = inputPassword({ userDb, encrypt })
const fetchApplicantsWithoutReferral_USECASE = fetchApplicantsWithoutReferral({ userDb })

const returnApplicantToReceived_USECASE = returnApplicantToReceived ({ userDb })
const findUserDetails = findUser({ userDb, decrypt, encrypt })

const userService = Object.freeze({
  fetchApplicantUseCase,
  deployedUsers,
  processReApplys,
  findApplicantById_USECASE,
  getReapplieds,
  addUser,
  userLogin,
  receivedUsers,
  usersChangeStatusOnlineExam,
  usersChangeStatusBlacklist,
  onlineExamUsers,
  usersChangeStatusEndorsement,
  endorsedUsers,
  usersChangeStatusRejected,
  rejectedUsers,
  blacklistedUsers,
  userChangeStatusAssessments,
  assessmentUsers,
  userChangeStatusPendings,
  pendingUsers,
  userChangeStatusReferences,
  referenceUsers,
  userChangeStatusShortLists,
  shortlistUsers,
  userChangeStatusDeployeds,
  loginApplicants,
  userScheduleAssessments,
  userChangeStatusJobOrders,
  jobOrderUsers,
  userFillUpApplications,
  notificationReceiveds,
  notificationEndorseds,
  loginApplicantIQBs,
  getApplicantNames,
  // ADMIN
  adminViewAllUsers,
  adminViewDetailsUsers,
  adminAddUsers,
  adminEditUsers,
  adminExportCSVJeonsofts,
  displayAllApplicantsJeonSofts,
  jeonsoftClientDownloads,
  applicationFormLogins,
  imageUploads,
  userDoneOnlineExams,
  getStartEndTimes,
  IQBESubtractTimes,
  userDoneIQBEs,
  getLoggedInUserFirstnames,
  userChangeStatusForEndorsements,
  forEndorsedUsers,
  makeAddReferrals,
  referralFormViews,
  referralFormViewOnes,
  updateReferrals,
  logsInserts,
  applicantOnlineExamResults,
  getApplicantNamePVs,
  fetchDataReferralForms,
  assessmentShowListPositionss,
  applicantUpdatePositions,
  notificationAssessments,
  notificationDeployeds,
  notificationShortlists,
  userUpdateDeployedDates,
  checkTokenss,
  googleSpreadSheetforQas,
  googleSpreadSheets,
  logsSelectAlls,
  notificationForEndorseds,
  referralCheckIfExists,
  onlineExamRetakes,
  changePasswords,
  filterReportsUseCase,
  getQuickApplicantsUseCase,
  getAllApplicantsUseCase,
  endorseToOtherDepartmentUseCase,
  getApplicantDateTrackingsUseCase,

  filterReportAllUseCase,
  countQuickApplicantsUseCase,
  rateAnswerUseCase,
  applicantResumeUpdateUseCase,

  updateToKeptRefUseCase,
  updateJOUseCase,
  viewAllIQTestUseCase,
  getAllShortListUseCase,
  KeepReferencesUseCase,
  jobControllerUseCase,
  getApplicantNotif_USECASE,
  addApplicantNotif_USECASE,
  updateApplicantNotif_USECASE,

  passwordValidation_USECASE,
  fetchApplicantsWithoutReferral_USECASE,
  returnApplicantToReceived_USECASE,
  findUserDetails
});

module.exports = userService;
module.exports = {
  processReApplys,
  getReapplieds,
  addUser,
  userLogin,
  receivedUsers,
  usersChangeStatusOnlineExam,
  usersChangeStatusBlacklist,
  onlineExamUsers,
  usersChangeStatusEndorsement,
  endorsedUsers,
  usersChangeStatusRejected,
  rejectedUsers,
  blacklistedUsers,
  userChangeStatusAssessments,
  assessmentUsers,
  userChangeStatusPendings,
  pendingUsers,
  userChangeStatusReferences,
  referenceUsers,
  userChangeStatusShortLists,
  shortlistUsers,
  userChangeStatusDeployeds,
  loginApplicants,
  userScheduleAssessments,
  userChangeStatusJobOrders,
  jobOrderUsers,
  userFillUpApplications,
  notificationReceiveds,
  notificationEndorseds,
  loginApplicantIQBs,
  getApplicantNames,
  // ADMIN
  adminViewAllUsers,
  adminViewDetailsUsers,
  adminAddUsers,
  adminEditUsers,
  adminExportCSVJeonsofts,
  displayAllApplicantsJeonSofts,
  jeonsoftClientDownloads,
  applicationFormLogins,
  imageUploads,
  userDoneOnlineExams,
  getStartEndTimes,
  IQBESubtractTimes,
  userDoneIQBEs,
  getLoggedInUserFirstnames,
  userChangeStatusForEndorsements,
  forEndorsedUsers,
  makeAddReferrals,
  referralFormViews,
  referralFormViewOnes,
  updateReferrals,
  logsInserts,
  applicantOnlineExamResults,
  getApplicantNamePVs,
  fetchDataReferralForms,
  assessmentShowListPositionss,
  applicantUpdatePositions,
  notificationAssessments,
  notificationDeployeds,
  notificationShortlists,
  userUpdateDeployedDates,
  checkTokenss,
  googleSpreadSheetforQas,
  googleSpreadSheets,
  logsSelectAlls,
  notificationForEndorseds,
  referralCheckIfExists,
  onlineExamRetakes,
  changePasswords,
  filterReportsUseCase,
  getQuickApplicantsUseCase,
  getAllApplicantsUseCase,
  endorseToOtherDepartmentUseCase,
  getApplicantDateTrackingsUseCase,

  filterReportAllUseCase,
  countQuickApplicantsUseCase,
  rateAnswerUseCase,
  applicantResumeUpdateUseCase,

  updateToKeptRefUseCase,
  updateJOUseCase,
  viewAllIQTestUseCase,

  fetchApplicantUseCase,
  findApplicantById_USECASE,
  getAllShortListUseCase,
  KeepReferencesUseCase,
  jobControllerUseCase,
  deployedUsers,
  getApplicantNotif_USECASE,
  addApplicantNotif_USECASE,
  updateApplicantNotif_USECASE,

  passwordValidation_USECASE,
  fetchApplicantsWithoutReferral_USECASE,
  returnApplicantToReceived_USECASE,
  findUserDetails
};
