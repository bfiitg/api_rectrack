const endorseToOtherDepartmentUseCase = ({ userDb }) => {
    return async function put(userId, posId) {
        const updatedPosition = await userDb.updatePositionOfApplicant(userId, posId);
        let data;

        if (!updatedPosition) {
            throw new Error("Failed to update applicant's position.");
        }
        const updateReceived = await userDb.changeStatusReceived(userId);
        if (!updateReceived) {
            throw new Error("Failed to update applicant status.");
        }

        data = { msg: "Applicant endorsed successfully" };
        return data;
    }
}

module.exports = endorseToOtherDepartmentUseCase;

