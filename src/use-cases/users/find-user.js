const findUser = ({ userDb, decrypt, encrypt }) => {
   return async function find(input) {
      try {

         let query

         if (isNaN(input) === false) {
            query = Number(input)
         }

         if (isNaN(input) === true) {
            let result = input.charAt(0).toUpperCase() + input.slice(1)
            query = encrypt(result)
         }
            
         let result = await userDb.fetchUsers(query),
               userData = result.rows,
               userArray = []

         for (user of userData) {
            userArray.push({
               userID: user.id,
               firstName: decrypt(user.firstname),
               lastName: decrypt(user.lastname),
               middleName: (user.middlename) ? decrypt(user.middlename) : '',
               mobile: user.mobile,
               email: decrypt(user.email),
               roleID: user.roleID,
               role: user.role,
               positionID: user.positionID,
               position: user.position,
               teamID: user.teamID,
               team: user.team,
               departmentID: user.departmentID,
               department: user.department
            })
         }

         return userArray

      } catch (error) {

         console.log(error)

      }
   }
}

module.exports = findUser
