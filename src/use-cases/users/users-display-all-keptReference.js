const { decrypt } = require("../../entities/users/app");

const referenceUser = ({ userDb }) => {
  return async function get(params, reqMethod) {
    try {
        
      const { role, deptID, teamID, dateTo, dateFrom } = params
      let result;
      const DataArray = [];

      if (reqMethod === 'GET') {
        await userDb.findAllKeptForReference(params)
                    .then(res => result = res.rows)
      }  

      if (reqMethod === 'POST') {
        switch (role) {
          case "Administrator":
          case "People Group":
          case "Operations Head":
            await userDb.getAllkeepforReferences({dateTo, dateFrom})
                        .then(res => result = res.rows)
            break;
          case "Team Head":
            await userDb.findAllKeptForReferenceTh({teamID, dateTo, dateFrom})
                        .then(res => result = res.rows)
            break;
          case "Department Head":
            await userDb.findAllKeptForReferenceDh({deptID, dateTo, dateFrom})
                        .then(res => result = res.rows)
            break;
        }
      }

      for (let applicant of result) {
          DataArray.push({
              id: applicant.id,
              email: decrypt(applicant.email),
              name: `${decrypt(applicant.firstname)} ${decrypt(applicant.lastname)}`,
              mobile: applicant.mobile,
              livestock: applicant.livestock,
              willing_to_travel: applicant.willing_to_travel,
              done_IQBE: applicant.is_done_iqbe_exam,
              position: applicant.name,
              resume_upload_date: applicant.resume_upload_date,
              resume_upload_url: applicant.resume_upload_url,
              date_application: applicant.date_application,
              date_received: applicant.date_received,
              date_online_exam: applicant.date_online_exam,
              date_endorsement: applicant.date_endorsement,
              date_assessment: applicant.date_assessment,
              date_blacklist: applicant.date_blacklist,
              date_kept_reference: applicant.date_kept_reference,
              remarks_kept_reference: applicant.remarks_kept_reference,
              running_time: applicant.running_time,
              deptId: applicant.deptName,
              category_count: applicant.category_count,
              position_id: applicant.position_id,
              filled_up_form: applicant.filled_up_form
          });
      }

      return DataArray;

    } catch (error) {
      console.log(error)
    }
  };
};

module.exports = referenceUser;
