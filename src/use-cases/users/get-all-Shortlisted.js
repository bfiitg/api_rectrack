const { decrypt } = require("../../entities/users/app");
const getAllShortListUseCases = ({ userDb }) => {

    return async function post(info) {
        const { id, role, deptID, teamID, dateTo, dateFrom } = info;
        let result;
        const DataArray = [];
        const AccessRole_fetchAllShortListed = (dateTo && dateFrom) ? await userDb.findAllShortList(id, dateTo, dateFrom) : await userDb.findAllShortList(id, dateTo, dateFrom);
        const AccessRole_fetchAllShortListedTh = (dateTo && dateFrom) ? await userDb.findAllShortListTh(teamID, dateTo, dateFrom) : await userDb.findAllShortListTh(teamID, dateTo, dateFrom);
        const AccessRole_fetchAllShortListedDh = (dateTo && dateFrom) ? await userDb.findAllShortListDh(deptID, dateTo, dateFrom) : await userDb.findAllShortListDh(deptID, dateTo, dateFrom);
        if (role == 'Administrator' || role == 'People Group' || role == 'Operations Head') { result = AccessRole_fetchAllShortListed.rows }
        else if (role == 'Team Head') { result = AccessRole_fetchAllShortListedTh.rows } else if (role == 'Department Head') { result = AccessRole_fetchAllShortListedDh.rows }

        for (let i = 0; i < result.length; i++) {
            const e = result[i];
            DataArray.push({
                id: e.id,
                email: decrypt(e.email),
                name: `${decrypt(e.firstname)} ${decrypt(e.lastname)}`,
                mobile: e.mobile,
                livestock: e.livestock,
                willing_to_travel: e.willing_to_travel,
                position: e.name,
                resume_upload_date: e.resume_upload_date,
                resume_upload_url: e.resume_upload_url,
                date_received: e.date_received,
                scheduleAssessment: e.date_interview,
                onlineExamStartDate: e.online_exam_start,
                onlineExamEndDate: e.online_exam_end,
                date_for_endorsement: e.date_for_endorsement,
                date_endorsed: e.date_endorsed,
                date_assessment: e.date_assessment,
                date_pending: e.date_pending,
                running_time: e.running_time,
                deptId: e.deptid,
            });

        }

        return DataArray;
    }
}

module.exports = getAllShortListUseCases;