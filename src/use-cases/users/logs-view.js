const { decrypt } = require("../../entities/users/app");

const logsSelectAll = ({ userDb }) => {
  return async function selects(...info) {
    const date = info[0].date; //date from body
    const dateTo = info[0].dateTo //date To from body
    const users = await userDb.selectAllLogs(date,dateTo);
    const result = users.rows;
    const results = [];
    for (let i = 0; i < result.length; i++) {
      const fn = result[i].firstname;
      const ln = result[i].lastname;

      if (!fn || !ln) {
        results.push({
          id: result[i].id,
          name: "",
          module: result[i].module,
          table_affected: result[i].table_affected,
          activity: result[i].activity,
          dates: result[i].dates
        });
      } else {
        results.push({
          id: result[i].id,
          name:
            decrypt(result[i].firstname) + " " + decrypt(result[i].lastname),
          module: result[i].module,
          table_affected: result[i].table_affected,
          activity: result[i].activity,
          dates: result[i].dates
        });
      }
    }
    return results;
  };
};

module.exports = logsSelectAll;
