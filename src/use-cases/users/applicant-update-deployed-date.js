const userUpdateDeployedDate = ({ userDb }) => {
  return async function puts({ id, date } = {}) {

    if (!date) {
      return data = { msg: "Please enter a valid date of deployment." };
    }
    const updated = await userDb.updateDeployedDate(id, date);
    return data = { msg: "Updated successfully" };
  };
};

module.exports = userUpdateDeployedDate;
