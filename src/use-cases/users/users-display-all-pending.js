const { decrypt } = require("../../entities/users/app");

const pendingUser = ({ userDb }) => {
  return async function get(deptid, posid, info) {
    let result;
    
    if (deptid) {
      await userDb.findAllPendingPG(deptid)
                  .then(res => result = res.rows)
    }

    if (deptid && posid) {
      await userDb.findAllPendingPG(deptid, posid)
                  .then(res => result = res.rows)
    }
    
    const { id, role, deptID, teamID, dateTo, dateFrom } = info;
    const DataArray = [];

    switch (role) {
      case "Administrator":
      case "Operations Head":
      case "People Group":
        await userDb.fetchAllPendingStatus({dateTo, dateFrom})
                    .then(res => result = res.rows)
        break;
      case "Team Head":
        await userDb.findAllPendingTh({teamID, dateTo, dateFrom})
                    .then(res => result = res.rows)
        break;
      case "Department Head":
        await userDb.findAllPendingDh({deptID, dateTo, dateFrom})
                    .then(res => result = res.rows)
        break;
    }

    result.forEach(applicant => {
        DataArray.push({
            id: applicant.id,
            email: decrypt(applicant.email),
            name: `${decrypt(applicant.firstname)} ${decrypt(applicant.lastname)}`,
            mobile: applicant.mobile,
            livestock: applicant.livestock,
            willing_to_travel: applicant.willing_to_travel,
            position: applicant.name,
            resume_upload_date: applicant.resume_upload_date,
            resume_upload_url: applicant.resume_upload_url,
            date_received: applicant.date_received,
            scheduleAssessment: applicant.date_interview,
            onlineExamStartDate: applicant.online_exam_start,
            onlineExamEndDate: applicant.online_exam_end,
            date_for_endorsement: applicant.date_for_endorsement,
            date_endorsed: applicant.date_endorsed,
            date_assessment: applicant.date_assessment,
            date_pending: applicant.date_pending,
            running_time: applicant.running_time,
            deptId: applicant.deptid,
        });

    })

    return DataArray
  };
};

module.exports = pendingUser;
