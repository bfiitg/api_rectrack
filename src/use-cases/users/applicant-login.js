const { applicantEntityLogin, encrypt } = require("../../entities/users/app");

const loginApplicant = ({ userDb, moment }) => {
  return async function posts(userInfo) {

    const result = applicantEntityLogin(userInfo);
    const email = result.getEmail();
    const password = result.getPassword();
    //
    let userToken = "";

    await result.getToken().then(data => {
      userToken = data;
    });

    const loginExist = await userDb.loginsApplicant({
      email: result.getEmail(),
      password: encrypt(result.getPassword()),
      ip: result.getIP(),
      token: userToken
    });

    
    const userId = await userDb.getApplicantID(email);
    const id = userId.rows[0].id;


    if (loginExist.rowCount !== 0) {

      const remainingTime = await userDb.getRemainingOnlineExamTime({
        email: email,
        password: encrypt(password)
      });
  
      const remainingTimeRes = remainingTime.rows;
  
      if ((remainingTimeRes[0].remaining_minutes <= 0) && (remainingTimeRes[0].remaining_hours <= 0))
      {
        throw new Error("Exam overdue.")
      }

      // check if has questions inserted already
      const check = await userDb.checkCatRandom(id);

      if (check.rowCount === 0) {
        // get categories id
        const categories = await userDb.selectRandomCategories1st(id);
        const categoriesArray = categories.rows;

        // get number of categories
        const numberOfCategories = categoriesArray.length;

        // const get teams id
        const items = await userDb.getTeamNumberOfItems(id);
        const teamItems = items.rows[0].no_of_items; //28

        const itemsPerCategory = Math.round(teamItems / numberOfCategories); //5

        let remainder = 0;
        let totalTimeInSeconds = 0;
        // get questions per category
        for (let i = 0; i < categoriesArray.length; i++) {
          const time = categoriesArray[i].time_limit_on_seconds;
          let categoryId = categoriesArray[i].id;
          // insert into db categories random
          await userDb.insertRandomCategories(id, categoryId);
          // limit the question per category
          let limit = 0;

          // at first run of the loop don't subtract yet
          let difference = 0;
          if (i === 0) {
            difference = teamItems;
          } else {
            // remainder to limit the question per category
            remainder = remainder + itemsPerCategory;
            difference = teamItems - remainder;
          }
          // if difference is greater than or equal items per category
          // just get the items per category
          // else items per category + the difference
          // check also if equal to array length -1
          if (i !== categoriesArray.length - 1) {
            if (difference > itemsPerCategory) {
              limit = itemsPerCategory;
              // formula to get total time of exam in seconds
              totalTimeInSeconds = totalTimeInSeconds + time * limit;
            } else {
              limit = difference;
              totalTimeInSeconds = totalTimeInSeconds + time * limit;
            }
          } else {
            limit = difference;
            totalTimeInSeconds = totalTimeInSeconds + time * limit;
          }

          categoriesArray[i].limit = limit;
          // get questions
          const questions = await userDb.selectRandomQuestions(
            categoryId,
            limit
          );
          const questionsArray = questions.rows;
          if (questionsArray.length !== 0) {
            for (let i = 0; i < questionsArray.length; i++) {
              let questionId = questionsArray[i].id;
              // insert into db question random
              await userDb.insertRandomQuestions(id, questionId);
            }
          }
        }

        const cat = await userDb.selectRandomCategories(id)

        const t = new Date(); //start
        const e = new Date(); //end
        const startDateTime = t;

        // add total seconds
        e.setSeconds(e.getSeconds() + totalTimeInSeconds);
        const endDateTime = e;

        // update in db
        await userDb.examEndDate(
          startDateTime,
          endDateTime,
          totalTimeInSeconds,
          id
        );

        const result = {
          applicantID: id,
          msg: "Login successful",
          token: userToken,
          timeLeftToFinishExam: `${remainingTimeRes[0].remaining_hours}:${remainingTimeRes[0].remaining_minutes}`,
          totalExamTime: totalTimeInSeconds,
          elapsedExamTime: (remainingTimeRes[0].total_time_elapsed_seconds) ? remainingTimeRes[0].total_time_elapsed_seconds : 0,
          categories: cat.rows
        };
        //
        return result;
      } else {

        const remainingTime = await userDb.getRemainingOnlineExamTime({
          email: email,
          password: encrypt(password)
        });
    
        const examTimers = remainingTime.rows;

        if (examTimers[0].remaining_minutes < 0 )
        {
          throw new Error("Exam overdue.")
        }

        // get categories id
        const categories = await userDb.selectRandomCategories(id);
        const categoriesArray = categories.rows;
      
        const result = {
          msg: "Login successful",
          token: userToken,
          categories: categoriesArray,
          applicantID: id,
          timeLeftToFinishExam: `${examTimers[0].remaining_hours}:${examTimers[0].remaining_minutes}`,
          totalExamTime: examTimers[0].total_time_exam_seconds,
          elapsedExamTime: (examTimers[0].total_time_elapsed_seconds) ? examTimers[0].total_time_elapsed_seconds : 0,
          examStartDate: examTimers[0].exam_start, 
          examEndDate: examTimers[0].exam_end
        };

        return result
      }
    } else {
      throw new Error("Invalid account")
    }
    
  };
};

module.exports = loginApplicant;
