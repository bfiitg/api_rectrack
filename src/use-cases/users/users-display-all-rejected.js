const { decrypt } = require("../../entities/users/app");

const rejectedUser = ({ userDb }) => {
  return async function get(params, requestMethod) {

    let users;

    if (requestMethod === 'GET') {
      await userDb.getAllRejected(params)
                  .then(result => users = result)
                  .catch(err => {throw new Error(err.message)})
    }
   
    if (requestMethod === 'POST') {
      switch (params.role) {
        case "Administrator":
        case "Operations Head":
        case "People Group":
          users = await userDb.findAllRejected(params.dateFrom, params.dateTo).catch(err => {throw new Error(err.message)})
          break;
        case "Department Head":
          users = await userDb.findAllRejectDh(params.deptID, params.dateFrom, params.dateTo).catch(err => {throw new Error(err.message)});
          break;
        case "Team Head":
          users = await userDb.findAllRejectTh(params.teamID, params.dateFrom, params.dateTo).catch(err => {throw new Error(err.message)});
          break;
      }
    }
    

    const rejectedApplicants = users.rows;
    const listOfRejectedApplicants = [];

    for (let rejectedApplicant of rejectedApplicants) {
      let firstName = decrypt(rejectedApplicant.firstname),
          lastName = decrypt(rejectedApplicant.lastname),
          email = decrypt(rejectedApplicant.email),
          fullName = (rejectedApplicant.middlename) ? `${firstName} ${decrypt(rejectedApplicant.middlename)} ${lastName}` : `${firstName} ${lastName}`

          listOfRejectedApplicants.push({
            id: rejectedApplicant.id,
            email: email,
            name: fullName,
            mobile: rejectedApplicant.mobile,
            livestock: rejectedApplicant.livestock,
            willing_to_travel: rejectedApplicant.willing_to_travel,
            position: rejectedApplicant.name,
            resume_upload_date: rejectedApplicant.resume_upload_date,
            resume_upload_url: rejectedApplicant.resume_upload_url,
            remarks_reject: rejectedApplicant.remarks_reject,
            date_application: rejectedApplicant.date_application,
            date_received: rejectedApplicant.date_received,
            date_online_exam: rejectedApplicant.date_online_exam,
            date_endorsement: rejectedApplicant.date_endorsement,
            date_assessment: rejectedApplicant.date_assessment,
            date_blacklist: rejectedApplicant.date_blacklist,
            rejected_by: `${decrypt(rejectedApplicant.rb_firstname)} ${decrypt(rejectedApplicant.rb_lastname)}`,
            deptId: rejectedApplicant.deptid,
            date_rejected: rejectedApplicant.date_rejected
          })
    }
    
    return listOfRejectedApplicants;
  };
};

module.exports = rejectedUser;
