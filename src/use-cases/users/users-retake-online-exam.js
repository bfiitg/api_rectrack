const {
  generateString,
  encrypt
} = require("../../entities/users/app");

const onlineExamRetake = ({ userDb }) => {
  return async function puts({ id } = {}) {
    // auto genererated password
    const password = generateString();

    const updated = await userDb.onlineExamRetake(id, encrypt(password));
    const count = updated.rowCount;
    const data = (count > 0) ? "Updated successfully." : "Update failed."

    return data;
  };
};

module.exports = onlineExamRetake;
