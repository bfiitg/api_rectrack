const { decrypt } = require("../../entities/users/app");

const forEndorsedUser = ({ userDb }) => {
  return async function get(deptid, posid, {info}) {
    let getApplicants,
        applicantList = [],
        params = {
          deptid: deptid,
          posid: posid
        },
        arg

    const { deptID, teamID, role, dateFrom, dateTo } = info
    
    try {

      if (deptid) {
        getApplicants = await userDb.findAllForEndorsementFiltered(params)
      }
      if (deptid && posid) {
        getApplicants = await userDb.findAllForEndorsementFiltered(params)
      }

      switch (role) {
        case "Administrator":
        case "People Group":
        case "Operations Head":
          arg = {
            dateFrom: dateFrom,
            dateTo: dateTo
          }
          getApplicants = await userDb.findAllForEndorsement(arg)
          break;
        case "Department Head":
          arg = {
            deptID: deptID,
            dateFrom: dateFrom,
            dateTo: dateTo
          }
          getApplicants = await userDb.findAllForEndorsement(arg)
          break;
        case "Team Head":
          arg = {
            teamID: teamID,
            dateFrom: dateFrom,
            dateTo: dateTo
          }
          getApplicants = await userDb.findAllForEndorsement(arg)
          break;
      }

      let applicantData = getApplicants.rows

      for (let applicant of applicantData) {
        let firstName = decrypt(applicant.firstname),
            lastName = decrypt(applicant.lastname),
            email = decrypt(applicant.email),
            fullName = (applicant.middlename) ? `${firstName} ${decrypt(applicant.middlename)} ${lastName}` : `${firstName} ${lastName}`,
            applicationDate = (applicant.date_application) ? applicant.date_application : applicant.resume_upload_date

            applicantList.push({
              id: applicant.id,
              name: fullName,
              email: email,
              mobile: applicant.mobile,
              livestock: applicant.livestock,
              willingToTravel: applicant.willingToTravel,
              position: applicant.positionname,
              applicationDate,
              department: applicant.deptname,
              resumeUploadDate: applicant.resume_upload_date,
              resumeUploadUrl: applicant.resume_upload_url,
              stage: applicantList.stage,
              forEndorsementDate: applicant.date_for_endorsement
        })
      }
    
    } catch (error) {
      console.log("For-endorsement error: ", error)
      return "Failed to fetch for-endorsement applicants."
    }
    
    return applicantList;
  };
};

module.exports = forEndorsedUser;
