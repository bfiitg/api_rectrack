const { decrypt } = require('../../entities/users/app');

const filterReportsAllUseCase = ({ userDb }) => {
    return async function getReports(info){

        let fetched = [];
        let results = [];
        let reports = ""

        switch(info.status){
            case 'kept for reference':
                reports = await userDb.filterKeptfroRef(info);
                fetched = reports.rows;
                break;
            case 'deployed':
                reports = await userDb.filterDeployed(info);
                fetched = reports.rows;
                break;
            case 'blacklist':
                reports = await userDb.filterBlacklist(info);
                fetched = reports.rows;
                break;
            case 'rejected':
                reports = await userDb.filterRejected(info);
                fetched = reports.rows;
                break;
        }

        // const reports = await userDb.filterReports(queryInfo);
        // const fetched = reports.rows;
        // const results = [];

        for(let i = 0; i < fetched.length; i++){
            var rejectedBy = null;
            if(fetched[i].rejected_by_fname){
                rejectedBy = decrypt(fetched[i].rejected_by_fname) + ' ' + decrypt(fetched[i].rejected_by_lname)
            }

            results.push({
                email: decrypt(fetched[i].email),
                name: decrypt(fetched[i].firstname) + " " + decrypt(fetched[i].lastname),
                mobile: fetched[i].mobile,
                livestock: fetched[i].livestock,
                willing_to_travel: fetched[i].willing_to_travel,
                position: fetched[i].position_name,
                application_date: fetched[i].date_application,
                date_deployed: fetched[i].date_deployed,
                date_blacklist: fetched[i].date_blacklist,
                date_rejected: fetched[i].date_rejected,
                remarks_blacklist: fetched[i].remarks_blacklist,
                remarks_reject: fetched[i].remarks_reject,
                resume_upload_date: fetched[i].resume_upload_date,
                date_kept_reference: fetched[i].date_kept_reference,
                rejected_by: rejectedBy
            });
        }

        return results;
    }
}

module.exports = filterReportsAllUseCase;