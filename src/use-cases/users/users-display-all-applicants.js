const { decrypt } = require('../../entities/users/app');

const getAllApplicants = ({ userDb }) => {
   return async function get(info) {
      let result, applicantList = [], msg
      const {deptid, posid, role, deptID, teamID, dateFrom, dateTo, offset, limit} = info

      try {
         if (deptid && !posid) {
            result = await userDb.getAllApplicants(deptid)
         }

         else if (deptid && posid) {
            result = await userDb.getAllApplicants(deptid, posid)
         }

         else {
            switch (role) {
               case 'People Group':
               case 'Operations Head':
               case 'Administrator':
                  result = await userDb.findAllApplicants({dateFrom, dateTo})
                  break;
               case 'Team Head':
                  result = await userDb.findAllApplicantsTH({teamID, dateFrom, dateTo});
                  break;
               case 'Department Head':
                  result = await userDb.findAllApplicantsDH({deptID, dateFrom, dateTo})
                  break;
            }
         }

         if (result) {

            let results = result.rows
            
            for (let applicant of results) {
               let id = applicant.id,
                  firstName = decrypt(applicant.firstname),
                  lastName = decrypt(applicant.lastname),
                  email = decrypt(applicant.email),
                  fullName = `${firstName} ${lastName}`,
                  applicationDate = (applicant.date_application) ? applicant.date_application.toISOString().split('T')[0] : applicant.resume_upload_date
                  
               applicantList.push({
                  id: id,
                  email: email,
                  name: fullName,
                  mobile: applicant.mobile,
                  livestock: applicant.livestock,
                  willing_to_travel: applicant.willing_to_travel,
                  position: (applicant.name) ? applicant.name : '',
                  resume_upload_date: applicant.resume_upload_date,
                  resume_upload_url: applicant.resume_upload_url,
                  date_application: applicationDate,
                  running_time: applicant.running_time,
                  stage: applicant.stage
               })
            }

         }

         return applicantList

      } catch (e) {
         console.log(e)
         return msg = "Failed to load applicant list."
      }
   }
}

module.exports = getAllApplicants