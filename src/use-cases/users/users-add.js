const { makeUser, decrypt } = require("../../entities/users/app");

const makeAddUser = ({ userDb, sendEmail }) => {


  return async function posts(userInfo) {
    const result = makeUser(userInfo);

    const emailExist = await userDb.findByEmail(result.getEmail());
    const rows = emailExist.rows;
    
    if(rows.length != 0){
      const result = {
        msg: "User already exist!"
      };
      return  result
    }


    if (emailExist.rowCount !== 0) {

      const result = {
        msg: "Email already exists!",
        command: emailExist.command,
        rows
      };
      return result;
    }

    const userExist = await userDb.findByFullName(
      result.getFirstName(),
      result.getLastName()
    );

    
    if (userExist.rowCount !== 0) {
      const result = {
        msg: "User already exist!"
      };
      return result;
    }
    var letters =/^\d{10}$/;
    const mobiles = result.getMobile();
    if (mobiles.match(letters)) { throw new Error(`Error Contact number must not contain letters.`)} 

    let applicantID, departmentID, applicantPosition, prompt = 'Failed to add applicant.'

    await userDb.insert({
      email: result.getEmail(),
      firstname: result.getFirstName(),
      lastname: result.getLastName(),
      mobile: result.getMobile(),
      livestock: result.getLiveStock(),
      travel: result.getTravel(),
      resume_url: result.getResumeURL(),
      position_id: result.getPosition(),
      middlename: result.getMiddleName(),
      religion: result.getReligion(),
      isQuickApplicant: userInfo.isQuickApplicant
    })
    .then(res => {
      applicantID = res.applicantID
      prompt = 'Successfully added applicant.'
    })
    .catch(err => console.log(err))

    await userDb.fetchDepartmentTeamPositionBy(applicantID)
                .then(res => {
                  if (res.rows[0]) {
                    departmentID = res.rows[0].departmentid
                    applicantPosition = res.rows[0].position 
                  }
                })
                .catch(err => console.log(err))

    const datas = {
      prompt,
      data: {
        applicantID: applicantID,
        departmentID: departmentID,
        position: applicantPosition
      }
    };
    

    return datas;
  };
};

module.exports = makeAddUser;
