const {
  generateString,
  encrypt
} = require("../../entities/users/app");
const MobileVal = require('../../functions/app');

const userScheduleAssessment = ({ userDb }) => {
  return async function puts({ id, ...info } = {}) {
    
    const date = info.date;
    const name = info.name;
    const contact = MobileVal(info.contact);
    const location = info.location;

    var letters = /^\d{10}$/;

    if (contact.match(letters)) { throw new Error(`Error Contact number must not contain letters.`) }

    if (!date || !name || !contact || !location) {
      throw new Error("Please input all info needed.");
    } 

    const password = generateString();

    let assessmentSched = await userDb.scheduleForAssessment(id, date, encrypt(password));
    // insert or update info person's name, contact, location
    await userDb.assessmentContacts(id, name, contact, location);

    let result = (assessmentSched.rowCount > 0) ? "Update successful." : "Update failed."

    return {
      result
    };
    
  };
};

module.exports = userScheduleAssessment;
