const getApplicantDateTrackings = ({ userDb }) => {
    return async function getDateTrackings(id){
        fetch = await userDb.getApplicantDateTrackings(id); 
        return fetch.rows[0];
    }
}

module.exports = getApplicantDateTrackings;