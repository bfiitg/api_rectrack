const { makeReferralForms } = require("../../entities/users/app");

const makeAddReferral = ({ userDb }) => {
  return async function posts(userInfo) {
    const result = makeReferralForms(userInfo);

    let applicantReferralCheck = await userDb.checkIfHasReferral(result.getApplicantId())
    
    if(applicantReferralCheck.rowCount > 0) {
      throw new Error("Applicant has already been referred.")
    }

    let res = await userDb.addReferralForm({
      referrer_id: result.getReferrerId(),
      applicant_id: result.getApplicantId(),
      referral_relationship_referrer: result.getReferralRelationshipReferrer()
    })

    let prompt = (res.rowCount > 0) ? "Added new referral form successfully." : "Failed to add referral form."

    return prompt;
  };
};

module.exports = makeAddReferral;
