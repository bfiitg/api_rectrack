const { decrypt } = require("../../entities/users/app");

const getLoggedInUserFirstname = ({ userDb }) => {
  return async function get(token) {
    const users = await userDb.getNameThruToken(token);
    const result = users.rows;
    const results = [];
    for (let i = 0; i < result.length; i++) {
      results.push({
        firstname: decrypt(result[i].firstname),
        lastname: decrypt(result[i].lastname)
      });
    }
    return results;
  };
};

module.exports = getLoggedInUserFirstname;
