const checkTokens = ({ userDb }) => {
  return async function posts(token) {
    const data = await userDb.checkToken(token);
    return data.rowCount;
  };
};

module.exports = checkTokens;
