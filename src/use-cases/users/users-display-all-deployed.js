const { decrypt } = require("../../entities/users/app");

const deployedUser = ({ userDb }) => {
  return async function get(params, reqMethod) {
    let users, listOfDeployedUsers = []
    const { dateFrom, dateTo, teamID, deptID, offset, limit, role} = params

    try {
      if (reqMethod === 'GET') {
        await userDb.getAllDeployed(params)
                    .then(res => users = res)
      }

      if (reqMethod === 'POST') {
        switch (role) {
          case "Administrator":
          case "Operations Head":
          case "People Group":
            users = await userDb.findAllDeployed({dateFrom, dateTo, offset, limit})
            break;
          case "Team Head":
            users = await userDb.findAllDeployedTH({teamID, dateFrom, dateTo, offset, limit})
          case "Department Head":
            users = await userDb.findAllDeployedDH({deptID, dateFrom, dateTo, offset, limit})
            break;
        }
      }

      let deployedUsers = users.rows
      for (let deployedUser of deployedUsers) {
        let firstName = decrypt(deployedUser.firstname),
          lastName = decrypt(deployedUser.lastname),
          fullName = (deployedUser.middlename) ? `${firstName} ${decrypt(deployedUser.middlename)} ${lastName}` : `${firstName} ${lastName}`,
          email = decrypt(deployedUser.email)

        listOfDeployedUsers.push({
          id: deployedUser.id,
          email: email,
          name: fullName,
          mobile: deployedUser.mobile,
          livestock: deployedUser.livestock,
          done_IQBE: deployedUser.is_done_iqbe_exam,
          willing_to_travel: deployedUser.willing_to_travel,
          position: deployedUser.name,
          deptName: deployedUser.deptname,
          resume_upload_date: deployedUser.resume_upload_date,
          resume_upload_url: deployedUser.resume_upload_url,
          date_application: deployedUser.date_application,
          date_received: deployedUser.date_received,
          date_online_exam: deployedUser.date_online_exam,
          date_endorsement: deployedUser.date_endorsement,
          date_assessment: deployedUser.date_assessment,
          date_shortlist: deployedUser.date_shortlist,
          date_deployed: deployedUser.date_deployed,
          no_of_days_deployed: deployedUser.no_of_days_deployed,
          deptId: deployedUser.deptid,
          stage: deployedUser.status
        })
      }

      return listOfDeployedUsers;
    } catch (error) {
      console.log(error)
    }
  };
};

module.exports = deployedUser;
