const { decrypt, encrypt } = require("../../entities/users/app");

const endorsedUser = ({ userDb }) => {
  return async function get(deptid, posid, {info}) {
    let getEndorsedUsers, listOfEndorsedUsers = []
    const { deptID, teamID, dateFrom, dateTo, role } = info
    try {
      if (deptid) {
        getEndorsedUsers = await userDb.findAllEndorsementFiltered(deptid)
      }
      else if (deptid && posid) {
        getEndorsedUsers = await userDb.findAllEndorsementFiltered(deptid, posid)
      } else {
        switch (role) {
          case "Administrator":
          case "Operations Head":
          case "People Group":
            getEndorsedUsers = await userDb.findAllEndorsement({dateFrom, dateTo});
            break;
          case "Department Head":
            getEndorsedUsers = await userDb.findAllEndorsementDH({deptID, dateFrom, dateTo});
            break;
          case "Team Head":
            getEndorsedUsers = await userDb.findAllEndorsementTh({teamID, dateFrom, dateTo});
            break;
        }
      }
      
      let endorsedUsers = getEndorsedUsers.rows;

      for(let endorsedUser of endorsedUsers) {
        let firstName = decrypt(endorsedUser.firstname),
            lastName = decrypt(endorsedUser.lastname),
            fullName = (endorsedUser.middlename) ? `${firstName} ${decrypt(endorsedUser.middlename)} ${lastName}` : `${firstName} ${lastName}`,
            email = decrypt(endorsedUser.email)

            listOfEndorsedUsers.push({
              id: endorsedUser.id,
              email: email,
              name: fullName,
              mobile: endorsedUser.mobile,
              livestock: endorsedUser.livestock,
              willing_to_travel: endorsedUser.willing_to_travel,
              done_IQBE: endorsedUser.is_done_iqbe_exam,
              position: endorsedUser.name,
              resume_upload_date: endorsedUser.resume_upload_date,
              resume_upload_url: endorsedUser.resume_upload_url,
              startDateExam: endorsedUser.online_exam_start,
              endDateExam: endorsedUser.online_exam_end,
              date_received: endorsedUser.date_received,
              date_application: endorsedUser.date_application,
              running_time: endorsedUser.running_time,
              deptId: endorsedUser.deptid,
              endorsementDate: endorsedUser.date_endorsement,
              forEndorsementDate: endorsedUser.date_for_endorsement
        })
      }
    } catch (error) {
      console.log(error)
    }
    return listOfEndorsedUsers;
  };
};

module.exports = endorsedUser;
