const userDoneIQBE = ({ userDb }) => {
  return async function puts({ id } = {}) {
    const updated = await userDb.doneIQBE(id);
    return { ...updated };
  };
};

module.exports = userDoneIQBE;
