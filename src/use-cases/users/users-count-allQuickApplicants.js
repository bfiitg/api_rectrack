

const countQuickApplicantsUseCase = ({ userDb }) => {
    return async function get(roles){
      
    
        if(roles == "PG"){
            const quickApplicantPg = await userDb.notificationQuickApplicantPg()  
            const data = quickApplicantPg.rows[0].count;
          
            return data;
        }
        else if(roles == "Operations Head"){
            const quickApplicantOh = await userDb.notificationQuickApplicantOh()
            const data = quickApplicantOh.rows[0].count
           
            return data;
           
        }
        
        
      
    };
};

module.exports = countQuickApplicantsUseCase;