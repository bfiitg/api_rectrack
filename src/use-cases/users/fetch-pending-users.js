const { decrypt } = require("../../entities/users/app");
const fetchApplicantUseCases = ({ userDb, sendEmail }) => {
    return async function post(info) {
        const { id, role, deptID, teamID, dateTo, dateFrom } = info;
        let result;
        const DataArray = [];
        const AccessRole_fetchAllPendingStatus = (dateTo && dateFrom) ? await userDb.fetchAllPendingStatus(id, dateTo, dateFrom) : await userDb.fetchAllPendingStatus(id, dateTo, dateFrom);
        const AccessRole_findAllPendingTh = (dateTo && dateFrom) ? await userDb.findAllPendingTh(teamID, dateTo, dateFrom) : await userDb.findAllPendingTh(teamID, dateTo, dateFrom);
        const AccessRole_findAllPendingDh = (dateTo && dateFrom) ? await userDb.findAllPendingDh(deptID, dateTo, dateFrom) : await userDb.findAllPendingDh(deptID, dateTo, dateFrom);

        if (role == 'Administrator' || role == 'People Group' || role == 'Operations Head') { result = AccessRole_fetchAllPendingStatus.rows }
        else if (role == 'Team Head') { result = AccessRole_findAllPendingTh.rows } else if (role == 'Department Head') { result = AccessRole_findAllPendingDh.rows }

        for (let i = 0; i < result.length; i++) {
            const e = result[i];
            DataArray.push({
                id: e.id,
                email: decrypt(e.email),
                name: `${decrypt(e.firstname)} ${decrypt(e.middlename)} ${decrypt(e.lastname)}`,
                mobile: e.mobile,
                livestock: e.livestock,
                willing_to_travel: e.willing_to_travel,
                position: e.name,
                resume_upload_date: e.resume_upload_date,
                resume_upload_url: e.resume_upload_url,
                date_received: e.date_received,
                scheduleAssessment: e.date_interview,
                onlineExamStartDate: e.online_exam_start,
                onlineExamEndDate: e.online_exam_end,
                date_for_endorsement: e.date_for_endorsement,
                date_endorsed: e.date_endorsed,
                date_assessment: e.date_assessment,
                date_pending: e.date_pending,
                running_time: e.running_time,
                deptId: e.deptid,
            });

        }

        return DataArray
    }
}

module.exports = fetchApplicantUseCases;