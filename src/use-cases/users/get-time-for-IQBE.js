const IQBESubtractTime = ({ userDb }) => {
  return async function selects(token) {
    const result = await userDb.IQBEGetTime(token);
    const id = result.rows[0].id;
    let times = result.rows[0].iqbe_time;

    if (times !== 0) {
      times--;
      // update elapsed time in db
      await userDb.minusTimeForIQBE(times, token);
    }

    const data = {
      id,
      times
    };

    return data;
  };
};

module.exports = IQBESubtractTime;
