const logsInsert = ({ userDb }) => {
  return async function posts(info) {
    if (!info.module || !info.token || !info.table_affected || !info.activity) {
      // exit if lacking data
      return;
    }
    const token = info.token;

    const idData = await userDb.getIdThruToken(token);

    const userId = idData.rows[0].id;

    await userDb.insertUserLogs(userId, info);
    const data = {
      msg: "Inserted successfully."
    };
    return data;
  };
};

module.exports = logsInsert;
