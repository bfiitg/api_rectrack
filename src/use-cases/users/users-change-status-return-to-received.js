const returnApplicantToReceived = ({ userDb }) => {
   return async function set (applicantID) {
      try {
         if (!applicantID) {
            throw new Error("Please provide an applicant ID.")
         }
   
         let result = await userDb.changeStatusReceived(applicantID)
   
         let prompt = (result.rowCount > 0) ? 'Successfully set applicant stage to received.' : 'Failed to set applicant stage to received.'
         
         return {
            prompt
         }
      } catch (error) {
         console.log(error)
      }
   }
}

module.exports = returnApplicantToReceived