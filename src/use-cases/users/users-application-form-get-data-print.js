const { decrypt } = require("../../entities/users/app");

const getApplicantNamePV = ({ userDb }) => {
  return async function get(id) {
    const users = await userDb.getUsersDataApplicationFormPrintView(id);
    const result = users.rows;
    const results = [];
    for (let i = 0; i < result.length; i++) {
      results.push({
        userData: [
          {
            id: result[i].id,
            firstname: decrypt(result[i].firstname),
            lastname: decrypt(result[i].lastname),
            middlename: result[i].middlename
              ? decrypt(result[i].middlename)
              : result[i].middlename,
            positionFirstChoice: result[i].name,
            positionSecondChoice: result[i].names,
            userImage: result[i].user_image_path,
            mobile: result[i].mobile
          }
        ]
      });
    }
    
    const teamsId = results[0].userData[0].teams_id;
    const positions = await userDb.getSameTeamPositionApplicationForm(teamsId);
    // push positions data
    results.push({
      positions: positions.rows
    });

    // from here is the data the applicant fill up
    // fetch data from db
    const data = [];
    const appData = await userDb.applicationFormDataPrintView(id);
    // return appData.rows;

    // educationalBgArray
    const appEduc = await userDb.applicantsEducation(id);
    let educationalBgArray = [];
    if (appEduc.rows.length > 0) {
      let educational_attainment = [];
      let institution_name = [];
      let institution_address = [];
      let course_degree = [];
      let years_attended = [];
      let honors_awards = [];
      for (let i = 0; i < appEduc.rows.length; i++) {
        const e = appEduc.rows[i];
        educational_attainment.push(e.educational_attainment);
        institution_name.push(e.institution_name);
        institution_address.push(e.institution_address);
        course_degree.push(e.course_degree);
        years_attended.push(e.years_attended);
        honors_awards.push(e.honors_awards);
      }
      educationalBgArray.push({
        educational_attainment,
        institution_name,
        institution_address,
        course_degree,
        years_attended,
        honors_awards
      });
    }
    // end
    // relativeArray in bfi
    const appRelativeBFI = await userDb.applicantsRelative(id);
    let relativeArray = [];
    if (appRelativeBFI.rows.length > 0) {
      let full_name = [];
      let position = [];
      let branch = [];
      let relationship = [];
      for (let i = 0; i < appRelativeBFI.rows.length; i++) {
        const e = appRelativeBFI.rows[i];
        full_name.push(e.full_name);
        position.push(e.position);
        branch.push(e.branch);
        relationship.push(e.relationship);
      }
      relativeArray.push({
        full_name,
        position,
        branch,
        relationship
      });
    }
    // end
    // professionalLicensesArray
    const appProfLicenses = await userDb.applicantsProfLicenses(id);
    let professionalLicensesArray = [];
    if (appProfLicenses.rows.length > 0) {
      let professional_licenses_held = [];
      let certificate_image_path = [];
      let certification_number = [];
      for (let i = 0; i < appProfLicenses.rows.length; i++) {
        const e = appProfLicenses.rows[i];
        professional_licenses_held.push(e.professional_licenses_held);
        certificate_image_path.push(e.certificate_image_path);
        certification_number.push(e.certification_number);
      }
      professionalLicensesArray.push({
        professional_licenses_held,
        certificate_image_path,
        certification_number
      });
    }
    // end
    // orgMembershipArray
    const appOrgMemberships = await userDb.applicantsOrgMemberships(id);
    let orgMembershipArray = [];
    if (appOrgMemberships.rows.length > 0) {
      let association_name = [];
      let position_title = [];
      let year_participation = [];
      for (let i = 0; i < appOrgMemberships.rows.length; i++) {
        const e = appOrgMemberships.rows[i];
        association_name.push(e.associaton_name);
        position_title.push(e.position_title);
        year_participation.push(e.year_participation);
      }
      orgMembershipArray.push({
        association_name,
        position_title,
        year_participation
      });
    }
    // end
    // seminarsAttendedArray
    const appSeminarsAttended = await userDb.applicantsSeminars(id);
    let seminarsAttendedArray = [];
    if (appSeminarsAttended.rows.length > 0) {
      let seminar_name = [];
      let description = [];
      let venue = [];
      let conducted_by = [];
      let dates = [];
      let certificate_image_path = [];
      for (let i = 0; i < appSeminarsAttended.rows.length; i++) {
        const e = appSeminarsAttended.rows[i];
        seminar_name.push(e.seminar_name);
        description.push(e.description);
        venue.push(e.venue);
        conducted_by.push(e.conducted_by);
        dates.push(e.dates);
        certificate_image_path.push(e.certificate_image_path);
      }
      seminarsAttendedArray.push({
        seminar_name,
        description,
        venue,
        conducted_by,
        dates,
        certificate_image_path
      });
    }
    // end
    // workExperienceArray
    const appWorkExperience = await userDb.applicantsWorkExp(id);
    let workExperienceArray = [];
    if (appWorkExperience.rows.length > 0) {
      let company_name = [];
      let address = [];
      let position = [];
      let start_date = [];
      let end_date = [];
      let last_salary = [];
      let reason_leaving = [];
      for (let i = 0; i < appWorkExperience.rows.length; i++) {
        const e = appWorkExperience.rows[i];
        company_name.push(e.company_name);
        address.push(e.address);
        position.push(e.position);
        start_date.push(e.start_date);
        end_date.push(e.end_date);
        last_salary.push(e.last_salary);
        reason_leaving.push(e.reason_leaving);
      }
      workExperienceArray.push({
        company_name,
        address,
        position,
        start_date,
        end_date,
        last_salary,
        reason_leaving
      });
    }
    // end
    // workWithBFIArray
    const appWorkWithBFI = await userDb.applicantsWorkWithBfi(id);
    let workWithBFIArray = [];
    if (appWorkWithBFI.rows.length > 0) {
      let have_work_with_bfi = "";
      let branch_company = [];
      let dates = [];
      let position = [];
      let department = [];
      for (let i = 0; i < appWorkWithBFI.rows.length; i++) {
        const e = appWorkWithBFI.rows[i];
        have_work_with_bfi = e.have_work_with_bfi;
        branch_company.push(e.branch_company);
        dates.push(e.dates);
        position.push(e.position);
        department.push(e.department);
      }
      workWithBFIArray.push({
        have_work_with_bfi,
        branch_company,
        dates,
        position,
        department
      });
    }
    // end
    // otherInfoArray
    const appOtherInfo = await userDb.applicantsOtherInfo(id);
    let otherInfoArray = [];
    if (appOtherInfo.rows.length > 0) {
      let typing_wpm = "";
      let steno_wpm = "";
      let hobbies = "";
      let language_speak_write = "";
      let machines_can_operate = "";
      let any_crime_committed = "";
      let been_hospitalized = "";
      let computer = "";
      let art_work = "";
      let crime_details = "";
      let hospitalized_details = "";
      for (let i = 0; i < appOtherInfo.rows.length; i++) {
        const e = appOtherInfo.rows[i];
        typing_wpm = e.typing_wpm;
        steno_wpm = e.steno_wpm;
        hobbies = e.hobbies;
        language_speak_write = e.language_speak_write;
        machines_can_operate = e.machines_can_operate;
        any_crime_committed = e.any_crime_commited;
        been_hospitalized = e.been_hospitalized;
        computer = e.computer;
        art_work = e.art_work;
        crime_details = e.crime_details;
        hospitalized_details = e.hospitalized_details;
      }
      otherInfoArray.push({
        typing_wpm,
        steno_wpm,
        hobbies,
        language_speak_write,
        machines_can_operate,
        any_crime_committed,
        been_hospitalized,
        computer,
        art_work,
        crime_details,
        hospitalized_details
      });
    }
    // end
    // familyBgArray
    const appFamilyBg = await userDb.applicantsFamilyBg(id);
    let familyBgArray = [];
    if (appFamilyBg.rows.length > 0) {
      let name = [];
      let relationship = [];
      let age = [];
      let occupation = [];
      let company_address = [];
      for (let i = 0; i < appFamilyBg.rows.length; i++) {
        const e = appFamilyBg.rows[i];
        name.push(e.name);
        relationship.push(e.relationship);
        age.push(e.age);
        occupation.push(e.occupation);
        company_address.push(e.company_address);
      }
      familyBgArray.push({
        name,
        relationship,
        age,
        occupation,
        company_address
      });
    }
    // end
    // isMarriedArray
    const appMarried = await userDb.applicantsMarried(id);
    let isMarriedArray = [];
    if (appMarried.rows.length > 0) {
      let is_married = "";
      let number_children = "";
      let range_age = "";
      let do_you = "";
      let with_who = "";
      for (let i = 0; i < appMarried.rows.length; i++) {
        const e = appMarried.rows[i];
        is_married = e.is_married;
        number_children = e.number_children;
        range_age = e.range_age;
        do_you = e.do_you;
        with_who = e.with_who;
      }
      isMarriedArray.push({
        is_married,
        number_children,
        range_age,
        do_you,
        with_who
      });
    }
    // end
    // referencesArray
    const appReference = await userDb.applicantsReference(id);
    let referencesArray = [];
    if (appReference.rows.length > 0) {
      let name = [];
      let address = [];
      let telephone = [];
      let occupation = [];
      let how_long_known = [];
      for (let i = 0; i < appReference.rows.length; i++) {
        const e = appReference.rows[i];
        name.push(e.name);
        address.push(e.address);
        telephone.push(e.telephone);
        occupation.push(e.occupation);
        how_long_known.push(e.how_long_known);
      }
      referencesArray.push({
        name,
        address,
        telephone,
        occupation,
        how_long_known
      });
    }
    // end
    // willingnessArray
    const appWillingness = await userDb.applicantsWillingness(id);
    let willingnessArray = [];
    if (appWillingness.rows.length > 0) {
      let explain_why_hire = "";
      let willing_train_apprentice = "";
      let willing_to_BOND = "";
      let sss = "";
      let tin = "";
      let residence_certificate = "";
      let residence_certificate_date_issue = "";
      let residence_certificate_place_issue = "";
      for (let i = 0; i < appWillingness.rows.length; i++) {
        const e = appWillingness.rows[i];
        explain_why_hire = e.explain_why_hire;
        willing_train_apprentice = e.willing_train_apprentice;
        willing_to_BOND = e.willing_to_BOND;
        sss = e.sss;
        tin = e.tin;
        residence_certificate = e.residence_certificate;
        residence_certificate_date_issue = e.residence_certificate_date_issue;
        residence_certificate_place_issue = e.residence_certificate_place_issue;
      }
      willingnessArray.push({
        explain_why_hire,
        willing_train_apprentice,
        willing_to_BOND,
        sss,
        tin,
        residence_certificate,
        residence_certificate_date_issue,
        residence_certificate_place_issue
      });
    }
    data.push({
      results,
      userData: appData.rows,
      educationalBgArray,
      relativeArray,
      professionalLicensesArray,
      orgMembershipArray,
      seminarsAttendedArray,
      workExperienceArray,
      workWithBFIArray,
      otherInfoArray,
      familyBgArray,
      isMarriedArray,
      referencesArray,
      willingnessArray
    });
    return data;
    // end
    //-------------------
    // images to fetch
    // end
  };
};

module.exports = getApplicantNamePV;
