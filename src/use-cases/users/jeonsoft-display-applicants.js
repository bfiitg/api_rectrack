const { decrypt } = require("../../entities/users/app");

const displayAllApplicantsJeonSoft = ({ userDb }) => {
  return async function get() {
    const users = await userDb.displayApplicantsReadyForExport();
    const result = users.rows;
    const results = [];
    for (let i = 0; i < result.length; i++) {
      results.push({
        id: result[i].id,
        email: decrypt(result[i].email),
        firstname: decrypt(result[i].firstname),
        middlename: result[i].middlename
          ? decrypt(result[i].middlename)
          : result[i].middlename,
        lastname: decrypt(result[i].lastname),
        is_exported: result[i].is_exported
      });
    }
    return results;
  };
};

module.exports = displayAllApplicantsJeonSoft;
