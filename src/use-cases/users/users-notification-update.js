const updateApplicantNotification = ({ userDb }) => {
   return async function ( info ) {
      const { entryID, role } = info

      if (!entryID) {
         throw new Error("Please provide a row id.")
      }

      if (!role) {
         throw new Error("Please provide user role.")
      }
      
      let prompt

      await userDb.updateApplicantNotification(info)
                  .then(res => prompt = `Successfully updated row id: ${res.rows[0].id}`)
                  .catch(err => prompt = "Failed to update row id.")

      return prompt
   }
}

module.exports = updateApplicantNotification