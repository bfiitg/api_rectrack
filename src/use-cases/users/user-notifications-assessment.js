const notificationAssessment = ({ userDb }) => {
  return async function get(id, teamId, role) {
    if(role){
      const users = await userDb.notificationAssessmentOh();
      return users.rows;
    }
    else if(teamId){
      const users = await userDb.notificationAssessmentTh(teamId);
      return users.rows;
    } else{
      const users = await userDb.notificationAssessmentDp(id);
      return users.rows;
    }
  };
};

module.exports = notificationAssessment;
