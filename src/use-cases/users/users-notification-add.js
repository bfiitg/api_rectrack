const addApplicantNotification = ({userDb}) => {
   return async function ({ userID, applicantID, comment, actionID, role, deptID, teamID}) {

      let prompt, result

      if (!userID) {
         throw new Error("Please provide a user ID.")
      }

      if (!applicantID) {
         throw new Error("Please provide an applicant ID.")
      }

      if (!comment) {
         throw new Error("Please provide a comment.")
      }

      if (!actionID) {
         throw new Error("Please provide an action ID.")
      }

      if (!role) {
         throw new Error("Please provide a role.")
      }

      await userDb.addApplicantNotification(info)
                  .then(res => result = res)
                  .catch(err => {throw new Error(err.message)})

      prompt = (result) ? ` Successfully added notification.` : ` Failed to add notification. `


      return prompt, result
   }
}

module.exports = addApplicantNotification