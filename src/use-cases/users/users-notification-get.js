const getApplicantNotification = ({userDb, decrypt}) => {
   return async function (info) {
      const {role, deptID, teamID, dateFrom, dateTo} = info
      let results, prompt, listOfNotifs = []

      // if (!role) {
      //    throw new Error("Please provide a role.")
      // }

      await userDb.getApplicantNotification({role, deptID, teamID, dateFrom, dateTo})
                  .then(res => results = res.rows)
                  .catch(err => {throw new Error(err.message)})
      
      for (result of results) {
         let uFirstName = decrypt(result.u_fn),
            uLastName = decrypt(result.u_ln),
            appFirstName = decrypt(result.app_fn),
            appLastName = decrypt(result.app_ln)

         listOfNotifs.push({
            entryID: result.id,
            date: result.date.toISOString().split('T')[0],
            userID: result.user_id,
            userFirstName: uFirstName,
            userLastName: uLastName,
            userEmail : decrypt(result.userEmail),
            userMobile: result.userMobile,
            userRole: result.userRole,
            userPosition: result.userPosition,
            userTeam: result.userTeam,
            userDepartment: result.userDepartment,
            applicantID: result.applicant_id,
            applicantFirstName: appFirstName,
            applicantLastName: appLastName,
            applicantEmail: decrypt(result.applicantEmail),
            applicantMobile: result.applicantMobile,
            applicantPosition: result.applicantPosition,
            applicantTeam: result.applicantTeam,
            applicantDepartment: result.applicantDepartment,
            comment: result.comment,
            actionID: result.action_id,
            action: result.action
         })
      }

      prompt = (results.rowCount > 0) ? ` Successfully fetched notifications.` : ` Failed to fetch notifications. `

      return prompt, listOfNotifs
   }
}

module.exports = getApplicantNotification