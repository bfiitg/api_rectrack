const notificationDeployed = ({ userDb }) => {
  return async function get(id, teamId, role) {
    let users
    switch (role) {
      case "Operations Head":
        users = await userDb.notificationDeployedOh();
        break;
      case "Administrator":
        users = await userDb.notificationDeployedAdmin();
        break;
      case "Team Head":
        users = await userDb.notificationDeployedTh(teamId);
        break;
      case "Department Head":
        users = await userDb.notificationDeployedDp(id)
        break;
    }
    return users.rows
  };
};

module.exports = notificationDeployed;
