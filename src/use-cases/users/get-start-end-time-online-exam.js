const getStartEndTime = ({ userDb }) => {
  return async function selects(token, time) {
    const result = await userDb.returnStartEndDate(token);
    const id = result.rows[0].id;
    const totalTime = result.rows[0].total_time_exam_seconds
    const monitor = await userDb.monitorAnswer(token)
    const answers = monitor.rows
    let prompt

    await userDb.elapsedTimes(time, token)
                .then(res => {
                  prompt = (res.rowCount > 0) ? 'Successfully updated elapsed time' : 'Token for existing user does not exist.'
                })
                .catch(err => {
                  throw new Error(err.message)
                })

    const data = {
      id,
      prompt,
      totalTime,
      answers
    };

    return data;
  };
};

module.exports = getStartEndTime;
