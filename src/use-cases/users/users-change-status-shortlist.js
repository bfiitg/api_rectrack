const userChangeStatusShortList = ({ userDb }) => {
  return async function puts({ id } = {}) {

    const updated = await userDb.changeStatusShortList(id);

    let result = (updated.rowCount > 0) ? "Successfully updated applicant stage to shortlist." : "Failed to update applciant status to shortlist."

    return {
      result
    }
  };
};

module.exports = userChangeStatusShortList;
