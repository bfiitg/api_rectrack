const { decrypt } = require('../../entities/users/app');

const filterReportsUseCase = ({ userDb }) => {
    return async function getReports(info) {

        const users = [
            "",
            "department = " + info.dept_id + " AND",
            "team = " + info.team_id + " AND"
        ];
        const dates = ["date_kept_reference", "date_deployed", "date_blacklist", "date_rejected"];

        let user, date;

        switch (info.user) {
            case 'Operations Head':
                user = users[0];
                break;
            case 'People Group':
                user = users[0];
                break;
            case 'Department Head':
                user = users[1];
                break;
            case 'Team Head':
                user = users[2];
        };

        switch (info.status) {
            case 'Keep for Reference':
                date = dates[0];
                break;
            case 'Deployed':
                date = dates[1];
                break;
            case 'Blacklisted':
                date = dates[2];
                break;
            case 'Rejected':
                date = dates[3];
                break;
        }

        var queryInfo = {
            user: user,
            date: date,
            status: info.status,
            startDate: info.startDate,
            endDate: info.endDate,
            posid: info.pos_id,
            deptid: info.dept_id
        }

        const reports = await userDb.filterReports(queryInfo);
        const fetched = reports.rows;
        const results = [];

        for (let i = 0; i < fetched.length; i++) {
            var rejectedBy = null;
            if (fetched[i].rejected_by_fname) {
                rejectedBy = decrypt(fetched[i].rejected_by_fname) + ' ' + decrypt(fetched[i].rejected_by_lname)
            }

            results.push({
                email: decrypt(fetched[i].email),
                name: decrypt(fetched[i].firstname) + " " + decrypt(fetched[i].lastname),
                mobile: fetched[i].mobile,
                livestock: fetched[i].livestock,
                willing_to_travel: fetched[i].willing_to_travel,
                position: fetched[i].position_name,
                application_date: fetched[i].date_application,
                date_deployed: fetched[i].date_deployed,
                date_rejected: fetched[i].date_rejected,
                date_blacklist: fetched[i].date_blacklist,
                remarks_blacklist: fetched[i].remarks_blacklist,
                remarks_reject: fetched[i].remarks_reject,
                resume_upload_date: fetched[i].resume_upload_date,
                date_kept_reference: fetched[i].date_kept_reference,
                rejected_by: rejectedBy
            });
        }

        return results;
    }
}

module.exports = filterReportsUseCase;