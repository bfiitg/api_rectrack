const applicantUpdatePosition = ({ userDb }) => {
  return async function puts(userId, posId) {

    const find = await userDb.findUser(userId)
    
    if(find.rows[0].status=="endorsement"){
      const update = await userDb.updatePositionToRecieved(userId)
    }
    const updated = await userDb.updatePositionOfApplicant(userId, posId);
    if (updated.rowCount > 0) {
      const data = { msg: "Updated successfully." };
      return data;
    } else {
      const data = { msg: "Update unsuccessfull." };
      return data;
    }
  };
};

module.exports = applicantUpdatePosition;
