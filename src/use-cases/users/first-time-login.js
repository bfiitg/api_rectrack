const { encrypt, decrypt } = require("../../entities/users/app");

const changePassword = ({ userDb, sendEmail, makeToken }) => {
  return async function puts({
    id,
    password,
    isForgotPassword,
    pw,
    checkpw,
    isRequestForgotPassword,
    email,
    baseUrl,
  } = {}) {
    // to update password
    if (isForgotPassword) {
      // forgot password
      if (!id) throw new Error(`Please enter ID of the user.`);
      if (!pw) throw new Error(`Please enter password.`);
      if (!checkpw) throw new Error(`Please re-enter password.`);
      if (pw !== checkpw)
        throw new Error(`Password doesn't match, please try again.`);

      const info = {
        id,
        pw: encrypt(pw),
      };
      const res = await userDb.forgotPassword({ info });

      let msg = `Error on updating the account, please try again.`;
      if (res.rowCount > 0) {
        msg = `Successfully updated the account.`;
      }
      return msg;
    }

    // to email link for forgot password
    if (isRequestForgotPassword) {
      if (!email) throw new Error(`Please enter email.`);
      if (!baseUrl) throw new Error(`Please enter base URL.`);
      const encryptEmail = encrypt(email);
      // get existing password
      const pw = await userDb.getPassword(encryptEmail);
      if (pw.rowCount == 0) throw new Error(`Please contact administrator.`);
      const password = pw.rows[0].password;
      const id = pw.rows[0].id;

      const token = await makeToken(encryptEmail, password);

      // update token to db
      const update = await userDb.updateToken(token, id);
      if (update.rowCount == 0)
        throw new Error(`Encountered an error, please try again.`);

      // for email

      // const userInfo = await userDb.returnEmailAndName(id);

      // const rawName = userInfo.rows[0].names;
      // const nameArray = rawName.split(" ");
      // const firstName = decrypt(nameArray[0]);

      // email composition
      // const mailOptions = {
      //   from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
      //   to: email,
      //   subject: "BFI : RecTrack Forgot Password",
      //   html: `Hi ${firstName},<br/><br/>
      //   You have requested to reset your password for your account on RecTrack System. Please click the button to reset it.<br/><br/>

      //   <a href="${baseUrl}?token=${token}&id=${id}" target="_blank"
      //   style="background: green; color: white; padding: 10px;">Reset</a>

      //   <br/><br/>
      //   Regards,<br/><br/>
      //   Biotech Farms Inc.
      //   <br/><br/>
      //   <div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email.
      //   This mailbox does not allow incoming messages.</div>`,
      // };

      // send email
      // const send = await sendEmail(mailOptions);
      // let msg = `Error on sending email, please try again.`;
      // if (send.status == 200) {
      //   msg = `We have sent you an email for your request, please check your email.`;
      //   return msg;
      // }
      // throw new Error(msg);
      // end
    }

    // first time login
    const encryptPassword = encrypt(password);
    const updated = await userDb.firstTimeLoginChangePass(id, encryptPassword);
    if (updated.rowCount > 0) {
      // updated successfully
      const data = {
        msg: "Updated password successfully.. We will logout you shortly...",
      };
      return data;
    }
  };
};

module.exports = changePassword;
