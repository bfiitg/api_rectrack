const assessmentShowListPositions = ({ userDb }) => {
  return async function get(id) {
    const users = await userDb.assessmentUpdatePositionShowList(id);
    return users;
  };
};

module.exports = assessmentShowListPositions;
