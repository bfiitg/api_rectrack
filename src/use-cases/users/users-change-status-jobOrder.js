const userChangeStatusJobOrder = ({ userDb }) => {
  return async function puts({ id } = {}) {
    
    // change status job order
    const updated = await userDb.changeStatusJobOrder(id);
    const count = updated.rowCount;
    const result = (count > 0) ? "Updated successfully." : "Update failed."

    return {
      result
    };
  };
};

module.exports = userChangeStatusJobOrder;
