const { decrypt } = require("../../entities/users/app");

const userChangeStatusBlacklist = ({ userDb }) => {
  return async function puts({ id, remarks } = {}) {
    if (!remarks) {
      const data = { msg: "Please enter some remarks." };
      return data;
    }
    
    const updated = await userDb.changeStatusBlacklist(id, remarks);
    const result = (updated.rowCount > 0) ? "Update successful." : "Update failed,"
    
    return {
      result
    };
  };
};

module.exports = userChangeStatusBlacklist;
