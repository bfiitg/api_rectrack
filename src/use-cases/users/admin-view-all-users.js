const { decrypt } = require("../../entities/users/app");

const adminViewAllUser = ({ userDb }) => {
  return async function get() {
    const users = await userDb.viewAllUsers();
    const result = users.rows;
    const results = [];
    for (let i = 0; i < result.length; i++) {
      results.push({
        id: result[i].id,
        email: decrypt(result[i].email),
        name: decrypt(result[i].firstname) + " " + decrypt(result[i].lastname),
        mobile: result[i].mobile,
        status: result[i].status,
        position: result[i].position,
        team: result[i].team,
        department: result[i].department
      });
    }
    return results;
  };
};

module.exports = adminViewAllUser;
