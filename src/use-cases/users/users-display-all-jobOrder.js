const { decrypt } = require("../../entities/users/app");
const jobOrderUser = ({ userDb }) => {
  return async function get(deptid, posid, {info}) {
    try {
      let result;
      if (deptid && !posid) {
        await userDb.findAllJobOrder(deptid).then(res => result = res.rows)
      }
      if (deptid && posid){
        await userDb.findAllJobOrder(deptid, posid).then(res => result = res.rows)
      }

      const { role, deptID, teamID, dateFrom, dateTo } = info;

      const DataArray = [];

      switch (role) {
        case "Administrator":
        case "People Group":
        case "Operations Head":
          await userDb.getAllJobOffer({dateTo, dateFrom})
                      .then(res => result = res.rows)
          break;
        case "Department Head":
          await userDb.getJobOfferDh({deptID, dateTo, dateFrom})
                      .then(res => result = res.rows)
          break;
        case "Team Head":
          await userDb.getJobOrderTh({teamID, dateTo, dateFrom})
                      .then(res => result = res.rows)
          break;
      }


      for (let applicant of result) {
          DataArray.push({
              id: applicant.id,
              email: decrypt(applicant.email),
              name: `${decrypt(applicant.firstname)} ${decrypt(applicant.lastname)}`,
              mobile: applicant.mobile,
              livestock: applicant.livestock,
              willing_to_travel: applicant.willing_to_travel,
              position: applicant.name,
              resume_upload_date: applicant.resume_upload_date,
              resume_upload_url: applicant.resume_upload_url,
              date_application: applicant.date_application,
              date_received: applicant.date_received,
              date_online_exam: applicant.date_online_exam,
              date_endorsement: applicant.date_endorsement,
              date_assessment: applicant.date_assessment,
              date_shortlist: applicant.date_shortlist,
              date_joborder: applicant.date_joborder,
              running_time: applicant.running_time,
              deptId: applicant.deptid
          });

      }
      return DataArray;
    } catch (error) {
      console.log(error)
    }
  };
};

module.exports = jobOrderUser;
