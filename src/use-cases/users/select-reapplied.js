const { decrypt } = require("../../entities/users/app");

const getReapplied = ({ userDb }) => {
  return async function get(info) {
    const data = [];
    const res = await userDb.selectReAppliedApplicant({});
    if (res.rowCount > 0) {
      const rows = res.rows;
      for (let i = 0; i < rows.length; i++) {
        const e = rows[i];

        data.push({
          id: e.id,
          email: decrypt(e.email),
          firstname: decrypt(e.firstname),
          lastname: decrypt(e.lastname),
          created_at: e.created_at,
          mobile: e.mobile,
          resume_url: e.resume_url,
          reapply_count: e.reapply_count,
          position: e.position_id
            ? {
                id: e.position_id,
                name: e.name,
              }
            : `Quick Apply`,
          previous_position: e.previous_position_id
            ? {
                id: e.previous_position_id,
                name: e.prev_position,
              }
            : null,
          isprocessed: e.isprocessed,
        });
      }
    }

    return data;
  };
};
module.exports = getReapplied;
