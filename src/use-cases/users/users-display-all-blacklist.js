const { decrypt } = require("../../entities/users/app");

const blacklistedUser = ({ userDb }) => {
  return async function get(params, reqMethod) {
    let blacklistedUsers, listOfBlacklistedApplicants = []
    try {

      const { role, dateFrom, dateTo, offset, limit, departmentID, teamID } = params
      if (reqMethod === 'GET') {
        blacklistedUsers = await userDb.getAllBlacklisted(params)
      } 
      if (reqMethod === 'POST') {
        switch (role) {
          case "Administrator":
          case "Operations Head":
          case "People Group":
            blacklistedUsers = await userDb.findAllBlacklisted({dateFrom, dateTo, offset, limit});
            break;
          case "Department Head":
            blacklistedUsers = await userDb.findAllBlacklistedDH({departmentID, dateFrom, dateTo, offset, limit});
            break;
          case "Team Head":
            blacklistedUsers = await userDb.findAllBlacklistedTH({teamID, dateFrom, dateTo, offset, limit});
            break;
        }
      }
      const blacklistedApplicants = blacklistedUsers.rows;

      for (let blacklistedApplicant of blacklistedApplicants) {
        let firstName = decrypt(blacklistedApplicant.firstname),
            lastName = decrypt(blacklistedApplicant.lastname),
            fullName = (blacklistedApplicant.middlename) ? `${firstName} ${decrypt(blacklistedApplicant.middlename)} ${lastName}` : `${firstName} ${lastName}`,
            email = decrypt(blacklistedApplicant.email),
            applicationDate = (blacklistedApplicant.date_application) ? blacklistedApplicant.date_application : blacklistedApplicant.resume_upload_date

            listOfBlacklistedApplicants.push({
            id: blacklistedApplicant.id,
            email: email,
            name: fullName,
            mobile: blacklistedApplicant.mobile,
            livestock: blacklistedApplicant.livestock,
            willing_to_travel: blacklistedApplicant.willing_to_travel,
            position: blacklistedApplicant.name,
            resume_upload_date: blacklistedApplicant.resume_upload_date,
            resume_upload_url: blacklistedApplicant.resume_upload_url,
            remarks_blacklist: blacklistedApplicant.remarks_blacklist,
            date_application: applicationDate,
            date_received: blacklistedApplicant.date_received,
            date_online_exam: blacklistedApplicant.date_online_exam,
            date_endorsement: blacklistedApplicant.date_endorsement,
            date_assessment: blacklistedApplicant.date_assessment,
            date_blacklist: blacklistedApplicant.date_blacklist,
            deptId: blacklistedApplicant.deptid
        })
      }
    } catch (error) {
      console.log(error)
    }
    return listOfBlacklistedApplicants;
  };
};

module.exports = blacklistedUser;
