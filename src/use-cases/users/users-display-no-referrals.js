const { decrypt } = require("../../entities/users/app");

const fetchApplicantsWithoutReferral = ({ userDb }) => {
   return async function get() {
      
      let data = await userDb.fetchApplicantsWithoutReferral()
                                 .catch(err => console.log(err)),
         results = data.rows, listOfUsers = []

      for (let result of results) {
         listOfUsers.push({
            ID: result.id,
            name: `${decrypt(result.firstname)} ${decrypt(result.lastname)}`,
            position: result.position
         })
      }

      let prompt = (data.rowCount > 0) ? 'Successfully fetched applicants without referral.' : 'Failed to fetch applicants without referral.'

      return {prompt, listOfUsers}
   }
}

module.exports = fetchApplicantsWithoutReferral