const notificationForEndorsed = ({ userDb }) => {
  return async function get(id, teamId, role) {
    if(role == 'Operations Head'){
      const users = await userDb.notificationForEndorsedOh();
      return users.rows;
    }
    else if(teamId){
      const users = await userDb.notificationForEndorsedTh(teamId);    
      return users.rows;
    }else{
      const users = await userDb.notificationForEndorsed(id);    
      return users.rows;
    }
  };
};

module.exports = notificationForEndorsed;
