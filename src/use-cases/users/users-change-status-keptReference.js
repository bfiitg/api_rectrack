const userChangeStatusReference = ({ userDb }) => {
  return async function puts({ id, remarks } = {}) {
    
    const updated = await userDb.changeStatusKeptForReference(id, remarks);
    
    let result = (updated.rowCount > 0) ? "Successfully updated applicant stage to Kept for Reference." : "Failed to update applicant stage to Kept for Reference."
    // delete IAF
    await userDb.deleteIAFWhenReference(id);

    return {
      result
    };
    
  };
};

module.exports = userChangeStatusReference;
