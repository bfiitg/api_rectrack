const { loginEntity } = require("../../entities/users/app");
// const { decrypt, encrypt } = require("../../entities/users/app");
const loginUser = ({ userDb, fs, path ,accessRightsDb,actionsDb,moduleDb}) => {
  return async function posts(userInfo) {
    const result = loginEntity(userInfo);
    //
    let userToken = "";
    await result.getToken().then(data => {
      userToken = data;
    });

    
    // console.log(userInfo);
    // console.log(decrypt(userInfo.password));

    const loginExist = await userDb.logins({
      email: result.getEmail(),
      password: result.getPassword(),
      ip: result.getIP(),
      token: userToken
    });
    const roles = await userDb.getRoleOfUser(
      result.getEmail(),
      result.getPassword()
    );
    const verify = await userDb.getRoleOfUserVerify(
      result.getEmail(),
      result.getPassword()
    )
    const roleverify=verify.rows
    
    
//access rights
let access = []
// if(roles.rows[0].access_rights_id!=null){
//   const access_rights = await accessRightsDb.select({id:roles.rows[0].access_rights_id});
//   const item = access_rights.rows

//   // console.log(item)
//   for (let a = 0; a < item.length; a++) {
//       const value = {}
//       let actions =[]
//       // let modules = []
//       const element = item[a];
//       const convert = element.actions
//       // const convertm = element.modules
//       const array = JSON.parse("[" + convert + "]");
//       // const arraym= JSON.parse("[" + convertm + "]");

//       value.id = element.id
//       value.discription = element.discription
//       value.time_created = element.time_created
//       value.time_updated = element.time_updated
//       value.status = element.status

//       for (let a = 0; a < array.length; a++) {
//           const act = {}
//           const element = array[a];

//           const findAction = await actionsDb.select(element)
//           act.actions = findAction.rows

//           value.actions = act
//           actions.push(act)
//       }
// // TEEEEEEEEEEEEEST
//       // for (let a = 0; a < arraym.length; a++) {
//       //     const mdl = {}
//       //     const element = arraym[a];

//       //     const findModule = await moduleDb.findModuleById(element)
//       //     mdl.modules = findModule.rows

//       //     value.modules = mdl
//       //     modules.push(mdl)
//       // }
//       value.actions=actions
//       // value.modules=modules

//       access.push(value)

//   }
// // end access rights   
// }
    

    if (loginExist.rowCount !== 0) {
      // need to work this out
      // await autoDeleteFiles({ fs, userDb, path }); // delete files
      const idData = await userDb.getIdThruToken(userToken);
      const userId = idData.rows[0];
      const result = {
        msg: "Login successful",
        token: userToken,
        roles: roles.rows,roleverify,access,userId
      };
      return result;
    } else {
      const result = {
        msg: "Invalid Account."
      };
      return result;
    }
  };
};

// worker delete files in uploads folder
// which are not connected to database
const autoDeleteFiles = async ({ fs, userDb, path }) => {
  const directory = "uploads";

  const dbFiles = await userDb.selectAllFilesInDb();
  let fileExist = []; //hold files exist in db
  let toDeleteFiles = []; //hold files to be deleted
  let allFiles = []; //hold all files

  // get all files in a folder
  fs.readdir(directory, (err, files) => {
    if (err) {
      return console.log("Unable to scan directory: " + err);
    }

    // to get all the files that exist in db
    dbFiles.forEach(d => {
      const dbFile = d.split("\\");
      fileExist.push(dbFile[1]);
    });
    // end to get file that exist in db

    // get all files
    files.forEach(file => {
      allFiles.push(file);
    });
    // end get all files

    // get all to delete files
    toDeleteFiles = allFiles.filter(function(x) {
      return fileExist.indexOf(x) < 0;
    });

    // delete files
    toDeleteFiles.forEach(e => {
      const pathDirectory = path.join(directory, e);
      fs.unlink(pathDirectory, err => {
        if (err) {
          console.error(err);
          return;
        }
        //file removed
      });
    });
  });
};

module.exports = loginUser;
