const { applicantEntityLogin, encrypt } = require("../../entities/users/app");

const applicationFormLogin = ({ userDb }) => {
  return async function posts(userInfo) {
    const accountInfo = applicantEntityLogin(userInfo);
    //
    let userToken = await accountInfo.getToken()

    const loginExist = await userDb.loginsApplicantApplication({
      email: accountInfo.getEmail(),
      password: encrypt(accountInfo.getPassword()),
      ip: accountInfo.getIP(),
      token: userToken
    });
    if (loginExist.rowCount !== 0) {
      result = {
        msg: "Login successful",
        token: userToken,
        applicantID: loginExist.rows[0].id
      };

      //
      return result;
    } else {
      result = {
        msg: "Invalid Account."
      };
      return result;
    }
  };
};

module.exports = applicationFormLogin;
