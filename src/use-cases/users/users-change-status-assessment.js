const userChangeStatusAssessment = ({ userDb }) => {
  return async function puts({ id } = {}) {
    // check status; if any status other than shortlist change to assessment
    const data = await userDb.checkStatusOfApplicant(id);
    const status = data.rows[0].status;
    let updated

    if (status !== 'shortlist') {
      updated = await userDb.changeStatusAssessment(id);
    }

    let result = (updated.rowCount > 0) ? "Update successful." : "Update failed."

    return {
      result
    }
  };
};

module.exports = userChangeStatusAssessment;
