const { decrypt } = require("../../entities/users/app");

const updateJO = ({ userDb,sendEmail }) => {
    return async function puts(info) {
      const id = info.id
      const remarks = info.remarks

      const find = await userDb.find(id)
        
      const elem = find.rows[0]

      if(find.rows.length =0){
        const data = { msg: "User cannot find" };
        return data;
      }
      const email = decrypt(elem.email)
      const fname = decrypt(elem.firstname)
      const lname = decrypt(elem.lastname)


      // email composition
    const mailOptions = {
        from: `Biotech Farms Inc. <${process.env.EMAIL}>`,
        to: email,
        subject: "BFI : Test",
        html:
          `Dear ${fname} ${lname}, <br/><br/>` +

          `Hi this is a test to Job Offer. <br/><br/>`+
        //   "We appreciate your interest in applying at Biotech Farms Inc.<br/><br/> " +
        //   "We would like to note that competition for jobs at Biotech Farms Inc. is always strong and that we  <br/>"+
        //   "often have to make difficult choices between many high-caliber candidates. Since we've gotten the  <br/>"+
        //   "opportunity to know more about you, we will keep your application for future openings that better fit your profile.<br/><br/>" +
        //   "Thank you for your enthusiasm for Biotech Farms, Always Better – Improving Lives, Beyond World-Class Products. Better wishes on your pursuit of employment.<br/><br/>" +
          "Regards,<br/><br/>" +
          "Biotech Farms Inc." +
          "<br/><br/>" +
          "<div style='font-weight:bold;font-style: italic;'>Note:<br/>Please do not reply to this computer generated email. " +
          "This mailbox does not allow incoming messages.</div>"
      };

        // send email
    await sendEmail(mailOptions);
    // end

      const updated = await userDb.changeJobOrder(id, remarks);

      const data = { msg: "Updated successfully" };
      return data
    };
  };
  
  module.exports = updateJO;
  