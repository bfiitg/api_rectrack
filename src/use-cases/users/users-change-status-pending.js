const userChangeStatusPending = ({ userDb }) => {
  return async function puts({ id } = {}) {
    const updated = await userDb.changeStatusPending(id);

    let result = (updated.rowCount > 0) ? "Update successful." : "Update failed."
    return {
      result
    };
  };
};

module.exports = userChangeStatusPending;
