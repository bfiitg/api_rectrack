const { adminEditUsers } = require("../../entities/users/app");
const { encrypt } = require("../../entities/users/app");

const adminEditUser = ({ userDb }) => {
  return async function put({ id, ...userInfo } = {}) {
    
    if(userInfo.position_id){
    
      const result = adminEditUsers(userInfo);
    const updated = await userDb.adminUpdateUser({
      id: id,
      email: result.getEmail(),
      password: result.getPassword(),
      firstname: result.getFirstName(),
      lastname: result.getLastName(),
      mobile: result.getMobile(),
      role_id: result.getRole(),
      position_id: result.getPosition(),
      middlename: result.getMiddleName(),
      status: result.getStatus()
    });
    const data = {
      msg: "Updated successfully.",
      ...updated
     };
    return data; 
    }else{
      // userInfo.email.include("@")
      const a = userInfo.email
      if(a.includes("@")){
        const updated = await userDb.updateUserEmail({
          id:id,
          email:encrypt(userInfo.email)
        });
  
        const data = {
          msg: "Updated successfully.",
          updated:updated
         };
        return data; 
      }else{
      throw new Error("Invalid mail.");
      }
     
    }
    
    
  };
};

module.exports = adminEditUser;
