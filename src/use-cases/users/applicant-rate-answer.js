const {rateAnswer} = require("../../entities/users/app");

const applicantRateAnswer=({userDb}) => {
    return async function put (info){
        const rate = rateAnswer(info)

        const datas = {
            msg: "Applicant rated successfully.",
            data: await userDb.rateAnswer({
              id: rate.getId(),
              is_correct: rate.getIs_correct(),
            })
          };
        return datas
    }
}
module.exports = applicantRateAnswer