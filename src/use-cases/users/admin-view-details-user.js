const { decrypt } = require("../../entities/users/app");

const adminViewDetailsUser = ({ userDb }) => {
  return async function get(id) {
    const users = await userDb.viewDetailsSpecificUser(id);
    const result = users.rows;
    

    if(result.length!=0){
      const results = [];
      for (let i = 0; i < result.length; i++) {
        results.push({
          id: result[i].id,
          email: decrypt(result[i].email),
          passwordHash: result[i].password,
          firstname: decrypt(result[i].firstname),
          lastname: decrypt(result[i].lastname),
          middlename: result[i].middlename ? decrypt(result[i].middlename) : result[i].middlename,
          mobile: result[i].mobile,
          sign_in_count: result[i].sign_in_count,
          status: result[i].status,
          position: result[i].position,
          positionDescription: result[i].positiondescription,
          positionId: result[i].positionid,
          team: result[i].team,
          teamDescription: result[i].teamdescription,
          department: result[i].department,
          deptDescription: result[i].deptdescription,
          roles: result[i].roles,
          roleDescription: result[i].roledescription,
          roleId: result[i].roleid
        });
      }
      return results;
    }else{
      const users = await userDb.findUserEmail(id)
   
      const result = users.rows;
      const results = [];
      for (let i = 0; i < result.length; i++) {
        results.push({
          email: decrypt(result[i].email)
          // passwordHash: result[i].password,
          // firstname: decrypt(result[i].firstname),
          // lastname: decrypt(result[i].lastname),
          // middlename: result[i].middlename ? decrypt(result[i].middlename) : result[i].middlename,
          // mobile: result[i].mobile,
          // sign_in_count: result[i].sign_in_count,
          // status: result[i].status,
          // position: result[i].position,
          // positionDescription: result[i].positiondescription,
          // positionId: result[i].positionid,
          // team: result[i].team,
          // teamDescription: result[i].teamdescription,
          // department: result[i].department,
          // deptDescription: result[i].deptdescription,
          // roles: result[i].roles,
          // roleDescription: result[i].roledescription,
          // roleId: result[i].roleid
        });
      }
      return results;
    }
    
  };
};

module.exports = adminViewDetailsUser;
