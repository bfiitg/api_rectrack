const { decrypt } = require("../../entities/users/app");

const assessmentUser = ({ userDb }) => {
  return async function get(deptid, posid, {info}) {
    const { id, dateFrom, dateTo, deptID, teamID, offset, limit } = info
    let getAssessedUsers, listOfAssessedApplicants = [], departmentId
    try {

      //deptid and posid are parameters for filtering assessed applicants by department and position
      if (deptid && !posid){
        getAssessedUsers = await userDb.getAllAssessment({deptid, offset, limit})
      }
      else if (deptid && posid) {
        getAssessedUsers = await userDb.getAllAssessment({deptid, posid, offset, limit})
      }
      else {
        //for getting applicants via POST request
        switch (info.role) {
          case "Administrator":
          case "Operations Head":
          case "People Group":
            getAssessedUsers = await userDb.findAllAssessment({dateFrom, dateTo, offset, limit})
            break;
          case "Department Head":
            getAssessedUsers = await userDb.findAllAssessmentDH({deptID, dateFrom, dateTo, offset, limit});
            break;
          case "Team Head":
            getAssessedUsers = await userDb.findAllAssessmentTH({teamID, dateFrom, dateTo, offset, limit});
            break;
        }
      }

      let assessedApplicants = getAssessedUsers.rows
      for (let assessedApplicant of assessedApplicants) {
        let firstName = decrypt(assessedApplicant.firstname),
            lastName = decrypt(assessedApplicant.lastname),
            fullName = (assessedApplicant.middlename) ? `${firstName} ${decrypt(assessedApplicant.middlename)} ${lastName}` : `${firstName} ${lastName}`,
            email = decrypt(assessedApplicant.email)

            listOfAssessedApplicants.push({
              id: assessedApplicant.id,
              email: email,
              name: fullName,
              mobile: assessedApplicant.mobile,
              livestock: assessedApplicant.livestock,
              willing_to_travel: assessedApplicant.willing_to_travel,
              done_IQBE:assessedApplicant.is_done_iqbe_exam,
              position: assessedApplicant.name,
              position_id: assessedApplicant.position_id,
              resume_upload_date: assessedApplicant.resume_upload_date,
              resume_upload_url: assessedApplicant.resume_upload_url,
              assessment_schedule: assessedApplicant.schedule_for_assessment,
              online_exam_start: assessedApplicant.online_exam_start,
              online_exam_end: assessedApplicant.online_exam_end,
              date_endorsement: assessedApplicant.date_endorsement,
              running_time: assessedApplicant.running_time,
              deptId: assessedApplicant.deptid,
              filled_up_form: assessedApplicant.filled_up_form
          })
        }

    } catch (error) {
      console.log(error)
    }
    
    return listOfAssessedApplicants;

  };
};

module.exports = assessmentUser;
