const { makeReferralForms } = require("../../entities/users/app");

const updateReferral = ({ userDb }) => {
  return async function put({ id, ...userInfo } = {}) {
    const result = makeReferralForms(userInfo);
    //
    const datas = {
      msg: "Updated referral form successfully.",
      data: await userDb.updateReferralForm({
        id: id,
        referrer_name: result.getReferrerName(),
        referrer_dept: result.getReferrerDept(),
        referrer_position: result.getReferrerPos(),
        referrer_contact: result.getReferrerContact(),
        referrer_id: result.getReferrerId(),
        referral_relationship_referrer: result.getReferralRelationshipReferrer()
      })
    };

    return datas;
  };
};

module.exports = updateReferral;
