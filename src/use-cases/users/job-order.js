const { decrypt } = require("../../entities/users/app");
const jobControllerUseCases = ({ userDb }) => {
    return async function post(info) {
        const { id, role, deptID, teamID, dateTo, dateFrom } = info;
        let result;
        const DataArray = [];
        const AccessRole_getAllJobOffer = (dateTo && dateFrom) ? await userDb.getAllJobOffer(id, dateTo, dateFrom) : await userDb.getAllJobOffer(id, dateTo, dateFrom);
        const AccessRole_getJobOrderTh = (dateTo && dateFrom) ? await userDb.getJobOrderTh(teamID, dateTo, dateFrom) : await userDb.getJobOrderTh(teamID, dateTo, dateFrom);
        const AccessRole_getJobOfferDh = (dateTo && dateFrom) ? await userDb.getJobOfferDh(deptID, dateTo, dateFrom) : await userDb.getJobOfferDh(deptID, dateTo, dateFrom);
        if (role == 'Administrator' || role == 'People Group' || role == 'Operations Head') { result = AccessRole_getAllJobOffer.rows }
        else if (role == 'Team Head') { result = AccessRole_getJobOrderTh.rows }
        else if (role == 'Department Head') { result = AccessRole_getJobOfferDh.rows }

        for (let i = 0; i < result.length; i++) {
            const e = result[i];
            DataArray.push({
                id: e.id,
                email: decrypt(e.email),
                name: `${decrypt(e.firstname)} ${decrypt(e.middlename)} ${decrypt(e.lastname)}`,
                mobile: e.mobile,
                livestock: e.livestock,
                willing_to_travel: e.willing_to_travel,
                position: e.name,
                resume_upload_date: e.resume_upload_date,
                resume_upload_url: e.resume_upload_url,
                date_application: e.date_application,
                date_received: e.date_received,
                date_online_exam: e.date_online_exam,
                date_endorsement: e.date_endorsement,
                date_assessment: e.date_assessment,
                date_shortlist: e.date_shortlist,
                date_joborder: e.date_joborder,
                running_time: e.running_time,
                deptId: e.deptid
            });

        }
        return DataArray;
    }
}

module.exports = jobControllerUseCases;