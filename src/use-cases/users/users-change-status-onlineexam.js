const {
  generateString,
  encrypt
} = require("../../entities/users/app");

const userChangeStatusOnlineExam = ({ userDb }) => {
  return async function puts({ id } = {}) {
    try {
      
    // randomly genererated password
    const password = generateString();

    const updated = await userDb.changeStatusOnlineExam(id, encrypt(password));

    const result = (updated.rowCount > 0) ? "Update success." : "Update failed."

    return {
      result
    };
    } catch (error) {
      console.log(error)
    }
  };
};

module.exports = userChangeStatusOnlineExam;
