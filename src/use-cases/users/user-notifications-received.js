const notificationReceived = ({ userDb }) => {
  return async function get(id, teamId, role) {
    
    if(role == 'Operations Head'){
      const users = await userDb.notificationReceivedOh();
      return users.rows;
    }
    else if(teamId){
      const users = await userDb.notificationReceivedTh(teamId);
      return users.rows;
    } else if (id) {
      const users = await userDb.notificationReceivedDp(id);
      return users.rows;
    } else {
      const users = await userDb.notificationReceived();
      return users.rows;
    }
  };
};

module.exports = notificationReceived;
