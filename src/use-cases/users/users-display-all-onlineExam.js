const { decrypt } = require("../../entities/users/app");

const onlineExamUser = ({ userDb }) => {
  return async function get(deptid, posid, { info }) {
    let getOnlineExaminees, listOfOnlineExaminees = [];
    const { id, role, dateFrom, dateTo, deptID, teamID } = info
    try {
      if (deptid) {
        getOnlineExaminees = await userDb.getAllOnlineExam(deptid)
      } else if (deptid && posid) {
        getOnlineExaminees = await userDb.getAllOnlineExam(deptid, posid)
      }
      else {
        switch (role) {
          case "Administrator":
          case "Operations Head":
          case "People Group":
            getOnlineExaminees = await userDb.findAllOnlineExam({dateFrom, dateTo});
            break;
          case "Department Head":
            getOnlineExaminees = await userDb.findAllOnlineExamDH({deptID, dateFrom, dateTo})
            break;
          case "Team Head":
            getOnlineExaminees = await userDb.findAllOnlineExamTH({teamID, dateFrom, dateTo})
            break;
        }
      }


      const onlineExaminees = getOnlineExaminees.rows;

      for (let onlineExaminee of onlineExaminees) {
        let firstName = decrypt(onlineExaminee.firstname),
          lastName = decrypt(onlineExaminee.lastname),
          fullName = (onlineExaminee.middlename) ? `${firstName} ${decrypt(onlineExaminee.middlename)} ${lastName}` : `${firstName} ${lastName}`,
          email = decrypt(onlineExaminee.email),
          applicationDate = (onlineExaminee.date_application) ? onlineExaminee.date_application : onlineExaminee.resume_upload_date

        listOfOnlineExaminees.push({
          id: onlineExaminee.id,
          email: email,
          name: fullName,
          mobile: onlineExaminee.mobile,
          livestock: onlineExaminee.livestock,
          willing_to_travel: onlineExaminee.willing_to_travel,
          position: onlineExaminee.name,
          resume_upload_date: onlineExaminee.resume_upload_date,
          resume_upload_url: onlineExaminee.resume_upload_url,
          startDateExam: onlineExaminee.online_exam_start,
          date_application: applicationDate,
          date_received: onlineExaminee.date_received,
          date_online_exam: onlineExaminee.date_online_exam,
          status: onlineExaminee.status,
          remaining_hours: onlineExaminee.remaining_hours,
          remaining_minutes: onlineExaminee.remaining_minutes,
          running_time: onlineExaminee.running_time,
          deptId: onlineExaminee.deptid,
          total_time_exam_seconds: onlineExaminee.total_time_exam_seconds,
          total_time_elapsed_seconds: onlineExaminee.total_time_elapsed_seconds,
          not_answered_categories: onlineExaminee.not_answered_categories
        })
      }
    } catch (error) {
      console.log(error)
    }
    return listOfOnlineExaminees;
  };
};

module.exports = onlineExamUser;
