const PhoneValidations = ({ }) => {
  return function MobileNumberVal(Pnumber) {
    var RegexMatch = /^\+{0,2}([\-\. ])?(\(?\d{0,3}\))?([\-\. ])?\(?\d{0,3}\)?([\-\. ])?\d{3}([\-\. ])?\d{4}/;
    //validation based on Regex That i made 
    var Matcher = RegexMatch.test(Pnumber);
    if (Matcher == true) {
      if (Pnumber.length == 11 || Pnumber.length == 13) {
        return Pnumber
      }
      else {
        throw new Error(`Wrong phone number format`)
      }
    }
    else {
      throw new Error(`Wrong phone number format`)
    }
  }
}

module.exports = PhoneValidations;