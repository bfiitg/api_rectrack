 const formatApplicantList = ({decrypt}) => {
   return function formatData({id, firstname, middlename, lastname, mobile, livestock, willing_to_travel, name, resume_upload_date, resume_upload_url, date_application, running_time, stage, email } = applicant) {
      let fullName, formattedData

      middlename ? fullName = `${decrypt(firstname)} ${decrypt(middlename)} ${decrypt(lastname)}` : fullName = `${decrypt(firstname)} ${decrypt(lastname)}`

      return formattedData = {
         id: id,
         email: decrypt(email),
         name: fullName,
         mobile: mobile,
         livestock: livestock,
         willing_to_travel: willing_to_travel,
         position: name,
         resume_upload_date: resume_upload_date,
         resume_upload_url: resume_upload_url,
         date_application: date_application.toISOString().split('T')[0],
         running_time: running_time,
         stage: stage
      }

   }
}

module.exports = formatApplicantList