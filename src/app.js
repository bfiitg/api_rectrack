const express = require("express");
const dotenv = require("dotenv");
const cors = require("cors");
dotenv.config();
const app = express();
const socketNotification = require('./middleware/socket-notifications'),
      socketTokenVerification = require('./middleware/socket-token-verification'),
      socketExamTimer = require('./middleware/exam-timer')

const server = require('http').createServer(app);
const io = require('socket.io')(server, {
  cors: {
    origin: '*'
  } 
});
const middleware = require("./middleware/app")

app.use(cors());

// Body Parser middleware to handle raw JSON files
app.use(express.json());
// app.use(express.urlencoded({ extended: false }));

app.use(`/rectrack-${process.env.DM_ENV}/uploads`, express.static("uploads"));

// worker routes
app.use(`/rectrack-${process.env.DM_ENV}/workers`, require("./routes/worker"))

// user routes
app.use(`/rectrack-${process.env.DM_ENV}/users`, require("./routes/users"));

// roles routes middleware.validateIP,
app.use(`/rectrack-${process.env.DM_ENV}/roles`,  require("./routes/roles"));

// departments routes
app.use(`/rectrack-${process.env.DM_ENV}/departments`,  require("./routes/departments"));

// teams routes middleware.validateIP,
app.use(`/rectrack-${process.env.DM_ENV}/teams`,  require("./routes/teams"));

// positions routes  middleware.validateIP,
app.use(`/rectrack-${process.env.DM_ENV}/positions`, require("./routes/positions"));

// category routes
app.use(`/rectrack-${process.env.DM_ENV}/categories`,   require("./routes/categories"));

// questions route
app.use(`/rectrack-${process.env.DM_ENV}/questions`,  require("./routes/questions"));

// interview assessment route middleware.validateIP,
app.use(`/rectrack-${process.env.DM_ENV}/interview-assessment`,  require("./routes/interview-assessment"));

//system settings routes middleware.validateIP, 
app.use(`/rectrack-${process.env.DM_ENV}/system`,  require("./routes/system-settings"));

//system settings routes
app.use(`/rectrack-${process.env.DM_ENV}/fixes`, require("./routes/fixes"));

// question type routes
app.use(`/rectrack-${process.env.DM_ENV}/question_types`, require("./routes/question_types"))

// report routes
app.use(`/rectrack-${process.env.DM_ENV}/report`, require("./routes/reports"))

// module routes
app.use(`/rectrack-${process.env.DM_ENV}/modules`, require("./routes/modules")); 

// action routes
app.use(`/rectrack-${process.env.DM_ENV}/actions`, require("./routes/actions"));

// access rights routes
app.use(`/rectrack-${process.env.DM_ENV}/access-rights`, require("./routes/access-rights"))

app.use(`/rectrack-${process.env.DM_ENV}/email`, require('./routes/email'))

const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}.`);
});

socketNotification( io )
socketTokenVerification( io )
socketExamTimer( io )

console.log(`Server is connecting to db: ${process.env.PGDATABASE}.`);

module.exports = app;
 