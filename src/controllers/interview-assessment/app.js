const {
  addInterviewAssessments,
  interviewAssessmentViews,
  editInterviewAssessments,
  interviewAssessmentViewAlls,
  interviewAssessmentViewsRates,
  interviewAssessmentViewsRateRaters
} = require("../../use-cases/interview-assessment/app");

const interviewAssessmentAdd = require("./interview-assessment-add");
const viewInterviewAssessment = require("./interview-assessment-view");
const interviewAssessmentEdit = require("./interview-assessment-edit");
const allViewInterviewAssessment = require("./interview-assessment-view-all");
const viewInterviewAssessmentRate = require("./interview-assessment-view-raters");
const interviewAssessmentViewsRateRater = require("./interview-assessment-view-rate-raters")

const rateinterviewAssessmentViews = viewInterviewAssessmentRate({
  interviewAssessmentViewsRates
})
const interviewAssessmentAdds = interviewAssessmentAdd({
  addInterviewAssessments
});
const viewInterviewAssessments = viewInterviewAssessment({
  interviewAssessmentViews
});

const interviewAssessmentEdits = interviewAssessmentEdit({
  editInterviewAssessments
});

const allViewInterviewAssessments = allViewInterviewAssessment({
  interviewAssessmentViewAlls
});
const interviewAssessmentViewsRateRaterController = interviewAssessmentViewsRateRater({
  interviewAssessmentViewsRateRaters
})

const controller = Object.freeze({
  interviewAssessmentAdds,
  viewInterviewAssessments,
  interviewAssessmentEdits,
  allViewInterviewAssessments,
  rateinterviewAssessmentViews,
  interviewAssessmentViewsRateRaterController
});

module.exports = controller;
module.exports = {
  interviewAssessmentAdds,
  viewInterviewAssessments,
  interviewAssessmentEdits,
  allViewInterviewAssessments,
  rateinterviewAssessmentViews,
  interviewAssessmentViewsRateRaterController
};
