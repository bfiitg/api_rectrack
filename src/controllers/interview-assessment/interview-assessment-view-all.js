const allViewInterviewAssessment = ({ interviewAssessmentViewAlls }) => {
    return async function get(httpRequest) {
      const headers = {
        "Content-Type": "application/json"
      };
      try {
        const view = await interviewAssessmentViewAlls({
          // postId: httpRequest.query.postId
        });
     
        return {
          headers,
          statusCode: 200,
          body: { view }
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        return {
          headers,
          statusCode: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = allViewInterviewAssessment;
  