const {
  addQuestionTypeUseCase,
  updateQuestionTypeUseCase,
  selectAllQuestionTypeUseCase,
  changeActiveUseCase,
  changeInactiveUseCase,
  selectQuestionTypeUseCase
} = require("../../use-cases/questions_type/app");

const makeQuestionType = require("./questions-type-add");
const editQuestionType = require("./questions-type-edit");
const selectAllQuestionType = require("./select_all_question_type");
const changeActive = require("./changeActive");
const changeInactive = require("./changeInactive");
const selectQuestionType =require("./select_question_type");

const addQuestionTypeController = makeQuestionType({ addQuestionTypeUseCase });
const editQuestionTypeController = editQuestionType({updateQuestionTypeUseCase})
const selectAllQuestionTypeController = selectAllQuestionType({selectAllQuestionTypeUseCase});
const changeActiveController = changeActive({changeActiveUseCase})
const changeInactiveController = changeInactive({changeInactiveUseCase});
const selectQuestionTypeController = selectQuestionType({selectQuestionTypeUseCase})

const controller = Object.freeze({
  addQuestionTypeController,
  editQuestionTypeController,
  selectAllQuestionTypeController,
  changeActiveController,
  changeInactiveController,
  selectQuestionTypeController,


});

module.exports = controller;
module.exports = {
  addQuestionTypeController,
  editQuestionTypeController,
  selectAllQuestionTypeController,
  changeActiveController,
  changeInactiveController,
  selectQuestionTypeController,

};
