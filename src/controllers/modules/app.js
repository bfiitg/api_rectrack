const {
    makeAddModulesUseCase,
    selectAllModulesUseCase,
    updateModulesUseCase,
    selectModulesUseCase,
    selectModulesActiveUseCase
}=require("../../use-cases/modules/app")

const makeModules = require("./module-add")
const selectAllModules = require("./module-selectAll")
const updateModule = require("./module-edit")
const selectModule = require("./module-select")
const selectAllActiveModules = require("./module-selectAll-acive")

const makeModulesController = makeModules({makeAddModulesUseCase})
const selectAllModulesController = selectAllModules({selectAllModulesUseCase}); 
const updateModuleController = updateModule({updateModulesUseCase})
const selectModuleController = selectModule({selectModulesUseCase})
const selectAllActiveModulesController= selectAllActiveModules({selectModulesActiveUseCase})

const controller = Object.freeze({
    makeModulesController,
    selectAllModulesController,
    updateModuleController,
    selectModuleController,
    selectAllActiveModulesController
})

module.exports = controller;
module.exports = {
    makeModulesController,
    selectAllModulesController,
    updateModuleController,
    selectModuleController,
    selectAllActiveModulesController
}