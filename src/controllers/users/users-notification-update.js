const updateApplicantNotification = ({ updateApplicantNotif_USECASE }) => {
   return async function updateNotif (httpRequest) {
      try {
      const { source = {}, ...info} = httpRequest.body
      source.ip = httpRequest.ip
      source.browser = httpRequest.headers['User-Agent']
      if (httpRequest.headers['Referer']) {
         source.referrer = httpRequest.headers['Referer']
      }

      const update = await updateApplicantNotif_USECASE(info)

      return {
         headers: {
            'Content-Type' : 'application/json'
         },
         statusCode: 200,
         body: {
            update
         }
      }
      } catch (e) {
         console.log(e)
         return {
         headers: {
            'Content-Type': 'application/json'
         },
         statusCode: 400,
         body: {
            error: e.message
            }
         }
      }
   }
}

module.exports = updateApplicantNotification