const getApplicantNotification = ({ getApplicantNotif_USECASE }) => {
   return async function add (httpRequest) {

      const headers = { 'Content-Type' : 'applicantion/json'}
      try {
        let info = {
          role: httpRequest.query.role,
          deptID: httpRequest.query.deptID,
          teamID: httpRequest.query.teamID,
          dateFrom: httpRequest.query.from,
          dateTo: httpRequest.query.to
        }

         const get = await getApplicantNotif_USECASE(info)

         return {
           headers,
           statusCode: 201,
           body: { get }
         }
       } catch (e) {
         // TODO: Error logging
         console.log(e)
   
         return {
           headers,
           statusCode: 400,
           body: {
             error: e.message
           }
         }
       } 
   }
}

module.exports = getApplicantNotification