const inputPassword = ({ passwordValidation_USECASE }) => {
   return async function validate (httpRequest) {
      const headers = { 'Content-Type' : 'applicantion/json'}
      try {
        let info = httpRequest.body
         const get = await passwordValidation_USECASE(
           info
         )

         return {
           headers,
           statusCode: 201,
           body: { get }
         }
       } catch (e) {
         // TODO: Error logging
         console.log(e)
   
         return {
           headers,
           statusCode: 400,
           body: {
             error: e.message
           }
         }
      }
   }
}

module.exports = inputPassword