const makeUser = ({ addUser }) => {
  return async function post(httpRequest) {
    try {
      const { source = {}, ...userInfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      const posted = await addUser({
        ...userInfo,
        source
      });

      //bad security to practice to return user data after an error
      // if (posted.msg === "Email already exists!") {
      //   return {
      //     headers: {
      //       "Content-Type": "application/json",
      //       "Last-Modified": new Date(posted.modifiedOn).toUTCString()
      //     },
      //     statusCode: 400,
      //     body: { posted }
      //   };
      // }
      // if (posted.msg === "User already exist!") {
      //   return {
      //     headers: {
      //       "Content-Type": "application/json",
      //       "Last-Modified": new Date(posted.modifiedOn).toUTCString()
      //     },
      //     statusCode: 400,
      //     body: { posted }
      //   };
      // }
      return {
        headers: {
          "Content-Type": "application/json",
          "Last-Modified": new Date(posted.modifiedOn).toUTCString()
        },
        statusCode: 201,
        body: { posted }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      return {
        headers: {
          "Content-Type": "application/json",
          "Last-Modified": new Date(posted.modifiedOn).toUTCString()
        },
        statusCode: 400,
        body: {
          error: e.message,
          postedData
        }
      };
    }
  };
};

module.exports = makeUser;
