const findApplicant_CONTROLLER = ({ findApplicantById_USECASE }) => {
   return async function updates(httpRequest) {
     try {
       // get http request to submitted data
       const {source = {}, ...info} = httpRequest.body
       source.ip = httpRequest.ip;
       source.browser = httpRequest.headers["User-Agent"];
       if (httpRequest.headers["Referer"]) {
         source.referrer = httpRequest.headers["Referer"];
       }

       const response = await findApplicantById_USECASE({
        info
      })

       return {
         headers: {
           'Content-Type': 'application/json'
         },
         statusCode: 200,
         body: { response }
       }
     } catch (e) {
       // TODO: Error logging
       console.log(e);
 
       return {
         headers,
         statusCode: 400,
         body: {
           error: e.message
         }
       };
     }
   };
 };
 
 module.exports = findApplicant_CONTROLLER;
 