const {
  filterReportsUseCase,
  addUser,
  userLogin,
  receivedUsers,
  usersChangeStatusOnlineExam,
  usersChangeStatusBlacklist,
  onlineExamUsers,
  usersChangeStatusEndorsement,
  endorsedUsers,
  usersChangeStatusRejected,
  rejectedUsers,
  blacklistedUsers,
  userChangeStatusAssessments,
  assessmentUsers,
  userChangeStatusPendings,
  pendingUsers,
  userChangeStatusReferences,
  referenceUsers,
  userChangeStatusShortLists,
  shortlistUsers,
  userChangeStatusDeployeds,
  deployedUsers,
  adminViewAllUsers,
  adminViewDetailsUsers,
  adminAddUsers,
  adminEditUsers,
  loginApplicants,
  userScheduleAssessments,
  userChangeStatusJobOrders,
  jobOrderUsers,
  userFillUpApplications,
  notificationReceiveds,
  notificationEndorseds,
  loginApplicantIQBs,
  getApplicantNames,
  adminExportCSVJeonsofts,
  displayAllApplicantsJeonSofts,
  jeonsoftClientDownloads,
  applicationFormLogins,
  imageUploads,
  userDoneOnlineExams,
  getStartEndTimes,
  IQBESubtractTimes,
  userDoneIQBEs,
  getLoggedInUserFirstnames,
  userChangeStatusForEndorsements,
  forEndorsedUsers,
  makeAddReferrals,
  referralFormViews,
  referralFormViewOnes,
  updateReferrals,
  logsInserts,
  applicantOnlineExamResults,
  getApplicantNamePVs,
  fetchDataReferralForms,
  assessmentShowListPositionss,
  applicantUpdatePositions,
  notificationAssessments,
  notificationDeployeds,
  notificationShortlists,
  userUpdateDeployedDates,
  checkTokenss,
  googleSpreadSheetforQas,
  googleSpreadSheets,
  logsSelectAlls,
  notificationForEndorseds,
  referralCheckIfExists,
  onlineExamRetakes,
  changePasswords,
  getQuickApplicantsUseCase,
  getAllApplicantsUseCase,
  endorseToOtherDepartmentUseCase,
  getApplicantDateTrackingsUseCase,
  deployedUseCase,
  filterReportAllUseCase,
  countQuickApplicantsUseCase,
  rateAnswerUseCase,
  applicantResumeUpdateUseCase,
  getReapplieds,
  processReApplys,

  updateToKeptRefUseCase,
  updateJOUseCase,
  viewAllIQTestUseCase,
  fetchApplicantUseCase,
  findApplicantById_USECASE,
  getAllShortListUseCase, KeepReferencesUseCase, jobControllerUseCase, rejectedUseCase,
  addApplicantNotif_USECASE,
  getApplicantNotif_USECASE,
  updateApplicantNotif_USECASE,

  passwordValidation_USECASE,
  fetchApplicantsWithoutReferral_USECASE,
  returnApplicantToReceived_USECASE,
  findUserDetails
} = require("../../use-cases/users/app");
// --------------------
// --------------------
const reApplyProcess = require("./reapply-applicant-process");
const endorseToOtherDept = require("./user-endorse-to-other-dept");
const getQuickApplicants = require("./users-display-all-quickApplicants");
const getAllApplicants = require('./users-display-all-applicants')
const filterReports = require("./user-filter-reports");
const fetchApplicantDateTrackings = require("./user-get-applicant-date-trackings");

const passwordChange = require("./first-time-login");
const retakeOnlineExam = require("./users-retake-online-exam");
const ifExistReferralCheck = require("./user-check-if-has-referral");
const forEndorsedNotification = require("./user-notifications-for-endorsement");
const selectAllLogs = require("./logs-view");
const spreadSheetGoogleforQa = require("./spreadsheet-worker-qa");
const spreadSheetGoogle = require("./spreadsheet-worker");
const tokenCheck = require("./get-token-if-not-equal-logout");
const deployedDateUpdate = require("./applicant-update-deployed-date");
const shortlistNotification = require("./user-notifications-shortlist");
const deployedNotification = require("./user-notifications-deployed");
const assessmentNotification = require("./user-notifications-assessment");
const updatePositionApplicant = require("./applicant-update-position");
const listPositionsShowAssessment = require("./assessment-show-list-positions");
const referralFormFetchData = require("./users-fetch-referral-form");
const applicantNamesGetPV = require("./users-application-form-get-data-print");
const onlineExamResultApplicant = require("./applicant-view-online-exam-result");
const insertLogs = require("./logs-insert");
const referralUpdate = require("./update-referral-form");
const viewReferralForm = require("./view-referral-form");
const referralsAdd = require("./make-new-referral-form");
const makeUser = require("./users-add");
const cLoginUser = require("./users-login");
const usersReceived = require("./users-display-all-received");
const changeStatusUserOnlineExam = require("./users-change-status-onlineExam");
const changeStatusUserBlacklist = require("./users-change-status-blacklist");
const usersOnlineExam = require("./users-display-all-onlineExam");
const changeStatusUserEndorsement = require("./users-change-status-endorsement");
const usersEndorsed = require("./users-display-all-endorsed");
const changeStatusUserReject = require("./users-change-status-rejected");
const usersRejected = require("./users-display-all-rejected");
const usersBlacklisted = require("./users-display-all-blacklist");
const changeStatusUserAssessment = require("./users-change-status-assessment");
const usersAssessment = require("./users-display-all-assessment");
const changeStatusUserPending = require("./users-change-status-pending");
const usersPending = require("./users-display-all-pending");
const changeStatusUserReference = require("./users-change-status-keptReference");
const usersReference = require("./users-display-all-keptReference");
const changeStatusUserShortList = require("./users-change-status-shortlist");
const usersShortlisted = require("./users-display-all-shortlist");
const changeStatusUserDeployed = require("./users-change-status-deployed");
const usersDeployed = require("./users-display-all-deployed");
const cLoginApplicant = require("./applicant-login");
const scheduleAssessmentUser = require("./users-set-schedule-for-assessment");
const changeStatusUserJobOrder = require("./users-change-status-jobOrder");
const usersJobOrder = require("./users-display-all-jobOrder");
const fillUpApplicationUser = require("./users-fill-up-application-form");
const receivedNotification = require("./user-notifications-received");
const endorsedNotification = require("./user-notifications-endorsed");
const applicantLoginIQB = require("./applicant-login-iq-behavioral-exam");
const applicantNamesGet = require("./users-application-form-get-name");
// ADMIN
const viewAllUsersAdmin = require("./admin-view-all-users");
const viewUsersDetailsAdmin = require("./admin-view-details-user");
const usersAddAdmin = require("./admin-add-user");
const usersEditAdmin = require("./admin-edit-user");
// export jeon soft
const jeonSoftExportCSV = require("./jeonsoft-csv-export");
const jeonsoftDisplayAllApplicants = require("./jeonsoft-display-applicants");
const downloadClientJeonsoft = require("./jeonsoft-client-download");
// --------------------
const loginApplicationForm = require("./applicant-login-application-form");
const uploadImage = require("./image-upload");
const doneOnlineExamUser = require("./users-change-status-done-online-exam");
const startEndTimeGet = require("./get-start-end-time-online-exam");
const subtractTimeIQBE = require("./get-time-for-IQBE");
const IQBEDoneUser = require("./users-change-status-done-IQBE");
const loggedInUserFirstnameGet = require("./get-logged-in-user-name");
const changeStatusUserForEndorsement = require("./users-change-status-for-endorsement");
const usersForEndorsed = require("./users-display-all-for-endorsement");
const viewOneReferralForm = require("./view-referral-form-single");

const filterReportAll = require("./user-filter-reports-all");
const getcountQuickApplicants = require("./users-count-all-quickApplicants");
const rateAnswerApplicants = require("./applicant-rate-answer");
const updateResumes = require("./users-update-resume");
const reapplyGet = require("./select-reapplied");

const deployedUpdateKeptRef = require("./applicant-update-deployed-kept")
const deployedUpdateJO = require("./applicant-update-deployed-JO")
const viewAllIQTest = require("./view-all-IQ-test")
const findApplicantById = require('./users-find-applicant-by-id')

//----------------------- new

const KeepReferencesControllers = require("./get-keep-reference")
//----------------------- new

const fetchpendingApplicantControllers = require("./fetch-pending-controllers");

const getAllShortListControllers = require("./get-all-ShortListed");

const jobOfferControllers = require("./job-offer");

const deployedControllers = require("./deployed");
const rejectedControllers = require("./admin-rejected");

const getApplicantNotifcation = require('./users-notification-get')
const addApplicantNotifcation = require('./users-notification-add')
const updateApplicantNotification = require('./users-notification-update')

const inputPassword = require('./users-password-input-validation')
const fetchApplicantsWithoutReferral = require('./users-display-no-referrals')
const returnApplicantToReceived = require('./users-change-status-return-to-received')
const fetchUsers = require('./find-user')

// --------------------
const reApplyProcesss = reApplyProcess({ processReApplys });
const reapplyGets = reapplyGet({ getReapplieds });
const passwordChanges = passwordChange({ changePasswords });
const retakeOnlineExams = retakeOnlineExam({ onlineExamRetakes });
const ifExistReferralChecks = ifExistReferralCheck({ referralCheckIfExists });
const forEndorsedNotifications = forEndorsedNotification({
  notificationForEndorseds,
});
const selectAllLogss = selectAllLogs({ logsSelectAlls });
const spreadSheetGoogleforQas = spreadSheetGoogleforQa({
  googleSpreadSheetforQas,
});
const spreadSheetGoogles = spreadSheetGoogle({ googleSpreadSheets });
const tokenChecks = tokenCheck({ checkTokenss });
const deployedDateUpdates = deployedDateUpdate({ userUpdateDeployedDates });
const shortlistNotifications = shortlistNotification({
  notificationShortlists,
});
const deployedNotifications = deployedNotification({ notificationDeployeds });
const assessmentNotifications = assessmentNotification({
  notificationAssessments,
});
const updatePositionApplicants = updatePositionApplicant({
  applicantUpdatePositions,
});
const listPositionsShowAssessments = listPositionsShowAssessment({
  assessmentShowListPositionss,
});
const referralFormFetchDatas = referralFormFetchData({
  fetchDataReferralForms,
});
const applicantNamesGetPVs = applicantNamesGetPV({ getApplicantNamePVs });
const onlineExamResultApplicants = onlineExamResultApplicant({
  applicantOnlineExamResults,
});
const insertLogss = insertLogs({ logsInserts });
const referralUpdates = referralUpdate({ updateReferrals });
const viewOneReferralForms = viewOneReferralForm({ referralFormViewOnes });
const viewReferralForms = viewReferralForm({ referralFormViews });
const referralsAdds = referralsAdd({ makeAddReferrals });
const usersForEndorseds = usersForEndorsed({ forEndorsedUsers });
const changeStatusUserForEndorsements = changeStatusUserForEndorsement({
  userChangeStatusForEndorsements,
});
const loggedInUserFirstnameGets = loggedInUserFirstnameGet({
  getLoggedInUserFirstnames,
});
const IQBEDoneUsers = IQBEDoneUser({ userDoneIQBEs });
const startEndTimeGets = startEndTimeGet({ getStartEndTimes });
const uploadImages = uploadImage({ imageUploads });
const loginApplicationForms = loginApplicationForm({ applicationFormLogins });
const applicantNamesGets = applicantNamesGet({ getApplicantNames });
const postUser = makeUser({ addUser });
const loginUser = cLoginUser({ userLogin });
const userReceived = usersReceived({ receivedUsers });
const changesStatusUserOnlineExam = changeStatusUserOnlineExam({
  usersChangeStatusOnlineExam,
});
const changesStatusUserBlacklist = changeStatusUserBlacklist({
  usersChangeStatusBlacklist,
});
const usersOnlineExams = usersOnlineExam({
  onlineExamUsers,
});
const changeStatusUserEndorsements = changeStatusUserEndorsement({
  usersChangeStatusEndorsement,
});
const userEndorsed = usersEndorsed({
  endorsedUsers,
});
const changeStatusUserRejects = changeStatusUserReject({
  usersChangeStatusRejected,
});
const userRejected = usersRejected({
  rejectedUsers,
});
const userBlacklisted = usersBlacklisted({
  blacklistedUsers,
});
const changeStatusUserAssessments = changeStatusUserAssessment({
  userChangeStatusAssessments,
});
const userAssessment = usersAssessment({
  assessmentUsers,
});
const changeStatusUserPendings = changeStatusUserPending({
  userChangeStatusPendings,
});
const usersPendings = usersPending({
  pendingUsers,
});
const changeStatusUserReferences = changeStatusUserReference({
  userChangeStatusReferences,
});
const usersReferences = usersReference({
  referenceUsers,
});
const changeStatusUserShortLists = changeStatusUserShortList({
  userChangeStatusShortLists,
});
const userShortlisted = usersShortlisted({
  shortlistUsers,
});
const changeStatusUserDeployeds = changeStatusUserDeployed({
  userChangeStatusDeployeds,
});
const usersDeployeds = usersDeployed({
  deployedUsers,
});
const cLoginApplicants = cLoginApplicant({ loginApplicants });
const scheduleAssessmentUsers = scheduleAssessmentUser({
  userScheduleAssessments,
});
const changeStatusUserJobOrders = changeStatusUserJobOrder({
  userChangeStatusJobOrders,
});
const usersJobOrders = usersJobOrder({ jobOrderUsers });
const fillUpApplicationUsers = fillUpApplicationUser({
  userFillUpApplications,
});
const receivedNotifications = receivedNotification({ notificationReceiveds });
const endorsedNotifications = endorsedNotification({ notificationEndorseds });
const applicantLoginIQBs = applicantLoginIQB({ loginApplicantIQBs });
// ADMIN
const viewAllUsersAdmins = viewAllUsersAdmin({
  adminViewAllUsers,
});
const viewUsersDetailsAdmins = viewUsersDetailsAdmin({
  adminViewDetailsUsers,
});
const usersAddAdmins = usersAddAdmin({
  adminAddUsers,
});
const usersEditAdmins = usersEditAdmin({
  adminEditUsers,
});
// export csv for jeonsoft
const jeonSoftExportCSVs = jeonSoftExportCSV({ adminExportCSVJeonsofts });
const jeonsoftDisplayAllApplicant = jeonsoftDisplayAllApplicants({
  displayAllApplicantsJeonSofts,
});
const downloadClientJeonsofts = downloadClientJeonsoft({
  jeonsoftClientDownloads,
});
const doneOnlineExamUsers = doneOnlineExamUser({ userDoneOnlineExams });
const subtractTimeIQBEs = subtractTimeIQBE({ IQBESubtractTimes });

const filterReportsController = filterReports({ filterReportsUseCase });
const getQuickApplicantsController = getQuickApplicants({
  getQuickApplicantsUseCase,
});
const getAllApplicantsController = getAllApplicants({ getAllApplicantsUseCase })
const endorseToOtherDepartmentController = endorseToOtherDept({
  endorseToOtherDepartmentUseCase,
});
const fetchApplicantDateTrackingsController = fetchApplicantDateTrackings({
  getApplicantDateTrackingsUseCase,
});
// -------------------
const getcountQuickApplicantsController = getcountQuickApplicants({
  countQuickApplicantsUseCase,
});
const rateAnswerController = rateAnswerApplicants({ rateAnswerUseCase });
const filterReportAllController = filterReportAll({ filterReportAllUseCase });
const updateResumesController = updateResumes({ applicantResumeUpdateUseCase });
// -------------------

const deployedUpdateKeptRefController = deployedUpdateKeptRef({ updateToKeptRefUseCase })
const deployedUpdateJOController = deployedUpdateJO({ updateJOUseCase })
const viewAllIQTestController = viewAllIQTest({ viewAllIQTestUseCase })

const fetchpendingApplicantController = fetchpendingApplicantControllers({ fetchApplicantUseCase })

const findApplicantById_CONTROLLER = findApplicantById({ findApplicantById_USECASE })
const getAllShortListController = getAllShortListControllers({ getAllShortListUseCase })

const KeepReferencesController = KeepReferencesControllers({ KeepReferencesUseCase });

const jobOfferController = jobOfferControllers({ jobControllerUseCase })
const deployedController = deployedControllers({ deployedUseCase })
const rejectedController = rejectedControllers({ rejectedUseCase })

const getApplicantNotifcation_CONTROLLER = getApplicantNotifcation ({ getApplicantNotif_USECASE })
const addApplicantNotifcation_CONTROLLER = addApplicantNotifcation ({ addApplicantNotif_USECASE })
const updateApplicantNotification_CONTROLLER = updateApplicantNotification ({
  updateApplicantNotif_USECASE
})

const passwordValidation_CONTROLLER = inputPassword({ passwordValidation_USECASE })
const fetchApplicantsWithoutReferral_CONTROLLER = fetchApplicantsWithoutReferral ({ fetchApplicantsWithoutReferral_USECASE })
const returnApplicantToReceived_CONTROLLER = returnApplicantToReceived ({ returnApplicantToReceived_USECASE })

const fetchListOfUsers = fetchUsers({ findUserDetails })

const userController = Object.freeze({
  reApplyProcesss,
  reapplyGets,
  fetchApplicantDateTrackingsController,
  endorseToOtherDepartmentController,
  getQuickApplicantsController,
  getAllApplicantsController,
  filterReportsController,
  postUser,
  loginUser,
  userReceived,
  changesStatusUserOnlineExam,
  changesStatusUserBlacklist,
  usersOnlineExams,
  changeStatusUserEndorsements,
  userEndorsed,
  changeStatusUserRejects,
  userRejected,
  userBlacklisted,
  changeStatusUserAssessments,
  userAssessment,
  changeStatusUserPendings,
  usersPendings,
  changeStatusUserReferences,
  usersReferences,
  changeStatusUserShortLists,
  userShortlisted,
  changeStatusUserDeployeds,
  usersDeployeds,
  viewAllUsersAdmins,
  viewUsersDetailsAdmins,
  usersAddAdmins,
  usersEditAdmins,
  cLoginApplicants,
  scheduleAssessmentUsers,
  changeStatusUserJobOrders,
  usersJobOrders,
  fillUpApplicationUsers,
  receivedNotifications,
  endorsedNotifications,
  applicantLoginIQBs,
  applicantNamesGets,
  jeonSoftExportCSVs,
  jeonsoftDisplayAllApplicant,
  downloadClientJeonsofts,
  loginApplicationForms,
  uploadImages,
  jobOfferController,
  doneOnlineExamUsers,
  startEndTimeGets,
  subtractTimeIQBEs,
  IQBEDoneUsers,
  loggedInUserFirstnameGets,
  changeStatusUserForEndorsements,
  usersForEndorseds,
  referralsAdds,
  viewReferralForms,
  viewOneReferralForms,
  referralUpdates,
  insertLogss,
  onlineExamResultApplicants,
  applicantNamesGetPVs,
  referralFormFetchDatas,
  listPositionsShowAssessments,
  updatePositionApplicants,
  assessmentNotifications,
  deployedNotifications,
  shortlistNotifications,
  deployedDateUpdates,
  tokenChecks,
  spreadSheetGoogleforQas,
  spreadSheetGoogles,
  selectAllLogss,
  forEndorsedNotifications,
  ifExistReferralChecks,
  retakeOnlineExams,
  passwordChanges,

  filterReportAllController,
  getcountQuickApplicantsController,
  rateAnswerController,
  updateResumesController,

  deployedUpdateKeptRefController,
  deployedUpdateJOController,
  viewAllIQTestController,
  fetchpendingApplicantController,
  getAllShortListController, KeepReferencesController, deployedController, rejectedController,
  getApplicantNotifcation_CONTROLLER,
  addApplicantNotifcation_CONTROLLER,
  updateApplicantNotification_CONTROLLER,

  passwordValidation_CONTROLLER,
  fetchApplicantsWithoutReferral_CONTROLLER,
  returnApplicantToReceived_CONTROLLER,
  fetchListOfUsers
});

module.exports = userController;
module.exports = {
  reapplyGets,
  reApplyProcesss,
  fetchApplicantDateTrackingsController,
  endorseToOtherDepartmentController,
  getQuickApplicantsController,
  getAllApplicantsController,
  filterReportsController,
  postUser,
  loginUser,
  userReceived,
  changesStatusUserOnlineExam,
  changesStatusUserBlacklist,
  usersOnlineExams,
  changeStatusUserEndorsements,
  userEndorsed,
  changeStatusUserRejects,
  userRejected,
  userBlacklisted,
  changeStatusUserAssessments,
  userAssessment,
  changeStatusUserPendings,
  usersPendings,
  changeStatusUserReferences,
  usersReferences,
  changeStatusUserShortLists,
  userShortlisted,
  changeStatusUserDeployeds,
  usersDeployeds,
  viewAllUsersAdmins,
  viewUsersDetailsAdmins,
  usersAddAdmins,
  usersEditAdmins,
  cLoginApplicants,
  scheduleAssessmentUsers,
  changeStatusUserJobOrders,
  usersJobOrders,
  fillUpApplicationUsers,
  receivedNotifications,
  endorsedNotifications,
  applicantLoginIQBs,
  applicantNamesGets,
  jeonSoftExportCSVs,
  jeonsoftDisplayAllApplicant,
  downloadClientJeonsofts,
  loginApplicationForms,
  uploadImages,
  doneOnlineExamUsers,
  startEndTimeGets,
  subtractTimeIQBEs,
  IQBEDoneUsers,
  loggedInUserFirstnameGets,
  changeStatusUserForEndorsements,
  usersForEndorseds,
  referralsAdds,
  viewReferralForms,
  viewOneReferralForms,
  referralUpdates,
  insertLogss,
  onlineExamResultApplicants,
  applicantNamesGetPVs,
  referralFormFetchDatas,
  listPositionsShowAssessments,
  updatePositionApplicants,
  assessmentNotifications,
  deployedNotifications,
  shortlistNotifications,
  deployedDateUpdates,
  tokenChecks,
  spreadSheetGoogleforQas,
  spreadSheetGoogles,
  selectAllLogss,
  forEndorsedNotifications,
  ifExistReferralChecks,
  retakeOnlineExams,
  passwordChanges,

  filterReportAllController,
  getcountQuickApplicantsController,
  rateAnswerController,
  updateResumesController,
  rejectedController,
  deployedUpdateKeptRefController,
  deployedUpdateJOController,
  viewAllIQTestController,
  findApplicantById_CONTROLLER,
  fetchpendingApplicantController, getAllShortListController, KeepReferencesController, jobOfferController, deployedController,
  getApplicantNotifcation_CONTROLLER,
  addApplicantNotifcation_CONTROLLER,
  updateApplicantNotification_CONTROLLER,

  passwordValidation_CONTROLLER,
  fetchApplicantsWithoutReferral_CONTROLLER,
  returnApplicantToReceived_CONTROLLER,
  fetchListOfUsers
};
