const usersBlacklisted = ({ blacklistedUsers }) => {
  return async function get(httpRequest) {
    const headers = {
      "Content-Type": "application/json"
    };
    try {
      //get the httprequest body
      const { source = {}, ...info } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }

      
      let offset = httpRequest.query.offset,
          limit= httpRequest.query.limit

      info.offset = offset
      info.limit = limit

      let queryParams = {
        deptid: httpRequest.query.deptid ? httpRequest.query.deptid : httpRequest.params.deptid,
        posid: httpRequest.query.posid ? httpRequest.query.posid : httpRequest.params.posid,
        dateTo: httpRequest.query.to,
        dateFrom: httpRequest.query.from,
        offset: offset,
        limit: limit
      }, reqMethod = httpRequest.method

      let params = (reqMethod === 'GET') ? queryParams : info

      const view = await blacklistedUsers(
        params,
        reqMethod
      );
      
      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 200,
        body: { view }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = usersBlacklisted;
