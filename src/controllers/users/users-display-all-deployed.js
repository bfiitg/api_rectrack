const usersDeployed = ({ deployedUsers }) => {
  return async function get(httpRequest) {
    const headers = {
      "Content-Type": "application/json"
    };
    try {
      //get the httprequest body
      const { source = {}, ...info } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }

      let reqMethod = httpRequest.method,
          offset = httpRequest.query.offset,
          limit = httpRequest.query.limit,
          queryParams = {
          deptid: httpRequest.query.deptid ? httpRequest.query.deptid : httpRequest.params.deptid,
          posid: httpRequest.query.posid ? httpRequest.query.posid : httpRequest.params.posid,
          to: httpRequest.query.to,
          from: httpRequest.query.from,
          offset: offset,
          limit: limit
      }
      
      info.offset = offset
      info.limit = limit


      let params = (reqMethod === 'GET') ? queryParams : info

      const view = await deployedUsers(
        params,
        reqMethod
      );

      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 200,
        body: { view }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = usersDeployed;
