const addApplicantNotification = ({ addApplicantNotif_USECASE }) => {
   return async function add (httpRequest) {

      const headers = { 'Content-Type' : 'applicantion/json'}
      try {
         const { source = {}, ...info } = httpRequest.body
         source.ip = httpRequest.ip
         source.browser = httpRequest.headers['User-Agent']
         if (httpRequest.headers['Referer']) {
           source.referrer = httpRequest.headers['Referer']
         }
         const posted = await addApplicantNotif_USECASE({
           ...info
         })

         return {
           headers,
           statusCode: 201,
           body: { posted }
         }
       } catch (e) {
         // TODO: Error logging
         console.log(e)
   
         return {
           headers,
           statusCode: 400,
           body: {
             error: e.message
           }
         }
       } 
   }
}

module.exports = addApplicantNotification