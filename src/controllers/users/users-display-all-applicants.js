const getAllApplicantsController = ({ getAllApplicantsUseCase }) => {
   return async function get(httpRequest) {
     const headers = {
       "Content-Type": "application/json"
     };
     try {
       //get the httprequest body
       const { source = {}, ...info } = httpRequest.body;
       source.ip = httpRequest.ip;
       source.browser = httpRequest.headers["User-Agent"];
       if (httpRequest.headers["Referer"]) {
         source.referrer = httpRequest.headers["Referer"];
       }
       
      let deptid = httpRequest.params.deptid,
          posid = httpRequest.params.posid,
          offset = httpRequest.query.offset,
          limit = httpRequest.query.limit
      
      info.deptid = deptid
      info.posid = posid
      info.offset = offset
      info.limit = limit

      const applicantList = await getAllApplicantsUseCase(info);

       return {
         headers: {
           "Content-Type": "application/json"
         },
         statusCode: 200,
         body: { applicantList }
       };
     } catch (e) {
       // TODO: Error logging
       console.log(e);
       return {
         headers,
         statusCode: 400,
         body: {
           error: e.message
         }
       };
     }
   };
 };
 
 module.exports = getAllApplicantsController;
 