const filterReportsController = ({ filterReportsUseCase }) => {
    
  return async function get(httpRequest) {
      const headers = {
        "Content-Type": "application/json"
      };
      try {
        const view = await filterReportsUseCase(httpRequest.body);
        return {
          headers,
          statusCode: 200,
          // body: {
          //   command: view.command,
          //   result: view.rows
          // }
          body: { view }
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        return {
          headers,
          statusCode: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = filterReportsController;