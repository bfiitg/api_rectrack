const jeonsoftDisplayAllApplicants = ({ displayAllApplicantsJeonSofts }) => {
  return async function get(httpRequest) {
    const headers = {
      "Content-Type": "application/json"
    };
    try {
      const view = await displayAllApplicantsJeonSofts({
        // postId: httpRequest.query.postId
      });
      return {
        headers,
        statusCode: 200,
        // body: {
        //   command: view.command,
        //   result: view.rows
        // }
        body: { view }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = jeonsoftDisplayAllApplicants;
