const subtractTimeIQBE = ({ IQBESubtractTimes }) => {
  return async function getOne(httpRequest) {
    try {
      //get the httprequest body
      const { source = {}, ...employeeInfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      const toView = {
        ...employeeInfo,
        source,
        token: httpRequest.params.token
      };
      // end here; to retrieve id

      const view = await IQBESubtractTimes(toView.token);
      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 200,
        body: { view }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      if (e.name === "RangeError") {
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 404,
          body: {
            error: e.message
          }
        };
      }
      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = subtractTimeIQBE;
