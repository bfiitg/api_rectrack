const usersReference = ({ referenceUsers }) => {
  return async function get(httpRequest) {
    const headers = {
      "Content-Type": "application/json"
    };
    try {
      //get the httprequest body
      const { source = {}, ...info } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      
      let reqQuery = {
        deptid: httpRequest.query.deptid ? httpRequest.query.deptid : httpRequest.params.deptid,
        posid: httpRequest.query.posid ? httpRequest.query.posid : httpRequest.params.posid,
        to: httpRequest.query.to,
        from: httpRequest.query.from
      }, reqMethod = httpRequest.method
      let params = (reqMethod === 'GET') ? reqQuery : info
      
      const view = await referenceUsers(
        params,
        reqMethod
      );
      
      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 200,
        body: { view }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = usersReference;
