const usersRejected = ({ rejectedUsers }) => {
  return async function get(httpRequest) {
    const headers = {
      "Content-Type": "application/json"
    };
    try {
      //get the httprequest body
      const { source = {}, ...info } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      let queryParams = {
        deptid: httpRequest.query.deptid ? httpRequest.query.deptid : httpRequest.params.deptid,
        posid: httpRequest.query.posid ? httpRequest.query.posid : httpRequest.params.posid,
        from: httpRequest.query.from,
        to:httpRequest.query.to
      }, requestMethod = httpRequest.method

      let params = (httpRequest.method === 'GET') ? queryParams : info

      // end here; to retrieve id
      const view = await rejectedUsers(
        params,
        requestMethod
      );
      
      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 200,
        body: { view }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = usersRejected;
