const fetchApplicantsWithoutReferral = ({ fetchApplicantsWithoutReferral_USECASE }) => {
   return async function get(httpRequest) {
      const headers = {
         "Content-Type" : "application/json"
      }

      try {
         const { source = {}} = httpRequest.body;
         source.ip = httpRequest.ip;
         source.browser = httpRequest.headers["User-Agent"];
         if (httpRequest.headers["Referer"]) {
            source.referrer = httpRequest.headers["Referer"];
         }
         const view = await fetchApplicantsWithoutReferral_USECASE()

         return {
            headers,
            statusCode: 200,
            body: { view }
         }
      } catch (error) {
         console.log(e);
         return {
            headers,
            statusCode: 400,
            body: {
               error: e.message
            }
         };
      }
   }
}

module.exports = fetchApplicantsWithoutReferral