const usersAssessment = ({ assessmentUsers }) => {
  return async function get(httpRequest) {
    const headers = {
      "Content-Type": "application/json"
    };
    try {
      //get the httprequest body
      const { source = {}, ...info } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      const filter = {
        deptid: httpRequest.params.deptid,
        posid: httpRequest.params.posid
       }
      
      let offset = httpRequest.query.offset,
          limit = httpRequest.query.limit

      info.offset = offset
      info.limit = limit
       
      const view = await assessmentUsers(
        filter.deptid, 
        filter.posid,
        {info}
        );

      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 200,
        body: { view }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = usersAssessment;
