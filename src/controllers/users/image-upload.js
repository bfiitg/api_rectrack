const uploadImage = ({ imageUploads }) => {
  return async function post(httpRequest) {
    try {
      const { source = {}, ...userInfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      const posted = await imageUploads({
        source
      });
      // file path of the upload image in the server (uploads folder)
      const path = httpRequest.file[0].path;
      return {
        headers: {
          "Content-Type": "application/json",
          "Last-Modified": new Date(posted.modifiedOn).toUTCString()
        },
        statusCode: 201,
        body: { posted, imagePath: path }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = uploadImage;
