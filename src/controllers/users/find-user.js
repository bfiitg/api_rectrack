const fetchUsers = ({ findUserDetails }) => {
   return async function fetch(httpRequest) {
       const headers = {
           "Content-Type": "application/json"
       };

       try {
           //get the httprequest body
           let input = httpRequest.query.u

           const view = await findUserDetails(input);

           return {
               headers,
               statusCode: 200,
               body: { view }
           };
       } catch (e) {
           // TODO: Error logging
           console.log(e);
           return {
               headers,
               statusCode: 400,
               body: {
                   error: e.message
               }
           };
       }
   };
};

module.exports = fetchUsers;
