const shortlistNotification = ({ notificationShortlists }) => {
  return async function get(httpRequest) {
    const headers = {
      "Content-Type": "application/json"
    };
    try {
      const { source = {}, ...employeeInfo } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-Agent"];
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
      const view = await notificationShortlists(
        // postId: httpRequest.query.postId
        employeeInfo.role
      );
      return {
        headers,
        statusCode: 200,
        // body: {
        //   command: view.command,
        //   result: view.rows
        // }
        body: { view }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = shortlistNotification;
