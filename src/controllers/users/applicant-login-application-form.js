const loginApplicationForm = ({ applicationFormLogins }) => {
  return async function put(httpRequest) {
    try {
      // get http request to submitted data
      const { source = {}, ...info } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      // end
      const toEdit = {
        ...info,
        source
      };
      const patched = await applicationFormLogins(toEdit);
      if (patched.msg === "Login successful") {
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 200,
          body: { patched }
        };
      } else {
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 400,
          body: { patched }
        };
      }
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = loginApplicationForm;
