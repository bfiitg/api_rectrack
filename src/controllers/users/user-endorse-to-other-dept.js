const endorseToOtherDeptController = ({ endorseToOtherDepartmentUseCase }) => {
    return async function changeStatus(httpRequest) {
      try {
        // get http request to submitted data
        const { source = {}, ...info } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-Agent"];
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
        // end
        
        const toEdit = {
          ...info,
          source,
          eUserId: httpRequest.params.eUserId,
          userId: httpRequest.params.userId,
          posId: httpRequest.params.posId
        };
        []
        const patched = await endorseToOtherDepartmentUseCase(
          toEdit.userId,
          toEdit.posId
        );
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 200,
          body: { patched }
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
  
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = endorseToOtherDeptController;
  