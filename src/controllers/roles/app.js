const {
  addRole,
  rolesSelectAll,
  rolesSelectOnes,
  adminEditRoless
} = require("../../use-cases/roles/app");

const selectOneRole = require("./roles-select-one");
const makeRoles = require("./roles-add");
const selectAllRoles = require("./roles-select-all");
const rolesEditAdmin = require("./roles-edit");

const rolesEditAdmins = rolesEditAdmin({ adminEditRoless });
const selectOneRoles = selectOneRole({ rolesSelectOnes });
const postRoles = makeRoles({ addRole });
const selectAllRole = selectAllRoles({ rolesSelectAll });

const rolesController = Object.freeze({
  postRoles,
  selectAllRole,
  selectOneRoles,
  rolesEditAdmins
});

module.exports = rolesController;
module.exports = {
  postRoles,
  selectAllRole,
  selectOneRoles,
  rolesEditAdmins
};
