const deleteTestController = ({ delete_testUseCase }) => {
  return async function getRejectors(httpRequest) {
    const headers = {
      "Content-Type": "application/json"
    };
    try {
      const views = await delete_testUseCase({
        deptname: httpRequest.query.deptname
      });
      return {
        headers,
        statusCode: 200,
        body: views
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = deleteTestController;
