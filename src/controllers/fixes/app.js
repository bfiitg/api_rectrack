const {
  findRejectorsUseCase,
  resultImportUseCase,
  delete_testUseCase,
  insertDeployedApplicants,
  insertRejectedApplicants,
  insertReferenceApplicants,
  insertBlacklistApplicants,

  alterTabels,
  alterTabel1s,
  alterTabel2s,
  updateColumns,

  deleteDuplicateUseCase,
  deleteUserUseCase,
  userClearOnlineExamUserCase,

  updateToDeployUseCase,
  updateToOnlineExamUseCase
} = require("../../use-cases/fixes/app");

const findRejector = require("./find-rejected-applicant-rejector");
const resultImport = require("./result-import");
const delete_Test = require("./delete_test");
const deployedApplicantsInsert = require("./insert-applicant-deployed");
const rejectedApplicantsInsert = require("./insert-applicant-rejected");
const referenceApplicantsInsert = require("./insert-applicant-reference");
const blacklistApplicantsInsert = require("./insert-applicant-blacklist");
const alterTableController = require("./alter-table");
const alterTable1Controller = require("./alter-table_1");
const alterTable2Controller = require("./alter-table_2");
const updateColumnController= require("./updateColumn");
const DeleteDuplicate = require("./deleteDuplicate");
const deleteUser = require("./deleteUser")
const resetOnlineExamRecord = require("./resetOnlineExamRecord")
const updateToDeploy = require("./update-user-to-deploy")
const updateToOnlineExam = require("./update-user-to-online")

const findRejectorController = findRejector({ findRejectorsUseCase });
const resultImportController = resultImport({ resultImportUseCase });
const delete_testController = delete_Test({ delete_testUseCase });
const deployedApplicantsInserts = deployedApplicantsInsert({
  insertDeployedApplicants
});
const rejectedApplicantsInserts = rejectedApplicantsInsert({
  insertRejectedApplicants
});
const referenceApplicantsInserts = referenceApplicantsInsert({
  insertReferenceApplicants
});
const blacklistApplicantsInserts = blacklistApplicantsInsert({
  insertBlacklistApplicants
});

const alterTableControllers = alterTableController({alterTabels})
const alterTable1Controllers = alterTable1Controller({alterTabel1s})
const alterTable2Controllers = alterTable2Controller({alterTabel2s})
const updateColumnControllers = updateColumnController({updateColumns})

const DeleteDuplicateController = DeleteDuplicate({deleteDuplicateUseCase})
const deleteUserController = deleteUser({deleteUserUseCase})
const resetOnlineExamRecordController = resetOnlineExamRecord({userClearOnlineExamUserCase})
const updateToDeployController = updateToDeploy({updateToDeployUseCase})
const updateToOnlineExamController = updateToOnlineExam({updateToOnlineExamUseCase})

const controller = Object.freeze({
  findRejectorController,
  resultImportController,
  delete_testController,
  deployedApplicantsInserts,
  rejectedApplicantsInserts,
  referenceApplicantsInserts,
  blacklistApplicantsInserts,

  alterTableControllers,
  alterTable1Controllers,
  alterTable2Controllers,
  updateColumnControllers,

  DeleteDuplicateController,
  deleteUserController,
  resetOnlineExamRecordController,

  updateToDeployController,
  updateToOnlineExamController
});

module.exports = controller;
module.exports = {
  findRejectorController,
  resultImportController,
  delete_testController,
  deployedApplicantsInserts,
  rejectedApplicantsInserts,
  referenceApplicantsInserts,
  blacklistApplicantsInserts,

  alterTableControllers,
  alterTable1Controllers,
  alterTable2Controllers,
  updateColumnControllers,

  DeleteDuplicateController,
  deleteUserController,
  resetOnlineExamRecordController,

  updateToDeployController,
  updateToOnlineExamController
};
