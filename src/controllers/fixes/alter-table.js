const alterTableController = ({ alterTabels }) => {
    return async function getRejectors(httpRequest) {
      const headers = {
        "Content-Type": "application/json"
      };
      try {
        const views = await alterTabels();
        return {
          headers,
          statusCode: 200,
          body: views
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        return {
          headers,
          statusCode: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = alterTableController;
  