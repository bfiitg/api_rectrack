const alterTable2Controller = ({ alterTabel2s }) => {
    return async function getRejectors(httpRequest) {
      const headers = {
        "Content-Type": "application/json"
      };
      try {
        const views = await alterTabel2s();
        return {
          headers,
          statusCode: 200,
          body: views
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        return {
          headers,
          statusCode: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = alterTable2Controller;
  