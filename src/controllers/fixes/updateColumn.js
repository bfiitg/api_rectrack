const updateColumn = ({ updateColumns }) => {
    return async function getRejectors(httpRequest) {
      const headers = {
        "Content-Type": "application/json"
      };
      try {
        const views = await updateColumns({
            question_type_id:httpRequest.query.question_type_id,
            categories_id:httpRequest.query.categories_id

        });
        return {
          headers,
          statusCode: 200,
          body: views
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        return {
          headers,
          statusCode: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = updateColumn;

  