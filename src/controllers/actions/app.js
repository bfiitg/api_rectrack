const {
    selectAllActionsUseCase,
    makeAddActionUseCase,
    updateActionUseCase,
    selectActionsUseCase
} = require("../../use-cases/actions/app")

const selectAllActions = require("./action-selectAll");
const makeActions = require("./action-add");
const updateActions = require("./action-edit")
const selectActions = require("./action-select")

const selectAllActionsController = selectAllActions({selectAllActionsUseCase});
const makeActionsController = makeActions({makeAddActionUseCase})
const updateActionsController= updateActions({updateActionUseCase})
const selectActionsController = selectActions({selectActionsUseCase})

const controller = Object.freeze({
    selectAllActionsController,
    makeActionsController,
    updateActionsController,
    selectActionsController,
})

module.exports = controller;
module.exports = {
    selectAllActionsController,
    makeActionsController,
    updateActionsController,
    selectActionsController
}