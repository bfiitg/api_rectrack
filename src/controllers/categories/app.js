const {
  addCategory,
  categoriesSelectAlls,
  categorySelectOnes,
  updateCategorys,
  categorySelectAllFilterTeams
} = require("../../use-cases/categories/app");

const filterTeamCategorySelectAll = require("./categories-view-filter-team");
const makeCategories = require("./categories-add");
const selectAllCategories = require("./categories-view-all");
const selectOneCategory = require("./categories-view-one");
const categoriesUpdate = require("./categories-update");

const filterTeamCategorySelectAlls = filterTeamCategorySelectAll({
  categorySelectAllFilterTeams
});
const selectOneCategorys = selectOneCategory({ categorySelectOnes });
const postCategory = makeCategories({ addCategory });
const selectAllCategory = selectAllCategories({ categoriesSelectAlls });
const categoriesUpdates = categoriesUpdate({ updateCategorys });

const controller = Object.freeze({
  postCategory,
  selectAllCategory,
  selectOneCategorys,
  categoriesUpdates,
  filterTeamCategorySelectAlls
});

module.exports = controller;
module.exports = {
  postCategory,
  selectAllCategory,
  selectOneCategorys,
  categoriesUpdates,
  filterTeamCategorySelectAlls
};
