const getCurrentSystemSettingsController = ({ getCurrentSystemSettingsUseCase }) => {
    return async function getColors(httpRequest){
        
        const headers = {
            "Content-Type": "application/json"
        };

        try{
            const settings = await getCurrentSystemSettingsUseCase();
            return {
                headers,
                statusCode: 200,
                body: {
                    settings
                }
            }
        } catch(err){
            console.log(err);
            
            return {
                headers,
                statusCode:400,
                body: {
                    error: err.message
                }
            }
        }
        
    }//end method
}

module.exports = getCurrentSystemSettingsController;