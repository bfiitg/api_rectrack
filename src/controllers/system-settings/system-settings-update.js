const updateSystemSettingsController = ({ updateSystemSettingsUseCase }) => {
    return async function updateSettings(httpRequest){

        try{
            const { source = {}, ...info } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers['User-Agent'];

            if(httpRequest.headers['Referer']){
                source.referrer = httpRequest.headers['Referrer'];
            }

            const logoPath = httpRequest.file[0].path;
            const updates = {
                ...info,
                source,
                logoPath
            };

            const patched = await updateSystemSettingsUseCase(updates);

            return {
                headers: {
                    "Content-Type": "application/json"
                },
                statusCode: 200,
                body: {
                    patched
                }
            }

        }//end try
        catch(err){
            console.log(err);

            return {
                headers: {
                    "Content-Type": "application/json"
                },
                statusCode: 400,
                body: {
                    error: err.message
                }
            }
        }//end catch

    }//end
}//end

module.exports = updateSystemSettingsController;