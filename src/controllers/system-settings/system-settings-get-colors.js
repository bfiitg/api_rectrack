const getSystemColorsController = ({ getSystemColorsUseCase }) => {
    return async function getColors(httpRequest){
        
        const headers = {
            "Content-Type": "application/json"
        };

        try{
            const colors = await getSystemColorsUseCase();
            return {
                headers,
                statusCode: 200,
                body: {
                    colors
                }
            }
        } catch(err){
            console.log(err);
            
            return {
                headers,
                statusCode:400,
                body: {
                    error: err.message
                }
            }
        }
        
    }//end method
}

module.exports = getSystemColorsController;