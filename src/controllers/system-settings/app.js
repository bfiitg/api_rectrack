const {
    getSystemColorsUseCase,
    updateSystemSettingsUseCase,
    getCurrentSystemSettingsUseCase
} = require('../../use-cases/system-settings/app');


const getSystemColors = require('./system-settings-get-colors');
const updateSystemSettings = require('./system-settings-update');
const getCurrentSystemSettings = require('./get-current-system-settings');

const getSystemColorsController = getSystemColors({ getSystemColorsUseCase });
const updateSystemSettingsController = updateSystemSettings({ updateSystemSettingsUseCase });
const getCurrentSystemSettingsController = getCurrentSystemSettings({ getCurrentSystemSettingsUseCase });

const systemSettingsController = Object.freeze({
    getSystemColorsController,
    updateSystemSettingsController,
    getCurrentSystemSettingsController
});

module.exports = systemSettingsController;
module.exports = {
    getSystemColorsController,
    updateSystemSettingsController,
    getCurrentSystemSettingsController
};