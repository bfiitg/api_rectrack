const {
    selectAllAccessRightsUseCase,
    selectAccessRightsUseCase,
    makeAddAccessRightsUseCase,
    updateAccessRightsUseCase,
    removeAccessRightsUseCase,
    additionalAccessRightsUseCase
} = require("../../use-cases/access-rights/app")

const selectAllAccessRights = require("./access-rights-selectAll");
const selectAccessRights = require("./access-rights-select");
const makeAccessRights = require("./access-rights-add");
const updateAccessRights = require("./access-rights-edit");
const removeAccessRights = require("./access-rights-remove")
const additionalAccessRights = require("./access-rights-addActions")
const selectAllAccessRightsController = selectAllAccessRights({ selectAllAccessRightsUseCase });
const selectAccessRightsController = selectAccessRights({ selectAccessRightsUseCase })
const makeAccessRightsController = makeAccessRights({ makeAddAccessRightsUseCase })
const updateAccessRightsController = updateAccessRights({ updateAccessRightsUseCase })
const removeAccessRightsController = removeAccessRights({ removeAccessRightsUseCase })
const additionalAccessRightsController = additionalAccessRights({ additionalAccessRightsUseCase })
const controller = Object.freeze({
    selectAllAccessRightsController,
    selectAccessRightsController,
    makeAccessRightsController,
    updateAccessRightsController,
    removeAccessRightsController,
    additionalAccessRightsController
})

module.exports = controller;

module.exports = {
    selectAllAccessRightsController,
    selectAccessRightsController,
    makeAccessRightsController,
    updateAccessRightsController,
    removeAccessRightsController,
    additionalAccessRightsController
}