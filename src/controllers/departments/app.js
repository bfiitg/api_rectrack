const {
  addDept,
  departmentSelectAlls,
  deptChangeStatusInactives,
  adminEditDepts,
  departmentSelectOnes
} = require("../../use-cases/departments/app");

const selectOneDept = require("./department-view-one");
const makeDepts = require("./department-add");
const selectAllDepartment = require("./department-view-all");
const changeStatusDeptInactive = require("./department-change-status-inactive");
const deptEditAdmin = require("./department-edit");

const selectOneDepts = selectOneDept({ departmentSelectOnes });
const postDepts = makeDepts({ addDept });
const selectAllDepartments = selectAllDepartment({ departmentSelectAlls });
const changeStatusDeptInactives = changeStatusDeptInactive({
  deptChangeStatusInactives
});
const deptEditAdmins = deptEditAdmin({ adminEditDepts });

const controller = Object.freeze({
  postDepts,
  selectAllDepartments,
  changeStatusDeptInactives,
  deptEditAdmins,
  selectOneDepts
});

module.exports = controller;
module.exports = {
  postDepts,
  selectAllDepartments,
  changeStatusDeptInactives,
  deptEditAdmins,
  selectOneDepts
};
