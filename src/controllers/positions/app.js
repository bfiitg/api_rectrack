const {
  addPositions,
  positionsDisplayVacants,
  positionSelectsAlls,
  positionNotVacants,
  positionVacants,
  positionsDisplayNotVacants,
  positionSelectOnes,
  updatePositionss,
  positionSelectAllByDepts
} = require("../../use-cases/positions/app");

const selectAllPositionByDept = require("./position-select-all-by-dept");
const selectOnePosition = require("./position-select-one");
const makePositions = require("./positions-add");
const selectAllVacantPosition = require("./positions-display-vacant");
const selectAllPositions = require("./positions-select-all");
const notVacantPosition = require("./positions-to-not-vacant");
const vacantPosition = require("./positions-to-vacant");
const selectAllNotVacantPosition = require("./positions-display-not-vacant");
const positionUpdate = require("./positions-update");

const selectAllPositionByDepts = selectAllPositionByDept({
  positionSelectAllByDepts
});
const positionUpdates = positionUpdate({ updatePositionss });
const selectOnePositions = selectOnePosition({ positionSelectOnes });
const postPositions = makePositions({ addPositions });
const selectAllVacantPositions = selectAllVacantPosition({
  positionsDisplayVacants
});
const selectAllPosition = selectAllPositions({ positionSelectsAlls });
const notVacantPositions = notVacantPosition({ positionNotVacants });
const vacantPositions = vacantPosition({ positionVacants });
const selectAllNotVacantPositions = selectAllNotVacantPosition({
  positionsDisplayNotVacants
});

const controller = Object.freeze({
  postPositions,
  selectAllVacantPositions,
  selectAllPosition,
  notVacantPositions,
  vacantPositions,
  selectAllNotVacantPositions,
  selectOnePositions,
  positionUpdates,
  selectAllPositionByDepts
});

module.exports = controller;
module.exports = {
  postPositions,
  selectAllVacantPositions,
  selectAllPosition,
  notVacantPositions,
  vacantPositions,
  selectAllNotVacantPositions,
  selectOnePositions,
  positionUpdates,
  selectAllPositionByDepts
};
