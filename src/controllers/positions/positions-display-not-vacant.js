const selectAllNotVacantPosition = ({ positionsDisplayNotVacants }) => {
    // selectAnime must have the same name all through out
    return async function selects(httpRequest) {
      const headers = {
        "Content-Type": "application/json"
      };
      try {
        const views = await positionsDisplayNotVacants({
          // postId: httpRequest.query.postId
        });
        return {
          headers,
          statusCode: 200,
          body: views
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        return {
          headers,
          statusCode: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = selectAllNotVacantPosition;
  