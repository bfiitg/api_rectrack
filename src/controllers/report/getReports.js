const getReports = ({ getReportUseCase }) => {
    // selectAnime must have the same name all through out
    return async function selects(httpRequest) {
      const headers = {
        "Content-Type": "application/json"
      };
      const depId = httpRequest.body
      const stage = httpRequest.params
     
      try {
        const views = await getReportUseCase(
          {stage,depId}
        );
        return {
          headers,
          statusCode: 200,
          body: views
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        return {
          headers,
          statusCode: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = getReports;
  