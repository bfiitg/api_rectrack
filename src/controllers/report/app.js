const {
getReportUseCase,
dlReportsUseCase
}=require("../../use-cases/report/app")

const getReports = require('./getReports')
const dlReports = require("./dlReports")

const getReportsController = getReports({getReportUseCase})
const dlReportsController = dlReports({dlReportsUseCase})



const reportController = Object.freeze({
    getReportsController,
    dlReportsController
  });
  
  module.exports = reportController;
  module.exports = {
    getReportsController,
    dlReportsController
  };