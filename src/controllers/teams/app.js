const {
  addTeams,
  teamsSelectAlls,
  teamChangeStatusInactives,
  adminEditTeams,
  teamSelectOnes,
  teamsSelectAllFilterDepts
} = require("../../use-cases/teams/app");

const deptFilterSelectAllTeam = require("./teams-view-filter-dept");
const selectOneTeam = require("./team-view-one");
const makeTeams = require("./teams-add");
const selectAllTeams = require("./teams-view-all");
const changeStatusTeamInactive = require("./teams-change-status-inactive");
const teamsEditAdmin = require("./teams-edit");

const deptFilterSelectAllTeams = deptFilterSelectAllTeam({
  teamsSelectAllFilterDepts
});
const selectOneTeams = selectOneTeam({ teamSelectOnes });
const postTeams = makeTeams({ addTeams });
const selectAllTeam = selectAllTeams({ teamsSelectAlls });
const changeStatusTeamInactives = changeStatusTeamInactive({
  teamChangeStatusInactives
});
const teamsEditAdmins = teamsEditAdmin({ adminEditTeams });

const controller = Object.freeze({
  postTeams,
  selectAllTeam,
  changeStatusTeamInactives,
  teamsEditAdmins,
  selectOneTeams,
  deptFilterSelectAllTeams
});

module.exports = controller;
module.exports = {
  postTeams,
  selectAllTeam,
  changeStatusTeamInactives,
  teamsEditAdmins,
  selectOneTeams,
  deptFilterSelectAllTeams
};
