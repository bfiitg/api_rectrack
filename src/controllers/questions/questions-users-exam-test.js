const randomQuestionsUserTest = ({
    selectUsersRandomQuestion,
    checkUsersCategoryIfDones
  }) => {
    return async function getOne(httpRequest) {
      try {
        //get the httprequest body
        const { source = {}, ...info } = httpRequest.body;
        source.ip = httpRequest.ip;
        source.browser = httpRequest.headers["User-Agent"];
        if (httpRequest.headers["Referer"]) {
          source.referrer = httpRequest.headers["Referer"];
        }
  
        //   get the category array from body
        const index = httpRequest.body.index;
        const categoryIdArray = httpRequest.body.categoryId;
        const token = httpRequest.body.token;
        let view;
  
  
        const toView = {
            ...info,
            source,
            id: categoryIdArray[index]
          };
  
        view = {
          categoryId: categoryIdArray[index],
          questions: await selectUsersRandomQuestion(toView.id, token)
        };
  
        // for (let i = 0; i < categoryIdArray.length; i++) {
        //   const toView = {
        //     ...info,
        //     source,
        //     id: categoryIdArray[i]
        //   };
        //   // end here; to retrieve id
        //   const isDone = await checkUsersCategoryIfDones(toView.id);
        //   if (isDone === "no") {
        //     view = {
        //       categoryId: categoryIdArray[i],
        //       questions: await selectUsersRandomQuestion(toView.id, token)
        //     };
        //     index ++
        //     break;
        //   }
        // }
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 200,
          body: { index, view }
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        if (e.name === "RangeError") {
          return {
            headers: {
              "Content-Type": "application/json"
            },
            statusCode: 404,
            body: {
              error: e.message
            }
          };
        }
        return {
          headers: {
            "Content-Type": "application/json"
          },
          statusCode: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = randomQuestionsUserTest;
  