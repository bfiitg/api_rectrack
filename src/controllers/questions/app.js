const {
  addQuestions,
  selectUsersRandomQuestion,
  checkUsersCategoryIfDones,
  userSubmitAnswers,
  makeAddQuestionsImports,
  userViewQuestions,
  editQuestions,
  adminViewQuestionss,
  selectUsersRandomQuestionsIQBEs,
  userSubmitAnswerIQBEs,
  userSubmitAnswersTest,
  //butch part
  addQuestionsEssayUseCase,
  viewQTypeUseCase,
  viewAllIQTestUseCase,
  makeAddQuestionsIQUseCase
} = require("../../use-cases/questions/app");

const answerSubmitUserIQBE = require("./questions-IQBE-submit");
const randomQuestionsUserIQBE = require("./questions-IQBE");
const makeQuestions = require("./questions-with-choices-add");
const randomQuestionsUsers = require("./questions-users-exam");
const answerSubmitUser = require("./questions-user-submit");
const makeQuestionsImport = require("./questions-import");
const questionsUserView = require("./questioins-view-per-category");
const questionsEdit = require("./questions-edit");
const questionsAdminView = require("./questions-display");
//butch part
const makeQuestionsEssay = require("./questions-add-essay");
const viewQTypes = require("./questions-find-by-Qtype");
const viewIQBE = require("./questions-view-all-IQBE")
const makeQuestionIQ = require("./questions-with-choice-add-IQ")


const answerSubmitUserIQBEs = answerSubmitUserIQBE({ userSubmitAnswerIQBEs });
const postQuestions = makeQuestions({ addQuestions });
const randomQuestionsUser = randomQuestionsUsers({
  selectUsersRandomQuestion,
  checkUsersCategoryIfDones
});
const answerSubmitUsers = answerSubmitUser({ userSubmitAnswers });
const makeQuestionsImports = makeQuestionsImport({ makeAddQuestionsImports });
const questionsUserViews = questionsUserView({
  userViewQuestions
});
const questionsEdits = questionsEdit({ editQuestions });
const questionsAdminViews = questionsAdminView({ adminViewQuestionss });
const randomQuestionsUserIQBEs = randomQuestionsUserIQBE({
  selectUsersRandomQuestionsIQBEs,
  checkUsersCategoryIfDones
});
//butch part
const addQuestionsEssayController = makeQuestionsEssay({addQuestionsEssayUseCase});
const viewQTypeController = viewQTypes({viewQTypeUseCase});
const viewIQBEController = viewIQBE({viewAllIQTestUseCase})
const makeQuestionIQController = makeQuestionIQ({makeAddQuestionsIQUseCase})

//////////////////////
//for test
const randomQuestionsUsersTest = require('./questions-users-exam-test');
const answerSubmitUserTest = require("./questions-user-submit");
//for test
const randomQuestionsUserTest = randomQuestionsUsersTest({
  selectUsersRandomQuestion,
  checkUsersCategoryIfDones
});
const answerSubmitUsersTest = answerSubmitUserTest({ userSubmitAnswersTest });
////////////////

const controller = Object.freeze({
  postQuestions,
  randomQuestionsUser,
  answerSubmitUsers,
  makeQuestionsImports,
  questionsUserViews,
  questionsEdits,
  questionsAdminViews,
  randomQuestionsUserIQBEs,
  answerSubmitUserIQBEs,
  randomQuestionsUserTest,
  answerSubmitUsersTest,
  //butch part
  addQuestionsEssayController,
  viewQTypeController,
  viewIQBEController,
  makeQuestionIQController,
  
});

module.exports = controller;
module.exports = {
  postQuestions,
  randomQuestionsUser,
  answerSubmitUsers,
  makeQuestionsImports,
  questionsUserViews,
  questionsEdits,
  questionsAdminViews,
  randomQuestionsUserIQBEs,
  answerSubmitUserIQBEs,
  randomQuestionsUserTest,
  answerSubmitUsersTest,
  //butch part
  addQuestionsEssayController,
  viewQTypeController,
  viewIQBEController,
  makeQuestionIQController,
};
