const viewAllIQTest = ({questionsDb}) => {
    return async function view(){ 
        const view = await questionsDb.displayQuestionsIQtest();
        const result = view.rows;

    const results = [];
    for (let i = 0; i < result.length; i++) {
      const choices = result[i].choices_or_imagepath;
      
      if (i === 0) {
        results.push({
          id: result[i].id,
          questions: result[i].questions,
          correct_answer: result[i].correct_answer,
          type:result[i].question_type,
        
          details: [
            {
              choice: choices
            }
          ]
        });
      } else {
        if (result[i].id !== result[i - 1].id) {
          results.push({
            id: result[i].id,
            questions: result[i].questions,
            correct_answer: result[i].correct_answer,
            type:result[i].question_type,

            details: [
              {
                choice: choices
              }
            ]
          });
        } else {
          const x = results.length - 1;
          results[x].details.push({
            choice: choices
          });
        }
      }
    }
    return results;
  };
}

module.exports = viewAllIQTest;