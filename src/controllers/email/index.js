const { 
   assessmentEmail_USE,
   blacklistEmail_USE,
   deployedEmail_USE,
   jobOfferEmail_USE,
   keptForReferenceEmail_USE,
   onlineExamEmail_USE,
   returnToReceivedEmail_USE,
   rejectedEmail_USE,
   shortlistedEmail_USE,
   userCredentialsEmail_USE,
   assessmentFormEmail_USE,
   receivedEmail_USE,
   endorsedToOtherDepartment_USE,
   reapplyApplicantEmail_USE,
   forgotPasswordEmail_USE,
   examRetakeEmail_USE
} = require('../../use-cases/email')

const sendAssessmentEmail = require('./assessment'),
      sendBlacklistEmail = require('./blacklist'),
      sendDeployedEmail = require('./deployed'),
      sendJobOfferEmail = require('./job-offer'),
      sendKeptForReferenceEmail = require('./kept-for-reference'),
      sendOnlineExamEmail = require('./online-exam'),
      sendReturnToReceivedEmail = require('./return-to-received'),
      sendRejectedEmail = require('./rejected'),
      sendShortlistedEmail = require('./shortlist'),
      sendUserCredentialsEmail = require('./user-credentials'),
      sendAssessmentFormEmail = require('./assessment-form'),
      sendReceivedEmail = require('./received'),
      sendEndorseToOtherDeptEmail = require('./endorsed-to-other-dept'),
      sendReapplyEmail = require('./reappy-applicant'),
      sendForgotPasswordEmail = require('./forgot-password'),
      sendExamRetakeEmail = require('./exam-retake')

      

const sendAssessmentEmail_CON = sendAssessmentEmail(assessmentEmail_USE),
      sendBlacklistEmail_CON = sendBlacklistEmail(blacklistEmail_USE),
      sendDeployedEmail_CON = sendDeployedEmail(deployedEmail_USE),
      sendJobOfferEmail_CON = sendJobOfferEmail(jobOfferEmail_USE),
      sendKeptForRerenceEmail_CON = sendKeptForReferenceEmail(keptForReferenceEmail_USE),
      sendOnlineExamEmail_CON = sendOnlineExamEmail(onlineExamEmail_USE),
      sendReturnToReceivedEmail_CON = sendReturnToReceivedEmail(returnToReceivedEmail_USE),
      sendRejectedEmail_CON = sendRejectedEmail(rejectedEmail_USE),
      sendShortlistedEmail_CON = sendShortlistedEmail(shortlistedEmail_USE),
      sendUserCredentialsEmail_CON = sendUserCredentialsEmail(userCredentialsEmail_USE),
      sendAssessmentFormEmail_CON = sendAssessmentFormEmail(assessmentFormEmail_USE),
      sendReceivedEmail_CON = sendReceivedEmail(receivedEmail_USE),
      sendEndorseToOtherDeptEmail_CON = sendEndorseToOtherDeptEmail(endorsedToOtherDepartment_USE),
      sendReapplyEmail_CON = sendReapplyEmail(reapplyApplicantEmail_USE),
      sendForgotPasswordEmail_CON = sendForgotPasswordEmail(forgotPasswordEmail_USE),
      sendExamRetakeEmail_CON = sendExamRetakeEmail(examRetakeEmail_USE)

const emailController = Object.freeze({
   sendAssessmentEmail_CON,
   sendBlacklistEmail_CON,
   sendDeployedEmail_CON,
   sendJobOfferEmail_CON,
   sendKeptForRerenceEmail_CON,
   sendOnlineExamEmail_CON,
   sendReturnToReceivedEmail_CON,
   sendRejectedEmail_CON,
   sendShortlistedEmail_CON,
   sendUserCredentialsEmail_CON,
   sendAssessmentFormEmail_CON,
   sendReceivedEmail_CON,
   sendEndorseToOtherDeptEmail_CON,
   sendReapplyEmail_CON,
   sendForgotPasswordEmail_CON,
   sendExamRetakeEmail_CON
})

module.exports = emailController
module.exports = {
   sendAssessmentEmail_CON,
   sendBlacklistEmail_CON,
   sendDeployedEmail_CON,
   sendJobOfferEmail_CON,
   sendKeptForRerenceEmail_CON,
   sendOnlineExamEmail_CON,
   sendReturnToReceivedEmail_CON,
   sendRejectedEmail_CON,
   sendShortlistedEmail_CON,
   sendUserCredentialsEmail_CON,
   sendAssessmentFormEmail_CON,
   sendReceivedEmail_CON,
   sendEndorseToOtherDeptEmail_CON,
   sendReapplyEmail_CON,
   sendForgotPasswordEmail_CON,
   sendExamRetakeEmail_CON
}