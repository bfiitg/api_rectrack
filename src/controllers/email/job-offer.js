const sendJobOfferEmail = ( jobOfferEmail_USE ) => {
   return async function send(httpRequest) {
      try {
         const { source = {}, ...info } = httpRequest.body,
               headers = {
                  "Content-Type": "application/json"  
               }

         source.ip = httpRequest.ip;
         source.browser = httpRequest.headers["User-Agent"];

         if (httpRequest.headers["Referer"]) {
            source.referrer = httpRequest.headers["Referer"];
         }
         
         info.applicantID = httpRequest.params.applicantID

         let posted = await jobOfferEmail_USE( info )
         
         return {
            headers,
            statusCode: 200,
            body: posted
         }

      } catch (error) {
         console.log(error)
         return {
            headers,
            statusCode: 400,
            body: {
               error: error.message
            }
         }
      }
   }
}

module.exports = sendJobOfferEmail