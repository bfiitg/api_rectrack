const express = require("express")
const router = express.Router();

const{
    addQuestionTypeController,
    editQuestionTypeController,
    selectAllQuestionTypeController,
    changeActiveController,
    changeInactiveController,
    selectQuestionTypeController
}= require("../../src/controllers/questions_type/app");

const {getToken} = require("../../src/token/app")

const makeExpressCallBack= require("../../src/express-callback/app")

// add question type
router.post("/add", makeExpressCallBack(addQuestionTypeController))

// edit question type
router.put("/update/:id", makeExpressCallBack(editQuestionTypeController))

// view all question type
router.get("/view_all", makeExpressCallBack(selectAllQuestionTypeController))

// change to inactive
router.put("/inactive/:id", makeExpressCallBack(changeInactiveController));

// view selected question type by id
router.get("/view/:id", makeExpressCallBack(selectQuestionTypeController))

// change to active
router.put("/active/:id", makeExpressCallBack(changeActiveController));
module.exports = router