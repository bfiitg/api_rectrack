const express = require("express");
const router = express.Router();

const {
    getReportsController,
    dlReportsController
} = require("../../src/controllers/report/app");

const makeExpressCallback = require("../../src/express-callback/app");
const makeExpressCallbackDownload = require("../../src/express-callback-reports/app")

router.get(
  "/getreport/:stage",
  makeExpressCallback(getReportsController)
);

router.get("/download", makeExpressCallbackDownload(dlReportsController))


module.exports = router;
