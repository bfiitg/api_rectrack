const express = require("express");
const router = express.Router();

const {
    selectAllAccessRightsController,
    selectAccessRightsController,
    makeAccessRightsController,
    updateAccessRightsController,
    removeAccessRightsController,
    additionalAccessRightsController
} = require("../controllers/access-rights/app");

const { getToken } = require("../token/app");

const makeExpressCallback = require("../express-callback/app");
const Mail = require("nodemailer/lib/mailer");

router.get("/admin/view", makeExpressCallback(selectAllAccessRightsController))
router.get("/admin/view/:id", makeExpressCallback(selectAccessRightsController))
router.post("/admin/add", makeExpressCallback(makeAccessRightsController))
router.put("/admin/update/:id", makeExpressCallback(updateAccessRightsController))
router.post("/admin/remove/:id", makeExpressCallback(removeAccessRightsController))

router.post("/admin/add-action/:id", makeExpressCallback(additionalAccessRightsController))
module.exports = router