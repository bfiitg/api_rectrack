const express = require("express");
const router = express.Router();

const {
  fetchpendingApplicantController,
  fetchApplicantDateTrackingsController, rejectedController
  ,
  endorseToOtherDepartmentController,
  getQuickApplicantsController,
  filterReportsController,
  deployedController,
  postUser,
  loginUser,
  userReceived,
  changesStatusUserOnlineExam,
  changesStatusUserBlacklist,
  usersOnlineExams,
  changeStatusUserEndorsements,
  userEndorsed,
  changeStatusUserRejects,
  userRejected,
  userBlacklisted,
  changeStatusUserAssessments,
  userAssessment,
  changeStatusUserPendings,
  usersPendings,
  changeStatusUserReferences,
  usersReferences,
  changeStatusUserShortLists,
  userShortlisted,
  changeStatusUserDeployeds,
  usersDeployeds,
  viewAllUsersAdmins,
  viewUsersDetailsAdmins,
  usersAddAdmins,
  usersEditAdmins,
  cLoginApplicants,
  scheduleAssessmentUsers,
  changeStatusUserJobOrders,
  usersJobOrders,
  fillUpApplicationUsers,
  receivedNotifications,
  endorsedNotifications,
  applicantLoginIQBs,
  applicantNamesGets,
  jeonSoftExportCSVs,
  jeonsoftDisplayAllApplicant,
  downloadClientJeonsofts,
  loginApplicationForms,
  uploadImages,
  doneOnlineExamUsers,
  startEndTimeGets,
  subtractTimeIQBEs,
  IQBEDoneUsers,
  loggedInUserFirstnameGets,
  changeStatusUserForEndorsements,
  usersForEndorseds,
  referralsAdds,
  viewReferralForms,
  viewOneReferralForms,
  referralUpdates,
  insertLogss,
  onlineExamResultApplicants,
  applicantNamesGetPVs,
  referralFormFetchDatas,
  listPositionsShowAssessments,
  updatePositionApplicants,
  assessmentNotifications,
  deployedNotifications,
  shortlistNotifications,
  deployedDateUpdates,
  tokenChecks,
  // spreadSheetGoogleforQas,
  // spreadSheetGoogles,
  selectAllLogss,
  forEndorsedNotifications,
  ifExistReferralChecks,
  retakeOnlineExams,
  passwordChanges,
  filterReportAllController,
  getcountQuickApplicantsController,
  rateAnswerController,
  updateResumesController,
  reapplyGets,
  reApplyProcesss,

  deployedUpdateKeptRefController,
  deployedUpdateJOController,
  viewAllIQTestController,
  getAllApplicantsController,
  findApplicantById_CONTROLLER,
  getAllShortListController,
  KeepReferencesController,
  jobOfferController,
  getApplicantNotifcation_CONTROLLER,
  addApplicantNotifcation_CONTROLLER,
  updateApplicantNotification_CONTROLLER,
  passwordValidation_CONTROLLER,
  fetchApplicantsWithoutReferral_CONTROLLER,
  returnApplicantToReceived_CONTROLLER,
  fetchListOfUsers
} = require("../controllers/users/app");

const { getToken, paramToken } = require("../token/app");

const { upload, makeFolder } = require("../../upload_image/app");
const makeExpressCallback = require("../express-callback/app");
const makeExpressCallbackDownload = require("../express-callback-with-download/app");

// ************************//
// GET
// ************************//

// get applicants re applied
router.get("/reapply", getToken, makeExpressCallback(reapplyGets));

// check if user has already referral
router.get(
  "/referral-check/:id",
  getToken,
  makeExpressCallback(ifExistReferralChecks)
);

router.get(
  "/find",
  // getToken,
  makeExpressCallback(fetchListOfUsers) 
)

// router.post("/keep-reference", makeExpressCallback(KeepReferencesController));

// return status of request if 403 logout user
router.get("/check-token/:token", makeExpressCallback(tokenChecks));

// all posible positions to assign
// update positioin in assessment
router.get(
  "/position-list/:id",
  getToken,
  makeExpressCallback(listPositionsShowAssessments)
);

// get first name of logged in user
router.get(
  "/firstname/:token",
  getToken,
  makeExpressCallback(loggedInUserFirstnameGets)
);


// all received route for oh
// filtered by dept and/or position
// router.get("/endorsed/:id/:posid", getToken, makeExpressCallback(userEndorsed));

// // all endorsed route for depthead
// router.get("/endorsed/:id", getToken, makeExpressCallback(userEndorsed));

// // all endorsed route for team head
// router.get("/endorsed/th/:teamId", getToken, makeExpressCallback(userEndorsed));

// all endorsed route for oh
router.put("/endorsed", getToken, makeExpressCallback(userEndorsed));

// all endorsed route for pg
router.get("/endorsed", getToken, makeExpressCallback(userEndorsed));


// all blacklisted route for team head
router.get(
  "/blacklisted/th/:teamId",
  getToken,
  makeExpressCallback(userBlacklisted)
);

// all received count for notification route
router.get("/received-notif/count", getToken, makeExpressCallback(receivedNotifications));

router.post(
  "/received-notif/count",
  makeExpressCallback(receivedNotifications)
);

// all received count for notification route
// filter by team
router.get(
  "/received-notif/count/th/:teamId",
  makeExpressCallback(receivedNotifications)
);

// all received count for notification route
// filter by dept
router.get(
  "/received-notif/count/:id",
  makeExpressCallback(receivedNotifications)
);

// all endorsed count for notification route
router.get("/endorsed-notif/count", getToken, makeExpressCallback(endorsedNotifications));

// all endorsed count for notification route for oh
router.post(
  "/endorsed-notif/count",
  makeExpressCallback(endorsedNotifications)
);

// all shortlist count for notification route
router.get(
  "/shortlist-notif/count",
  makeExpressCallback(shortlistNotifications)
);

router.post(
  "/shortlist-notif/count",
  makeExpressCallback(shortlistNotifications)
);

// all assessment count for notification route for team head
router.get(
  "/assessment-notif/count/th/:teamId",
  makeExpressCallback(assessmentNotifications)
);

router.post(
  "/assessment-notif/count/",
  makeExpressCallback(assessmentNotifications)
);

router.get(
  "/assessment-notif/count/:id",
  makeExpressCallback(assessmentNotifications)
);

// all for endorsement count for notification route for OH
router.post(
  "/for-endorsed-notif/count/",
  makeExpressCallback(forEndorsedNotifications)
);

// all for endorsement count for notification route for TH
router.get(
  "/for-endorsed-notif/count/th/:teamId",
  makeExpressCallback(forEndorsedNotifications)
);

router.post(
  "/deployed-notif/count/",
  makeExpressCallback(deployedNotifications)
);

// all deployed count for notification route
router.get(
  "/deployed-notif/count/th/:teamId",
  makeExpressCallback(deployedNotifications)
);

// all deployed count for notification route
router.get(
  "/deployed-notif/count/:id",
  makeExpressCallback(deployedNotifications)
);

// all for endorsement count for notification route for DH
router.get(
  "/for-endorsed-notif/count/:id",
  makeExpressCallback(forEndorsedNotifications)
);

// ADMIN

// all user route
router.get("/admin/view", getToken, makeExpressCallback(viewAllUsersAdmins));

// specific user details route
router.get(
  "/admin/view/:id",
  getToken,
  makeExpressCallback(viewUsersDetailsAdmins)
);

// retrieve user's little info during fill up of applicatioin form
// param is token there not id
router.get(
  "/application-form/:id",
  // getToken,
  makeExpressCallback(applicantNamesGets)
);

// retrieve data for viewing and printing for dept head
// supply here is really the id of the applicant
router.get(
  "/application-form-view/:id",
  // getToken,
  makeExpressCallback(applicantNamesGetPVs)
);

// retrieve all applicants for export
router.get(
  "/jeonsoft",
  getToken,
  makeExpressCallback(jeonsoftDisplayAllApplicant)
);

// retrive file and download on client side
router.get(
  "/jeonsoft/download",
  paramToken,
  makeExpressCallbackDownload(downloadClientJeonsofts)
);

// get start and end time for online exam
router.patch("/time/:time/token/:token", getToken, makeExpressCallback(startEndTimeGets));

// subtract time in IQBE
router.get(
  "/time-iqbe/:token",
  getToken,
  makeExpressCallback(subtractTimeIQBEs)
);

// all referral form
router.get("/referral-form", getToken, makeExpressCallback(viewReferralForms));

// single referral form
router.get(
  "/referral-form/:id",
  getToken,
  makeExpressCallback(viewOneReferralForms)
);

//fetch all users without referrals
router.get(
  '/no-referrals',
  getToken,
  makeExpressCallback(fetchApplicantsWithoutReferral_CONTROLLER)
)

//  get applicant result online exam
router.get(
  "/online-exam-result/:id/:posid/:whichExam",

  getToken,
  makeExpressCallback(onlineExamResultApplicants)
);

// fetch existing data for referral form
router.get(
  "/referral-form-fetch/:id",
  getToken,
  makeExpressCallback(referralFormFetchDatas)
);
// ************************//
// POST
// ************************//

// get all logs
router.post("/view/logs", getToken, makeExpressCallback(selectAllLogss));

// add new user
router.post("/add",
  getToken,
  makeExpressCallback(postUser));

// //google spread sheet for quick applicant
// router.post("/google-sheet/qa/add", makeExpressCallback(spreadSheetGoogleforQas))
// // google spread sheet worker
// router.post("/google-sheet/add", makeExpressCallback(spreadSheetGoogles));

// user fill up application form
router.post(
  "/application-form/:id",
  // getToken,
  makeExpressCallback(fillUpApplicationUsers)
);

// ADMIN

// admin add new user
router.post("/admin/add", getToken, makeExpressCallback(usersAddAdmins));

// admin add new referral form
router.post("/referral-form/add", getToken, makeExpressCallback(referralsAdds));

// export CSV file for jeonsoft
router.post(
  "/deployed/export",
  getToken,
  makeExpressCallback(jeonSoftExportCSVs)
);

// route to upload image to server
router.post(
  "/upload",
  // getToken,
  makeFolder,
  upload.any(),
  makeExpressCallback(uploadImages)
);

// insert system logs
router.post("/add/logs", getToken, makeExpressCallback(insertLogss));

// ************************//
// PUT
// ************************//

// update some fields during LOGIN
// admin login routes
router.put("/login", makeExpressCallback(loginUser));

// change status to online exam
router.put(
  "/status/online-exam/:id",
  getToken,
  makeExpressCallback(changesStatusUserOnlineExam)
);

// change status to blacklist
router.put(
  "/status/blacklist/:id",
  getToken,
  makeExpressCallback(changesStatusUserBlacklist)
);

// change status to endorsement
router.put(
  "/status/endorsement/:id",
  getToken,
  makeExpressCallback(changeStatusUserEndorsements)
);

// change status to rejected
router.put(
  "/status/reject/:id",
  getToken,
  makeExpressCallback(changeStatusUserRejects)
);

// change status to assessment
router.put(
  "/status/assessment/:id",
  getToken,
  makeExpressCallback(changeStatusUserAssessments)
);

// change status to pending
router.put(
  "/status/pending/:id",
  getToken,
  makeExpressCallback(changeStatusUserPendings)
);

// change status to kept for reference
router.put(
  "/status/kept-reference/:id",
  getToken,
  makeExpressCallback(changeStatusUserReferences)
);

//returns applicant to received stage/status
router.patch(
  '/status/received/:id',
  getToken,
  makeExpressCallback(returnApplicantToReceived_CONTROLLER)
)

// process re-apply
router.put(
  "/process/reapply/:id",
  getToken,
  makeExpressCallback(reApplyProcesss)
);

// change status to kept for shortlist
router.put(
  "/status/shortlist/:id",
  getToken,
  makeExpressCallback(changeStatusUserShortLists)
);

// change status to deployed
router.put(
  "/status/deployed/:id",
  getToken,
  makeExpressCallback(changeStatusUserDeployeds)
);

// change status to for endorsement
router.put(
  "/status/for-endorsement/:id",
  getToken,
  makeExpressCallback(changeStatusUserForEndorsements)
);

// login for applicants for online exam
router.put(
  "/applicant/login/online-exam",
  makeExpressCallback(cLoginApplicants)
);

// login for applicants for IQ and behavioral exam
router.put(
  "/applicant/login/IQ-behavioral-exam",
  makeExpressCallback(applicantLoginIQBs)
);

// login for applicants for application form
router.put(
  "/applicant/login/application-form",
  makeExpressCallback(loginApplicationForms)
);

// all endorsed route set schedule for assessment
router.put(
  "/endorsed/set-date/:id",
  getToken,
  makeExpressCallback(scheduleAssessmentUsers)
);

// change status to job order
router.put(
  "/status/job-offer/:id",
  makeExpressCallback(changeStatusUserJobOrders)
);

// ADMIN

// admin edit user route
router.put(
  "/admin/edit/user/:id",
  getToken,
  makeExpressCallback(usersEditAdmins)
);

// change status to done online exam
router.put(
  "/online-exam/done/:id",
  getToken,
  makeExpressCallback(doneOnlineExamUsers)
);

// change status to done IQBE
router.put("/IQBE/done/:id", getToken, makeExpressCallback(IQBEDoneUsers));

// change status to done IQBE
router.put(
  "/referral-form/:id",
  getToken,
  makeExpressCallback(referralUpdates)
);

// update applicant's position
router.put(
  "/position-update/:userId/:posId",
  getToken,
  makeExpressCallback(updatePositionApplicants)
);

// update deployed date of user
router.put(
  "/deployed-date-update/:id",
  getToken,
  makeExpressCallback(deployedDateUpdates)
);

// retake online exam
router.put(
  "/online-exam-retake/:id",
  getToken,
  makeExpressCallback(retakeOnlineExams)
);

// change password for first time login
// forgot password
router.put(
  "/change-password/:id/:password",
  getToken,
  makeExpressCallback(passwordChanges)
);
router.put(
  "/forgot-password/:id",
  getToken,
  makeExpressCallback(passwordChanges)
);
router.put("/forgot-password", makeExpressCallback(passwordChanges)); // no token; will email to user forgot password link

router.post("/reports", getToken, makeExpressCallback(filterReportsController));


router.post("/quick-applicant/apply", getToken, makeExpressCallback(postUser));

router.put(
  "/endorse-to-dept/:eUserId/:userId/:posId",
  makeExpressCallback(endorseToOtherDepartmentController)
);

router.get(
  "/applicant-date-trackings/:id",
  makeExpressCallback(fetchApplicantDateTrackingsController)
);

// butch part
router.post("/reports/all", getToken, makeExpressCallback(filterReportAllController));
//count quickapplicant route
router.post(
  "/quick-applicants/notif/count",
  makeExpressCallback(getcountQuickApplicantsController)
);
//rate exam essay/problem solving route
router.put("/exam/rate", getToken, makeExpressCallback(rateAnswerController));

router.post(
  "/quick-applicants/notif/count",
  makeExpressCallback(getcountQuickApplicantsController)
);

router.put(
  "/applicant-resume/update",
  makeExpressCallback(updateResumesController)
);

router.put("/deploy/to-kept/:id", getToken, makeExpressCallback(deployedUpdateKeptRefController));

router.put("/deploy/joborder/:id", getToken, makeExpressCallback(deployedUpdateJOController))

router.get("/view/IQBE/all", getToken, makeExpressCallback(viewAllIQTestController))

router.post(
  "/find-applicant",
  getToken,
  makeExpressCallback(findApplicantById_CONTROLLER)
)

//below are proposed application stage routes made for the new rectrack UI
//less routes compared to the older one, with uniform parameters

router.post(
  '/all-applicants',
  getToken,
  makeExpressCallback(getAllApplicantsController)
)
//by dept and position
router.get(
  '/all-applicants/filter/:deptid/:posid',
  getToken,
  makeExpressCallback(getAllApplicantsController)
)
//by dept
router.get(
  '/all-applicants/filter/:deptid',
  getToken,
  makeExpressCallback(getAllApplicantsController)
)

//all quick applicants
router.post(
  "/quick-applicants",
  getToken,
  makeExpressCallback(getQuickApplicantsController)
);

//filter by dept
router.get(
  "/quick-applicants/filter/:deptid",
  getToken,
  makeExpressCallback(getQuickApplicantsController)
)

//filter by dept & position
router.get(
  "/quick-applicants/filter/:deptid/:posid",
  getToken,
  makeExpressCallback(getQuickApplicantsController)
)

router.post(
  "/received",
  getToken,
  makeExpressCallback(userReceived)
);

router.get(
  "/received/filter/:deptid",
  getToken,
  makeExpressCallback(userReceived)
);

router.get(
  "/received/filter/:deptid/:posid",
  getToken,
  makeExpressCallback(userReceived)
);

router.post(
  "/online-exam",
  makeExpressCallback(usersOnlineExams)
);

router.get(
  '/online-exam/filter/:deptid',
  getToken,
  makeExpressCallback(usersOnlineExams)
)

router.get(
  '/online-exam/filter/:deptid/:posid',
  getToken,
  makeExpressCallback(usersOnlineExams)
)

router.post(
  "/endorsed",
  getToken,
  makeExpressCallback(userEndorsed)
);

router.get(
  '/endorsement/filter/:deptid',
  getToken,
  makeExpressCallback(userEndorsed)
)

router.get(
  '/endorsement/filter/:deptid/:posid',
  getToken,
  makeExpressCallback(userEndorsed)
)

router.post(
  "/assessment",
  getToken,
  makeExpressCallback(userAssessment)
);

router.get(
  '/assessment/filter/:deptid',
  getToken,
  makeExpressCallback(userAssessment)
)

router.get(
  '/assessment/filter/:deptid/:posid',
  getToken,
  makeExpressCallback(userAssessment)
)

router.post(
  "/blacklisted",
  getToken,
  makeExpressCallback(userBlacklisted)
);

router.get(
  "/blacklisted/get",
  getToken,
  makeExpressCallback(userBlacklisted)
);

router.get(
  "/blacklisted/filter/:deptid/:posid",
  getToken,
  makeExpressCallback(userBlacklisted)
);

router.get(
  "/blacklisted/filter/:deptid",
  getToken,
  makeExpressCallback(userBlacklisted)
);

router.post(
  "/for-endorsed",
  getToken,
  makeExpressCallback(usersForEndorseds)
)

router.get(
  '/for-endorsed/filter/:deptid',
  getToken,
  makeExpressCallback(usersForEndorseds)
)

router.get(
  '/for-endorsed/filter/:deptid/:posid',
  getToken,
  makeExpressCallback(usersForEndorseds)
)

router.get(
  "/deployed/filter/:deptid",
  getToken,
  makeExpressCallback(usersDeployeds)
);

router.get(
  "/deployed/filter/:deptid/:posid",
  getToken,
  makeExpressCallback(usersDeployeds)
);

router.post(
  "/deployed",
  makeExpressCallback(usersDeployeds)
);

router.get(
  "/deployed/get",
  makeExpressCallback(usersDeployeds)
);

router.get(
  "/kept-reference/filter/:deptid/:posid",
  getToken,
  makeExpressCallback(usersReferences)
);

router.get(
  '/kept-reference/filter/:deptid',
  getToken,
  makeExpressCallback(usersReferences)
)

router.get(
  '/kept-reference/get',
  getToken,
  makeExpressCallback(usersReferences)
)

router.post(
  "/kept-reference",
  getToken,
  makeExpressCallback(usersReferences)
);

//fetch all applicants with pending status
router.post(
  "/pending",
  getToken,
  makeExpressCallback(usersPendings)
);

//fetch users filtered, by dept. id
router.get(
  "/pending/filter/:deptid",
  getToken,
  makeExpressCallback(usersPendings)
);

//fetch users filtered, by dept. id and position
router.get(
  "/pending/filter/:deptid/:posid",
  getToken,
  makeExpressCallback(usersPendings)
);

router.post(
  '/rejected',
  getToken,
  makeExpressCallback(userRejected)
);

// router.get(
//   "/rejected/get",
//   getToken,
//   makeExpressCallback(userRejected)
// );

router.get(
  "/rejected/filter/:deptid",
  getToken,
  makeExpressCallback(userRejected)
);

router.get(
  "/rejected/filter/:deptid/:posid",
  getToken,
  makeExpressCallback(userRejected)
);

router.post(
  '/shortlist',
  makeExpressCallback(userShortlisted)
);

router.get(
  "/shortlist/filter/:deptid/:posid",
  getToken,
  makeExpressCallback(userShortlisted)
);

router.get(
  "/shortlist/filter/:deptid",
  getToken,
  makeExpressCallback(userShortlisted)
);

//get all notifs
//get?deptId=1&role=Administrator
//get?teamId=1&role=Team Head
//get?deptId=1&role=Operations Head
router.get(
  '/notifications/get',
  getToken,
  makeExpressCallback(getApplicantNotifcation_CONTROLLER)
)

//add applicant notif
router.post(
  '/notifications/add',
  getToken,
  makeExpressCallback(addApplicantNotifcation_CONTROLLER)
)

//update notif by id
//update?id=1&role=Administrator
router.patch(
  '/notifications/update',
  getToken,
  makeExpressCallback(updateApplicantNotification_CONTROLLER)
)

router.post(
  '/job-offer', 
  getToken, 
  makeExpressCallback(usersJobOrders)
);


router.get(
  "/job-offer/filter/:deptid/:posid", 
  getToken, 
  makeExpressCallback(usersJobOrders)
);


router.get(
  "/job-offer/filter/:deptid", 
  getToken, 
  makeExpressCallback(usersJobOrders)
);

router.post(
  '/validate-password',
  getToken,
  makeExpressCallback(passwordValidation_CONTROLLER)
)


module.exports = router;
