const express = require("express");
const router = express.Router();

const {
  postQuestions,
  randomQuestionsUser,
  randomQuestionsUserTest,
  answerSubmitUsers,
  answerSubmitUsersTest,
  makeQuestionsImports,
  questionsUserViews,
  questionsEdits,
  questionsAdminViews,
  randomQuestionsUserIQBEs,
  answerSubmitUserIQBEs,
  //butch part
  addQuestionsEssayController,
  viewQTypeController,
  viewIQBEController,
  makeQuestionIQController
} = require("../../src/controllers/questions/app");
const { getToken } = require("../../src/token/app");

const makeExpressCallback = require("../../src/express-callback/app");

// GET selected question
// dont mind the name of the file ;)
router.get("/view/:id", getToken, makeExpressCallback(questionsUserViews));

// display questions per team dept and category
router.get(
  "/views/:teamId/:deptId/:catgId",
  getToken,
  makeExpressCallback(questionsAdminViews)
);

// PUT
router.put("/edit/:id", getToken, makeExpressCallback(questionsEdits));

router.put("/online-exam", getToken, makeExpressCallback(answerSubmitUsers));
//for test
router.put("/online-exam/test", getToken, makeExpressCallback(answerSubmitUsersTest));

router.put("/IQBE", getToken, makeExpressCallback(answerSubmitUserIQBEs));

// POST

// get question per category
router.post("/online-exam", getToken, makeExpressCallback(randomQuestionsUser));
//for test
router.post('/online-exam/test', getToken, makeExpressCallback(randomQuestionsUserTest));

router.post("/add", getToken, makeExpressCallback(postQuestions));

router.post("/add/import", makeExpressCallback(makeQuestionsImports));

// get question per category in IQBE
router.post("/IQBE", getToken, makeExpressCallback(randomQuestionsUserIQBEs));

////////////////////butch part
// add question essay
router.post("/essay/add", getToken, makeExpressCallback(addQuestionsEssayController));

router.post("/find/QTYPE/:question_type_id", makeExpressCallback(viewQTypeController));

router.get("/view/IQBE/all", makeExpressCallback(viewIQBEController));

router.post("/add/IQBE", makeExpressCallback(makeQuestionIQController))
module.exports = router;
