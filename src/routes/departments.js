const express = require("express");
const router = express.Router();

const {
  postDepts,
  selectAllDepartments,
  changeStatusDeptInactives,
  deptEditAdmins,
  selectOneDepts
} = require("../../src/controllers/departments/app");
const { getToken } = require("../../src/token/app");

const makeExpressCallback = require("../../src/express-callback/app");

// ************************//
// POST
// ************************//

// ADMIN
// add new department
router.post("/admin/add", getToken, makeExpressCallback(postDepts));

// ************************//
// GET
// ************************//

// ADMIN
// all department route
router.get("/admin/view", getToken, makeExpressCallback(selectAllDepartments));

// single department route
router.get("/admin/view/:id", getToken, makeExpressCallback(selectOneDepts));

// ************************//
// PUT
// ************************//

// ADMIN
// admin edit status department route
router.put(
  "/admin/edit-status-inactive/:id",
  getToken,
  makeExpressCallback(changeStatusDeptInactives)
);

// admin edit department route
router.put("/admin/edit/:id", getToken, makeExpressCallback(deptEditAdmins));

module.exports = router;
