const express = require('express');
const router = express.Router();

const {
    getSystemColorsController,
    updateSystemSettingsController,
    getCurrentSystemSettingsController
} = require('../../src/controllers/system-settings/app');

const {
    makeFolder,
    upload
} = require('../../upload_image/app');


const makeExpressCallback = require('../../src/express-callback/app');


router.get('/get-colors', makeExpressCallback(getSystemColorsController));
router.get('/get-system-settings', makeExpressCallback(getCurrentSystemSettingsController));
router.put('/update-settings', makeFolder, upload.any(), makeExpressCallback(updateSystemSettingsController));

module.exports = router;