const express = require("express");
const emailRoute = express.Router();

const { sendAssessmentEmail_CON,
         sendBlacklistEmail_CON,
         sendDeployedEmail_CON,
         sendJobOfferEmail_CON,
         sendKeptForRerenceEmail_CON,
         sendOnlineExamEmail_CON,
         sendReturnToReceivedEmail_CON,      
         sendRejectedEmail_CON,
         sendShortlistedEmail_CON,
         sendUserCredentialsEmail_CON,
         sendAssessmentFormEmail_CON,
         sendReceivedEmail_CON,
         sendEndorseToOtherDeptEmail_CON,
         sendReapplyEmail_CON,
         sendForgotPasswordEmail_CON,
         sendExamRetakeEmail_CON
      } = require('../../src/controllers/email'),
      { getToken } = require('../../src/token//app'),
      makeExpressCallback = require("../../src/express-callback/app");


emailRoute.post(
   '/applicant/:applicantID/assessment', 
   getToken, 
   makeExpressCallback(sendAssessmentEmail_CON)
)

emailRoute.post(
   '/applicant/:applicantID/blacklisted',
   getToken,
   makeExpressCallback(sendBlacklistEmail_CON)
)

emailRoute.post(
   '/applicant/:applicantID/deployed',
   getToken,
   makeExpressCallback(sendDeployedEmail_CON)
)

emailRoute.post(
   '/applicant/:applicantID/job-offer',
   getToken,
   makeExpressCallback(sendJobOfferEmail_CON)
)

emailRoute.post(
   '/applicant/:applicantID/keep-for-reference',
   getToken,
   makeExpressCallback(sendKeptForRerenceEmail_CON)
)

emailRoute.post(
   '/applicant/:applicantID/online-exam',
   getToken,
   makeExpressCallback(sendOnlineExamEmail_CON)
)

emailRoute.post(
   '/applicant/:applicantID/received',
   getToken,
   makeExpressCallback(sendReceivedEmail_CON)
)

emailRoute.post(
   '/applicant/:applicantID/return-to-received',
   getToken,
   makeExpressCallback(sendReturnToReceivedEmail_CON)
)

emailRoute.post(
   '/applicant/:applicantID/rejected',
   getToken,
   makeExpressCallback(sendRejectedEmail_CON)
)

emailRoute.post(
   '/applicant/:applicantID/shortlisted',
   getToken,
   makeExpressCallback(sendShortlistedEmail_CON)
)

emailRoute.post(
   '/applicant/:applicantID/user-credentials',
   getToken,
   makeExpressCallback(sendUserCredentialsEmail_CON)
)

emailRoute.post(
   '/applicant/:applicantID/assessment-form',
   getToken,
   makeExpressCallback(sendAssessmentFormEmail_CON)
)

emailRoute.post(
   '/applicant/:applicantID/endorse-to-other-dept',
   getToken,
   makeExpressCallback(sendEndorseToOtherDeptEmail_CON)
)

emailRoute.post(
   '/applicant/:applicantID/reapply',
   getToken,
   makeExpressCallback(sendReapplyEmail_CON)
)

emailRoute.post(
   '/applicant/forgot-password',
   makeExpressCallback(sendForgotPasswordEmail_CON)
)

emailRoute.post(
   '/applicant/:applicantID/exam-retake',
   getToken,
   makeExpressCallback(sendExamRetakeEmail_CON)
)




module.exports = emailRoute