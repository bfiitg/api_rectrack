const express = require("express");
const router = express.Router();

const {
  interviewAssessmentAdds,
  viewInterviewAssessments,
  interviewAssessmentEdits,
  allViewInterviewAssessments,
  rateinterviewAssessmentViews,
  interviewAssessmentViewsRateRaterController
} = require("../../src/controllers/interview-assessment/app");
const { getToken } = require("../../src/token/app");

const makeExpressCallback = require("../../src/express-callback/app");

router.post("/user/view/rated", 
//getToken,
makeExpressCallback(rateinterviewAssessmentViews)
)

router.post("/add", getToken, makeExpressCallback(interviewAssessmentAdds));

router.get(
  "/user/view/:id",
  //  getToken,
  makeExpressCallback(viewInterviewAssessments)
);
router.get(
  "/user/view",
  //  getToken, 
  makeExpressCallback(allViewInterviewAssessments)
);
router.put(
  "/user/view/:id",
  getToken,
  makeExpressCallback(interviewAssessmentEdits)
);

router.post(
  "/user/view/rated/rater",
  // getToken,
  makeExpressCallback(interviewAssessmentViewsRateRaterController)
)

module.exports = router;
