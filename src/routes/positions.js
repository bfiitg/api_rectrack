const express = require("express");
const router = express.Router();

const {
  postPositions,
  selectAllVacantPositions,
  selectAllPosition,
  notVacantPositions,
  vacantPositions,
  selectAllNotVacantPositions,
  selectOnePositions,
  positionUpdates,
  selectAllPositionByDepts
} = require("../../src/controllers/positions/app");
const { getToken } = require("../../src/token/app");

const makeExpressCallback = require("../../src/express-callback/app");

// ************************//
// GET
// ************************//

// display all vacant position
router.get("/vacant", getToken, makeExpressCallback(selectAllVacantPositions));

// display all not vacant position
router.get(
  "/not-vacant",
  getToken,
  makeExpressCallback(selectAllNotVacantPositions)
);

// display all positions
router.get("/view", getToken, makeExpressCallback(selectAllPosition));

// display one positions
router.get("/view/:id", getToken, makeExpressCallback(selectOnePositions));

// view all position filter per department
router.get(
  "/view-by-department/:id",
  getToken,
  makeExpressCallback(selectAllPositionByDepts)
);
// ************************//
// POST
// ************************//

// add new position route
router.post("/add", getToken, makeExpressCallback(postPositions));

// ************************//
// PUT
// ************************//

// change status to not vacant
router.put(
  "/change/not-vacant/:id",
  getToken,
  makeExpressCallback(notVacantPositions)
);

// change status to vacant
router.put(
  "/change/vacant/:id",
  getToken,
  makeExpressCallback(vacantPositions)
);

// update position
router.put("/update/:id", getToken, makeExpressCallback(positionUpdates));

module.exports = router;
