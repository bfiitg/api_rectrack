const express = require("express");
const router = express.Router();

const {
    selectAllActionsController,
    makeActionsController,
    updateActionsController,
    selectActionsController
} = require("../../src/controllers/actions/app");

const {getToken} = require("../../src/token/app");

const makeExpressCallback = require("../../src/express-callback/app");

router.get("/admin/view", makeExpressCallback(selectAllActionsController));
router.get("/admin/view/:id", makeExpressCallback(selectActionsController));
router.post("/admin/add", makeExpressCallback(makeActionsController));
router.put("/admin/update/:id", makeExpressCallback(updateActionsController));

module.exports = router