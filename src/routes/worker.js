const express = require("express");
const router = express.Router();

const{  
    spreadSheetGoogleforQas,
    spreadSheetGoogles,
} = require("../../src/controllers/users/app");

const makeExpressCallback = require("../../src/express-callback/app");



//google spread sheet for quick applicant
router.post("/google-sheet/qa/add", makeExpressCallback(spreadSheetGoogleforQas))
// google spread sheet worker
router.post("/google-sheet/add", makeExpressCallback(spreadSheetGoogles));


module.exports = router;