const express = require("express");
const router = express.Router();

const {
    makeModulesController,
    selectAllModulesController,
    updateModuleController,
    selectModuleController,
    selectAllActiveModulesController
} = require("../../src/controllers/modules/app")

const {getToken} = require("../../src/token/app");

const makeExpressCallback = require("../../src/express-callback/app");

router.post("/admin/add", makeExpressCallback(makeModulesController));
router.get("/admin/view", makeExpressCallback(selectAllModulesController));
router.get("/admin/view_active", makeExpressCallback(selectAllActiveModulesController));
router.get("/admin/view/:id", makeExpressCallback(selectModuleController))
router.put("/admin/update/:id", makeExpressCallback(updateModuleController))


module.exports = router