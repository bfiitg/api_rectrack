const express = require("express");
const router = express.Router();

const {
  postRoles,
  selectAllRole,
  selectOneRoles,
  rolesEditAdmins
} = require("../../src/controllers/roles/app");
const { getToken } = require("../../src/token/app");

const makeExpressCallback = require("../../src/express-callback/app");

router.post("/add",  getToken, makeExpressCallback(postRoles));
router.get("/view",  makeExpressCallback(selectAllRole));
router.get("/view/:id", getToken, makeExpressCallback(selectOneRoles));
router.put("/edit/:id", getToken, makeExpressCallback(rolesEditAdmins));

module.exports = router;
