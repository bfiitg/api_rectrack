const express = require("express");
const router = express.Router();

const {
  postTeams,
  selectAllTeam,
  changeStatusTeamInactives,
  teamsEditAdmins,
  selectOneTeams,
  deptFilterSelectAllTeams
} = require("../../src/controllers/teams/app");
const { getToken } = require("../../src/token/app");

const makeExpressCallback = require("../../src/express-callback/app");

// ************************//
// POST
// ************************//

// admin add new team
router.post("/admin/add", getToken, makeExpressCallback(postTeams));

// ************************//
// GET
// ************************//

// admin view all teams
router.get("/admin/view", makeExpressCallback(selectAllTeam));

// view one team
router.get("/admin/view/:id", makeExpressCallback(selectOneTeams));

// filter by dept
router.get(
  "/admin/view-dept/:id",
  getToken,
  makeExpressCallback(deptFilterSelectAllTeams)
);
// ************************//
// PUT
// ************************//

// admin edit status teams route
router.put(
  "/admin/edit-status-inactive/:id",
  getToken,
  makeExpressCallback(changeStatusTeamInactives)
);

// admin edit  teams route
router.put("/admin/edit/:id", getToken, makeExpressCallback(teamsEditAdmins));

module.exports = router;
