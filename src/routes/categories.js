const express = require("express");
const router = express.Router();

const {
  postCategory,
  selectAllCategory,
  selectOneCategorys,
  categoriesUpdates, 
  filterTeamCategorySelectAlls
} = require("../controllers/categories/app");

const { getToken } = require("../token/app");

const makeExpressCallback = require("../express-callback/app");

router.post("/add", getToken, makeExpressCallback(postCategory));
// view all
router.get("/view",  makeExpressCallback(selectAllCategory));

// view all filter by dept
router.get("/view-dept/:id", getToken, makeExpressCallback(selectAllCategory));

// view specific
router.get("/view/:id", getToken, makeExpressCallback(selectOneCategorys));

// filter which team
router.get(
  "/view-team/:id",
  getToken,
  makeExpressCallback(filterTeamCategorySelectAlls)
);
router.put("/edit/:id", getToken, makeExpressCallback(categoriesUpdates));

module.exports = router;
