const express = require("express");
const router = express.Router();

const {
  findRejectorController,
  resultImportController,
  delete_testController,
  deployedApplicantsInserts,
  rejectedApplicantsInserts,
  referenceApplicantsInserts,
  blacklistApplicantsInserts,

  alterTableControllers,
  alterTable1Controllers,
  alterTable2Controllers,
  updateColumnControllers,

  DeleteDuplicateController,
  deleteUserController,
  resetOnlineExamRecordController,

  updateToDeployController,
  updateToOnlineExamController
} = require("../../src/controllers/fixes/app");

const makeExpressCallback = require("../../src/express-callback/app");

router.get(
  "/find-applicant-rejector",
  makeExpressCallback(findRejectorController)
);
router.post("/result-import", makeExpressCallback(resultImportController));
router.post("/delete_test", makeExpressCallback(delete_testController));
router.post("/insert/deployed", makeExpressCallback(deployedApplicantsInserts));
router.post("/insert/rejected", makeExpressCallback(rejectedApplicantsInserts));
router.post(
  "/insert/blacklist",
  makeExpressCallback(blacklistApplicantsInserts)
);
router.post(
  "/insert/reference",
  makeExpressCallback(referenceApplicantsInserts)
);

router.post("/alter-table", makeExpressCallback(alterTableControllers));
router.post("/alter-table_1", makeExpressCallback(alterTable1Controllers));
router.post("/alter-table_2", makeExpressCallback(alterTable2Controllers));
router.post("/update-column", makeExpressCallback(updateColumnControllers));

router.get("/deleteDuplicate", makeExpressCallback(DeleteDuplicateController))
router.post("/deleteUser", makeExpressCallback(deleteUserController))
router.post("/resetOnlineExam", makeExpressCallback(resetOnlineExamRecordController))

router.put("/updateToDeploy/:id", makeExpressCallback(updateToDeployController))
router.put("/updateToOnline/:id", makeExpressCallback(updateToOnlineExamController))

module.exports = router;
