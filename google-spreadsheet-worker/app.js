
require('dotenv').config()
const { GoogleSpreadsheet } = require('google-spreadsheet'),
      { client_email, private_key } = require('../client_secret.json'),spreadsheetId = process.env.SSW,
      moment = require('moment');

const accessSpreadSheet = async () => {
  try {
    let doc = new GoogleSpreadsheet(spreadsheetId),
      result = [];

  await doc.useServiceAccountAuth({
        client_email: client_email,
        private_key: private_key
  })

  await doc.loadInfo()

  let quickApplicantSheet = doc.sheetsByIndex[1],
      currentDate = moment().format('M/D/YYYY')
    
  await quickApplicantSheet.getRows({offset:1})
        .then(res => {

          for(let rowData of res){

            if(currentDate === rowData.Timestamp.split(" ")[0]) {
              result.push({
                  timestamp: rowData.Timestamp,
                  email: rowData['Email Address'],
                  firstname: rowData['First name'],
                  lastname: rowData['Last name'],
                  mobile: rowData['Phone number'],
                  livestock: rowData['Do your family own or raise livestocks? (Pig, Cattle, Chicken, Goat, Cow, Sheep, etc.)'],
                  travel: rowData['Are you willing to travel?'],
                  position: rowData['Position you would like to apply'],
                  resume: rowData['Resume upload']
              })
            }
            
          }

        })

    return result;

  } catch (error) {
    console.log(error)
  }
};

module.exports = { accessSpreadSheet };
